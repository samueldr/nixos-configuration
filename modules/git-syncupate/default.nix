{ config, lib, pkgs, ... }:

let
  inherit (lib)
    attrNames
    attrValues
    concatStringsSep
    escapeShellArg
    escapeShellArgs
    mapAttrsToList
    mkEnableOption
    mkForce
    mkIf
    mkOption
    optional
    types
  ;

  cfg = config.samueldr.git-syncupate;
  globalCfg = cfg;

  repoSubmodule = types.submodule (
    { name, config, ... }:
    {
      options = {
        name = mkOption {
          internal = true;
          type = types.str;
          default = name;
        };
        repo = mkOption {
          type = types.str;
          default = config.name;
          defaultText = "\${name}";
          description = ''
            Location, relative to the global option, where the repository is stored.

            This can be used to e.g. run `--prune` on only some remotes, by having
            a second submodule with a different name pointing to the same repository.
          '';
        };
        location = mkOption {
          type = types.str;
          default = "${globalCfg.location}/${config.repo}";
          defaultText = "\${samueldr.git-syncupate.location}/\${repo}";
          description = ''
            Location where this repository is stored
          '';
        };
        remotes = mkOption {
          type = with types; listOf str;
          default = [];
          description = ''
            Remotes to fetch
          '';
        };
        prune = mkOption {
          type = types.bool;
          default = false;
          description = ''
            Whether or not to use `--prune`.
          '';
        };
        all = mkOption {
          type = types.bool;
          default = false;
          description = ''
            Whether or not to use `--all`.

            This is not recommended, as it prevents trivially adding remotes that may not be reachable.
          '';
        };

        fetchArguments = mkOption {
          type = with types; listOf str;
          description = ''
            Arguments for the fetch operation.

            Contents varies according to configuration.

            Overriding with `mkForce` is supported, but you lose all implicit configuration.

            Prepending/appending with `mkBefore/mkAfter` is supported, but YMMV.
          '';
        };

        outputs = {
          snippet = mkOption {
            internal = true;
          };
        };
      };
      config = {
        remotes = mkIf (config.all) (mkForce ["--all"]);
        fetchArguments = [
          "--multiple"
          "--jobs=${toString globalCfg.jobs}"
        ]
        ++ optional config.prune "--prune"
        ++ config.remotes
        ;
        outputs.snippet = ''
          printf '\n'
          printf ":: -> %s\n" ${escapeShellArg config.name}
          (
            set -e
            set -x
            cd ${escapeShellArg "${config.location}"}
            ${globalCfg.git} fetch ${escapeShellArgs config.fetchArguments}
          )
        '';
      };
    }
  );
in
{
  options = {
    samueldr = {
      git-syncupate = {
        enable = mkEnableOption "the git-syncupate tooling";
        location = mkOption {
          type = types.str;
          description = ''
            Default location where the git repositories are stored.
          '';
        };
        repos = mkOption {
          default = {};
          type = types.attrsOf repoSubmodule;
          description = ''
            Per-repository configuration
          '';
        };
        jobs = mkOption {
          default = 1;
          type = types.int;
          description = ''
            Number of parallel children to be used for all forms of fetching.

            Note that this is *per-repository* and all repositories are always
            fetched sequentially.
          '';
        };

        git = mkOption {
          default     =  "${pkgs.git}/bin/git";
          defaultText = "\${pkgs.git}/bin/git";
          type = types.path;
        };

        enableSchedule = mkOption {
          type = types.bool;
          default = false;
          description = ''
          '';
        };
        timerConfig = mkOption {
          default = {
            OnUnitActiveSec = "12h";
            RandomizedDelaySec = "5h";
          };
          description = ''
            timerConfig value for the `git-synupate` systemd timer,
            when enabled via the `enableSchedule` option.
          '';
        };

        outputs = {
          script = mkOption {
            type = types.path;
            internal = true;
          };
        };
      };
    };
  };

  config = mkIf (cfg.enable) {
    environment.systemPackages = [
      cfg.outputs.script
    ];
    
    samueldr.git-syncupate.outputs = {
      script = pkgs.writeShellScriptBin "git-syncupate" ''
        set -u
        PS4=" $ "
        printf '`git-syncupate`\n'
        printf '===============\n'
        ${concatStringsSep "\n" (mapAttrsToList (_: repoConfig: repoConfig.outputs.snippet) cfg.repos)}
      '';
    };

    systemd.user = mkIf (cfg.enableSchedule) {
      services = {
        "git-syncupate" = {
          description = ''
            Regularly updates Git repositories.
          '';
          script = ''
            exec env -i ${cfg.outputs.script}/bin/git-syncupate
          '';
          wantedBy = [ "default.target" ];
          wants = [ "git-syncupate.timer" ];
          serviceConfig = {
            Type = "oneshot";
            RemainAfterExit = false;
          };
        };
      };
      timers = {
        "git-syncupate" = {
          wantedBy = [
            "timers.target"
            "default.target"
          ];
          timerConfig = cfg.timerConfig;
        };
      };
    };
  };
}
