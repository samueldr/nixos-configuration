`git-syncupate`
===============

> No, this is not a typo for `git-syncupdate`, that wouldn't be a good name.

To “syncupate” a git repo is to *abbreviate* sync times by regularly using
`git fetch` on given remotes, such that when needed, the remotes are already
fresh enough, and a needed `git fetch` will be quicker.

Also, it's just to sync-up, duh.

* * *

Usage
-----

I'm not sure yet.

Looks like it might be mostly Nix glue to generate the appropriate NixOS
configuration for systemd user services.

Though it might be interesting to produce agnostic outputs that can be used
on non-NixOS, non-Nix-using systems.
