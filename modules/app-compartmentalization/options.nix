{ config, lib, pkgs, ... }:

let
  inherit (lib)
    any
    attrValues
    concatMapStringsSep
    concatStringsSep
    escapeShellArgs
    setPrio
    literalExpression
    makeBinPath
    mapAttrs
    mapAttrsToList
    mkForce
    mkIf
    mkMerge
    mkOption
    types
    optional
    optionalString
  ;

  overridePrio = setPrio (-100);

  anyEnabled =
    any
    (enabled: enabled)
    (
         (mapAttrsToList (_: service: service.enable) cfg.services)
      ++ (mapAttrsToList (_: scoped-app: scoped-app.enable) cfg.scoped-apps)
    )
  ;

  # Used to ensure `$out/share/applications` is a directory in symlinkJoin.
  emptyShareApplications = pkgs.callPackage (
    { runCommand }:
    runCommand "share-applications" {} ''
      mkdir -p $out/share/applications
    ''
  ) {};

  # This ensures desktop files don't use `/nix/store` paths.
  # They would refer to the original package...
  # ... but we also drop any store path to help out desktop environments on package upgrades.
  desktopFileFixup = { package }: ''
    if test -e $out/share/applications; then
      echo " :: Patching store path in desktop files... "
      (
      cd $out/share/applications
      # We have to work with the original files, as symlinkJoin (as documented)
      # may not work well when working paths containing symlinks to directories.
      for f in ${package}/share/applications/*.desktop; do
        rm -f $(basename "$f")
        sed -e "s;Exec=${package}/bin/;Exec=;" "$f" > $(basename "$f")
      done
      )
      echo " :: Checking for '/nix/store' refs in desktop files..."
      if grep 'Exec=${builtins.storeDir}/' $out/share/applications/*.desktop; then
        echo ""
        echo "ABORTING: '/nix/store/' refs found in desktop files for ${package.name}."
        exit 1
      fi
    fi
  '';

  cfg = config.app-compartmentalization;

  # Providing the app config as an input is required, as this submodule has no
  # idea which parent submodule is using it.
  commandSubmoduleForApp = app: types.submodule({ name, config, ... }: {
    options = {
      enable = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Whether to enable the specified wrapper.
        '';
      };
      outputCommand = mkOption {
        type = types.str;
        default = name;
        description = ''
          Name of the shim script.

          Can be used to provide helper shims.
        '';
      };
      command = mkOption {
        type = types.str;
        default = name;
        description = ''
          Command that will be run by the shim.

          Can be used to provide helper shims.
        '';
      };
      arguments = mkOption {
        type = with types; listOf str;
        default = [];
        description = ''
          List of additional arguments to pass to the target command in the shim.

          This can be used to make additional helper commands or to use
          an argument to target a main process or bypass using a main process.

          **Note**: arguments are **NOT** sanitized, they will be provided as-they-are
          to the script. This is by design, to allow complex behaviour.

          If you need actual complex behaviour, you should consider instead overriding
          the input package with your own custom shims.
        '';
      };
      systemdRunArguments = mkOption {
        type = with types; listOf str;
        description = ''
          List of arguments passed to systemd-run before the target command.

          For the target command and arguments, use `command` and `arguments`.

          **Note**: The arguments will be sanitized.
        '';
      };
      environment = mkOption {
        type = with types; attrsOf str;
        description = ''
          Environment variables provided to the unit.
        '';
      };
      properties = mkOption {
        type = with types; attrsOf str;
        description = ''
          From systemd's help:

          > Sets a property on the scope or service unit that is created. This
          > option takes an assignment in the same format as `systemctl(1)`'s
          > `set-property` command.
        '';
      };
      serviceName = mkOption {
        type = types.str;
        internal = true;
        default = "app.${config.app.name}";
      };
      app = mkOption {
        type = types.unspecified;
        default = app;
        internal = true;
        description = ''
          The calling submodule's config.
        '';
      };
      package = mkOption {
        type = types.package;
        internal = true;
        default = config.app.package;
        description = ''
          Provided by the submodule consumer.
        '';
      };
      useGlobalEnvironment = mkOption {
        type = types.bool;
        default = config.app.useGlobalEnvironment;
        description = ''
          When enabled, the scoped application will inherit the login
          environment through a shim login shell.

          **Note**: This reduces hermeticity, but still provides the
          advantage of tracking the processes in a scope or unit.
        '';
      };
      additionalPackages = mkOption {
        type = with types; listOf (either package string);
        default = config.app.additionalPackages;
        description = ''
          Packages to add to this scoped app's environment.
        '';
      };
      shim = mkOption {
        type = types.package;
        internal = true;
        description = ''
          The realized command shim.
        '';
      };
    };
    config = {
      shim =
        let
          inherit (cfg) notify;
          inherit (config) app command outputCommand serviceName package arguments systemdRunArguments useGlobalEnvironment;
          inherit (app) name serviceType;
        in
        pkgs.writeShellScript "${serviceName}-${outputCommand}-wrapper" ''
          set -eu

          ${
            if serviceType == "transient"
            then ''
              serviceName="${serviceName}"
              if systemctl --quiet --user is-active "${serviceName}"; then
                # Append a unique string for transient "rpc" calls.
                # It is assumed they will be culled ASAP anyway.
                serviceName="${serviceName}.$(${pkgs.util-linux}/bin/uuidgen)"
              fi
            ''
            else ''
              serviceName="${serviceName}.$(${pkgs.util-linux}/bin/uuidgen)"
            ''
          }

          ARGS=(
            # systemdRunArguments
            ${escapeShellArgs systemdRunArguments}
            --unit="$serviceName"

            # command
            ${optionalString useGlobalEnvironment pkgs.with-profile}
            "${package}/bin/${command}"

            # arguments
            ${concatStringsSep "\n" arguments}

            "$@"
          )

          ${optionalString (serviceType == "transient") ''
            if ! systemctl --quiet --user is-active "${serviceName}"; then
              systemctl --quiet --user reset-failed "${serviceName}" || :
              ${optionalString notify ''
                ${pkgs.libnotify}/bin/notify-send \
                  --transient \
                  --icon "${name}" \
                  "Starting ${name} service" || :
                echo "(Launching '${serviceName}' service)"
              ''}
            fi
          ''}
          exec -a "${name}" ${pkgs.systemd}/bin/systemd-run "''${ARGS[@]}"
        ''
      ;
      systemdRunArguments =
        [
          "--user"
          "--quiet"
          "--same-dir"
          "--collect"
          # When using only `--pipe`, the stdout is provided to the
          # calling invocation...
          # Note that ^C will not exit the service.
          # And it's *expected* that ^C will not exit the service.
          # Using `--pty` and `--pipe` here means the stdout is provided to the
          # calling invocation, and since it's using `--pty` ^C will work as expected.
          "--pipe"
        ]
        ++ (mapAttrsToList (name: value: "--setenv=${name}=${value}") config.environment)
        ++ (mapAttrsToList (name: value: "--property=${name}=${value}") config.properties)
        ++ (optional (config.app.serviceType != "transient") "--pty")
      ;
      properties = {
      };
      environment = {
        PATH = makeBinPath config.additionalPackages;
      };
    };
  });
in
{
  options = {
    app-compartmentalization = {
      notify = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Will notify upon starting services.

          **Note**: this will only notify for services.
        '';
      };
      configureResourceControl = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Enables cgroup-v2 and unified hierarchy.

          This allows using the different accounting
          and weight parameters from systemd.resource-control(5).
        '';
      };
      scoped-apps = mkOption {
        type = types.attrsOf (types.submodule ({ name, config, ... }: {
          options = {
            name = mkOption {
              type = types.str;
              default = name;
              internal = true;
            };
            package = mkOption {
              type = types.package;
              default = pkgs."${name}";
              defaultText = ''pkgs."'${name}"'';
              description = ''
                Package to wrap with a scope shim.
              '';
            };
            commands = mkOption {
              type = types.attrsOf (commandSubmoduleForApp config);
              example = literalExpression ''
                {
                  hello = "";
                }
              '';
              description = ''
                Commands to wrap with the shim.

                **Note**: A shortcut to enable shimming without additional
                configuration consists of assigning an empty attrset.
              '';
            };
            enable = mkOption {
              type = types.bool;
              default = false;
              description = ''
                Whether to enable the scoped app.
              '';
            };
            useGlobalEnvironment = mkOption {
              type = types.bool;
              default = false;
              description = ''
                When enabled, the scoped application will inherit the login
                environment through a shim login shell.

                **Note**: This reduces hermeticity, but still provides the
                advantage of tracking the processes in a scope or unit.

                It is preferrable to add a limited set of packages to
                additionalPackages, if possible.

                This should be used for applications that are expected to run
                anything, like file managers or launchers.
              '';
            };
            additionalPackages = mkOption {
              type = with types; listOf (either package string);
              default = [];
              description = ''
                Packages to add to this scoped app's environment.
              '';
            };
            # TODO handle mixed scoped apps / services...
            # chrome/firefox don't mix the concepts in their packages...
            serviceType = mkOption {
              type = types.enum [
                null
                "transient"
              ];
              default = null;
              description = ''
                Used to describe the type of the service for the application.

                Has to be `null` when `service` is `null`. Has to be set to
                other values if `service` is configured.

                `transient` is used to wrap an application that uses a main
                process, but has to respond correctly to provided parameters.
                An example of such programs are web browsers like firefox and
                chrome. The wrapped service *does not* have a discrete systemd
                user service that can be started; starting the service requires
                using the shim wrapper.
              '';
            };
            out = mkOption {
              type = types.package;
              description = ''
                The realized wrapped package.

                This is part of the public API of this module.
                If you want to refer to the scoped app, use this option.

                For example:

                    {
                      something = "${cfg.rofi.out}/bin/rofi";
                    }
              '';
            };
          };
          config = {
            out = overridePrio (pkgs.symlinkJoin {
              name = "${config.package.name}.app";
              postBuild = desktopFileFixup { inherit (config) package; };
              paths = [
                emptyShareApplications
                (pkgs.runCommand "${config.package.name}.app-wrappers" {} ''
                  mkdir -v -p $out/bin
                  ${concatMapStringsSep " " (
                    config: ''
                      cp -v ${config.shim} $out/bin/${config.outputCommand}
                    ''
                  ) (attrValues config.commands)}
                '')
                config.package
              ];
            });
          };
        }));
      };
      services = mkOption {
        type = types.attrsOf (types.submodule ({ name, config, ... }: {
          options = {
            name = mkOption {
              type = types.str;
              default = name;
              internal = true;
            };
            package = mkOption {
              type = types.package;
              default = pkgs."${name}";
              defaultText = ''pkgs."'${name}"'';
              description = ''
                Package in which the command lives.
              '';
            };
            command = mkOption {
              type = types.str;
              default = name;
              defaultText = ''"'${name}"'';
              description = ''
                Command to run.
              '';
            };
            enable = mkOption {
              type = types.bool;
              default = false;
              description = ''
                Whether to enable the service.
              '';
            };
            useGlobalEnvironment = mkOption {
              type = types.bool;
              default = false;
              description = ''
                When enabled, the service will inherit the login
                environment through a shim login shell.

                **Note**: This reduces hermeticity, but still provides a
                service.

                It is preferrable to add a limited set of packages to
                additionalPackages, if possible.

                This should be used for applications that are expected to run
                anything, like file managers or launchers.
              '';
            };
            additionalPackages = mkOption {
              type = with types; listOf (either package string);
              default = [];
              description = ''
                Packages to add to this scoped app's environment.
              '';
            };
            environment = mkOption {
              type = with types; attrsOf str;
              default = {};
              description = ''
                Environment variables provided to the unit.
              '';
            };
            out = mkOption {
              type = types.attrs;
              internal = true;
              description = ''
                The systemd user service snippet.
              '';
            };
            outShim = mkOption {
              type = types.attrs;
              internal = true;
            };
          };
          config = {
            out = {
              serviceConfig = {
                ExecStart = ''
                  ${
                    optionalString config.useGlobalEnvironment pkgs.with-profile
                  } ${config.package}/bin/${config.command}
                '';
              };
              enable = config.enable;
              environment = mkMerge [
                config.environment
                { PATH = mkForce null; }
              ];
              partOf = [ "graphical-session.target" ];
            };
            outShim = {
              systemPackages = mkIf config.enable [
                (overridePrio (pkgs.symlinkJoin {
                  name = "${config.package.name}.app";
                  postBuild = desktopFileFixup { inherit (config) package; };
                  paths = [
                    emptyShareApplications
                    (pkgs.writeShellScriptBin "${config.command}" ''
                      exec systemctl --user start ${escapeShellArgs [ name ]} "$@"
                    '')
                    config.package
                  ];
                }))
              ];
            };
          };
        }));
        description = ''
          Helper to quickly make user services for graphical sessions.

          Applications described here will be configured as
          `systemd.user.services` options.
        '';
      };
    };
  };
  config.environment = mkMerge [
    (mkMerge (
      mapAttrsToList (
        _: cfg:
        {
          systemPackages = mkIf cfg.enable [
            cfg.out
          ];
        }
      ) cfg.scoped-apps
    ))
    (mkMerge (
      mapAttrsToList
      (_: service: service.outShim)
      cfg.services
    ))
  ];
  config.systemd.user.services =
    mapAttrs
      (_: service: mkIf service.out.enable service.out)
      cfg.services
  ;
  config.boot = mkIf (anyEnabled && cfg.configureResourceControl) {
    # See:
    #  - https://discourse.nixos.org/t/ram-limiting-firefox-for-pathological-tabbers/5117/2
    #  - https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html
    kernelParams = [
      "cgroup_no_v1=all"
      "systemd.unified_cgroup_hierarchy=yes"
    ];
  };

}
