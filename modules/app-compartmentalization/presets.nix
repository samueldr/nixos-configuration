{ pkgs, lib, ... }:
{
  app-compartmentalization = {
    scoped-apps = {
      "angelfish" = {
        serviceType = "transient";
        commands.angelfish = {};
        commands.angelfish-webapp = {};
      };
      "arandr" = {
        commands.arandr = {};
        # unxrandr is a CLI utility, no need to wrap.
      };
      "chromium" = {
        serviceType = "transient";
        commands.chromium = {};
        commands.chromium-browser = {};
      };
      "darktable" = {
        commands.darktable = {};
      };
      "evince" = {
        commands.evince = {};
      };
      "firefox" = {
        serviceType = "transient";
        commands.firefox = {};
      };
      "gcolor3" = {
        commands.gcolor3 = {};
      };
      "gimp" = {
        commands.gimp = {};
      };
      "gnumeric" = {
        commands = {
          "gnumeric" = {};
          "gnumeric-${pkgs.gnumeric.version}" = {};
        };
      };
      "inkscape" = {
        commands.inkscape = {};
        commands.inkview = {};
      };
      "krita" = {
        commands.krita = {};
      };
      "libreoffice" = {
        commands.libreoffice = {};
        commands.sbase = {};
        commands.scalc = {};
        commands.sdraw = {};
        commands.simpress = {};
        commands.smath = {};
        commands.soffice = {};
        commands.swriter = {};
        commands.unopkg = {};
      };
      "mpv" = {
        commands = {
          "mpv" = {};
          "umpv" = {};
        };
      };
      "mumble" = {
        serviceType = "transient";
        # Note: mumble-overlay is not wrapped here, as it is meant to be used
        #       to wrap a call to a third-party program with LD_PRELOAD.
        commands.mumble = {};
      };
      "mypaint" = {
        commands.mypaint = {};
      };
      "paprefs" = {
        commands.paprefs = {};
      };
      "pavucontrol" = {
        commands.pavucontrol = {};
      };
      "pcmanfm" = {
        commands.pcmanfm = {
          # End-users may want to disable this and rely on *only scoped apps*.
          useGlobalEnvironment = true;
        };
      };
      "rnote" = {
        commands.rnote = {};
      };
      "rox-filer" = {
        commands = {
          # --new is needed otherwise it fails to launch once, and would otherwise
          # control a main process (which we don't want here).
          rox = {
            arguments = [ "--new" ];
            # `sh` is being called unqualified to start applications
            # (Always provided, in case useGlobalEnvironment is disabled by the end-user.)
            additionalPackages = [ pkgs.runtimeShellPackage ];
            # End-users may want to disable this and rely on *only scoped apps*.
            useGlobalEnvironment = true;
          };
        };
      };
      "scrcpy" = {
        commands.scrcpy = {};
      };
      "tigervnc" = {
        commands.vncserver = {};
        commands.vncsession = {};
        commands.vncviewer = {};
        commands.x0vncserver = {};
        commands.Xvnc = {};
      };
      "transmission-remote-gtk" = {
        commands.transmission-remote-gtk = {};
      };
      "unison" = {
        commands.unison = {};
        commands."unison-${
          with lib;
          concatStringsSep "." (take 2 (splitString "." pkgs.unison.version))
        }" = {};
      };
      "xournal" = {
        commands.xournal = {};
      };
      "zathura" = {
        commands.zathura = {};
      };

      # Emulation
      # NOTE: "full screen experience" emulators go under services.
      "dolphin-emu" = {
        commands.dolphin-emu = {};
        commands.dolphin-tool = {};
        commands.dolphin-emu-nogui = {};
      };
      "pcsx2" = {
        commands.pcsx2 = {};
      };
      "ppsspp" = {
        commands.ppsspp = {};
        commands.ppsspp-headless = {};
      };

      # Gnome software
      "eog" = {
        package = lib.mkDefault pkgs.eog;
        commands.eog = {};
      };
      "file-roller" = {
        package = lib.mkDefault pkgs.file-roller;
        commands.file-roller = {};
      };
      "gnome-disk-utility" = {
        package = lib.mkDefault pkgs.gnome-disk-utility;
        commands.gnome-disks = {};
        commands.gnome-disk-image-mounter = {};
      };

      # Wine
      # Note: it might not be "correct" enough to just wrap with scoped-apps
      # A more involved "containerization" for wine things might be worthwhile.
      "wine" = {
        # Apps you're likely to run
        commands."wine" = {};
        commands."winecfg" = {};
        commands."winedbg" = {};

        # Unsure
        commands."wineboot" = {};
        commands."wineserver" = {};
        commands."wine-preloader" = {};

        # Misc components
        commands."regedit" = {};
        commands."regsvr32" = {};

        # TODO: fix hemeticity issue upstream
        # .../bin/notepad: line 23: basename: command not found    
        # .../bin/notepad: line 33: dirname: command not found     
        # Apps
        commands."msidb" = {};
        commands."msiexec" = {};
        commands."notepad" = {};
        commands."wineconsole" = {};
        commands."winefile" = {};
        commands."winemine" = {};
      };
    };

    services = {
      "audacious" = {};
      "claws-mail" = {};
      "keepassxc" = {};
      "nheko" = {};
      "quasselclient" = {
        package = lib.mkDefault pkgs.quasselClient;
      };
      "thunderbird" = {};
    };
  };
}
