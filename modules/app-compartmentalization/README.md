app-compartmentalization
========================

What's this?
------------

WIP tooling to improve process tree hermeticity aimed at GUI apps.

The goal here is that any "GUI" app (or similar long-lived application)
**should not inherit the calling process's environment**.

Why? Inheriting the calling process's environment is "environmental pollution"
and can cause issues with mismanaged environments, or hide issues with a lack
of hermetically defined dependencies.

> For example, an application might intrinsically depend on commands
> installed in the global scope, which is not good as those commands may or may
> not be the ones that the tool was designed to use, or less worse, the ones
> matching the same Nixpkgs revision.
> 
> Another example of issues would be an application that relies on `PATH`-like
> environment variables for plugins. If that application is launched from another
> one using similar semantics, the `PATH`-like variable may be concatenated, and
> thus the child program may act differently.

Another goal, which comes as a side-effect of the implementation detail, is
that now every application is ran in a systemd unit, which allows it to be
controlled, or inspected.

> For example `systemd-cgls --unit user-$UID.slice`  will show the process
> trees for the started units. This can be used to better understand the
> relationships between processes.

A yet unrealised goal is that this will enable users to associate systemd
properties to applications, which can help with limiting CPU or memory use
by a specific application, or *lightly* harden the application by providing
a different view on the filesystem (read-only home, app-specific tmp, etc).


How does this work?
-------------------

You tell us.


But really though?
------------------

This provides two main semantic building blocks:

 - Shortcut to systemd user services (`app-compartmentalization.services`)
 - Scoped applications (`app-compartmentalization.scoped-apps`)

The shortcut to systemd user services are helpers that, by convention, creates
a service and a shim command for packages. It helps reduce the amount of
boilerplate needed when writing a user service. Those services are *tangible*
in that they can be `systemctl --user start`ed.

Scoped applications provide wrappers around commands that will start the target
applications in transient systemd services.

If marked as `transient`, the service will be named in a unique fashion (e.g.
`app.chromium.service`) and is assumed to be unique and "RPC-controlled".

Otherwise, the app is assumed to require a new "scope" (actually a service) for
every invocation. This will be true for most applications. For example, if it
starts a new window and stays attached to the terminal even if already
launched. These services will be suffixed with a GUID to make them unique.

All of these are made with *transient services*. The reason it has to be done
with transient services is that it's the only way to escape environmental
pollution from the calling process. As transient services, the systemd instance
for the user launches the processes with a minimal environment. Any other
schemes will keep the processes as child of the calling process, and make it
inherit the "scope" of the process (e.g. environment).


* * *

Notes
-----

> ```
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT DOCUMENTATION OF ANY KIND, EXPRESS OR IMPLIED
> ```
