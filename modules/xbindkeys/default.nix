{ config, lib, pkgs, ... }:

let
  inherit (lib)
    attrValues
    concatMapStringsSep
    makeBinPath
    mkEnableOption
    mkForce
    mkIf
    mkOption
    types
  ;

  cfg = config.services.xbindkeys;

  configFile =
    concatMapStringsSep "\n"
    ({keys, command, ...}: ''
      # ${command}
      ${concatMapStringsSep "\n" (s: "\"${command}\"\n    ${s}") keys}
    '')
    (attrValues cfg.bindings)
  ;

  keybind = { ... }: {
    options = {
      keys = mkOption {
        type = types.listOf types.str;
        description = "List of key combinations.";
      };
      command = mkOption {
        type = with types; oneOf [ str package ];
        description = "Command to run.";
      };
    };
  };
in
{
  options = {
    services.xbindkeys = {
      enable = mkEnableOption "configured xbindkeys";
      bindings = mkOption {
        type = types.attrsOf (types.submodule keybind);
        default = {};
      };
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [
      (final: super: {
        xbindkeys = final.callPackage ./xbindkeys {
          inherit (super) xbindkeys;
        };
      })
    ];
    systemd.user.services."xbindkeys" = {
      enable = true;
      environment.PATH = mkForce (makeBinPath [ pkgs.runtimeShellPackage ]);
      serviceConfig = {
        KillMode = "process";
        Restart = "always";
        RestartSec = "1";
        ExecStart = ''
          ${pkgs.xbindkeys}/bin/xbindkeys -f /etc/xdg/xbindkeysrc --nodaemon
        '';
      };
      unitConfig = {
        ConditionPathExists = "/run/user/%U";
      };
      wantedBy = [ "graphical-session.target" ];
      bindsTo = [ "graphical-session.target" ];
      partOf = [ "graphical-session.target" ];
      restartTriggers = [
        # So that some hardcoded system paths in the config gets refreshed
        config.environment.systemPackages
      ];
    };
    environment.etc."xdg/xbindkeysrc" = {
      text = configFile;
    };
  };
}
