{ xbindkeys, lib }:

let
  inherit (lib) optional;
in
xbindkeys.overrideDerivation(oldAttrs: {
    patches = [
      ./0001-run_command-exits-on-execlp-failure.patch
      ./0002-run_command-Use-POSIXLY-correct-bin-sh.patch
    ]
    ++ optional (oldAttrs ? patches) oldAttrs.patches
    ;
})

