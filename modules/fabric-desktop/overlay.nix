{ config, lib, ... }:

let
  fabric = final: super:
  let
    # This mixes `callPackage` and `fetchFromGitHub` semantics
    # for the Fabric Desktop repositories... but without IFD.
    callFabricPackage =
      { repo, rev, hash, owner ? "fabric-desktop" }:
      final.callPackage (
        let
          src = fetchTarball {
            url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
            sha256 = hash;
          };
        in
          "${src}/package.nix"
      )
      {}
    ;
  in
    {
      fabric-ui = callFabricPackage {
        repo = "fabric-ui";
        rev = "6f7978b381be79ef8531fa8c7f87aaab12973bcd";
        hash = "sha256:19qpln7dkrg9hc3lhazchacass7q535xcj3hk13macr9rg69af8b";
      };
      hello = callFabricPackage {
        repo = "fabric-hello";
        rev = "f832f8854782a4bcbcafaa7369af66921e251fbf";
        hash = "sha256:15vxarhsq9w2nbysyd1q7fs5bzdilnrcs7a4964is9kn010jx6r7";
      };
      hue-hi = callFabricPackage {
        repo = "hue-hi";
        rev = "3cfcf3da2776d8776fd8e9ad662ad2fc7c6851d5";
        hash = "sha256:1wymxp2fd4ms0073riqwygw0xj6ywmg90qspql68xi4gyhafp9zb";
      };
      launcher = callFabricPackage {
        repo = "fabric-launcher";
        rev = "d4597b95481f63f6faf2c84a0887e0e53619343f";
        hash = "sha256:14sx4fayadndhly9c7qyraqy9f1ih482hc18mzhm3x3jpvxmdirw";
      };
      webapp = callFabricPackage {
        repo = "fabric-webapp";
        rev = "0065951906b44cf93dfaeb215a5dfbf9e4c6a38f";
        hash = "sha256:09pw1fzkz9h26yp0xjw4ff38a229kl96cm1y410ypxx4iq2m2zf5";
      };
    }
  ;

  inherit (lib)
    mkEnableOption
    mkIf
    mkMerge
  ;
  cfg = config.fabric.overlay;

in
{
  options = {
    fabric.overlay = {
      enable = mkEnableOption "use of the fabric packages set overlay";
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [(final: _:
      let
        # Makes an empty extensible set, and call `extend`.
        # This provides an overlay-like signature (final: super:)
        # to build custom package sets.
        pkgsSet =
          (final.lib.makeExtensible (
            self: { callPackage = final.newScope self; }
          )).extend
        ;
      in
      {
        fabric = pkgsSet fabric;
      }
    )];
  };
}
