{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkEnableOption
    mkIf
    mkMerge
  ;
  cfg = config.fabric.desktop.launcher;
in
{
  options = {
    fabric.desktop.launcher = {
      enable = mkEnableOption "use of the fabric launcher as a user service";
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      systemd.user.services."fabric.desktop.launcher" = {
        enable = true;
        #environment.PATH = lib.mkForce null;
        serviceConfig = {
          Restart = "always";
          RestartSec= "5";
          # TODO: fold convergent session within fabric desktop
          ExecStart = ''
            ${pkgs.samueldr-convergent-session.with-profile} ${pkgs.fabric.launcher}/bin/launcher
          '';
        };
        unitConfig = {
          ConditionPathExists = "/run/user/%U";
        };
        wantedBy = [ "convergent-session.session.target" ];
        bindsTo = [ "convergent-session.session.target" ];
        partOf = [ "convergent-session.session.target" ];
        restartTriggers = [
          pkgs.fabric.launcher
        ];
      };
    })
  ];
}
