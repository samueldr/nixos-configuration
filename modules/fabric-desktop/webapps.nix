{ config, lib, pkgs, ... }:

let
  inherit (lib)
    filterAttrs
    mapAttrsToList
    mkEnableOption
    mkOption
    mkIf
    mkMerge
    types
  ;
  cfg = config.fabric.desktop.launcher;

  webappSubmodule = types.submodule (
    { name, config, ... }:
    {
      options = {
        name = mkOption {
          internal = true;
          type = types.str;
          default = name;
        };
        enable = mkEnableOption "the ${name} webapp.";
        id = mkOption {
          type = types.str;
          description = ''
            Identifier for the webapp.

            Used for stateful data.
          '';
        };
        applicationName = mkOption {
          type = types.str;
          default = name;
          description = ''
            Name exposed in the desktop file for this application
          '';
        };
        applicationIcon = mkOption {
          type = types.path;
          default = "${pkgs.adwaita-icon-theme}/share/icons/Adwaita/symbolic/status/dialog-warning-symbolic.svg";
          description = ''
            Icon for the webapp.
          '';
        };
        iconPackage = mkOption {
          type = types.path;
          defaultText = "/* Made from applicationIcon with iconName name */";
          description = ''
            Package containing the themed icons for the webapp.
          '';
        };
        url = mkOption {
          type = types.str;
          description = ''
            URL to load at launch.
          '';
        };
        cookies = mkOption {
          type = types.bool;
          default = false;
          description = ''
            Whether or not to save stateful data (cookies, web storage) for this webapp.
          '';
        };
        title = mkOption {
          type = types.str;
          default = "%title%";
          description = ''
            Title for this application.

            The `%title%` token will be replaced by the page title.
          '';
        };
        allowedUriPatterns = mkOption {
          type = with types; listOf str;
          description = ''
            URL patterns (regexes) this webapp handles.

            Any navigation to another URL will call to the default URL handler.

            In otherwords, URLs not matching this list will open in the user's browser.
          '';
        };

        iconName = mkOption {
          type = types.str;
          default = "webapp-${config.name}-icon";
          description = ''
            Theme icon name for the webapp.
          '';
        };

        outputs = {
          config = mkOption {
            internal = true;
          };
          desktopFile = mkOption {
            internal = true;
          };
          shim = mkOption {
            internal = true;
          };
          package = mkOption {
            internal = true;
          };
        };
      };
      config = {
        iconPackage = pkgs.runCommand "webapp-${config.name}-icon" {
          inherit (config) applicationIcon;
        } ''
          (
          mkdir -p $out/share/icons/hicolor/scalable/apps
          cd $out/share/icons/hicolor/scalable/apps
          ext="''${applicationIcon##*.}"
          cp "$applicationIcon" "${config.iconName}.$ext"
          )
        '';
        outputs = {
          config = pkgs.writers.writeJSON "webapp-${name}.config.json" {
            inherit (config)
              id
              title
              url
              cookies
            ;
            allowed_uri_patterns = config.allowedUriPatterns;
          };
          desktopFile = pkgs.makeDesktopItem {
            desktopName = config.applicationName;
            name = "webapp-${config.name}";
            exec = "${config.outputs.shim}/bin/webapp-${config.name}";
            icon = config.iconName;
          };
          shim = pkgs.writeShellScriptBin "webapp-${config.name}" ''
            exec "${pkgs.fabric.webapp}/bin/webapp" "${config.outputs.config}"
          '';
          package = pkgs.symlinkJoin {
            name = "webapp-${config.name}";
            #postBuild = ''
            #  (
            #  mkdir -p $out/share/applications
            #  cd $out/share/applications
            #  ln -sf "${config.outputs.desktopFile}" "webapp-${config.name}"
            #  )
            #'';
            paths = [
              config.outputs.desktopFile
              config.outputs.shim
              config.iconPackage
            ];
          };
        };
      };
    }
  );
  enabledWebapps = filterAttrs (_: webapp: webapp.enable) config.fabric.webapps;
  webapps = mapAttrsToList (_: cfg: cfg.outputs.package) enabledWebapps;
in
{
  options = {
    fabric.webapps = mkOption {
      default = {};
      type = types.attrsOf webappSubmodule;
      description = ''
        Webapp configurations
      '';
    };
  };

  config = mkMerge [
    {
      environment.systemPackages =
        webapps
      ;
    }
  ];
}
