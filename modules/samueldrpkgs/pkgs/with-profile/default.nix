{ writeShellScript
, runtimeShell
, gnused
}:
# Wrapper allowing starting a command (parameters given) with the "login" profile.
writeShellScript "with-profile" ''
  exec -a "$1" "${runtimeShell}" --login -s -- "$@" <<EOF
  # Ugh... unclear the reason why, but under at least phosh this doesn't have the pam environment.
  # Let's force the environment in the scope!
  eval $( ${gnused}/bin/sed -e 's/\s\+DEFAULT=/=/' -e 's/PAM_USER/USER/' -e 's/@{/''${/g' < /etc/pam/environment )
  exec -a "$1" "\$@"
  EOF
''
