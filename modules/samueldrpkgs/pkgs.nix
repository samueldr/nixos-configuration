final: super:

builtins.listToAttrs (
  builtins.map
  (name: {
    inherit name;
    value = (
      final.callPackage (./pkgs + "/${name}") { }
    );
  })
    (builtins.attrNames (builtins.readDir ./pkgs)
  )
) // {
  # XXX workaround...
  # https://github.com/NixOS/nixpkgs/pull/368828
  # https://github.com/NixOS/nixpkgs/pull/368590
  # https://nixpk.gs/pr-tracker.html?pr=368590
  # https://nixpk.gs/pr-tracker.html?pr=368828
  pcmanfm = super.pcmanfm.override({
    stdenv = final.gcc13Stdenv;
  });
  libfm = super.libfm.override({
    stdenv = final.gcc13Stdenv;
  });
  # https://github.com/NixOS/nixpkgs/pull/367989
  # https://nixpk.gs/pr-tracker.html?pr=367989
  lrzsz = super.lrzsz.override({
    stdenv = final.gcc13Stdenv;
  });
}
