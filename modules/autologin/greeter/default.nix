# Mostly adapted from Jovian Greeter...
{ lib, stdenv, python3, plymouth, nodePackages }:

stdenv.mkDerivation {
  name = "autologin-greeter";

  src = ./.;

  nativeBuildInputs = [ python3.pkgs.wrapPython ];
  buildInputs = [ python3 ];
  pythonPath = [ python3.pkgs.systemd ];

  nativeCheckInputs = [
    nodePackages.pyright
  ];

  checkPhase = ''
    runHook preCheck

    pyright *.py

    runHook postCheck
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 greeter.py $out/bin/autologin-greeter
    wrapPythonPrograms --prefix PATH : ${lib.makeBinPath [ plymouth ]}

    runHook postInstall
  '';
}
