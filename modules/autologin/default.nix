{ config, pkgs, lib, options, ... }:
let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkOption
    mkMerge
    mkIf
    types
  ;
  greetdCfg = config.services.greetd;
  cfg = config.samueldr.autologin;
  autologin-greeter = pkgs.callPackage ./greeter {};

  tty = "tty${toString cfg.vt}";
  settingsFormat = pkgs.formats.toml { };
in
{
  options = {
    samueldr.autologin = {
      enable = mkEnableOption "usage of the autologin session";
      session = mkOption {
        type = with types ; /*nullOr*/ str // {
          check = userProvidedDesktopSession:
            lib.assertMsg (userProvidedDesktopSession != null -> (str.check userProvidedDesktopSession && lib.elem userProvidedDesktopSession config.services.displayManager.sessionData.sessionNames)) ''
              Desktop session '${userProvidedDesktopSession}' not found.
              Valid values for 'jovian.steam.desktopSession' are:
                ${lib.concatStringsSep "\n  " config.services.displayManager.sessionData.sessionNames}
              If you don't want a desktop session to switch to, set 'jovian.steam.desktopSession' to 'gamescope-wayland'.
            '';
        };
        default = null;
        example = "plasma";
        description = ''
          The session to launch for this autologin configuration.
        '';
      };
      user = mkOption {
        type = types.str;
        default = config.services.displayManager.autoLogin.user;
        defaultText = "config.services.displayManager.autoLogin.user";
        description = ''
          Username of the user to autologin as.
        '';
      };
      restart = options.services.greetd.restart;
      vt = options.services.greetd.vt;
      settings = options.services.greetd.settings;
    };
  };
  config = mkMerge [
    (mkIf cfg.enable {
      nixpkgs.overlays = [(final: super: {
        inherit autologin-greeter;
      })];
      services = {
        xserver.displayManager.lightdm.enable = mkDefault false;
        displayManager.sddm.enable = mkDefault false;
      };

      # Enable greetd for its systemd-wide configs
      services.greetd.enable = true;

      # Ensures the actual greetd service is not enabled when using VT1.
      systemd.services.greetd.enable = !(config.services.greetd.settings.terminal.vt == cfg.vt);

      # Configure the system
      systemd.services."autovt@${tty}".enable = false;

      # Instead we'll rely on greetd, and a custom greeter.
      samueldr.autologin.settings.default_session = {
        command = "${autologin-greeter}/bin/autologin-greeter ${cfg.session} ${cfg.user}";
      };

      security.pam.services = {
        greetd.text = ''
          auth      requisite     pam_nologin.so
          auth      sufficient    pam_succeed_if.so user = ${cfg.user} quiet_success
          auth      required      pam_unix.so

          account   sufficient    pam_unix.so

          password  required      pam_deny.so

          session   optional      pam_keyinit.so revoke
          session   include       login
        '';
      };
    })
    (mkIf cfg.enable {
      ################################################################################
      # Keep synced with NixOS's config.
      # (hermetic re-usable modules when?)
      samueldr.autologin.settings.terminal.vt = lib.mkDefault cfg.vt;
      samueldr.autologin.settings.default_session.user = lib.mkDefault "greeter";
      systemd.services."autologin@${tty}" = {
        aliases =
          mkIf (cfg.vt == 1)
          [ "display-manager.service" ]
        ;

        unitConfig = {
          Wants = [
            "systemd-user-sessions.service"
          ];
          After = [
            "systemd-user-sessions.service"
            "getty@${tty}.service"
          ] ++ lib.optionals (!greetdCfg.greeterManagesPlymouth) [
            "plymouth-quit-wait.service"
          ];
          Conflicts = [
            "getty@${tty}.service"
          ];
        };

        serviceConfig = {
          ExecStart = "${pkgs.greetd.greetd}/bin/greetd --config ${settingsFormat.generate "greetd.toml" cfg.settings}";

          Restart = lib.mkIf cfg.restart "on-success";

          # Defaults from greetd upstream configuration
          IgnoreSIGPIPE = false;
          SendSIGHUP = true;
          TimeoutStopSec = "30s";
          KeyringMode = "shared";

          Type = "idle";
        };

        # Don't kill a user session when using nixos-rebuild
        restartIfChanged = false;

        wantedBy = [ "graphical.target" ];
      };
      # End of bits to keep synced...
      ################################################################################
    })
  ];
}
