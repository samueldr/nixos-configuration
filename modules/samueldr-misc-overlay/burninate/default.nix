{ pkgs, callPackage }:

let
  rev = "16c6eeeb697372b3d115d013e542b10996e370d9";
  sha256 = "sha256:1b8d8drsdqqq7xrh8ff03cpr13ykdzy5vyvfyxrh1mzn8ns6b84a";
  # I'm purposefully dogfooding the `default.nix` from the repo.
  # This is done to ensure it stays true and working.
  tarball = (builtins.fetchTarball {
    url = "https://github.com/samueldr/burninate/archive/${rev}.tar.gz";
    inherit sha256;
  });
in
callPackage (tarball) { inherit pkgs; }
