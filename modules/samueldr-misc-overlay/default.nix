{
  nixpkgs.overlays = [
    (final: super: {
      burninate = final.callPackage ./burninate {};
    })
  ];
}
