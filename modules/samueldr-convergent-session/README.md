samueldr's convergent session
=============================

This is a WIP thing that *I am making for my own use*, wherein I'm building up
my own window management paradigm and tooling from existing components so it
is just as useful on a desktop, a laptop, a convertible, a tablet, and a
smartphone.

YMMV.
