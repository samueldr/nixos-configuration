# Misc additional services required.
{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkForce
    mkIf
  ;
  inherit (config.lib.samueldr.convergent-session)
    userServiceFromAutostart
  ;
  cfg = config.samueldr.convergent-session;
in
{
  config = mkIf cfg.enable {
    systemd.user.services = lib.mkMerge [
      (userServiceFromAutostart { name = "nm-applet"; package = pkgs.networkmanagerapplet; })
    ];
  };
}
