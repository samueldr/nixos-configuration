{ config, lib, ... }:

let
  inherit (lib)
    mkIf
  ;

  cfg = config.samueldr.convergent-session;
in
{
  config = mkIf cfg.enable {
    services.libinput.enable = true;
    services.libinput.touchpad.naturalScrolling = true;
    services.libinput.touchpad.disableWhileTyping = true;
  };
}
