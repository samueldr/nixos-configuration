{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkForce
    mkIf
  ;
  cfg = config.samueldr.convergent-session;
in
{
  config = mkIf cfg.enable {
    environment.systemPackages = [
      (pkgs.callPackage (
        { runCommand, ruby, upower }:
        runCommand "samueldr.battery-information" {} ''
          mkdir -p $out/bin/
          cat > $out/bin/samueldr.battery-information <<EOF
          #!${pkgs.ruby}/bin/ruby
          EOF
          cat ${./scripts/samueldr.battery-information} \
            >> $out/bin/samueldr.battery-information
          substituteInPlace $out/bin/samueldr.battery-information \
            --replace 'UPOWER = "upower"' 'UPOWER = "${upower}/bin/upower"'
          chmod +x $out/bin/samueldr.battery-information
        ''
      ) {})
    ];
  };
}
