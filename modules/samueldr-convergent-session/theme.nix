{ config, lib, pkgs, ... }:

let
  inherit (lib)
    head
    mkIf
    mkMerge
    mkOption
    splitString
    toInt
    types
  ;
  cfg = config.samueldr.convergent-session;
  themeCfg = cfg.theme;

  # Truncates floating point part.
  floatToIntString = f: (head (splitString "." (toString f)));
  # Truncates floating point part.
  floatToInt = f: toInt (floatToIntString f);
in
{
  options = {
    samueldr.convergent-session = {
      theme = {
        cursor = {
          theme = mkOption {
            type = types.str;
            default = "Adwaita";
          };
          size = mkOption {
            type = types.int;
            default = if themeCfg.font.DPI < 192 then 48 else 64;
          };
        };

        icons = {
          theme = mkOption {
            type = types.str;
            default = "elementary";
          };
        };

        applications = {
          theme = mkOption {
            type = types.str;
            default = "Adapta";
          };
        };

        font = {
          DPI = mkOption {
            type = types.int;
            default = 96;
          };

          applications = {
            name = mkOption {
              type = types.str;
              default = "Roboto Condensed";
            };
            size = mkOption {
              type = types.int;
              default = 14;
            };
            asString = mkOption {
              type = types.str;
              default = "${themeCfg.font.applications.name} ${toString themeCfg.font.applications.size}";
              internal = true;
            };
            scaledWithDPI = {
              asString = mkOption {
                type = types.str;
                internal = true;
                default = "${themeCfg.font.applications.name} ${toString themeCfg.font.applications.scaledWithDPI.size}";
              };
              size = mkOption {
                type = types.int;
                default = themeCfg.font.applications.size * (floatToInt (1.0 * themeCfg.font.DPI / 96));
              };
            };
          };
          monospace = {
            name = mkOption {
              type = types.str;
              default = "Go Mono";
            };
            size = mkOption {
              type = types.int;
              default = 12;
            };
            asString = mkOption {
              type = types.str;
              default = "${themeCfg.font.monospace.name} ${toString themeCfg.font.monospace.size}";
              internal = true;
            };
            scaledWithDPI = {
              asString = mkOption {
                type = types.str;
                internal = true;
                default = "${themeCfg.font.monospace.name} ${toString themeCfg.font.monospace.scaledWithDPI.size}";
              };
              size = mkOption {
                type = types.int;
                default = themeCfg.font.monospace.size * (floatToInt (1.0 * themeCfg.font.DPI / 96));
              };
            };
          };
        };
      };
    };
  };
  config = mkIf cfg.enable {
    # Make default fonts self-contained
    # No need to add them to your config if you use the defaults!
    fonts.packages = mkMerge [
      (mkIf (themeCfg.font.monospace.name == "Go Mono") [ pkgs.go-font ])
      (mkIf (themeCfg.font.applications.name == "Roboto Condensed") [ pkgs.roboto ])
    ];
    environment.systemPackages = mkMerge [
      (mkIf (themeCfg.applications.theme == "Adapta") [ pkgs.adapta-gtk-theme ])
      (mkIf (themeCfg.cursor.theme == "Adwaita") [ pkgs.adwaita-icon-theme ])
      (mkIf (themeCfg.icons.theme == "elementary") [ pkgs.pantheon.elementary-icon-theme ])
    ];
  };
}
