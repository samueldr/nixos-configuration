{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkForce
    mkIf
    mkOption
    types
  ;
  cfg = config.samueldr.convergent-session;

  convergent-locker-implementation = pkgs.callPackage (
    { lib
    , writeShellScriptBin
    , i3lock
    , xorg
    }:
    writeShellScriptBin "convergent.locker" ''
      PARAMS=(
        --nofork
        --show-failed-attempts
        --ignore-empty-password
        --color=001E5C
        --pointer=default
        --image=/etc/wallpaper
      )
      PATH="${lib.makeBinPath [ i3lock ]}:$PATH"

      PS4=" $ "
      set -x
      exec i3lock "''${PARAMS[@]}" "$@"
    ''
  ) {
  };
in
{
  options = {
    samueldr.convergent-session = {
      locker.usePinpad = mkOption {
        default = true;
        type = types.bool;
      };
    };
  };
  config = mkIf cfg.enable {
    environment.systemPackages = [
      (pkgs.callPackage (
        { lib
        , writeShellScriptBin
        , i3lock
        , xorg
        }:
        # Stub, to cause xss-lock to activate
        writeShellScriptBin "convergent.locker" ''
          exec loginctl lock-session
        ''
      ) { })
    ];

    programs.xss-lock = {
      enable = true;
      lockerCommand =
        "${pkgs.samueldr-convergent-session.with-profile} ${convergent-locker-implementation}/bin/convergent.locker"
      ;
      extraOptions = [
        "--verbose"
      ];
    };

    systemd.user.services."xss-lock" = {
      wantedBy = [ "convergent-session.session.target" ];
      bindsTo = [ "convergent-session.session.target" ];
      partOf = [ "convergent-session.session.target" ];
    };

    powerManagement.resumeCommands = ''
      ${pkgs.procps}/bin/pkill -USR1 i3lock
    '';

    nixpkgs.overlays = mkIf (cfg.locker.usePinpad) [(final: super: {
      i3lock = super.i3lock.overrideAttrs({ ... }: {
        src = final.fetchFromGitHub {
          owner = "samueldr";
          repo = "i3lock-but-with-pinpad";
          rev = "4c12545d3b69e986e6de9448639d1cd71166e310";
          hash = "sha256-wNTeuhRdcm2/ev6HyTtwvNVNW90fmrUUc88IRGvAlGM=";
        };
      });
    })];
  };
}
