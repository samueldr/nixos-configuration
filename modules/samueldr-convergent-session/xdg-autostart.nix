{ config, lib, pkgs, ... }:

let
  inherit (pkgs.samueldr-convergent-session)
    with-profile
  ;
in
{
  lib.samueldr.convergent-session = {
    userServiceFromAutostart =
      { name
      , type ? "simple"
      , package ? "/run/current-system/sw"
      }:
      let
        path = "${package}/etc/xdg/autostart/${name}.desktop";
        script = pkgs.writeShellScript "run.xdg.autostart.${name}" ''
          source /etc/profile
          PATH="${package}/bin:$PATH"
          exec ${pkgs.dex}/bin/dex --wait "${path}"
        '';
      in
      {
        "xdg.autostart.${name}" = {
          enable = true;
          serviceConfig = {
            Type = type;
            Restart = lib.mkIf (type != "oneshot") "always";
            RestartSec= lib.mkIf (type != "oneshot") "5";
            ExecStart = "${with-profile} ${script}";
          };
          unitConfig = {
            ConditionPathExists = "/run/user/%U";
          };
          partOf = [ "desktop-session.target" ];
          wantedBy = [ "desktop-session.target" ];
          restartTriggers = [
            path
            script
          ];
        };
      }
    ;
  };
}
