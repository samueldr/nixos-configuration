{ config, pkgs, lib, ... }:

let
  inherit (lib)
    mkAfter
    mkDefault
    mkIf
    mkMerge
    mkOption
    types
  ;
  cfg = config.samueldr.convergent-session;
  convergent-session-launcher = pkgs.callPackage (
    { writeShellScript
    }:
    writeShellScript "convergent-session-launcher" ''
      echo
      echo ":: Starting convergent session..."
      echo

      at_exit() {
        # Ensures the session services don't stick around after the session is over.
        systemctl --user stop convergent-session.session.target 

        systemctl --user unset-environment DISPLAY XAUTHORITY XDG_SESSION_ID
      }
      trap at_exit EXIT SIGINT SIGTERM SIGKILL

      XDG_SESSION_ID="$(loginctl show-seat $(loginctl list-seats --no-legend) --property ActiveSession --value)"

      # Minimum required so that everything works.
      systemctl --user import-environment HOME DISPLAY XAUTHORITY XDG_SESSION_ID DBUS_SESSION_BUS_ADDRESS

      # Ensures we restart the session, if for some reason it's left hanging around...
      systemctl --user restart convergent-session.session.target

      # Stops the session only when this script is killed.
      (exec -a '==convergent-session==' ${pkgs.samueldr-hang-utility}/bin/hang '(session marker)')
    ''
  ) { };
  convergent-session-logout = pkgs.callPackage (
    { writeShellScriptBin
    }:
    writeShellScriptBin "convergent-session-logout" ''
      exec pkill -f '==convergent-session=='
    ''
  ) {};
in
{
  imports = [
    ./autostart.nix
    ./iio-autorotate.nix
    ./keyboard-launcher.nix
    ./notifications.nix
    ./on-screen-keyboard.nix
    ./screen-locker.nix
    ./scripts.nix
    ./services.nix
    ./theme.nix
    ./window-manager.nix
    ./xdg-autostart.nix
    ./xserver.nix
    ./xsettings.nix
  ];

  options = {
    samueldr.convergent-session = {
      convergent-session-launcher = mkOption {
        type = types.package;
        internal = true;
        default = convergent-session-launcher;
      };
      enable = mkOption {
        default = false;
        description = ''
          Enables usage and default configuration for the custom convergent session.
        '';
        type = types.bool;
      };
      autoLogin = {
        user = mkOption {
          type = types.str;
          description = ''
            Username of the user to autologin as.
          '';
        };
        enable = mkOption {
          default = false;
          type = types.bool;
          description = ''
            Whether to autologin or not.

            This will configure display manager for you.

            Note that the backing window manager may change.
          '';
        };
      };
    };
  };

  config = mkMerge [
    {
      # Needs to always be imported or else eval breaks.
      nixpkgs.overlays = [
        (import ./overlay.nix)
      ];
    }
    (mkIf cfg.enable {
      services.xserver = {
        enable = mkDefault true;
        displayManager.startx.enable = true;
      };

      # Use libinput to handle the input devices.
      services.libinput.enable = mkDefault true;

      environment.systemPackages = [
        convergent-session-logout
      ];

      services.displayManager.defaultSession = "convergent-session";
      services.xserver.displayManager.session = [
        {
          name = "convergent-session";
          manage = "desktop";
          start = ''
            exec ${cfg.convergent-session-launcher}
          '';
        }
      ];

      # Misc. power stuff.
      services.upower.enable = mkDefault true;
      powerManagement.enable = mkDefault true;

      # Let the (convergent) graphical environment handle all of this.
      services.logind = {
        powerKey = "ignore";
        rebootKey = "ignore";
        suspendKey = "ignore";
        hibernateKey = "ignore";
        lidSwitch = "suspend";
        lidSwitchDocked = "ignore";
        lidSwitchExternalPower = "ignore";
      };

      environment.variables = {
        # Those are lies, but it fixes some issues.
        XDG_CURRENT_DESKTOP= lib.mkDefault "xfce4";
        DESKTOP_SESSION = lib.mkDefault "gnome";
      };
      programs.firefox.preferences = {
        "browser.tabs.inTitlebar" = 0;
      };

      # Assume these services are part of this session only when enabled.
      systemd.user.services."redshift" = {
        wantedBy = [ "convergent-session.session.target" ];
        bindsTo = [ "convergent-session.session.target" ];
        partOf = [ "convergent-session.session.target" ];
      };
      systemd.user.services."unclutter-xfixes" = {
        wantedBy = [ "convergent-session.session.target" ];
        bindsTo = [ "convergent-session.session.target" ];
        partOf = [ "convergent-session.session.target" ];
      };
      systemd.user.services."xbindkeys" = {
        wantedBy = [ "convergent-session.session.target" ];
        bindsTo = [ "convergent-session.session.target" ];
        partOf = [ "convergent-session.session.target" ];
      };
    })
  ];
}
