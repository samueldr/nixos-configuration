{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkIf
    mkOption
    optionalString
    types
  ;

  cfg = config.samueldr.convergent-session;
  themeCfg = config.samueldr.convergent-session.theme;
  applications = themeCfg.applications;
  cursor = themeCfg.cursor;
  font = themeCfg.font;
  icons = themeCfg.icons;
in
{
  options = {
    samueldr.convergent-session = {
      xsettings = {
      };
    };
  };

  config = mkIf cfg.enable {
    environment.etc."xdg/xsettingsd/xsettingsd.conf" = {
      text = ''
        Gdk/UnscaledDPI ${toString (font.DPI * 1024)}
        Gdk/WindowScalingFactor 1
        Gtk/ButtonImages 1
        Gtk/CanChangeAccels 1
        Gtk/CursorThemeName "${cursor.theme}"
        Gtk/CursorThemeSize ${toString cursor.size}
        Gtk/EnableAnimations 1
        Gtk/FontName "${font.applications.asString}"
        Gtk/MenuImages 1
        Gtk/ToolbarStyle "both-horiz"
        Net/EnableEventSounds 0
        Net/EnableInputFeedbackSounds 0
        Net/IconThemeName "${icons.theme}"
        Net/ThemeName "${applications.theme}"
        Xft/Antialias 1
        Xft/DPI ${toString (font.DPI * 1024)}
        Xft/HintStyle "hintfull"
        Xft/Hinting 1
      '';
    };

    # Technically this is not related to xsettingsd...
    # ... but are X settings :)
    environment.etc."X11/Xresources" = {
      text = ''
        *.dpi: ${toString (font.DPI)}
        Xft.dpi: ${toString (font.DPI)}
        Xcursor.theme: ${cursor.theme}
        Xcursor.size: ${toString cursor.size}
      '';
    };

    systemd.user.services."xsettingsd" = {
      enable = true;
      environment.PATH = lib.mkForce null;
      # Ensures `/etc/xdg` is checked.
      environment.XDG_CONFIG_DIRS = config.environment.variables.XDG_CONFIG_DIRS;
      before = [ "convergent-session.window-manager.service" ];
      serviceConfig = {
        Restart = "always";
        RestartSec= "5";
        # Mainly used for things that xsettingsd can't handle.
        ExecStartPre = pkgs.writeShellScript "load-xresources" ''
          ${pkgs.xorg.xrdb}/bin/xrdb -merge < /etc/X11/Xresources
        '';
        ExecStart = ''
          ${pkgs.xsettingsd}/bin/xsettingsd
        '';
      };
      unitConfig = {
        ConditionPathExists = "/run/user/%U";
      };
      wantedBy = [ "convergent-session.session.target" ];
      bindsTo = [ "convergent-session.session.target" ];
      partOf = [ "convergent-session.session.target" ];
      restartTriggers = [
        "/etc/xdg/xsettingsd/xsettingsd.conf"
        "/etc/X11/Xresources"
      ];
    };
  };
}
