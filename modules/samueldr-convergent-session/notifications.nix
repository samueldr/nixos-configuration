# Notification daemon.
{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkForce
    mkIf
  ;
  cfg = config.samueldr.convergent-session;

  # Thin wrapper around the actual notification daemon
  notification-daemon = pkgs.writeShellScriptBin "convergent-session.notification-daemon" ''
    exec ${pkgs.xfce.xfce4-notifyd.out}/lib/xfce4/notifyd/xfce4-notifyd
  '';
in
{
  config = mkIf cfg.enable {
    systemd.user.services."convergent-session.notification-daemon" = {
      enable = true;
      environment.PATH = lib.mkForce null;
      serviceConfig = {
        Restart = "always";
        RestartSec= "5";
        ExecStart = ''
          ${notification-daemon}/bin/convergent-session.notification-daemon
        '';
      };
      unitConfig = {
        ConditionPathExists = "/run/user/%U";
      };
      wantedBy = [ "convergent-session.session.target" ];
      bindsTo = [ "convergent-session.session.target" ];
      partOf = [ "convergent-session.session.target" ];
      restartTriggers = [
        notification-daemon
      ];
    };
  };
}
