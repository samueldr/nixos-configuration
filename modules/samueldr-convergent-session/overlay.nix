final: super:

{
  samueldr-convergent-session = {
    # Customized window manager package
    awesome = final.awesome.override {
      lua = final.luajit;
    };
    inherit (final) with-profile;
  };
}
