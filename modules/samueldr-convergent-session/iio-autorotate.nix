{ config, lib, pkgs, ... }:

let
  inherit (lib)
    makeBinPath
    mkIf
    mkOption
    optionalString
    types
  ;
  cfg = config.samueldr.convergent-session;

  script = ''
    set -e
    set -u

    PATH="${makeBinPath (with pkgs;[
      coreutils
      gnugrep
      iio-sensor-proxy
      xorg.xinput
      xorg.xrandr
    ])}:$PATH"

    internal_display=$(xrandr | grep '^\(eDP\|DSI\)-\?1' | cut -d' ' -f1)

    rotate() {
        set +e
        xrandr --output "$internal_display" --rotate "''${1-normal}"
        for d in $(xinput list --id-only); do
          xinput map-to-output "$d" "$internal_display" 2>/dev/null &
        done
        set -e
    }

    while read -r line
    do
        case $line in
        "Accelerometer orientation changed:"*)
            side="''${line##* }"
            case $side in
                normal)
                    rotate normal
                    ;;
                bottom-up)
                    rotate inverted
                    ;;
                left-up)
                    rotate left
                    ;;
                right-up)
                    rotate right
                    ;;
            esac
            ;;
        #*)
        #   echo "Ignoring :$line:"
        #   ;;
        esac
    done < <( monitor-sensor )
  '';
in
{
  options = {
    samueldr.convergent-session = {
      autorotate = mkOption {
        default = false;
        type = types.bool;
      };
    };
  };
  config = mkIf (cfg.enable && cfg.autorotate) {
    hardware.sensor.iio.enable = true;

    # This is basically a raw import of what I was using before.
    # TODO: make an actual dbus-attached service instead.
    systemd.user.services."iio-autorotate" = {
      enable = true;
      inherit script;
      serviceConfig = {
        Restart = "always";
        RestartSec= "3";
      };
      unitConfig = {
        ConditionPathExists = "/run/user/%U";
      };
      wantedBy = [ "convergent-session.session.target" ];
      bindsTo = [ "convergent-session.session.target" ];
      partOf = [ "convergent-session.session.target" ];
    };
  };
}
