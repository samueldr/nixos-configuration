{ config, pkgs, lib, ... }:
let
  inherit (lib)
    mkMerge
    mkIf
  ;
  cfg = config.samueldr.convergent-session;
  convergent-greeter = pkgs.callPackage ./greeter {};
in
{
  config = mkMerge [
    (mkIf cfg.autoLogin.enable {
      services.xserver = {
        # We don't want LightDM when using autologin...
        displayManager.lightdm.enable = false;
        displayManager.startx.enable = true;
      };

      # Instead we'll rely on greetd, and a custom 
      services.greetd = {
        vt = 1;
        enable = true;
        settings = {
          default_session = {
            command = "${convergent-greeter}/bin/convergent-greeter ${cfg.autoLogin.user}";
          };
        };
      };

      # Ensure passwordless logins are possible
      security.pam.services = {
        greetd.text = ''
          auth      requisite     pam_nologin.so
          auth      sufficient    pam_succeed_if.so user = ${cfg.autoLogin.user} quiet_success
          auth      required      pam_unix.so

          account   sufficient    pam_unix.so

          password  required      pam_deny.so

          session   optional      pam_keyinit.so revoke
          session   include       login
        '';
      };
    })
  ];
}
