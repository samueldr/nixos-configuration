{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkForce
    mkIf
    mkOption
    optionalString
    types
  ;
  cfg = config.samueldr.convergent-session;

  inherit (pkgs.samueldr-convergent-session)
    with-profile
  ;
  inherit (cfg.theme) font;

  # Wrapper around "normal" rofi
  rofi = pkgs.writeScriptBin "rofi" ''
    #!${pkgs.stdenv.shell}
    set -u

    if (( $# < 1 )); then
        set -- "-show" "run"
    fi

    ARGS=(
      -i # case-insensitive
      -combi-modes run,drun,window,ssh
      -config "${rofiConfig}"
    )

    serviceName="orphaned.keyboard-launcher.child.$(${pkgs.util-linux}/bin/uuidgen)"

    exec \
      systemd-run \
        --user \
        --collect \
        --quiet \
        --service-type=forking \
        --unit="$serviceName" \
      ${with-profile} \
      ${pkgs.rofi}/bin/rofi "''${ARGS[@]}" "$@"
  '';

  # https://github.com/davatorium/rofi/blob/next/doc/rofi-theme.5.markdown#supported-properties
  # https://github.com/davatorium/rofi/blob/next/doc/rofi-theme.5.markdown#supported-element-path
  rofiConfig = pkgs.writeText "config.rasi" ''
    configuration {
      font: "${font.monospace.asString}";
      show-icons: true;
      click-to-exit: ${if cfg.keyboard-launcher.clickToExit then "true" else "false"};
      location: 2;
    }

    // Ensures no theme is loaded
    @theme "/dev/null"

    * {
      fg: #000000;
      bg: #ffffff;
      text-color: inherit;
      background-color: inherit;
      position: center;
    }

    entry, element-text {
    }
    element-text {
      vertical-align: 0.5;
    }
    element, prompt, entry {
      padding: 0.2em;
    }
    element normal {
      cursor: pointer;
      border: 0;
      text-color: @fg;
      background-color: @bg;
    }
    button selected,
    element.selected.normal,
    element.selected.urgent,
    element.selected.active {
      text-color: @bg;
      background-color: @fg;
    }
    list-view {
      scrollbar-width: 2em;
    }
    scrollbar {
    }
    window {
      border: 0.2em;
      background-color: @bg;
      border-color: @fg;
      padding: 0;
    }
    mainbox {
      padding: 0;
      border: 0;
    }
  '';
in
{
  imports = [
    # Explicit dependency
    ../xbindkeys
  ];

  options = {
    samueldr.convergent-session = {
      keyboard-launcher = {
        width = mkOption {
          type = types.int;
          default = 50;
        };
        clickToExit = mkOption {
          type = types.bool;
          default = true;
        };
      };
    };
  };

  config = mkIf cfg.enable {
    services.xbindkeys.enable = true;
    services.xbindkeys.bindings = {
      "keyboard-launcher" = {
        command = "${with-profile} ${rofi}/bin/rofi";
        keys = ["Mod1 + space"];
      };
    };
    environment.systemPackages = [
      rofi
    ];
  };
}
