{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkDefault
    mkForce
    mkIf
    mkOption
    types
  ;
  cfg = config.samueldr.convergent-session;

  inherit (pkgs.samueldr-convergent-session)
    awesome
    with-profile
  ;
in
{
  options = {
    samueldr.convergent-session = {
      window-manager = {
        devmode = mkOption {
          default = false;
          description = ''
            Whether the configuration is loaded directly from the directory
            where the source lives, or in a saved snapshot from the Nix store.

            NOTE: when deploying this configuration on another host, turning
            devmode on might result in weird and disfunctional results.
          '';
        };
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.user.targets."convergent-session.session" = {
      enable = true;
      unitConfig = {
        Upholds = [ "convergent-session.window-manager.service" ];
      };
      bindsTo = [ "graphical-session.target" ];
      # We don't want to have it started by `graphical-session` or else
      # it will cause issues when Steam starts with Jovian NixOS.
      # The session script launcher restarts this target anyways.
      wantedBy = [ /*"graphical-session.target"*/ ];
    };

    systemd.user.services."convergent-session.window-manager" = {
      enable = true;
      environment.PATH = mkForce null;
      serviceConfig = {
        Restart = "always";
        RestartSec= "1";
        ExecStart = ''
          ${with-profile} ${awesome}/bin/awesome
        '';
        WatchdogSec = "30";
      };
      unitConfig = {
        ConditionPathExists = "/run/user/%U";
      };
      bindsTo = [ "convergent-session.session.target" ];
      requisite = [ "convergent-session.session.target" ];
    };

    environment.etc = {
      "xdg/awesome".source = 
        (pkgs.callPackage (
          { runCommand, devmode }:

          runCommand "awesome-config" {} ''
            ${if devmode then ''
              ln -s   ${toString ./awesome} $out
            '' else ''
              cp -prf ${./awesome} $out
            ''}
            ''
        ) { inherit (cfg.window-manager) devmode; })
      ;
    };

    services.picom = {
      enable = mkDefault true;
      vSync = mkDefault true;
      backend = mkDefault "glx";
      # https://github.com/yshui/picom/blob/next/picom.sample.conf
      settings = {
        "unredir-if-possible" = true;
        "unredir-if-possible-exclude" = [
          ''_NET_WM_NAME@:s *?= "mpv"''
        ];
      };
    };
    systemd.user.services."picom" = {
      before = [ "convergent-session.window-manager.service" ];
      wantedBy = [ "convergent-session.session.target" ];
      bindsTo = [ "convergent-session.session.target" ];
      partOf = [ "convergent-session.session.target" ];
    };
    systemd.user.services."xset" = {
      enable = true;
      environment.PATH = lib.mkForce null;
      before = [ "convergent-session.window-manager.service" ];
      script = ''
        ${pkgs.xorg.xset}/bin/xset dpms 0 0 0
        ${pkgs.xorg.xset}/bin/xset s off
        ${pkgs.xorg.xset}/bin/xset s off -dpms
      '';
      unitConfig = {
        ConditionPathExists = "/run/user/%U";
      };
      wantedBy = [ "convergent-session.session.target" ];
      bindsTo = [ "convergent-session.session.target" ];
      partOf = [ "convergent-session.session.target" ];
    };
  };
}
