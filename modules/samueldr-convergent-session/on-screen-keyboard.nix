{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkForce
    mkIf
    mkOption
    types
  ;
  cfg = config.samueldr.convergent-session;
in
{
  options = {
    samueldr.convergent-session = {
      on-screen-keyboard = {
        enable = mkOption {
          type = types.bool;
          default = false;
        };
      };
    };
  };

  config = mkIf (cfg.enable && cfg.on-screen-keyboard.enable) {
    samueldr.convergent-session.keyboard-launcher = {
      # Otherwise using onboard exits rofi!
      clickToExit = false;
    };

    systemd.user.services."onboard" = {
      enable = true;
      environment.PATH = mkForce null;
      serviceConfig = {
        Restart = "always";
      };
      script = ''
        # A bit rude, but this ensures the keyboard always starts at a quarter
        # of the resolution.
        # onboard will not accept -s to set size with a docked keyboard.
        height=$(( $( ${pkgs.xorg.xwininfo}/bin/xwininfo -root | ${pkgs.gnugrep}/bin/grep '^\s\+Height:' | ${pkgs.coreutils}/bin/cut -d':' -f2 ) / 4 ))

        ${pkgs.dconf}/bin/dconf write /org/onboard/window/landscape/dock-height "$height"
        ${pkgs.dconf}/bin/dconf write /org/onboard/window/portrait/dock-height "$height"

        exec ${pkgs.onboard}/bin/onboard
      '';
      unitConfig = {
        ConditionPathExists = "/run/user/%U";
      };
      wantedBy = [ "convergent-session.session.target" ];
      bindsTo = [ "convergent-session.session.target" ];
      partOf = [ "convergent-session.session.target" ];
      restartTriggers = [
        pkgs.onboard
      ];
    };

    services.gnome.at-spi2-core.enable = true;

    environment.systemPackages = with pkgs; [
      onboard
      dconf # service files now needed for settings to save *sigh*
    ];

    # Needed for using the uinput backend
    services.udev.extraRules = ''
      # This rule is necessary for gamepad emulation.
      KERNEL=="uinput", SUBSYSTEM=="misc", TAG+="uaccess", OPTIONS+="static_node=uinput"
    '';
  };
}
