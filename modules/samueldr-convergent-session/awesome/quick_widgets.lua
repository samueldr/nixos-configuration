local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")

local QuickWidgets = {}

-- The wrapper itself has to be the background container
function wrapper(el)
	local p = wibox.container.place(el, "center")
	return wibox.container.background(p)
end

function QuickWidgets.image(img, args)
	local args = args or {}
	local color = args.color or nil
	local image = wibox.widget.imagebox()
	local img = THEME.get_image(img)
	if color then
		img = gears.color.recolor_image(img, color)
	end
	image:set_image(img)

	local container = wrapper(image)
	if DEBUG then
		container:set_bg("#ff0000")
	end

	return container
end

function QuickWidgets.themed_image(img)
	return QuickWidgets.image(img, { color = THEME.fg_normal })
end

function QuickWidgets.text(text)
	local txt = wibox.widget.textbox()
	txt:set_text(text)

	local container = wrapper(txt)
	if DEBUG then
		container:set_bg("#00ff00")
	end

	return container
end

function QuickWidgets.spawn_button(img, cmd)
	local img = THEME.get_image(img)
	img = gears.color.recolor_image(img, THEME.fg_normal)
	local launcher = awful.widget.launcher({
		image = (img),
		command = cmd,
	})

	local container = wrapper(launcher)
	if DEBUG then
		container:set_bg("#00ffff")
	end

	return container
end

return QuickWidgets
