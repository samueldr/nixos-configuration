local awful = require("awful")
local wibox = require("wibox")
local widgets = require("widgets")
local screenutils = require("_screen")
local CONFIG = require("config")

local BottomBar = {}

function side_spacer(width)
	return wibox.widget({
		forced_width = width,
		layout = wibox.layout.flex.horizontal,
	})
end

function BottomBar:new(screen)
	return setmetatable({}, {__index = self}):init(screen)
end

function BottomBar:init(screen)
	self.wibar = awful.wibar({ position = "bottom", screen = screen })

	-- root holds all widgets, stacked on top of each other
	local root = wibox.layout.align.horizontal()
	self.wibar:set_widget(root)

	local bar_start  = wibox.layout.flex.horizontal()
	local bar_center = wibox.layout.flex.horizontal()
	local bar_end    = wibox.layout.flex.horizontal()

	-- start
	bar_start:add(widgets.context_sensitive_back())
	--                 center
	bar_center:add(widgets.show_desktop())
	--                                      end
	bar_end:add(widgets.overview(screen))

	-- 10% of the width
	-- ~72px at 720w
	local sp = screen.geometry.width / 100 * 10

	-- But not smaller than 48 display units
	-- ~72px at 144dpi
	if screenutils.dpi_adjusted(48) > sp then
		sp = screenutils.dpi_adjusted(48)
	end

	root:set_first(side_spacer(sp))

	root:set_second(
		wibox.widget({
			bar_start,
			bar_center,
			bar_end,
			layout = wibox.layout.ratio.horizontal,
		})
	)

	root:set_third(side_spacer(sp))
	-- Add OSK toggle in the void next to the actual buttons
	root.third:add(widgets.toggle_osk())

	self.height = self.wibar.height
	local bar = self

	client.connect_signal("focus", function(c)
		bar.wibar.visible = true
		for _,obj in pairs(CONFIG.bottom_bar_hidden_for) do
			if obj.class and obj.class == c.class then
				bar.wibar.visible = false
			end
		end
	end)
	client.connect_signal("unfocus", function(c)
		for _,obj in pairs(CONFIG.bottom_bar_hidden_for) do
			if obj.class and obj.class == c.class then
				bar.wibar.visible = true
			end
		end
	end)

	return self
end

return setmetatable(BottomBar, {
	__call = BottomBar.new,
})
