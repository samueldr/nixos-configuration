local awful = require("awful")
local screenutils = require("_screen")
local wibox = require("wibox")
local chassis = require("_chassis")
local Qw = require("quick_widgets")
local widgets = require("widgets")
local gears = require("gears")
local ClientsMenu = require("clients_menu")

local NotchWorkaround = {}

function NotchWorkaround:new(screen)
	return setmetatable({}, {__index = self}):init(screen)
end

function NotchWorkaround:init(screen)
	local _screen = screenutils.xrandr_name_for_screen(screen)

	self.wibar = awful.wibar({
		position = "top",
		screen = screen,
		bg = "#000000",
		height = 79,
	})

	local w -- temp var
	-- root holds all widgets, stacked on top of each other
	local root = wibox.layout.stack()
	self.wibar:set_widget(root)

	local bar_start  = wibox.layout.fixed.horizontal()
	local bar_center = wibox.layout.fixed.horizontal()
	local bar_end    = wibox.layout.fixed.horizontal()

	-- Notch forbidden area
	w = wibox.container.background(wibox.widget.textbox(), "#000000")
	w:set_forced_width(318)
	bar_center:add(w)

	w = wibox.container.background()
	w:set_forced_width(64)
	bar_start:add(w)
	-- TODO: something useful here
	bar_start:add(wibox.widget.textbox("<span foreground='#666666'><small>@samueldr</small></span>"))

	w = wibox.container.background()
	bar_end:add(wibox.widget.textclock("<span foreground='#cccccc'><small>%H:%M:%S</small></span>", 1))
	w:set_forced_width(64)
	bar_end:add(w)

	--
	-- Now prepare the layout
	--

	local bar_content = wibox.layout.align.horizontal()
	root:add(bar_content)
	-- Align the clock to the center of the screen on mobile
	if chassis.is_kbless and screenutils.is_device_display(_screen) then
		bar_content.expand = "outside"
	end
	bar_content:set_left(wibox.container.place(bar_start, "left"))
	bar_content:set_middle(bar_center)
	bar_content:set_right(wibox.container.place(bar_end, "right"))

	bar_content.first:set_content_fill_vertical(true)
	bar_content.third:set_content_fill_vertical(true)

	-- To check alignment, change the center part colour.
	-- bar_center.bg = "#ff0000"
end

return setmetatable(NotchWorkaround, {
	__call = NotchWorkaround.new,
})
