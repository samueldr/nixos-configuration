local awful = require("awful")
local screenutils = require("_screen")
local wibox = require("wibox")
local chassis = require("_chassis")
local Qw = require("quick_widgets")
local widgets = require("widgets")
local ClientsMenu = require("clients_menu")
local CONFIG = require("config")

local scroll_tag = function(t, rel)
	local screen = t.screen
	local curr = screen.selected_tag.index
	local target = screen.tags[curr+rel]
	if target then
		target:view_only()
	end
end

local scroll_window = function(rel)
	local curr = client.focus
	if not curr then
		return
	end

	local clients = awful.client.visible(awful.screen.focused())
	local fcls = {}
	-- Remove all non-normal clients
	for _, c in ipairs(clients) do
		if awful.client.focus.filter(c) or c == sel then
			table.insert(fcls, c)
		end
	end
	clients = fcls

	target = nil
	for i,client in ipairs(clients) do
		if client == curr then
			target = clients[i+rel]
		end
	end

	if target then
		client.focus = target
	end
end

-- Button bindings {{{

-- Tags lists button config
local tag_list_buttons = awful.util.table.join(
	-- Left
	awful.button({        }, 1, awful.tag.viewonly),
	awful.button({ modkey }, 1, awful.client.movetotag),
	-- Middle
	awful.button({        }, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, awful.client.toggletag),
	-- Scroll
	awful.button({        }, 5, function(t) scroll_tag(t,  1) end),
	awful.button({        }, 4, function(t) scroll_tag(t, -1) end)
)

-- Tasks lists button config
local task_list_buttons = function(screen)
	return awful.util.table.join(
		awful.button({ }, 1, function (c)
			if c == client.focus then
				c.minimized = true
			else
				-- Without this, the following
				-- :isvisible() makes no sense
				c.minimized = false
				if not c:isvisible() then
					awful.tag.viewonly(c:tags()[1])
				end
				-- This will also un-minimize
				-- the client, if needed
				client.focus = c
				c:raise()
			end
		end),
		awful.button({ }, 3, function ()
			ClientsMenu:toggle(screen)
		end),
		awful.button({ }, 4, function ()
			scroll_window(-1)
		end),
		awful.button({ }, 5, function ()
			scroll_window(1)
		end)
	)
end

-- }}}

function scroll_layout(rel)
	local s = awful.screen[awful.screen.focused()]
	awful.layout.inc(rel, s, per_screen_available_layouts[awful.screen.focused()])
end

function layout_widget(screen)
	local widget = awful.widget.layoutbox(screen)
	widget:buttons(awful.util.table.join(
		awful.button({ }, 1, function () scroll_layout( 1) end),
		awful.button({ }, 3, function () scroll_layout(-1) end),
		awful.button({ }, 4, function () scroll_layout( 1) end),
		awful.button({ }, 5, function () scroll_layout(-1) end)
	))

	return widget
end

function tag_list_widget(screen)
	return awful.widget.taglist(
		screen,
		awful.widget.taglist.filter.all,
		tag_list_buttons,
		{
			-- Invert focus colours
			bg_focus = THEME.fg_focus,
			fg_focus = THEME.bg_focus,
			-- Tag pip
			squares_sel = THEME.squares_sel,
			squares_unsel = THEME.squares_unsel,
		}
	)
end

function task_list_widget(screen)
	return awful.widget.tasklist(
		screen,
		awful.widget.tasklist.filter.currenttags,
		task_list_buttons(screen)
	)
end

function maybe_systray(screen)
	-- TODO: prefer, in order: "screenutils.is_device_display", then
	-- "primary", finally, first screen found.
	-- For now we're just putting it on the first display :/
	if screen.index == 1 then
		return wibox.widget.systray()
	else
		return wibox.layout.fixed.horizontal()
	end
end

local TopBar = {}

function TopBar:new(screen)
	return setmetatable({}, {__index = self}):init(screen)
end

function TopBar:init(screen)
	local _screen = screenutils.xrandr_name_for_screen(screen)

	self.wibar = awful.wibar({ position = "top", screen = screen })

	-- root holds all widgets, stacked on top of each other
	local root = wibox.layout.stack()
	self.wibar:set_widget(root)

	local bar_start  = wibox.layout.fixed.horizontal()
	local bar_center = wibox.layout.fixed.horizontal()
	local bar_end    = wibox.layout.fixed.horizontal()

	--
	-- Bar content depends on the display "setup" it's on
	--
	if chassis.is_kbless and screenutils.is_device_display(_screen) then
		local left_rounded = Qw.image("left")
		left_rounded:buttons(awful.util.table.join(
		awful.button({ }, 1, function()
			-- TODO open quick settings
		end)
		))
		bar_start:add(left_rounded)
		if chassis.is_handset and not CONFIG.quirk_op6_notch then
			-- Left hanging smaller clock on phone.
			bar_start:add(wibox.widget.textclock("<small>%H:%M:%S</small>", 1))
			bar_start:add(widgets.spacer())
		end
		bar_start:add(maybe_systray(screen))
		bar_start:add(widgets.battery())
		bar_start:add(Qw.spawn_button("brightness.down", "light -T 0.9"))
		bar_start:add(Qw.spawn_button("brightness.up",   "light -T 1.2"))

		if chassis.is_tablet then
			-- Centered clock
			bar_center:add(wibox.widget.textclock("%H:%M:%S", 1))
		end

		-- End of the bar
		bar_end:add(widgets.spacer())
		bar_end:add(widgets.minimize())
		bar_end:add(widgets.spacer())
		bar_end:add(widgets.close())
		if not chassis.is_handset then
			bar_end:add(widgets.spacer())
		end
		bar_end:add(Qw.image("right"))
	else
		bar_start:add(widgets.spacer())
		bar_start:add(tag_list_widget(screen))

		bar_center:add(task_list_widget(screen))

		-- Mobile utilities
		if chassis.is_mobile and screenutils.is_device_display(_screen) then
			bar_end:add(Qw.spawn_button("brightness.down", "light -UT 1.2"))
			bar_end:add(Qw.spawn_button("brightness.up", "light -AT 1.2"))
			bar_end:add(widgets.battery())
		end
		bar_end:add(maybe_systray(screen))
		bar_end:add(widgets.spacer())
		bar_end:add(layout_widget(screen))
		bar_end:add(widgets.spacer())
		bar_end:add(wibox.widget.textclock("%m/%d %H:%M:%S ", 1))
	end

	--
	-- Now prepare the layout
	--

	local bar_content = wibox.layout.align.horizontal()
	root:add(bar_content)
	-- Align the clock to the center of the screen on mobile
	if chassis.is_kbless and screenutils.is_device_display(_screen) then
		bar_content.expand = "outside"
	end
	bar_content:set_left(wibox.container.place(bar_start, "left"))
	if #bar_center:get_children() > 0 then
		bar_content:set_middle(bar_center)
	end
	bar_content:set_right(wibox.container.place(bar_end, "right"))

	bar_content.first:set_content_fill_vertical(true)
	bar_content.third:set_content_fill_vertical(true)

	-- Create a promptbox for each screen
	-- Is a global since it's used with key bindings.
	local promptbox = awful.widget.prompt()
	screen_layout.promptbox[screen] = promptbox

	-- Overlaid by the prompt; by default dimensionless, so hidden
	root:add(wibox.container.place(promptbox, "left"))
end

return setmetatable(TopBar, {
	__call = TopBar.new,
})
