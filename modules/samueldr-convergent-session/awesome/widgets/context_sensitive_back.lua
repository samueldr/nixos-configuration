local awful = require("awful")
local Qw = require("quick_widgets")

function init()
	local widget = Qw.themed_image("back")
	widget:buttons(awful.util.table.join(
		awful.button({ }, 1, function()
			-- TODO: detect application in foreground and do different tasks
			--       which?
			awful.key.execute({}, "XF86Back")
		end)
	))

	return widget
end

return init
