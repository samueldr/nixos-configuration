local awful = require("awful")
local Qw = require("quick_widgets")

function init()
	local widget = Qw.themed_image("close")
	widget:buttons(awful.util.table.join(
		awful.button({ }, 1, function()
			if client.focus then
				client.focus:kill()
			end
		end)
	))

	return widget
end

return init
