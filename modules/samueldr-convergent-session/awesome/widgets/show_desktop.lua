local awful = require("awful")
local Qw = require("quick_widgets")

function minimize_all_clients_on_current_tag(val)
	for _, c in ipairs(mouse.screen.selected_tag:clients()) do
		c.minimized = val
	end
end

function init()
	local widget = Qw.themed_image("desktop")
	widget:buttons(awful.util.table.join(
		awful.button({ }, 1, function()
			-- *anything* is shown?
			if client.focus then
				-- Show the "desktop"
				minimize_all_clients_on_current_tag(true)
			else
				awful.spawn("dbus-send --session --type=method_call --dest=fabric.desktop.launcher /fabric/desktop/launcher fabric.desktop.launcher.ToggleVisibility")
			end
		end)
	))

	return widget
end

return init
