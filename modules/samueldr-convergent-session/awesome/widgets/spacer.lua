local Qw = require("quick_widgets")

function init()
	if DEBUG then
		return Qw.text("^")
	else
		return Qw.text(" ")
	end
end

return init
