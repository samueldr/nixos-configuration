local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local timer = gears.timer or timer


-- TODO: per-screen DPI, when the rest also does it.
local function dpiAdjusted(value)
	local factor = screen.primary.dpi / 96
	return value * factor
end

-- -----------------------------------------------------------------------------
-- The widget
-- -----------------------------------------------------------------------------
-- Use this way:
--
-- local battery = require("widgets/battery")
-- some_layout:add(battery())

local Battery = {}

function Battery:new(args)
	args = args or {}
	return setmetatable({}, {__index = self}):init(args)
end

function Battery:init(args)
	self.textbox = wibox.widget.textbox()
	self.textbox_margin = wibox.container.margin(self.textbox)
	self.widget = wibox.container.arcchart(wibox.container.place(self.textbox_margin))
	self.textbox.set_align("center")
	self.widget:set_border_width(0)
	self.widget:set_thickness(dpiAdjusted(3))
	self.widget:buttons(awful.util.table.join(
		awful.button({ }, 1, function() self:update() end),
		awful.button({ }, 3, function() self:update() end)
	))
	self.widget:set_min_value(0)
	self.widget:set_max_value(100)

	self.timer = timer({ timeout = args.timeout or 60 })
	self.timer:connect_signal("timeout", function() self:update() end)
	self.timer:start()
	self:update()

	local margin = dpiAdjusted(2)
	self.container = wibox.container.margin(self.widget, margin*2, margin*2, margin, margin)

	return self.container
end

function Battery:update()
	awful.spawn.easy_async_with_shell("samueldr.battery-information --grep | cut -d'.' -f2-", function(raw_data)
		local data = {}
		raw_data = gears.string.split(raw_data, "\n")
		for i,line in ipairs(raw_data) do
			local line = gears.string.split(line, "=")
			-- Assumes no `=` in the data, which is probably fine.
			data[line[1]] = line[2]
		end

		if data["battery.percentage"] then
			self.percentage = data["battery.percentage"]:gsub("%%", "")
			self.state = data["battery.state"]

			self:draw()
		else
			-- Actually no battery in this mobile chassis...
			self.timer:stop()
		end
	end)
end

function Battery:draw()
	local state = self.state
	self.textbox_margin:set_top(0)

	local label = "<span size='xx-small'>" .. self.percentage .. "</span>"
	if state == "charging" then
		self.textbox_margin:set_top(dpiAdjusted(3))
		label = "<span size='x-small'>🗲</span>"
	end

	self.widget:set_value(tonumber(self.percentage))
	self.textbox:set_markup(label)
end

return setmetatable(Battery, {
	__call = Battery.new,
})
