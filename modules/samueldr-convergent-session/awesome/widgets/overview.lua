local awful = require("awful")
local ClientsMenu = require("clients_menu")
local Qw = require("quick_widgets")

function init(screen)
	local widget = Qw.themed_image("overview")
	widget:buttons(awful.util.table.join(
		awful.button({ }, 1, function()
			-- if ClientsMenu is shown,
			-- close and switch to second window in the stack (double tap switch last)
			if ClientsMenu:is_visible() then
				awful.client.focus.history.previous()
				if client.focus then
					client.focus:raise()
				end
			end

			-- Not a mistake to be outside the previous conditional.
			-- We want to close if it's open.
			ClientsMenu:toggle(screen)
		end)
	))

	return widget
end

return init
