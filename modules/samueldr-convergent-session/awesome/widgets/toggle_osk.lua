local awful = require("awful")
local Qw = require("quick_widgets")

function init(args)
	local args = args or {}
	local widget = Qw.themed_image("keyboard")
	widget:buttons(awful.util.table.join(
		awful.button({ }, 1, function()
			awful.spawn("dbus-send --type=method_call --dest=org.onboard.Onboard /org/onboard/Onboard/Keyboard org.onboard.Onboard.Keyboard.ToggleVisible")
		end)
	))

	return widget
end

return init
