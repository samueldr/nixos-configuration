return {
	battery    = require("widgets/battery"),
	close      = require("widgets/close"),
	minimize   = require("widgets/minimize"),
	overview   = require("widgets/overview"),
	spacer     = require("widgets/spacer"),
	toggle_osk = require("widgets/toggle_osk"),
	show_desktop = require("widgets/show_desktop"),
	context_sensitive_back = require("widgets/context_sensitive_back"),
}
