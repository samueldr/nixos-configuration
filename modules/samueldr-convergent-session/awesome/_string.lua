---
-- This module modifies the string global table.
-- It adds functions unconditionally.

---
-- Splits strings.
function string:split(sep)
	local sep, fields = sep or ":", {}
	local pattern = string.format("([^%s]+)", sep)
	self:gsub(pattern, function(c) fields[#fields+1] = c end)
	return fields
end

---
-- Trims strings.
function string:trim()
	return (self:gsub("^%s*(.-)%s*$", "%1"))
end

---
-- Returns true if string is empty or composed entirely of spaces
function string:is_blank()
	return self:trim() == ""
end
