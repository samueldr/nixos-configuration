local gears = require("gears")
local naughty = require("naughty")

-- Config defaults
local CONFIG = {
	quirk_op6_notch = false,
	bottom_bar_hidden_for = {
		{ class = "KOReader", },
	},
}
local success, cfg = pcall(function ()
	return dofile("/etc/xdg/convergent-session/window-manager/config.lua")
end)
if success then
	gears.table.crush(CONFIG, cfg)
--else
--	naughty.notify({ text = "(No machine-specific config, or erroneous?)" })
end

return CONFIG
