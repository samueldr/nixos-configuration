local awful = require("awful")
local gears = require("gears")

---
-- This module "chords" together a leader key (power) with additional keys.
--
-- This effectively multiplies the amount of available shortcuts by 2, minus
-- one (can't chord leader with leader).
--
-- Though, with that said, it *also* adds an additional bind to the leader key,
-- where when it is held an action can be taken.
--

local Chorded = {}

setmetatable(Chorded, {
})

-- Currently held keys
local key_state = {}

-- Whether something happened in the current chord attempt.
-- If so, don't activate any other binds.
local did_action = false

---
-- Leader key.
-- Can't be changed after being bound, so don't expose for now
local leader_key = "XF86PowerOff"

-- no-op by default
local leader_on_hold_cb = function() end

-- Actions, indexed by chorded non-leader keys
local actions = {}

-- Timer used to detect leader key being held
local leader_timer = gears.timer({
	timeout = 1, -- reminder: count "0, 1, 2, ..." when testing
	callback = function()
		if did_action then return false end
		did_action = true
		leader_on_hold_cb()
	end,
	single_shot = true,
})

---
-- Looks for valid pairs of held keys and acts accordingly.
local function handle_chord()
	if did_action then return false end
	if not key_state[leader_key] then return false end

	for key, fn in pairs(actions) do
		if key_state[key] then
			did_action = true
			return fn(key)
		end
	end

	return false
end

---
-- When no action was taken, do the default non-chorded bind.
local function handle_release(key, release_fn)
	key_state[key] = nil

	if not did_action then
		release_fn(key)
	else
		if #(gears.table.keys(key_state)) == 0 then
			did_action = false
		end
	end
end

---
-- Bind actions on the chord leader.
-- 
-- @param release_fn When the leader key is released without chord or held action
-- @param hold_fn When the leader key was held for a while
function Chorded.on_leader(release_fn, hold_fn)
	if hold_fn then
		leader_on_hold_cb = hold_fn
	end
	return awful.key(
		{ }, leader_key,
		function () -- on press
			if not key_state[leader_key] then
				leader_timer:start()
			end
			key_state[leader_key] = true
			handle_chord()
		end,
		function () -- on release
			if leader_timer.started then
				leader_timer:stop()
			end
			handle_release(leader_key, release_fn)
		end
	)
end

---
-- Bind actions on a key for chorded use.
--
-- @param release_fn When released without chording
-- @param chord_fn When leader was chorded with this key
function Chorded.bind(key, release_fn, chord_fn)
	if key == leader_key then
		return error("Cannot bind on leader_key ("..leader_key..")")
	end

	-- Add the chorded bind action
	actions[key] = chord_fn

	return awful.key(
		{ }, key,
		function () -- on press
			key_state[key] = true
			handle_chord()
		end,
		function () -- on release
			handle_release(key, release_fn)
		end
	)
end

---
-- Use to clear the state.
--
-- This is meant to be used in bindings that activate keygrabbers, like menus.
-- These will intercept the release binds.
function Chorded.clear_state()
	for key, fn in pairs(key_state) do
		key_state[key] = nil
	end
	leader_timer:stop()
	did_action = false
end

return Chorded
