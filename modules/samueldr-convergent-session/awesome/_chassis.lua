local gears = require("gears")

local Chassis = {}

-- https://www.freedesktop.org/software/systemd/man/hostnamectl.html#chassis%20%5BTYPE%5D
Chassis.type = io.popen("hostnamectl chassis", "r"):read("*a"):trim():lower()

if Chassis.type:is_blank() then
	Chassis.type = "desktop"
end

Chassis.is_kbless = gears.table.hasitem({"tablet", "handset"}, Chassis.type)
Chassis.is_mobile = gears.table.hasitem({"tablet", "handset", "laptop", "convertible"}, Chassis.type)

Chassis.is_handset = Chassis.type == "handset"
Chassis.is_tablet = Chassis.type == "tablet"

print(
	" → Chassis: <" .. Chassis.type .. ">"
		.. " [" ..(Chassis.is_mobile and "is mobile" or "not mobile") .. "]"
		.. " [" ..(Chassis.is_kbless and "without keyboard" or "with keyboard") .. "]"
)

return Chassis
