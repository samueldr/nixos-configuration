local awful = require("awful")
local screenutils = require("_screen")
local chassis = require("_chassis")
local naughty = require("naughty")

-- TODO: better confirm dialog
local Confirm = function(question, yes, no)
	local height = nil
	height = screenutils.dpi_adjusted(48)
	local items = {
		{
			question,
			nil,
		},
		{
			"────────────────────",
			nil,
		},
		{
			"Yes",
			yes,
		},
		{
			"No",
			no,
		},

		-- Configuration
		theme = {
			width = screenutils.dpi_adjusted(300),
			height = height,
		}
	}

	awful.menu(items):show({ coords = { x = 99999, y = 0 }})
end

return {
	-- Keeps the menu reference for reuse
	menu = nil,

	---
	-- Whether the menu is shown
	is_visible = function (self)
		return self.menu and self.menu.wibox.visible
	end,

	---
	-- Implementation for showing the menu.
	--
	-- For some odd reason keeping the old menu instance around
	-- and using `:update()` does  not seem to work as expected.
	-- So `_show()` i used to actually show the menu, and save
	-- the reference so `:toggle()` can work as expected.
	_show = function(self)

		local height = nil
		height = screenutils.dpi_adjusted(48)
		local items = {
			{
				"Fullscreen window",
				function()
					local c = client.focus
					if c then
						c.fullscreen = not c.fullscreen 
					end
				end,
			},
			{
				"Minimize window",
				function()
					if client.focus then
						client.focus.minimized = true
					end
				end,
			},
			{
				"Close window",
				function()
					local c = client.focus
					if c then
						c:kill()
					end
				end,
			},
			{
				"────────────────────",
				function()
				end,
			},
			{
				"Restart WM",
				function()
					awesome.restart()
				end,
			},
			{
				"Lock",
				function()
					awful.spawn.with_shell("convergent.locker")
				end,
			},
			{
				"Reboot",
				function()
					Confirm(
						"Reboot?",
						function()
							awful.spawn.with_shell("reboot")
						end,
						function() end
					)
				end,
			},
			{
				"Power off",
				function()
					Confirm(
						"Power off?",
						function()
							awful.spawn.with_shell("poweroff")
						end,
						function() end
					)
				end,
			},
			{
				"────────────────────",
				function()
				end,
			},
			{
				"Screenshot",
				function()
					awful.spawn.with_shell("screenshot")
				end,
			},
			{
				"Screenshot area",
				function()
					awful.spawn.with_shell("screenshot -s")
				end,
			},
			-- {
			-- 	"Toggle touchscreen",
			-- 	function()
			-- 		-- TODO: make powermenu configurable, and add this in device-specific config
			-- 		awful.spawn.with_shell("input-config toggle 'Elan Touchscreen'")
			-- 	end,
			-- },
			{
				"Show keyboard",
				function()
					awful.spawn("dbus-send --type=method_call --dest=org.onboard.Onboard /org/onboard/Onboard/Keyboard org.onboard.Onboard.Keyboard.ToggleVisible")
				end,
			},

			-- Configuration
			theme = {
				width = screenutils.dpi_adjusted(300),
				height = height,
			}
		}

		self.menu = awful.menu(items)

		self.menu:show({ coords = { x = 99999, y = 0 }})
	end,

	---
	-- Toggle the menu
	toggle = function (self)
		if self:is_visible() then
			self.menu:hide()
			self.menu = nil
		else
			self:_show()
		end
	end,

	---
	-- Show the menu
	show = function (self)
		self:_show()
	end
}
