local awful = require("awful")
local screenutils = require("_screen")
local chassis = require("_chassis")

return {
	-- Keeps the menu reference for reuse
	menu = nil,

	---
	-- Whether the menu is shown
	is_visible = function (self)
		return self.menu and self.menu.wibox.visible
	end,

	---
	-- Implementation for showing the menu.
	--
	-- For some odd reason keeping the old menu instance around
	-- and using `:update()` does  not seem to work as expected.
	-- So `_show()` i used to actually show the menu, and save
	-- the reference so `:toggle()` can work as expected.
	_show = function(self, screen)
		local height = nil
		local filter = nil
		if screen then
			filter = function(c)
				return awful.widget.tasklist.filter.alltags(c, screen)
			end
		end
		height = screenutils.dpi_adjusted(48)
		self.menu = awful.menu.client_list({
			theme = {
				width = screenutils.dpi_adjusted(400),
				height = height,
			}
		}, {}, filter)
	end,

	---
	-- Toggle the clients menu
	toggle = function (self, screen)
		if self:is_visible() then
			self.menu:hide()
			self.menu = nil
		else
			self:_show(screen)
		end
	end,

	---
	-- Show the clients menu
	show = function (self)
		self:_show()
	end
}
