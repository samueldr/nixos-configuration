--DEBUG = true
print(":: samueldr's own convergent awesome config")
print(" → Launching configuration")

-- Patch string...
require("_string")

-- Disable notification daemon
-- https://github.com/awesomeWM/awesome/issues/1285#issuecomment-812923845
package.loaded["naughty.dbus"] = {}

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local awful_screen = require("awful.screen")

-- Chorded keybinds library
local Chorded = require("chorded")

-- Misc local libraries
local screenutils = require("_screen")
local chassis = require("_chassis")
local ClientsMenu = require("clients_menu")
local PowerMenu = require("power_menu")

local CONFIG = require("config")
local CONFIG_DIR = gears.filesystem.get_configuration_dir()

--
-- Themes define colours, icons, font and wallpapers.
--

-- Used internally by the theme; which is why this is a global :/
THEME_DIR = CONFIG_DIR .. "/samueldr-2016/"
THEME = require("samueldr-2016/theme")
beautiful.init(THEME_DIR.."theme.lua")

-- Assumed defaults
gears.wallpaper.set(THEME.bg_normal)
awful.screen.connect_for_each_screen(function(s)
	gears.wallpaper.maximized("/etc/wallpaper", s)
end)

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
	naughty.notify({ preset = naughty.config.presets.critical,
					 title = "Oops, there were errors during startup!",
					 text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function (err)
		-- Make sure we don't go into an endless error loop
		if in_error then return end
		in_error = true

		naughty.notify({ preset = naughty.config.presets.critical,
						 title = "Oops, an error happened!",
						 text = err })
		in_error = false
	end)
end
-- }}}

-- {{{ Variable definitions

-- Modifier key.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
-- FIXME: not a global...
per_screen_available_layouts = {}

local LAYOUTS_KBLESS = {
	awful.layout.suit.max,
	-- TODO: additional "simpler" split layouts
}
local LAYOUTS_FULL = {
		awful.layout.suit.tile,
		awful.layout.suit.tile.left,
		awful.layout.suit.tile.bottom,
		awful.layout.suit.tile.top,
		awful.layout.suit.fair,
		awful.layout.suit.fair.horizontal,
		-- awful.layout.suit.spiral,
		-- awful.layout.suit.spiral.dwindle,
		awful.layout.suit.max,
		-- Causes issues in tablet mode...
		-- awful.layout.suit.max.fullscreen,
		awful.layout.suit.magnifier,
		awful.layout.suit.floating,
	}
-- }}}

-- Additional "mobile" UX {{{

awful.menu.menu_keys.up = awful.util.table.join(awful.menu.menu_keys.up, {
	"XF86AudioRaiseVolume"
})
awful.menu.menu_keys.down = awful.util.table.join(awful.menu.menu_keys.down, {
	"XF86AudioLowerVolume"
})
awful.menu.menu_keys.enter = awful.util.table.join(awful.menu.menu_keys.enter, {
	"XF86PowerOff"
})

-- }}}

-- {{{ User interface

-- Holds per-topic tables related to the on-screen layout.
-- Elements of this table are themselves tables, indexed by screen.
-- (global)
screen_layout = {
	tags = {},
	promptbox = {},
}

--
-- Tags
--

-- Each screen has its own tag table.
awful.screen.connect_for_each_screen(function(s)
	-- Phone/tablet on-device display has only one tag.
	if chassis.is_kbless and screenutils.is_device_display(screenutils.xrandr_name_for_screen(s)) then
		per_screen_available_layouts[s] = LAYOUTS_KBLESS
		screen_layout.tags[s] = awful.tag({
			"📱",
		}, s, per_screen_available_layouts[s][1])
	else
		per_screen_available_layouts[s] = LAYOUTS_FULL
		screen_layout.tags[s] = awful.tag({
			"🍉",
			"🍈",
			"🍇",
			"🍓",
			"🍎",
			"🍋",
			"🍍",
			"🍆",
			"💩",
		}, s, per_screen_available_layouts[s][1])
	end
end)

--
-- Top bar
--

-- Make one bar per screen
awful.screen.connect_for_each_screen(function(s)
	if CONFIG.quirk_op6_notch then
		require("op6_notch_workaround")(s)
	end
	require("top_bar")(s)

	-- Optionally add a navbar for touch first devices.
	if chassis.is_kbless and screenutils.is_device_display(screenutils.xrandr_name_for_screen(s)) then
		require("bottom_bar")(s)
	end
end)

-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
	awful.key({ modkey,   }, "Left",   awful.tag.viewprev       ),
	awful.key({ modkey,   }, "Right",  awful.tag.viewnext       ),
	awful.key({ modkey,   }, "Escape", awful.tag.history.restore),

	awful.key({ modkey,   }, "h",
		function ()
			awful.client.focus.byidx( 1)
			if client.focus then client.focus:raise() end
		end),
	awful.key({ modkey,     }, "l",
		function ()
			awful.client.focus.byidx(-1)
			if client.focus then client.focus:raise() end
		end),

	-- Layout manipulation
	awful.key({ modkey, "Shift"   }, "h", function () awful.client.swap.byidx(  1)  end),
	awful.key({ modkey, "Shift"   }, "l", function () awful.client.swap.byidx( -1)  end),
	awful.key({ modkey, "Control" }, "h", function () awful.screen.focus_relative( 1) end),
	awful.key({ modkey, "Control" }, "l", function () awful.screen.focus_relative(-1) end),
	awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
	awful.key({ modkey,           }, "Tab",
		function ()
			awful.client.focus.history.previous()
			if client.focus then
				client.focus:raise()
			end
		end),

	-- Standard program
	awful.key({ modkey, "Control" }, "r", awesome.restart),

	-- Layout management.
	awful.key({ modkey,           }, "k",     function () awful.tag.incmwfact( 0.05)    end),
	awful.key({ modkey,           }, "j",     function () awful.tag.incmwfact(-0.05)    end),
	-- TODO: add bindings for window sizing.
	--awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
	--awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
	--awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
	--awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),

	-- Change layouts
	awful.key({ modkey,           }, "space", function ()
		local s = awful_screen[awful_screen.focused()]
		awful.layout.inc(1, s, per_screen_available_layouts[awful_screen.focused()])
	end),
	awful.key({ modkey, "Shift"   }, "space", function ()
		local s = awful_screen[awful_screen.focused()]
		awful.layout.inc(-1, s, per_screen_available_layouts[awful_screen.focused()])
	end),

	awful.key({ modkey, "Control" }, "n", awful.client.restore),

	-- Prompt
	--awful.key({ modkey },   "r",  function () screen_layout.promptbox[mouse.screen]:run() end),

	awful.key({ modkey }, "x",
		function ()
			awful.prompt.run({ prompt = "Run Lua code: " },
			screen_layout.promptbox[mouse.screen].widget,
			awful.util.eval, nil,
			awful.util.getdir("cache") .. "/history_eval")
		end),
	nil
)

clientkeys = awful.util.table.join(
	awful.key({ modkey,     }, "w", function(c) c:kill() end),
	awful.key({ modkey, "Shift"   }, "f",   function (c) c.fullscreen = not c.fullscreen  end),
	--awful.key({ modkey, "Shift"   }, "c",   function (c) c:kill()       end),
	awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle      ),
	awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
	awful.key({ modkey,        }, "o",    awful.client.movetoscreen      ),
	awful.key({ modkey,        }, "t",    function (c) c.ontop = not c.ontop   end),
	awful.key({ modkey,        }, "n",
		function (c)
			-- The client currently has the input focus, so it cannot be
			-- minimized, since minimized clients can't have the focus.
			c.minimized = true
		end),
	awful.key({ modkey,     }, "f",
		function (c)
			-- Ensures h/v maximized is not active.
			c.maximized_horizontal = false
			c.maximized_vertical   = false
			-- 4.1 maximized
			c.maximized = not c.maximized
		end)
)

-- Add Chorded keybings:
--   → See chorded.lua for some of the actions.
globalkeys = awful.util.table.join(globalkeys,
	Chorded.on_leader(
		function (key)
			awful.spawn.with_shell("convergent.locker")
		end,
		function ()
			-- Note: clear_state here may cause suspend to happen if PowerMenu does
			-- not cause a key grab to happen, e.g. if it fails to launch the menu.
			-- This might be helped by using a `pcall()`?
			Chorded.clear_state()
			PowerMenu:show()
		end
	),
	Chorded.bind(
		"XF86AudioRaiseVolume",
		function(key)
			awful.spawn("pactl -- set-sink-volume 0 +5%")
		end,
		function(key)
			local c = client.focus
			if c then
				c.fullscreen = not c.fullscreen 
			end
		end
	),
	Chorded.bind(
		"XF86AudioLowerVolume",
		function(key)
			awful.spawn("pactl -- set-sink-volume 0 -10%")
		end,
		function(key)
			Chorded.clear_state()
			ClientsMenu:show()
		end
	),
	{}
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
	globalkeys = awful.util.table.join(globalkeys,
	-- View tag only.
	awful.key({ modkey }, "#" .. i + 9,
		function ()
			local screen = mouse.screen
			local tag = awful.tag.gettags(screen)[i]
			if tag then
				awful.tag.viewonly(tag)
			end
		end),
	-- Toggle tag.
	awful.key({ modkey, "Control" }, "#" .. i + 9,
		function ()
			local screen = mouse.screen
			local tag = awful.tag.gettags(screen)[i]
			if tag then
				awful.tag.viewtoggle(tag)
			end
		end),
	-- Move client to tag.
	awful.key({ modkey, "Shift" }, "#" .. i + 9,
		function ()
			if client.focus then
				local tag = awful.tag.gettags(client.focus.screen)[i]
				if tag then
					awful.client.movetotag(tag)
				end
			end
		end),
	-- Toggle tag.
	awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
		function ()
			if client.focus then
				local tag = awful.tag.gettags(client.focus.screen)[i]
				if tag then
					awful.client.toggletag(tag)
				end
			end
		end))
end

clientbuttons = awful.util.table.join(
	awful.button({ }, 1, function (c)
		c:raise();
		if c.class == "Onboard" and c.instance == "onboard" then
			return
		end
		client.focus = c;
	end),
	awful.button({ modkey }, 1, awful.mouse.client.move),
	awful.button({ modkey }, 2, function(c)
		mousegrabber.run(function(_mouse)
			-- Lower immediately
			c:lower();

			-- Wait until button is released to change the cursor back
			for _,v in pairs(_mouse.buttons) do
				if v then return true end
			end

			return false
		end, "bottom_side")
	end),
	awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
	-- All clients will match this rule.
	{ rule = { },
		properties = {
			border_width = 0,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap+awful.placement.no_offscreen,
		}
	},
	{ rule = { class = "Gkrellm" },
		properties = {
			floating = true,
			border_width = 0,
			sticky = true,
			--dockable = true,

	} },

	-- Media players
	{ rule = { class = "mpv" },
		properties = {
			floating = true,
			border_width = 0,
			size_hints_honor = false,
		} },
	{ rule = { class = "vlc" },
		properties = { floating = true } },
	{ rule = { class = "MPlayer" },
		properties = { floating = true } },

	-- Custom term for pianobar
	{ rule = { instance = "pianobarterm" },
		properties = { floating = true, ontop = true, width = 700, height = 59 } },

	{ rule = { class = "pinentry" },
		properties = { floating = true } },

	-- Google Keep
	{ rule = { instance = "crx_hmjkmjkepdijhoojdojkdfohbdgmmhki" },
		properties = { floating = true } },

	-- Authy
	{ rule = { instance = "crx_gaedmjdfmmahhbjefcbgaolhhanlaolb" },
		properties = { floating = true } },

	-- Vysor
	{ rule = { instance = "crx_gidgenkbbabolejbgbpnhbimgjbffefm" },
		properties = { floating = true } },

	{ rule = { class = "VirtualBox" },
		except = { name = "Oracle VM VirtualBox Manager" },
		properties = { floating = true } },

	-- Waydroid running under weston
	{ rule = { class = "weston-waydroid" },
		properties = { fullscreen = true } },

	--
	-- gnome-screenruler
	--
	{ rule = { class = "Screenruler" },
		properties = { floating = true, border_width = 0, ontop = true } },

	--
	-- Onboard on-screen keyboard
	--
	{ rule = { instance = "onboard", class = "Onboard" },
		properties = {
			--floating = false,
			border_width = 0,
			sticky = true,
			focusable = false,
			nofocus = true,
		} },
}
-- }}}

-- {{{ Signals

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
	-- Enable sloppy focus
	c:connect_signal("mouse::enter", function(c)
		if c.class == "Onboard" and c.instance == "onboard" then
			return
		end

		if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
			and awful.client.focus.filter(c) then
			client.focus = c
		end
	end)

	if not startup then
		-- Set the windows at the slave,
		-- i.e. put it at the end of others instead of setting it master.
		-- awful.client.setslave(c)

		-- Put windows in a smart way, only if they does not set an initial position.
		if not c.size_hints.user_position and not c.size_hints.program_position then
			-- Attempts placing at the current focused window location.
			local my_unfocus = awful.client.focus.history.get(c.screen, 1, nil)
			if my_unfocus then
				awful.client.setslave(c)
				local c_step_init = nil
				while true do
					local c_step = awful.client.next(-1, c)
					if c_step_init == nil then
						c_step_init = c_step
					else
						if c_step_init == c_step then -- Prevent eternal loop.
							break
						end
					end
					-- Don't ask...
					-- Somehow c_step has vanished at this point sometimes...
					if c_step then
						c:swap(c_step)
					else
						break
					end
					if c_step == my_unfocus then
						break
					end
				end
			end
		end
	end

	-- Titlebar {{{
	local titlebars_enabled = false
	if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
		-- buttons for the titlebar
		local buttons = awful.util.table.join(
				awful.button({ }, 1, function()
					client.focus = c
					c:raise()
					awful.mouse.client.move(c)
				end),
				awful.button({ }, 3, function()
					client.focus = c
					c:raise()
					awful.mouse.client.resize(c)
				end)
				)

		-- Widgets that are aligned to the left
		local left_layout = wibox.layout.fixed.horizontal()
		left_layout:add(awful.titlebar.widget.iconwidget(c))
		left_layout:buttons(buttons)

		-- Widgets that are aligned to the right
		local right_layout = wibox.layout.fixed.horizontal()
		right_layout:add(awful.titlebar.widget.floatingbutton(c))
		right_layout:add(awful.titlebar.widget.maximizedbutton(c))
		right_layout:add(awful.titlebar.widget.stickybutton(c))
		right_layout:add(awful.titlebar.widget.ontopbutton(c))
		right_layout:add(awful.titlebar.widget.closebutton(c))

		-- The title goes in the middle
		local middle_layout = wibox.layout.flex.horizontal()
		local title = awful.titlebar.widget.titlewidget(c)
		title:set_align("center")
		middle_layout:add(title)
		middle_layout:buttons(buttons)

		-- Now bring it all together
		local layout = wibox.layout.align.horizontal()
		layout:set_left(left_layout)
		layout:set_right(right_layout)
		layout:set_middle(middle_layout)

		awful.titlebar(c):set_widget(layout)
	end
	-- }}}
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- {{{ Hack

local is_borderless = function(client)
	return (false
	or client.maximized
	or awful.rules.match(client, {class = "Gkrellm"})
	or awful.rules.match(client, {class = "mpv"})
	or awful.rules.match(client, {class = "Screenruler"})
	or awful.rules.match(client, {class = "Onboard"})
	)
end

-- TODO : Find out how to add a bottom border to my bar at the top.
-- TODO : border for floaters.
for s = 1, screen.count() do
	screen[s]:connect_signal("arrange", function ()
		local clients = awful.client.visible(s)
		local layout  = awful.layout.getname(awful.layout.get(s))

		local width = 0

		-- No borders with only one visible client or in maximized layout
		if #clients > 1 and layout ~= "max" then
			width = beautiful.border_width
		end
		for _, c in pairs(clients) do -- Floaters always have borders
			if is_borderless(c) then
				c.border_width = 0
			else
				c.border_width = width
			end
		end
	end)
end

-- }}}

-- GC {{{

if false then
-- Force frequent GC
gears.timer {
	timeout = 60,
	autostart = true,
	callback = function()
		--local start = collectgarbage("count")
		collectgarbage("collect")
		--local fin = collectgarbage("count")
		--naughty.notify({ text = "GC'd " .. (start-fin) .. " KiB" })
	end,
}
end

-- }}}

-- Watchdog {{{

-- ping systemd so that it can detect hangs.
do
	local ffi = require("ffi")

	ffi.cdef([[
		int sd_notify(int unset_environment, const char *state);
	]])

	if os.getenv("WATCHDOG_USEC") then

		local pet = function()
			ffi.C.sd_notify(0, "WATCHDOG=1")
		end

		gears.timer {
			timeout = tonumber(os.getenv("WATCHDOG_USEC")) / 1000000 / 2,
			autostart = true,
			callback = pet,
		}
		-- Prevents spurious watchdog bites if restarted near the time limit.
		pet() -- as a treat
	end
end

-- }}}
