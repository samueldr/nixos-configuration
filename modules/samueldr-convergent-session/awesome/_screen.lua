local gears = require("gears")
local awful = require("awful")
local xresources = require("beautiful.xresources")

local ScreenUtils = {}

function ScreenUtils.screen_names()
	-- Assumes 1:1 screen-to-outputs.
	-- Will not show disconnected screens (which is good)
	local screens = {}
	awful.screen.connect_for_each_screen(function(s)
		table.insert(screens, ScreenUtils.xrandr_name_for_screen(s))
	end)

	return screens
end

function ScreenUtils.has_screen(name)
	for _,screen in pairs(ScreenUtils.screen_names()) do
		if screen == name then
			return true
		end
	end

	return false
end

--
-- Returns geometry for given line.
--
-- (Gnarly...)
--
function ScreenUtils.geo_from_line(line)
	local geo = line:split("(")[1]:split(" ")
	local temp = nil
	local i = 0
	repeat
		temp = geo[#geo - i]
		i = i + 1
	until not temp or temp:match("+")

	if not temp then
		return nil
	end

	geo = temp:split("+")
	local _wh = geo[1]:split("x")
	return {
		w = tonumber(_wh[1]),
		h = tonumber(_wh[2]),
		x = tonumber(geo[2]),
		y = tonumber(geo[3]),
	}
end

--
-- Returns the xrandr name for a particular awesome screen.
--
function ScreenUtils.xrandr_name_for_screen(s)
	-- This assumes the awesome screen is on only one output.
	for k,v in pairs(s.outputs) do
		return k
	end
end

function ScreenUtils.is_device_display(screen)
	return gears.table.hasitem({
		"None-1", -- pinenote
		"default", -- fbdev
		"eDP",
		"eDP-1",
		"eDP1",
		"DSI-1",
		"DSI1",
	}, screen)
end

ScreenUtils.dpi_adjusted = xresources.apply_dpi

return ScreenUtils
