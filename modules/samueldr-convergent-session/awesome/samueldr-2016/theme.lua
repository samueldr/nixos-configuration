local awful = require("awful")
local ScreenUtils = require("_screen")

--
-- Theme basics parameters
-- -----------------------
--
theme = {}
theme.font          = "Go Mono 12"

--
-- Theme colours
-- -------------
--

local mode = "light"
--mode = "dark"

theme.bg_desktop    = "#ff00ff" -- ??
theme.bg_focus      = "#B1CAFF"
theme.bg_normal     = "#ffffff"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#e2e2e2"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#444444"
theme.fg_focus      = "#000000"
theme.fg_urgent     = "#000000"
theme.fg_minimize   = "#bbbbbb"

theme.border_width  = 2
theme.border_normal = "#000000"
theme.border_focus  = "#ff0000"
theme.border_marked = "#ff00ff"

if mode == "dark" then
	theme.bg_desktop    = "#222222"
	theme.bg_focus      = "#333333"
	theme.bg_normal     = "#333333"
	theme.bg_urgent     = "#ff0000"
	theme.bg_minimize   = "#000000"
	theme.bg_systray    = theme.bg_normal

	theme.fg_normal     = "#aaaaaa"
	theme.fg_focus      = "#cccccc"
	theme.fg_urgent     = "#000000"
	theme.fg_minimize   = "#999999"
end

theme.menu_border_color = theme.fg_focus
-- This is not an error...
-- `bg/fg_focus` is inverted for the taskbar button.
theme.menu_fg_focus = theme.bg_focus
theme.menu_bg_focus = theme.fg_focus
theme.menu_border_width = ScreenUtils.dpi_adjusted(2)

-- Helper to get an image's path.
local get_image = function(name)
	return THEME_DIR .. name .. ".svg"
end

-- Add it to the theme for use outside.
theme.get_image = get_image

--
-- Images
-- ------
--

-- For the layouts
theme.layout_fairh           = get_image("layouts/fairh")
theme.layout_fairv           = get_image("layouts/fairv")
theme.layout_floating        = get_image("layouts/floating")
theme.layout_magnifier       = get_image("layouts/magnify")
theme.layout_max             = get_image("layouts/max")
theme.layout_fullscreen      = get_image("layouts/fullscreen")
theme.layout_tilebottom      = get_image("layouts/tilebottom")
theme.layout_tileleft        = get_image("layouts/tileleft")
theme.layout_tile            = get_image("layouts/tileright")
theme.layout_tiletop         = get_image("layouts/tiletop")

-- For the selections.
theme.squares_sel   = get_image("squares_sel")
theme.squares_unsel = get_image("squares_unsel")

-- Finally, return the theme object.
return theme
