{
  imports =
    builtins.map
    (name: ./. + "/${name}")
    (builtins.filter (name: name != "default.nix" && name != "tests") (builtins.attrNames (builtins.readDir ./.)))
  ;
}
