{ config, lib, pkgs, ... }:

# This is the only touch-friendly screensaver.
# Though it requires configuring it to use `onboard`.
let
  inherit (lib)
    genAttrs
    mkIf
    mkOption
    mkEnableOption
    types
  ;
  cfg = config.samueldr.simple-numeric-pin;

  password-script = pkgs.callPackage (
    { writeScript, ruby }:
    writeScript "simple-numeric-pin" ''
      #!${ruby}/bin/ruby
      ${builtins.readFile ./simple-numeric-pin}
    ''
  ) { };

  # Used in the pam invocation to trace when debugging issues.
  debug = "";
  #debug = "debug log=/tmp/pam-debug.log"
in
{
  config = mkIf cfg.enable {
    security.pam.services = genAttrs cfg.pamServices (_:
      {
        unixAuth = true;
        text = lib.mkDefault (lib.mkBefore ''
          auth sufficient pam_exec.so expose_authtok ${debug} ${password-script}
        '');
      }
    );
  };

  options.samueldr.simple-numeric-pin = {
    enable = mkEnableOption "samueldr's screenshot utility";
    pamServices = mkOption {
      type = with types; listOf str;
      example = [ "xfce4-screensaver" "phosh" "kde" ];
      description = "List of names of PAM services where the pin is sufficient authentication.";
    };
  };
}
