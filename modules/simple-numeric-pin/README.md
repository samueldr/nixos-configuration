`simple-numeric-pin`
====================

A small module to allow numeric pin unlocking of specific PAM services.

This has not been independently audited.

YMMV.

> ```
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT DOCUMENTATION OF ANY KIND, EXPRESS OR IMPLIED
> ```
