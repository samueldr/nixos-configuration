{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkIf
  ;
  cfg = config.samueldr.profile;

  wrap = name: text: ''
    # ------------------------------------------------------------------------------
    # Start of ${name}
    # ------------------------------------------------------------------------------
  '' + text + ''
    # ------------------------------------------------------------------------------
    # End of ${name}
    # ------------------------------------------------------------------------------
  '';
in
{
  config = mkIf cfg.enable {
    environment.etc."profile".text = wrap "samueldr-profile/bash/profile" (builtins.readFile ./profile);

    # Setting XDG_CONFIG_DIRS in sessionVariables causes a conflict with NixOS defaults.
    # This is because `sessionVariables` are "applied" into their string forms, and attempted to be set to `avariables`.
    # It then fails because it conflicts with the list form.
    environment.variables = {
      XDG_CONFIG_DIRS = lib.mkForce [ "/etc/xdg" ];
    };
    # Sets XDG_CONFIG_DIRS in session variables so it's always set, even when profile is not involved.
    # This, among other things, fixes Plasma/KDE applications looking different when ran from the terminal.
    environment.sessionVariables = {
      XDG_CONFIG_DIRS = [ "/etc/xdg" ];
    };
  };
}
