{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkIf
  ;
  cfg = config.samueldr.profile;
in
{
  config = mkIf cfg.enable {
    programs.git.enable = true;
    programs.git.config = {
      user = {
        email = "samuel@dionne-riel.com";
        name = "Samuel Dionne-Riel";
      };
      format = {
        signOff = true;
      };
      credential = {
        helper = "cache --timeout 3600";
      };
      sendemail = {
        confirm = "always";
        smtpencryption = "tls";
        smtpserverport = 587;
      };
    };
  };
}
