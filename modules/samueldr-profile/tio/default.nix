{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkIf
  ;
  cfg = config.samueldr.profile;
in
{
  config = mkIf cfg.enable {
    # For upcoming versions (v2.5+)
    environment.etc."tio/config" = {
      source = ./config;
    };
    # Compatibility with v2.4 (current version as of writing)
    environment.etc."tio/tiorc" = {
      source = ./config;
    };

    environment.systemPackages = [
      pkgs.tio
    ];

    nixpkgs.overlays = [
      (final: super: {
        tio = super.tio.overrideAttrs({ postPatch ? "", ... }: {
          postPatch = postPatch + ''
            # Cheat a lot and force tio to load the config from `/etc`.
            substituteInPlace src/configfile.c \
              --replace 'getenv("XDG_CONFIG_HOME")' '"/etc"'

            # Ensure we don't have stray refs to XDG_CONFIG_HOME left.
            ! grep 'XDG_CONFIG_HOME' src/configfile.c
          '';
        });
      })
    ];
  };
}
