{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkIf
  ;
  cfg = config.samueldr.profile;
in
{
  config = mkIf cfg.enable {
    environment.variables.EDITOR = "nvim";

    environment.systemPackages = [
      pkgs.silver-searcher # Needed for :Ag
      pkgs.neovim
    ];
    nixpkgs.overlays = [
      (final: super: {
        neovim =
        let
          inherit (super.vimUtils) buildVimPlugin;
          inherit (super) fetchFromGitHub;
        in
        super.neovim.override {
          configure = {
            customRC = ''
              "
              " Basic vim setup
              " ===============
              "
              " All that follows is basically boilerplate for all
              " vim configurations. *sigh.*
              "

              scriptencoding utf-8
              if has('vim_starting')
                  if &compatible
                      set nocompatible
                  endif
              endif

              " UTF-8 everything.
              set encoding=utf-8
              set termencoding=utf-8

              " uuuuh, for stuff...
              set fileencodings=ucs-bom,utf-8,euc-jp,cp437,cp932,default,latin1

              "
              " neovim peculiarities
              " --------------------
              "

              " Disables cursor changes
              " https://github.com/neovim/neovim/issues/6798
              set guicursor=

              "
              " Formatting
              " ----------
              "

              " Automatically insert the current comment leader after hitting 'o' or 'O' in Normal mode.
              set formatoptions+=o
              " Autowraps lines to textwidth when textwidth is set, for lines and comments.
              set formatoptions+=t
              set formatoptions+=c
              " Though an already long line will not be forcefully broken
              set formatoptions+=l
              if v:version > 703 || v:version == 703 && has("patch541")
                set formatoptions+=j " Delete comment character when joining commented lines
              endif
              " Wrap text
              set wrap
              " When active, vim will break at one of the chars in "breakat" instead of
              " breaking words forcefully.
              set linebreak
              " I'm keeping the default breakat value for now.
              set display+=lastline
              set textwidth=0                " Don't wrap lines by default
              " Tab width
              set tabstop=4
              set shiftwidth=4

              " Tabs are not expanded to spaces
              set noexpandtab
              set backspace=indent,eol,start " allow backspacing over everything in insert mode

              "
              " User interface
              " --------------
              "
              " Start with some base configuration...
              " 
              syntax on                      " enable syntax highlighting

              " Color scheme
              colorscheme bclear
              colorscheme vim                " Works around neovim 0.10 change

              " FIXME : Review all this...
              let g:netrw_banner = 0         " do not show Netrw help banner
              set foldenable                 " Turn on folding
              set foldmethod=marker          " Fold on the marker
              set foldopen+=percent,mark
              set foldopen+=quickfix
              set foldopen=block,hor,tag     " what movements open folds
              set laststatus=2               " Always show status line
              set matchtime=2                " Bracket blinking.
              set mouse=a                    " Enable mouse in GUI mode
              set mousehide                  " Hide mouse after chars typed
              set novisualbell               " For console vim, does not use the visual bell.
              set number                     " Line numbers
              set ruler                      " Show the cursor position all the time
              set scrolloff=2                " If available, show three lines under cursor when at the bottom.
              set showmatch                  " Show matching brackets.
              set vb t_vb=                   " Sets the visual bell; will not ring the system bell

              set wildmenu                   " Menu for tab completion.
              " FIXME : Make those options.
              set wildignore+=*.o,*.obj      " Then some ignore rules for it.
              set wildignore+=.git,.svn,.hg
              set wildignore+=*.class
              set wildignore+=*.rbc,vendor/gems/*

              " Leader for some shortcuts.
              let mapleader = ","

              " Indenting and de-indenting

              " Remapping to keep selection when shifting
              vnoremap > >gv
              vnoremap < <gv
              " Alternative using Tab/Shift-Tab (for gvim).
              vnoremap   <Tab> >gv
              vnoremap <S-Tab> <gv

              " Replacement characters to show for invisible characters.
              if has("multi_byte_encoding")
                  " List of chars to show
                  set listchars+=eol:↲
                  set listchars+=tab:→ 
                  set listchars+=trail:·
                  set listchars+=extends:>
                  set listchars+=precedes:<
                  set listchars+=nbsp:␣

                  " When exiting insert mode, SHOW EVERYTING
                  " augroup trailing
                  "     au InsertEnter * :set nolist
                  "     au InsertLeave * :set list
                  " augroup END
              else
                  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
              endif
              " Map a command to show or hide them
              map <silent> <F2> :set list!<CR>
              nmap <F2>          :set list!<CR>
              imap <F2>     <ESC>:set list!<CR>
              vmap <F2>     <ESC>:set list!<CR>

              "
              " Ctrl+N Ctrl+P for buffers navigation
              "
              nnoremap <C-n> :bnext<CR>
              nnoremap <C-p> :bprevious<CR>
              nmap <C-PageDown> <C-n>
              nmap <C-PageUp> <C-p>

              " Allows leaving modified buffers.
              set hidden

              "
              " Then, "fix" issues with the terminal
              "

              " Fixes ^Left and ^Right with tmux.
              " FIXME : DOES NOT WORK FOR NEOVIM
              set <C-Left>=[D
              set <C-Right>=[C

              "
              " <Ctrl-l> redraws the screen and removes any search highlighting.
              "
              nnoremap <silent> <C-l> :nohl<CR>:mode<CR><C-l>

              "
              " Pasting in insert mode with CTRL+V only in insert mode (See mswin.vim)
              " I can never remember how to "properly" paste from the clipboard buffer.
              "
              exe 'inoremap <script> <C-V>' paste#paste_cmd['i']

              "
              " Terminal settings
              "

              if has('nvim')
                augroup terminal_settings
                  autocmd!

                  autocmd BufWinEnter,WinEnter term://* startinsert
                  autocmd BufLeave term://* stopinsert

                  " Ignore various filetypes as those will close terminal automatically
                  " Ignore fzf, ranger, coc
                  autocmd TermClose term://*
                      \ if (expand('<afile>') !~ "fzf") && (expand('<afile>') !~ "ranger") && (expand('<afile>') !~ "coc") |
                      \   call nvim_input('<CR>')  |
                      \ endif
                augroup END
              endif

              "
              " #### Tig
              "

              if has('nvim')
                fun! TigStatus()
                    call termopen("tig status")
                endfun

                nmap <F10> <ESC>:call TigStatus()<CR>i
                imap <F10> <ESC>:call TigStatus()<CR>i
                vmap <F10> <ESC>:call TigStatus()<CR>i
              endif

              "
              " #### netrw
              "
              let g:netrw_browse_split = 4
              let g:netrw_winsize = -25
              let g:netrw_banner = 0
              let g:netrw_liststyle = 3

              " eh, netrw is screwy :/
              "noremap <F12> :Lexplore<CR>

              "
              " ### airline
              "
              " Status line and buffers line plugin.
              "

              let g:airline_left_sep = ''
              let g:airline_left_alt_sep = ''
              let g:airline_right_sep = ''
              let g:airline_right_alt_sep = ''
              let g:airline_symbols_branch = ''
              let g:airline_symbols_readonly = ''
              let g:airline_symbols_linenr = ''

              let g:airline#extensions#tabline#enabled = 1
              let g:airline#extensions#tabline#show_buffers = 1
              let g:airline#extensions#tabline#left_sep = '''
              let g:airline#extensions#tabline#left_alt_sep = '''
              let g:airline#extensions#tabline#right_sep = '''
              let g:airline#extensions#tabline#right_alt_sep = '''

              " FIXME : Configurable by the theme section...
              let g:airline_theme='light'

              "
              " #### NERDTree
              "

              noremap <F12> :NERDTreeToggle<CR>
              noremap <S-F12> :NERDTreeFind<CR>
              let NERDTreeMinimalUI=1
              let NERDTreeWinSize=25

              let NERDTreeIgnore = ['\.pyc$', '\.o$']


              "
              " ### Bbye (Buffer Bye)
              "
              " A `:Bdelete` that behaves like a good citizen.
              "
              noremap <F4> :Bdelete<CR>

              "
              " ### FZF
              "
              " Do not lazy load, this is mostly always used.
              "

              " https://github.com/junegunn/fzf#respecting-gitignore-hgignore-and-svnignore
              " FIXME : This assumes presence of ag.
              let $FZF_DEFAULT_COMMAND= 'ag --hidden --ignore .git -g ""'

              "
              " ### Indent Guides
              "
              "Plug 'nathanaelkane/vim-indent-guides'
              "
              "let g:indent_guides_color_change_percent = 3
              "let g:indent_guides_guide_size = 2
              "let g:indent_guides_start_level = 2
              "let g:indent_guides_enable_on_vim_startup = 1
              "
              "" Other colours for terminal vim.
              "if !has('gui_running')
              "    autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=white
              "    autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=lightgrey
              "endif

              " noremap <C-F2> :IndentGuidesToggle<CR>

              "
              " ### Signify
              "

              "
              " ### Theme
              "

              "
              " ### ScreenShot
              "

              let ScreenShot = {'Icon':0, 'Credits':0, 'force_background':'#FFFFFF'}

              "
              " Browsing around
              " ---------------
              "
              " This heavily uses FZF.
              "
              " ### File finding
              "
              nmap <F1>          :FZF<CR>
              imap <F1>     <ESC>:FZF<CR>
              vmap <F1>     <ESC>:FZF<CR>
              "
              " ### Searching in all files (Ag)
              "
              nmap <F13>         :Ag<Space>
              imap <F13>    <ESC>:Ag<Space>
              vmap <F13>    <ESC>:Ag<Space>
              nmap <S-F1>        :Ag<Space>
              imap <S-F1>   <ESC>:Ag<Space>
              vmap <S-F1>   <ESC>:Ag<Space>
              nmap <C-F1>        :Ag<Space>
              imap <C-F1>   <ESC>:Ag<Space>
              vmap <C-F1>   <ESC>:Ag<Space>

              "
              " <F2> is for hidden chars...
              "

              "
              " ### Buffer finding, by name
              "
              nmap <F14>         :Buffers<CR>
              imap <F14>    <ESC>:Buffers<CR>
              vmap <F14>    <ESC>:Buffers<CR>
              nmap <S-F2>        :Buffers<CR>
              imap <S-F2>   <ESC>:Buffers<CR>
              vmap <S-F2>   <ESC>:Buffers<CR>
              nmap <C-F2>        :Buffers<CR>
              imap <C-F2>   <ESC>:Buffers<CR>
              vmap <C-F2>   <ESC>:Buffers<CR>

              "
              " ### Searching in buffer
              "
              nmap <F3>          :BLines<CR>
              imap <F3>     <ESC>:BLines<CR>
              vmap <F3>     <ESC>:BLines<CR>

              "
              " ### Searching in all buffers
              "
              nmap <F15>         :Lines<CR>
              imap <F15>    <ESC>:Lines<CR>
              vmap <F15>    <ESC>:Lines<CR>
              nmap <S-F3>        :Lines<CR>
              imap <S-F3>   <ESC>:Lines<CR>
              vmap <S-F3>   <ESC>:Lines<CR>
              nmap <C-F3>        :Lines<CR>
              imap <C-F3>   <ESC>:Lines<CR>
              vmap <C-F3>   <ESC>:Lines<CR>

              " Linters and helpers
              " -------------------


              "
              " ### Neomake
              "
              " Used to run syntax checkers, async.
              "
              let g:neomake_javascript_enabled_makers = ['eslint']
              let g:neomake_sh_enabled_makers = ['shellcheck']
              let g:neomake_ruby_enabled_makers = ['rubocop']
              autocmd! BufWritePost * Neomake

              "
              " Filetypes
              " ---------
              "
              " ### JavaScript / JSX
              "
              let g:javascript_plugin_jsdoc = 1
              let g:jsx_ext_required = 0 " Allow JSX in normal JS files
              let g:used_javascript_libs = 'jquery,underscore,react'

              "
              " ### Markdown
              "
              let g:vim_markdown_conceal = 0
              let g:vim_markdown_folding_disabled = 1
              let g:vim_markdown_new_list_item_indent = 2
            '';

            packages.myPlugins = {
              start = with super.vimPlugins; [
                # Plug 'tpope/vim-sensible'
                vim-sensible

                # Plug 'vim-airline/vim-airline'
                vim-airline
                # Plug 'vim-airline/vim-airline-themes'
                vim-airline-themes

                nerdtree
            
                # Plug 'moll/vim-bbye'
                (buildVimPlugin {
                  pname = "vim-bbye";
                  version = "2018-03-03";
                  src = fetchFromGitHub {
                    owner = "moll";
                    repo = "vim-bbye";
                    rev = "25ef93ac5a87526111f43e5110675032dbcacf56";
                    sha256 = "0dlifpbd05fcgndpkgb31ww8p90pwdbizmgkkq00qkmvzm1ik4y4";
                  };
                })

                # Plug 'junegunn/fzf'
                fzfWrapper
                # Plug 'junegunn/fzf.vim'
                fzf-vim

                # Plug 'vim-scripts/bclear'
                (buildVimPlugin {
                  pname = "bclear";
                  version = "2010-10-19";
                  src = fetchFromGitHub {
                    owner = "vim-scripts";
                    repo = "bclear";
                    rev = "c2cd612430f332f4f939ea3f025c0775f21e2931";
                    sha256 = "0h7wp641yx0cfqwqa6njy3nrqhd6nxp9pqa8ykv9c629izy8l55g";
                  };
                })

                # Plug 'mhinz/vim-signify'
                vim-signify

                # Plug 'neomake/neomake'
                neomake

                # Dumps the vim visual state as HTML.
                # https://github.com/vim-scripts/ScreenShot
                (buildVimPlugin {
                  pname = "ScreenShot";
                  version = "2010-10-18";
                  src = fetchFromGitHub {
                    owner = "vim-scripts";
                    repo = "ScreenShot";
                    rev = "d29fd83ab448ff4c6033ab88d823a6581bf2d5d4";
                    sha256 = "1633j5wm8nn9knlr42cmh67axvgpq4d7ln5dg554xf83axrqgh6j";
                  };
                })

                # Plug 'LnL7/vim-nix'
                vim-nix

                # Plug 'JulesWang/css.vim', {'for':['css', 'scss', 'sass', 'less']}
                (buildVimPlugin {
                  pname = "css-vim";
                  version = "2018-11-22";
                  src = fetchFromGitHub {
                    owner = "JulesWang";
                    repo = "css.vim";
                    rev = "a167ad37b13d14125e569dc18866ca1beaf83a73";
                    sha256 = "1irpb0vdsrzwmsbkmfj8k9kv24n9cgzmvmvnxrjdf7ihbmfavx73";
                  };
                })

                # Plug 'ap/vim-css-color',  {'for':['css', 'scss', 'sass', 'less', 'styl']}
                vim-css-color

                # Plug 'groenewege/vim-less', {'for': ['less']}
                (buildVimPlugin {
                  pname = "vim-less";
                  version = "2016-04-25";
                  src = fetchFromGitHub {
                    owner = "groenewege";
                    repo = "vim-less";
                    rev = "6e818d5614d5fc18d95a48c92b89e6db39f9e3d6";
                    sha256 = "0rhqcdry8ycnfbg534q4b3hm78an7mnqhiazxik7k08a57dk9dbm";
                  };
                })

                # Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx'] }
                vim-javascript
                
                # Plug 'mxw/vim-jsx', {'for': ['javascript', 'javascript.jsx']}
                (buildVimPlugin {
                  pname = "vim-jsx";
                  version = "2018-08-07";
                  src = fetchFromGitHub {
                    owner = "mxw";
                    repo = "vim-jsx";
                    rev = "ffc0bfd9da15d0fce02d117b843f718160f7ad27";
                    sha256 = "0ff4w5n0cvh25mkhiq0ppn0w0lzc6sds1zwvd5ljf0cljlkm3bbg";
                  };
                })
                
                # Plug 'othree/javascript-libraries-syntax.vim', {'for': ['javascript', 'javascript.jsx']}
                (buildVimPlugin {
                  pname = "javascript-libraries-syntax-vim";
                  version = "2018-07-02";
                  src = fetchFromGitHub {
                    owner = "othree";
                    repo = "javascript-libraries-syntax.vim";
                    rev = "5ef435d8c28ebc3c9b52fb865f4c06db629857f7";
                    sha256 = "1sxdxgisrg6i46spl8gcrrcjfdkykq2bx3rgffg9l7nyjrnchw51";
                  };
                })
                
                # Plug 'peitalin/vim-jsx-typescript', {'for' : ['typescript', 'typescript.tsx']}
                (buildVimPlugin {
                  pname = "vim-jsx-typescript";
                  version = "2019-03-28";
                  src = fetchFromGitHub {
                    owner = "peitalin";
                    repo = "vim-jsx-typescript";
                    rev = "9abb310f2b71be869f936c0ed715ae98fc7d703a";
                    sha256 = "0fr6zxm3qf3c7b6xx32p890f1gz2i922jz6cfb2cwxb8j5kpby1w";
                  };
                })
                
                # Plug 'leafgarland/typescript-vim', {'for' : ['typescript', 'typescript.tsx']}
                typescript-vim
                
                # Plug 'plasticboy/vim-markdown', {'for': ['markdown']}
                vim-markdown
                
                # Plug 'vim-ruby/vim-ruby', {'for': ['ruby', 'eruby']}
                vim-ruby

                # https://github.com/bakpakin/fennel.vim
                fennel-vim

                # https://github.com/bellinitte/uxntal.vim
                (buildVimPlugin {
                  pname = "uxntal.vim";
                  version = "2023-12-23";
                  src = fetchFromGitHub {
                    owner = "bellinitte";
                    repo = "uxntal.vim";
                    rev = "2ba479268252510cc7741c0cf67051feb0e82203";
                    hash = "sha256-g1VpY0CSRL5GxfJedOVTbCFV6nTTVtzgX9LD47YsqOQ="; 
                  };
                })

                # Vala                                                         
                (buildVimPlugin {
                  pname = "vala.vim";
                  version = "2020-05-02";
                  src = fetchFromGitHub {
                    owner = "vala-lang";
                    repo = "vala.vim";
                    rev = "ce569e187bf8f9b506692ef08c10b584595f8e2d";
                    sha256 = "sha256-1D9xITQiRdVwcUSol+zVpi4g0tQ3aal4lsUQ1TfAapA="; 
                  };
                })
              ];
            };
          };
        };
      })
    ];
  };
}
