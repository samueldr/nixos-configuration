`samueldr-profile`
==================

"Profile" files packaged as NixOS configuration for the usual
programs you'd find in dotfiles.
