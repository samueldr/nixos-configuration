{ config, lib, pkgs, ... }:

let
  inherit (lib)
    concatStringsSep
    mkIf
  ;
  cfg = config.samueldr.profile;

  wrap = name: text: ''
    # ------------------------------------------------------------------------------
    # Start of ${name}
    # ------------------------------------------------------------------------------
  '' + text + ''
    # ------------------------------------------------------------------------------
    # End of ${name}
    # ------------------------------------------------------------------------------
  '';
in
{
  config = mkIf cfg.enable {
    environment.systemPackages = [
      pkgs.fzf
    ];

    environment.etc."inputrc".text = concatStringsSep "\n" [
      (wrap "${pkgs.path}/nixos/modules/programs/bash/inputrc" (builtins.readFile "${pkgs.path}/nixos/modules/programs/bash/inputrc"))
      (wrap "samueldr-profile/bash/inputrc" (builtins.readFile ./inputrc))
    ];
    environment.etc."bashrc".text = wrap "samueldr-profile/bash/bashrc" (builtins.readFile ./bashrc);
    environment.etc."bash_logout".text = wrap "samueldr-profile/bash/bash_logout" (builtins.readFile ./bash_logout);
  };
}
