from ranger.api.commands import *

class trash(Command):
    """:trash

    Adds file to the trash bin.
    """

    def execute(self):
        action = ['trash']
        action.extend(f.path for f in self.fm.thistab.get_selection())
        self.fm.execute_command(action)
