{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkIf
  ;
  cfg = config.samueldr.profile;
in
{
  config = mkIf cfg.enable {
    environment.etc."ranger/rc.conf" = {
      source = ./rc.conf;
    };
    environment.etc."ranger/rifle.conf" = {
      source = ./rifle.conf;
    };
    environment.etc."ranger/commands.py" = {
      source = ./commands.py;
    };

    environment.systemPackages = [
      pkgs.ranger
    ];

    nixpkgs.overlays = [
      (final: super: {
        ranger = final.symlinkJoin {
          name = "ranger";
          paths =
            let
              ranger = super.ranger.overrideAttrs({ patches ? [], ... }: {
                patches = patches ++ [
                  # This hack is NOT stand-alone and complete.
                  # It requires /etc/ranger/rifle.conf to exist.
                  ./0001-HACK-forces-a-global-system-rifle.conf.patch
                ];
              });
            in
            [
              # Replace rifle with a wrapper that knows about our config.
              (final.writeShellScriptBin "rifle" ''
                exec ${ranger}/bin/rifle -c "/etc/ranger/rifle.conf" "$@"
              '')
              ranger
            ]
          ;
        };
      })
    ];
  };
}
