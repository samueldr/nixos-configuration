{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkIf
  ;
  cfg = config.samueldr.profile;

  maybeSwitch = pkgs.writeShellScript "tmux-maybe-switch" ''
    set -e
    set -u

    if [[ "$1" =~ ^=.+:[0-9]+ ]]; then
      exec tmux link-window -s "$1"
    else
      exec tmux display-message "A session was selected; please pick a window."
    fi
  '';
in
{
  config = mkIf cfg.enable {
    environment.systemPackages = [
      pkgs.tmux
    ];

    # Merges defaults with our customizations.
    environment.etc."tmux.conf".text = ''
      # Custom tmux configuration
      # =========================

      # Keep the daemon open when last session exits.
      # This configuration assumes a systemd user service is used.
      set-option -g exit-empty no

      # Look and feel
      # -------------

      # ### Colours
      # Colours follow default fg/bg of term, inverted for selected window.
      set-option -g status-style bg=default
      set-option -g status-style fg=default

      # Boldened
      set-window-option -g window-status-current-style reverse
      set-window-option -g window-status-style default

      # ### Statusbar
      set-option -g status-left ""
      set-option -g status-left-length 25
      set-option -g status-right "@#H %H:%M"
      set-option -g status-justify left
      # Refresh the status at 1s interval for a more dynamic clock.
      set-option -g status-interval 10
      set-option -g window-status-current-format " #I #W "
      set-option -g window-status-format         " #I#{?window_flags,#F, }#W "

      # Behaviour
      # ---------

      # Setting the title
      set-option -g set-titles on
      set-option -g set-titles-string "#I:#T"

      # Bell
      set-option -g bell-action any
      set-option -g visual-bell off

      # Sets tmux's reported TERM to screen-256color
      # This gets us some vim color goodness.
      set -g default-terminal "screen-256color"

      # ### Additional bindings

      # [^b j] Splits, but instead of creating a new pane, joins another pane.
      bind-key j command-prompt -p "join pane from:"  "join-pane -s '%%'"

      # [^/M + PageUp] to switch to previous tab
      bind-key -n C-PPage previous-window
      bind-key -n M-PPage previous-window

      # [^/M + PageDown] to switch to next tab
      bind-key -n C-NPage next-window
      bind-key -n M-NPage next-window

      # Open new windows and splits with the current pane's path.
      bind-key '"'  split-window -c "#{pane_current_path}"
      bind-key % split-window -h -c "#{pane_current_path}"
      bind-key c      new-window -c "#{pane_current_path}"
      bind-key -n C-t new-window -c "#{pane_current_path}"

      # Linking windows through a menu
      bind-key l choose-tree "run-shell \"${maybeSwitch} '%%'\""
      bind-key u unlink-window

      # For neovim.
      # See https://github.com/neovim/neovim/wiki/FAQ#esc-in-tmux-or-gnu-screen-is-delayed
      set -g escape-time 10

      run-shell ${pkgs.tmuxPlugins.sensible}/share/tmux-plugins/sensible/sensible.tmux
      run-shell ${pkgs.tmuxPlugins.sessionist}/share/tmux-plugins/sessionist/sessionist.tmux
    '';
  };
}
