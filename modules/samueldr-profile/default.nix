{ lib, ... }:

let
  inherit (lib)
    mkOption
    types
  ;
in
{
  imports = [
    ./bash
    ./git
    ./neovim
    ./profile
    ./ranger
    ./tio
    ./tmux
  ];

  options = {
    samueldr.profile = {
      enable = mkOption {
        default = false;
        type = types.bool;
        description = ''
          Enable use of samueldr's configs.
        '';
      };
    };
  };
}
