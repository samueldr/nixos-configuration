{ stdenv, lib, fetchFromGitHub
, pkg-config, autoconf, automake, intltool, gettext
, gtk3, vte, pcre2
}:

stdenv.mkDerivation rec {
  pname = "samueldr-terminal";
  version = "2022-11-02";

  src = fetchFromGitHub {
    owner = "samueldr";
    repo = "terminal";
    rev = "c38313c930f8af77a624a5033ec1852b968f50eb";
    sha256 = "sha256-DP9J0HqBq0FMikjBISae7PAlSYS1rxocl3PVQqFWzZs=";
  };

  nativeBuildInputs = [ pkg-config autoconf automake intltool gettext ];
  buildInputs = [ gtk3 vte pcre2 ];

  preConfigure = "sh autogen.sh";

  configureFlags = [
    "--enable-nls"
    "--enable-safe-mode"
  ];

  meta = with lib; {
    description = "A fork of lilyterm";
    license = licenses.gpl3;
    maintainers = with maintainers; [ samueldr ];
    platforms = platforms.linux;
  };
}
