{ pkgs, lib, config, ... }:

let
  inherit (lib)
    mkIf
    mkOption
    mkEnableOption
    types
  ;
  cfg = config.samueldr.programs.screenshot;
  screenshot_config = {
    inherit
      (cfg)
      rsync_destination
      rsync_url_host
      environment
    ;
  };
in
{
  config = mkIf cfg.enable {
    environment.systemPackages = [
      (pkgs.callPackage ./package { inherit (cfg) environment; })
    ];
    environment.etc."xdg/samueldr/screenshot.conf".text = builtins.toJSON screenshot_config;
  };

  options.samueldr.programs.screenshot = {
    enable = mkEnableOption "samueldr's screenshot utility";
    rsync_url_host = mkOption {
      type = types.str;
      example = "https://my.server/screenshots";
      description = "URL prefix to a location where the screenshot will be made available. Will be copied to the clipboard.";
    };
    rsync_destination = mkOption {
      type = types.str;
      example = "my.server:some/path/www/screenshots";
      description = "Rsync-compatible prefix to a location where the screenshot will be sync'd.";
    };
    environment = mkOption {
      type = types.enum [ "X11" "Plasma" "Wayland" ];
      description = "Environment under which the script is running.";
    };
  };
}
