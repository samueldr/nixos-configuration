{ stdenv
, lib
, ruby
, libnotify
, rsync

, environment

, maim
, xclip

, grim
, slurp
, wl-clipboard
, spectacle
}:

let
  inherit (lib)
    optionalString
  ;
in
stdenv.mkDerivation {
  pname = "screenshot";
  version = "0";

  buildInputs = [
    ruby
  ];

  src = ./.;

  patchPhase = ''
    substituteInPlace screenshot \
      ${optionalString (environment == "X11") ''
      --replace 'MAIM="maim"' 'MAIM="${maim}/bin/maim"' \
      --replace 'XCLIP="xclip"' 'XCLIP="${xclip}/bin/xclip"' \
      ''} \
      ${optionalString (environment == "Plasma") ''
      --replace 'SPECTACLE="spectacle"' 'SPECTACLE="${spectacle}/bin/spectacle"' \
      --replace 'WL_COPY="wl-copy"' 'WL_COPY="${wl-clipboard}/bin/wl-copy"' \
      ''} \
      ${optionalString (environment == "Wayland") ''
      --replace 'GRIM="grim"' 'GRIM="${grim}/bin/grim"' \
      --replace 'SLURP="slurp"' 'SLURP="${slurp}/bin/slurp"' \
      --replace 'WL_COPY="wl-copy"' 'WL_COPY="${wl-clipboard}/bin/wl-copy"' \
      ''} \
      --replace 'NOTIFY_SEND="notify-send"' 'NOTIFY_SEND="${libnotify}/bin/notify-send"' \
      --replace 'RSYNC="rsync"' 'RSYNC="${rsync}/bin/rsync"'
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp screenshot $out/bin
  '';
}
