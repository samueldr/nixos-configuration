{ lib, fetchpatch, fetchurl }:

kernel:

let
  atLeast = lib.versionAtLeast kernel.version;
in

# Guard clause to make diffs easier to parse. bleh
if atLeast "999.99" then throw "This should not happen..."

else if atLeast "6.12" then
  {
    name = "gobohide-6.12";
    # fetchpatch breaks multiple patches with new files
    patch = fetchurl {
      url = "https://github.com/samueldr/linux/compare/v6.12...8976e900683555ed33e49ec65a6df4900a768a29.patch";
      hash = "sha256-22XMbeDdzSnD2bniSd4+sW5MhWr4tJfeVHZOp/OV3qU=";
    };
  }
else if atLeast "6.8" then
  {
    name = "gobohide-6.8";
    # fetchpatch breaks multiple patches with new files
    patch = fetchurl {
      url = "https://github.com/samueldr/linux/compare/v6.8...ca911c8884fbd79f1690d7e1ca55c5b0daac10f0.patch";
      hash = "sha256-vmv5hd/apOUTXjmfmTgBd3IaCHZUc2pDcAdoFDcjDnc=";
    };
  }
else if atLeast "6.5" then
  {
    name = "gobohide-6.5";
    patch = fetchpatch {
      url = "https://github.com/samueldr/linux/commit/52bca4669513eecae7cecf48ebd9e17313b5da51.patch";
      hash = "sha256-bIS7xdvQozYdRCR0RvKm/ZsNal7cQ3Fygss82kD8QbA=";
    };
  }
else if atLeast "6.4" then
  {
    name = "gobohide-6.4";
    patch = fetchpatch {
      url = "https://github.com/samueldr/linux/commit/f4e6668ee85ce3181ab6aa8d3423570e29ec691e.patch";
      hash = "sha256-4rWg2sqxMaSVbc1kZs3utkiI13QkE+dum+W4wyjQVww=";
    };
  }
/* NOTE: after a couple 6.3 patches it stopped applying. This is not an LTS so I'm not backporting fixes. */
else if atLeast "6.3" then
  {
    name = "gobohide-6.3";
    patch = fetchpatch  {
      url = "https://github.com/samueldr/linux/commit/c132e33ccaf75187396dcd32a5e12880f763e5f9.patch";
      sha256 = "sha256-gavudWSuQr1QnYuzCRR1Ez1MeEE9gzFgGrfi+mJWn98=";
    };
  }
else if atLeast "6.1" then
  {
    name = "gobohide-6.1";
    patch = fetchpatch  {
      url = "https://github.com/samueldr/linux/commit/38822dca5a52bc3f68f3d31a6c6227dcba3a172c.patch";
      sha256 = "sha256-Sx/KilxTGO68R4tK9shevhcZoDQofTCr3xul6lGnMyk=";
    };
  }
else if atLeast "5.16.5" then
  {
    name = "gobohide-5.16.16";
    patch = fetchpatch  {
      url = "https://github.com/samueldr/linux/commit/5b448f147ff525ec415fbaab472671088fbc609e.patch";
      sha256 = "sha256-3Squ4TaRfJOOrHcETxq1MLOf9F0qFIJPXf7T1U8+nkE=";
    };
  }
else if atLeast "5.16" then
  {
    name = "gobohide-5.16";
    patch = fetchpatch  {
      url = "https://github.com/samueldr/linux/commit/9edd0e21b0572f2c8ec8c84496bc4f58d2be9ed2.patch";
      sha256 = "sha256-GTm3MkOV072pdfM04bQyiMGfDNoMjEZBXzBzzQ9lt9A=";
    };
  }
else if atLeast "5.12" then
  {
    name = "gobohide-5.12";
    patch = fetchpatch  {
      url = "https://github.com/samueldr/linux/commit/6f53b195ae84285ba8cc64e640a5f1d8a6691d09.patch";
      sha256 = "0sslfzjkbg3vayprkyxyccvdd55fj99m1m34l7b44k73j1akb87m";
    };
  }
else if atLeast "5.11" then
  {
    name = "gobohide-5.11";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/5.10.26/01-gobohide.patch";
      sha256 = "1vlpp8nxyawyrpx9xqw8vldf179x7jczygp9a6rn3zr0j0lqwa56";
      postFetch = ''
        substituteInPlace $out \
          --replace nla_strlcpy nla_strscpy
      '';
    };
  }
else if atLeast "5.10" then
  {
    name = "gobohide-5.10";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/5.10.26/01-gobohide.patch";
      sha256 = "1s1i0baj1nzl9ya2ppsasbv0psqlp8ix103vx0pambvqyxqjclam";
    };
  }
else if atLeast "5.5" then
  {
    name = "gobohide-5.5";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/5.5.6/01-gobohide.patch";
      sha256 = "0pm109cc23wxflzz9ksxdhdx2azx5a9acf22fi7vlb1hdxim25jw";
    };
  }
else if atLeast "5.4" then
  {
    name = "gobohide-5.4";
    # The actual 5.4 patch doesn't apply (anymore?).
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/5.5.6/01-gobohide.patch";
      sha256 = "0pm109cc23wxflzz9ksxdhdx2azx5a9acf22fi7vlb1hdxim25jw";
    };
  }
else if atLeast "4.13" then
  {
    name = "gobohide-4.13";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/4.13.2/01-gobohide.patch";
      sha256 = "16i8wylhx02bfql67bk1vgna4laj3vs8m3jh79yx5fsdp4d66gka";
    };
  }
else if atLeast "4.9" then
  {
    name = "gobohide-4.9";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/4.9.16-r3/01-gobohide.patch";
      sha256 = "1fzgxf0cnn0fivhqr24rf0h24s0hcy5xiahb7wm10zmw608i1nkz";
    };
  }
else if atLeast "4.7" then
  {
    name = "gobohide-4.7";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/4.7.0-r1/01-gobohide.patch";
      sha256 = "0f9gy82ic7b2z31vs50wly6rr7k1lh2xx73azz2r7khm6a8cabwr";
    };
  }
else if atLeast "3.13" then
  {
    name = "gobohide-3.13";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/3.13.3-r1/01-gobohide.patch";
      sha256 = "0jh69dvs31xybqrnapcp7pqq78dpx3xny9livl20zj8b17l7chvd";
    };
  }
else if atLeast "3.9" then
  {
    name = "gobohide-3.9";
    patch = fetchpatch  {
      url = "https://raw.githubusercontent.com/gobolinux/Recipes/master/Linux/3.9.4-r2/01-gobohide.patch";
      sha256 = "0zb13j3hckd0mkari77p2nwbynpb6qq9dfpr6pjqkvawwaqkci4g";
    };
  }
else throw "No gobohide patch for kernel version ${kernel.version}"
