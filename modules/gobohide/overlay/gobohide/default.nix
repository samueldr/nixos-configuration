{ stdenv
, lib
, fetchFromGitHub
, fetchpatch
, pkg-config
, autoreconfHook
, gettext
, libnl
}:

stdenv.mkDerivation rec {
  name = "GoboHide-${version}";
  version = "unstable-2024-12-29";
  
  src = fetchFromGitHub {
    owner = "gobolinux";
    repo = "GoboHide";
    rev = "375ba74b0ba707abb5def668bc32d59b24bc8a32";
    hash = "sha256-nGCdzdY9I2bUW1Esx4W91ce4cOPVeV2kJMQf0KmsCv0=";
  };

  patches = [
    # https://github.com/gobolinux/GoboHide/pull/8
    (fetchpatch {
      url = "https://github.com/samueldr/GoboHide/commit/0978acbff7f5829008fc8433c7fc8fdd52da3571.patch";
      hash = "sha256-bp+JPMmDpCm3DieSva9Ej0azzL5gqTS0OTdHUob7wkg=";
    })
  ];

  buildInputs = [
    gettext
    libnl
  ];

  nativeBuildInputs = [
    pkg-config
    autoreconfHook
  ];

  preConfigure = ''
    patchShebangs autogen.sh
    ./autogen.sh
  '';

  meta = with lib; {
    description = "GoboHide userspace client";
    homepage    = https://github.com/gobolinux/GoboHide;
    license     = licenses.gpl2Plus;
    #maintainers = with maintainers; [ samueldr ];
    platforms   = platforms.linux;
  };
}

