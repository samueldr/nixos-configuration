final: super:
{
  # Userland tools related to those extra stuff.
  gobohide = final.callPackage ./gobohide { };
  gobohidePatchFor = final.callPackage ./gobohide/patches.nix { };
}
