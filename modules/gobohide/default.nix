{config, lib, pkgs, ...}:

let
  inherit (lib)
    attrNames
    escapeShellArgs
    filterAttrs
    mkIf
    mkMerge
    mkOption
    mkOptionDefault
    types
  ;
  cfg = config.gobohide;

  hiddenPaths =
    attrNames (filterAttrs (k: v: v) cfg.hiddenPaths)
  ;
in
{
  options = {
    gobohide = {
      enable = mkOption {
        description = "Whether to enable gobohide.";
        default = false;
        type = types.bool;
      };
      hiddenPaths = mkOption {
        description = "Attrset of of paths to hide.";
        type = types.attrsOf types.bool;
      };
    };
  };

  config = mkMerge [
    {
      nixpkgs.overlays = [
        (import ./overlay)
      ];
    }
    (mkIf cfg.enable {
      gobohide.hiddenPaths =
        let
          # mkOptionDefault to make it maximally mergeable.
          yes = mkOptionDefault true;
        in
        {
          "/bin"   = yes;
          "/boot"  = yes;
          "/dev"   = yes;
          "/etc"   = yes;
          "/mnt"   = yes;
          "/proc"  = yes;
          "/root"  = yes;
          "/run"   = yes;
          "/srv"   = yes;
          "/sys"   = yes;
          "/tmp"   = yes;
          "/usr"   = yes;
          "/var"   = yes;
          "/lib"   = yes;
          "/lib64" = yes;

          "/lost+found" = yes;

          "/nix" = yes;
        }
      ;
      boot.kernelPatches = [
        (pkgs.gobohidePatchFor config.boot.kernelPackages.kernel)
      ];
      systemd.services.gobohide = {
        description = "Gobohide userland service";
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
          ExecStop = pkgs.writeShellScript "gobohide-stop" ''
            ${pkgs.gobohide}/bin/gobohide --flush
          '';
          ExecStart = pkgs.writeShellScript "gobohide-start" ''
            ${pkgs.gobohide}/bin/gobohide --flush
            for f in ${escapeShellArgs hiddenPaths}; do
              if test -e  "$f"; then
                ${pkgs.gobohide}/bin/gobohide --hide "$f"
              fi
            done
         '';
        };
      };
    })
  ];
}
