`nm-localbonding`
=================

This module provides a higher-level interface around the `networking.networkmanager.ensureProfiles.profiles` options.

The bond interfaces created serve to unify how a "roaming" computer's diverse networking interfaces are presented to a network.

The usual use-case is to have things like audio streaming, voice chat, or other network activity continue uninterrupted when moving from dock to dock, relying on Wi-Fi as a fallback.


Assumptions
-----------

This module builds on these assumptions:

 - One wifi interface
 - Varied external ethernet interfaces/docks
 - Interfaces or docks are not used outside of a single network

The latter assumption can be worked around by disconnecting and making a new connection with any NetworkManager interface.
