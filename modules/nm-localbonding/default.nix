{ config, lib, options, ... }:

let
  cfg = config.samueldr.nm-localbonding;
  inherit (lib)
    concatStrings
    fixedWidthNumber
    imap1
    mapAttrsToList
    mergeAttrsList
    mkOption
    recursiveUpdate
    types
  ;

  macAddressType =
    options.networking.networkmanager.ethernet.macAddress.type
  ;
  profilesAttrsType =
    options.networking.networkmanager.ensureProfiles.profiles.type
  ;
  profileType = profilesAttrsType.nestedTypes.elemType;

  bondSubmodule = types.submodule (
    { name, config, ... }:
    {
      options = {
        name = mkOption {
          internal = true;
          type = types.str;
          default = name;
        };

        series = mkOption {
          type = types.int;
          description = ''
            Required unique number, used for profile UUIDs.

            Must be between 0 and 999.
          '';
        };

        enable = mkOption {
          type = types.bool;
          default = false;
          description = ''
            Enable this bond.
          '';
        };

        macAddress = mkOption {
          type = macAddressType;
        };

        wired = mkOption {
          type = with types;
            listOf (submodule {
              options = {
                name = mkOption {
                  type = types.str;
                  description = "Dock identifier";
                };
                address = mkOption {
                  type = macAddressType;
                  description = "MAC address of the dock";
                };
              };
            })
          ;
          default = [];
          description = ''
            Ordered mapping of *dock name* → *mac address*.

            All of these will be added to the main preferred wired bond.
          '';
        };

        networkConfig = mkOption {
          type = profileType;
          default = {};
          description = ''
            Network configuration for the bonded interfaces.

            The MAC address provided here will be overriden.
          '';
        };

        wifiConfig = mkOption {
          type = profileType;
          default = {};
          description = ''
            Network configuration for the wireless interface.

            This should be used to provide `ssid` and security configuration, mainly.
          '';
        };

        uuidPrefix = mkOption {
          type = types.str;
          default = "5b61bfd7-badb-44c4-bb3b";
          internal = true;
          description = ''
            This implementation (rudely) presumes this whole UUID prefix is reserved for this scheme.

           ```
           ⇒ "5b61bfd7-badb-44c4-bb3b"
           ```
          '';
        };

        outputs = {
          # NetworkManager profiles list for this bond.
          profiles = mkOption {
            internal = true;
            type = profilesAttrsType;
          };
        };
      };
      config =
      let
        # Format:
        #    "001000000230" -> 23rd dock in series 1
        #    "SSS         " epoch
        #    "        NNN0" per-dock identifier
        #    "           1" main bond
        #    "           2" wired bond
        #    "           5" wifi connection
        mkUUID = { type ? 0, dock ? 0 }:
          concatStrings [
            config.uuidPrefix
            "-"
            (fixedWidthNumber 3 config.series)
            "00000" # Free real-estate
            (fixedWidthNumber 3 dock)
            (fixedWidthNumber 1 type)
          ]
        ;
      in
      {
        networkConfig = {
          connection = {
            id = "${name}";
            uuid = mkUUID { type = 1; };
            type = "bond";
            autoconnect-priority = "999";
            interface-name = "${name}";
          };
          ethernet = {
            cloned-mac-address = config.macAddress;
          };
          bond = {
            downdelay = "0";
            miimon = "1";
            mode = "active-backup";
            primary = "${name}-wired";
            updelay = "0";
          };
        };
        wifiConfig = {
          connection = {
            id = "${name}-99-wifi";
            uuid = mkUUID { type = 5; };
            type = "wifi";
            autoconnect-priority = "10";
            master = "${name}";
            slave-type = "bond";
          };
          wifi = {
            cloned-mac-address = config.macAddress;
            mode = "infrastructure";
          };
        };
        outputs.profiles = {
          "${name}" = config.networkConfig;
          "${name}-00-wired" = {
            connection = {
              id = "${name}-00-wired";
              uuid = mkUUID { type = 2; };
              type = "bond";
              autoconnect-priority = "50";
              interface-name = "${name}-wired";
              master = "${name}";
              slave-type = "bond";
            };
            ethernet = {
              cloned-mac-address = config.macAddress;
            };
            bond = {
              downdelay = "0";
              miimon = "1";
              # The mode probably doesn't matter too much since it's expected only one will be connected at once...
              # If there was `any`, that would be the one to pick.
              mode = "balance-rr";
              updelay = "0";
            };
          };
          "${name}-99-wifi" = config.wifiConfig;
        }
        // (mergeAttrsList (imap1 (i: dockInfo:
          {
            "${name}-wired-${dockInfo.name}" = {
              connection = {
                id = "${name}-wired-${dockInfo.name}";
                uuid = mkUUID { dock = i; };
                type = "ethernet";
                autoconnect-priority = "100";
                master = "${name}-wired";
                slave-type = "bond";
              };
              ethernet = {
                cloned-mac-address = config.macAddress;
                mac-address = dockInfo.address;
              };
            };
          }
        ) config.wired))
        ;
      };
    }
  );
in
{
  options = {
    samueldr.nm-localbonding = {
      bonds = mkOption {
        default = {};
        type = types.attrsOf bondSubmodule;
        description = ''
          Bond configurations.
        '';
      };
    };
  };

  config = {
    networking.networkmanager.ensureProfiles.profiles = lib.mkMerge
      (mapAttrsToList (_: sub:
        if sub.enable
        then sub.outputs.profiles
        else {}
      ) cfg.bonds)
    ;
  };
}
