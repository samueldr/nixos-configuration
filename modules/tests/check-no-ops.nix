#!/usr/bin/env nix-instantiate
# What's this? A shebang and +x in my Nix expression??
# ¯\_(ツ)_/¯
#
# Test that eval for unconfigured modules are no-ops.
#

let nixpkgs = (import ../../npins).nixpkgs; in
{ pkgs ? import nixpkgs {} }:

let
  baseCfg = {
    # Prevent stray warnings
    system.stateVersion = "00.00";
    # Ignore bootloader asserts
    boot.isContainer = true;
    # Manual builds will differ since new options are added. This is okay.
    documentation.nixos.enable = false;
  };
  compareEvals = a: b: a.config.system.build.toplevel == b.config.system.build.toplevel;
  evalConfig = cfg: (import (pkgs.path + "/nixos")) { configuration = { imports = [ cfg baseCfg ]; }; };
  evalModule = module: evalConfig { imports = [ module ]; };
  virginEval = evalConfig {};
  modules = (import ../.).imports;
in
map (modulePath:
  let
    moduleEval = evalConfig modulePath;
    toplevel = moduleEval.config.system.build.toplevel;
    vToplevel = virginEval.config.system.build.toplevel;
  in
  if !(compareEvals moduleEval virginEval)
  then
    builtins.trace ''
      Module '${toString modulePath}' is not a no-op.
             ${toString modulePath} != virginEval
             ${toplevel} != ${vToplevel}
    ''
    toplevel
  else toplevel
) modules
