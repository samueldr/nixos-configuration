{ config, lib, ... }:

let
  inherit (lib)
    attrNames
    mkIf
    mkMerge
    mkOption
    types
  ;
  presets = {
    "whiteOnBlack" = [
      "000000"
      "bd2523"
      "7eba4e"
      "c34a07"
      "183570"
      "8307c3"
      "3b8dc5"
      "d2d2d2"
      "5c80a3"
      "f52f2d"
      "6ad541"
      "ffab0d"
      "5277c3"
      "da93ff"
      "7ebae4"
      "ffffff"
    ];
    "nixos" = [
      "091d33"
      "bd2523"
      "7eba4e"
      "c34a07"
      "183570"
      "8307c3"
      "3b8dc5"
      "d2d2d2"
      "5c80a3"
      "f52f2d"
      "6ad541"
      "ffab0d"
      "5277c3"
      "da93ff"
      "7ebae4"
      "ffffff"
    ];
    blackOnWhite = [
      "ffffff"
      "bc5558"
      "4fba5f"
      "b4ba58"
      "564dc5"
      "bc4dbd"
      "4fb3c5"
      "000000"
      "a1a0a8"
      "cf8fbb"
      "84b3ab"
      "caca97"
      "8d88bd"
      "b493c7"
      "84a4c5"
      "686868"
    ];
  };

  # See <nixos/modules/config/console.nix>
  cfg = config.console;
  inherit (lib) concatMapStringsSep substring;
  makeColor = i: concatMapStringsSep "," (x: "0x" + substring (2*i) 2 x);
in
{
  options = {
    console = {
      colorPreset = mkOption {
        default = null;
        type = types.nullOr (types.enum (attrNames presets));
        description = ''
          Framebuffer console color preset to choose from.
        '';
      };
    };
  };
  config = {
    console.colors = mkIf (config.console.colorPreset != null) presets."${config.console.colorPreset}";
    system.activationScripts.console-colors = (mkIf (cfg.colors != []) {
      supportsDryActivation = true;
      # console_codes(4)
      # ESC ] R: reset palette
      text = ''
        printf "${makeColor 0 cfg.colors}" > /sys/module/vt/parameters/default_red
        printf "${makeColor 1 cfg.colors}" > /sys/module/vt/parameters/default_grn
        printf "${makeColor 2 cfg.colors}" > /sys/module/vt/parameters/default_blu
        printf '\033]R'
      '';
    });
  };
}
