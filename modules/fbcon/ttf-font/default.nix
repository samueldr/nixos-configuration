{ config, pkgs, lib, ... }:

let
  cfg = config.console.ttf-font;
  inherit (lib)
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options = {
    console.ttf-font = {
      fontfile = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = ''
          Path to ttf font to be used as the console font.
        '';
        example = ''
         ''${pkgs.source-code-pro}/share/fonts/opentype/SourceCodePro-Regular.otf
        '';
      };

      dpi = mkOption {
        type = types.int;
        default = 192;
        description = ''
          DPI to render console font at.
        '';
      };

      ptSize = mkOption {
        type = types.int;
        default = 9;
        description = ''
          Font size in points of console font.
        '';
      };
    };
  };

  config = mkMerge [
    {
      nixpkgs.overlays = [ (import ./overlay) ];
    }
    (mkIf (cfg.fontfile != null) {
      console.earlySetup = true;
      console.font = pkgs.ttf-console-font { inherit (cfg) fontfile dpi ptSize; };
      system.activationScripts.console-ttf-font = {
        supportsDryActivation = true;
        text = ''
          for console in /dev/tty[0-9]*; do
            ${pkgs.kbd}/bin/setfont -C "$console" "${config.console.font}"
          done
        '';
      };
    })
  ];
}
