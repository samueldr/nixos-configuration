# This module can be imported to erase any trace of zfs support.
# Useful for e.g. working around installer config issues.
{
  nixpkgs.overlays = [
    (final: super: {
      zfs = super.zfs // {
        meta.platforms = [];
      };
    })
  ];
}
