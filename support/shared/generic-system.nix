{ lib, pkgs, ... }:

let
  inherit (lib)
    mkEnableOption
  ;
in
{
  options = {
    samueldr.hardware.definitions.cpu.Generic = {
      enable = mkEnableOption "...";
    };
    samueldr.hardware.definitions.gpu.Generic = {
      enable = mkEnableOption "...";
    };
    samueldr.hardware.definitions.system."Generic system" = {
      enable = mkEnableOption "...";
    };
  };
  config = {
    samueldr = {
      hardware = {
        system = "Generic system";
        cpu = {
          type = "Generic";
          generation = "Generic";
          # Good enough value for the generic system use-casesé
          cores = 4;
        };
        gpu = {
          type = "Generic";
          generation = "Generic";
        };
        memory = lib.mkDefault (2 * 1024);
        storage.size = 32 * 1024;
      };
      boot.type = "efi";
    };
  };
}
