Nixpkgs shim for NixOS × npins
==============================

This directory is passed on the `NIX_PATH` by a `nixos-rebuild` wrapper.

It makes it possible to use stock `nixos-rebuild` with the actual current pin.

> [!NOTE]
> Without this workaround, a rebuild will *lag behind* with regard to the pinned Nixpkgs. 
> The `NIX_PATH`'s `<nixpkgs>` from this configuration refers to the pinned Nixpkgs *at the time the generation was built*.
