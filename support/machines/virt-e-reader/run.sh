#!/usr/bin/env bash

here="${BASH_SOURCE[0]%/*}"
top="$here/../../.."
vm="virt-e-reader"

set -e
set -o pipefail
PS4=" $ "
set -x

rm "$vm"-efi-vars.fd || :
rm "$vm".qcow2 || :

export NIX_PATH="$top/channels" 

result=$(nix-build '<nixpkgs/nixos>' --arg configuration "{ imports = [ $here $top/configuration ]; }" -A vm --no-out-link)
"${result}/bin/run-${vm}-vm"
