{ lib, pkgs, options, ... }:

let
  inherit (lib)
    mkAfter
    mkDefault
  ;
  # Test tablet (asus-dumo), conveniently the same aspect ratio as the PineNote
  #xres = "1536"; yres = "2048"; # (Doesn't work in X11)
  # PineNote resolution
  #xres = "1404"; yres = "1872"; # (Doesn't work in X11)

  # Same aspect ratio as the PineNote
  # But actually works in X11 with QEMU.
  #xres = "544"; yres = "720";  # Cramped
  #xres = "768"; yres = "1024"; # Doesn't fit vertically on 1080p with taskbar and titlebar
  xres = "704"; yres = "936";   # Goldilock's resolution, also about half in every dimensions
in
{
  imports = [
    ../../shared/generic-system.nix
    {
      config =
        if (options.virtualisation ? qemu)
        then {
          virtualisation.qemu.options = mkAfter [
            # Using virtio-gpu doesn't work with custom resolutions :(
            #"-device" "virtio-gpu,edid=on,xres=${xres},yres=${yres}"
            # Using VGA doesn't satisfy wlroots requirements
            #"-device" "VGA,vgamem_mb=128,edid=on,xres=${xres},yres=${yres}"
            "-device" "qxl-vga,xres=${xres},yres=${yres}"
          ];
        }
        else {}
      ;
    }
  ];

  networking.hostName = "virt-e-reader";

  # Only enable the minimum set of options that would be automatically for an eInk reader.
  samueldr = {
    use-case = {
      system-type.e-reader.enable = mkDefault true;
    };
    convergent-session.theme = {
      font = {
        DPI = 192 / 2; # 192 used on the PineNote
      };
    };
  };

  # Documentation is expensive to build on every tiny change.
  # Skipping its build helps re-using system paths.
  documentation.nixos.enable = false;

  system.stateVersion = "21.05";

  # Nudge X11 in selecting our custom resolution.
  services.xserver.screenSection = ''
    SubSection "Display"
      Depth     24
      Modes     "${xres}x${yres}"
    EndSubSection
  '';
}
