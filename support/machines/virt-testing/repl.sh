#!/usr/bin/env bash

here="${BASH_SOURCE[0]%/*}"
top="$here/../../.."
vm="virt-testing"

set -e
set -o pipefail
PS4=" $ "
set -x

unset NIX_PATH
nix repl "$top" --arg configuration "{ imports = [ $here $top/configuration ]; }"
