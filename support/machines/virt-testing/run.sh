#!/usr/bin/env bash

here="${BASH_SOURCE[0]%/*}"
top="$here/../../.."
vm="virt-testing"

set -e
set -o pipefail
PS4=" $ "
set -x

rm "$vm"-efi-vars.fd || :
rm "$vm".qcow2 || :

unset NIX_PATH
system_build=(
	env -i

	nix-build "$top"
	--no-out-link
	--arg configuration "{ imports = [ $here $top/configuration ]; }"
	-A vm
)
result=$("${system_build[@]}")
"${result}/bin/run-${vm}-vm"
