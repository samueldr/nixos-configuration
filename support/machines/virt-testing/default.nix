{ config, lib, pkgs, options, ... }:

{
  imports = [
    ../../shared/generic-system.nix
    {
      config =
        if (options.virtualisation ? qemu)
        then {
          virtualisation.useBootLoader = true;
          virtualisation.useEFIBoot = true;
        }
        else {}
      ;
    }
  ];

  networking.hostName = "virt-testing";

  samueldr = {
    boot.type = "efi";
    use-case.silent-boot.enable = true;
    use-case.graphical-boot.enable = true;
    # Only used without flickerFree, without hackBGRT
    use-case.graphical-boot.resolution = {
      width = 800;
      height = 600;
    };
    /*
    use-case.graphical-boot.flickerFree.enable = false; # true;
    use-case.graphical-boot.hackBGRT.enable = false; # true;
    # Ensure the VM will boot if I disable hackBGRT
    use-case.refind.enable = false; #!config.samueldr.use-case.graphical-boot.hackBGRT.enable;
    */
    use-case.graphical-boot.flickerFree.enable = true;
    use-case.graphical-boot.hackBGRT.enable = true;

    # Ensure the VM will boot if I disable hackBGRT
    use-case.refind.enable = !config.samueldr.use-case.graphical-boot.hackBGRT.enable;

    # Required for proper and complete flickerFree.
    use-case.stage-1.chosen = "systemd";
  };

  # Ensure the VM will boot when both rEFInd and hackBGRT is disabled.
  boot.loader.grub.efiInstallAsRemovable =
    !config.samueldr.use-case.graphical-boot.hackBGRT.enable
    && !config.samueldr.use-case.refind.enable
  ;

  # Somehow grub-install is broken for the VM with bootloader...
  boot.loader.efi.canTouchEfiVariables = lib.mkForce false;

  # It ends-up being forced to `/dev/vdb`, which breaks on AArch64.
  boot.loader.grub.device = lib.mkOverride 1 "nodev";

  # Documentation is expensive to build on every tiny change.
  # Skipping its build helps re-using system paths.
  documentation.nixos.enable = false;

  environment.systemPackages = with pkgs; [
    efibootmgr
  ];

  system.stateVersion = "21.05";
}
