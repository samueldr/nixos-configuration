{ config, lib, options, pkgs, ... }:

let
  cfg = config.samueldr.use-case.mobile-nixos-stage-1;
  inherit (lib)
    mkIf
    mkOption
    mkMerge
    optionals
    types
  ;
in
{
  imports = [
    {
      # When the module is present, configure it as default.
      samueldr.use-case.stage-1.availableStage-1 = [ "mobile-nixos" ];
      samueldr.use-case.stage-1.chosen = lib.mkDefault "mobile-nixos";
    }
  ];
  options.samueldr.use-case.mobile-nixos-stage-1 = {
    enable = mkOption {
      default = config.samueldr.use-case.stage-1.chosen == "mobile-nixos";
      type = types.bool;
      internal = true;
      readOnly = true;
      description = ''
        Enable Mobile NixOS Stage-1 usage in a "normal" NixOS system.
      '';
    };
  };
  config = mkIf cfg.enable (mkMerge [
    (
      # Ensure evaluation works even when Mobile NixOS is not enabled, if this module is not enabled.
      if options ? mobile
      then {
        mobile = {
          enable = lib.mkDefault false;
          boot.stage-1.enable = true;
          # This module is for using on "normal" "non-mobile" devices.
          # In turn, we're likely to want to use USB connected devices.
          # This won't add the delay to other Mobile NixOS devices (those using `samueldr.use-case.mobile-nixos`).
          boot.stage-1.gui.waitForDevices.enable = lib.mkDefault true;
          beautification = {
            silentBoot = true;
            splash = true;
          };
        };

        # <nixpkgs/nixos/modules/system/boot/kernel.nix> ends-up not adding default modules.
        boot.initrd.availableKernelModules = [
            # Note: most of these (especially the SATA/PATA modules)
            # shouldn't be included by default since nixos-generate-config
            # detects them, but I'm keeping them for now for backwards
            # compatibility.

            # Some SATA/PATA stuff.
            "ahci"
            "sata_nv"
            "sata_via"
            "sata_sis"
            "sata_uli"
            "ata_piix"
            "pata_marvell"

            # Standard SCSI stuff.
            "sd_mod"
            "sr_mod"

            # SD cards and internal eMMC drives.
            "mmc_block"

            # Support USB keyboards, in case the boot fails and we only have
            # a USB keyboard, or for LUKS passphrase prompt.
            "uhci_hcd"
            "ehci_hcd"
            "ehci_pci"
            "ohci_hcd"
            "ohci_pci"
            "xhci_hcd"
            "xhci_pci"
            "usbhid"
            "hid_generic" "hid_lenovo" "hid_apple" "hid_roccat"
            "hid_logitech_hidpp" "hid_logitech_dj" "hid_microsoft" "hid_cherry"
          ] ++ optionals pkgs.stdenv.hostPlatform.isx86 [
            # Misc. x86 keyboard stuff.
            "pcips2" "atkbd" "i8042"

            # x86 RTC needed by the stage 2 init script.
            "rtc_cmos"
        ];
      }
      else {}
    )
    # Ensure we fail the build if this is enabled and Jovian NixOS is not available.
    {
      assertions = [
        {
          assertion = options ? mobile;
          message = ''
            Mobile NixOS Stage-1 configuration is enabled, but Mobile NixOS could not be imported.
              Make sure it is present in channels.
          '';
        }
      ];
    }
  ]);
}
