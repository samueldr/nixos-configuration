{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.emulation;
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.emulation = {
    enable = mkEnableOption "configs for emulation";
    arcade = mkEnableOption "use of selected Arcade emulator";
    gamecube = mkEnableOption "use of selected Gamecube emulator";
    gba = mkEnableOption "use of selected Nintendo Gameboy Advance emulator";
    n64 = mkEnableOption "use of selected Nintendo 64 emulator";
    ps1 = mkEnableOption "use of selected Playstation 1 emulator";
    ps2 = mkEnableOption "use of selected Playstation 2 emulator";
    psp = mkEnableOption "use of selected PSP emulator";

    useRetroarch = mkOption {
      type = types.bool;
      internal = true;
      default = false;
    };
    retroarchCores = mkOption {
      type = with types; listOf package;
      internal = true;
      default = [];
    };
  };

  # NOTE: we're not using app-compartmentalization so that the
  #       emulators launch correctly as expected under steam.

  config = mkMerge [
    (mkIf cfg.enable {
      samueldr.use-case.emulation.gamecube = mkDefault true;
      samueldr.use-case.emulation.arcade = mkDefault true;
      samueldr.use-case.emulation.gba = mkDefault true;
      samueldr.use-case.emulation.n64 = mkDefault true;
      samueldr.use-case.emulation.ps1 = mkDefault true;
      samueldr.use-case.emulation.ps2 = mkDefault true;
      samueldr.use-case.emulation.psp = mkDefault true;
    })
    (mkIf cfg.useRetroarch {
      environment.systemPackages = [
        (pkgs.retroarch.withCores (cores: cfg.retroarchCores))
      ];
    })
    (mkIf cfg.arcade {
      samueldr.use-case.emulation.useRetroarch = mkDefault true;
      samueldr.use-case.emulation.retroarchCores = [ pkgs.libretro.fbneo ];
    })
    (mkIf cfg.gamecube {
      environment.systemPackages = [
        pkgs.dolphin-emu-beta
      ];
      samueldr.use-case.emulation.useRetroarch = mkDefault true;
      samueldr.use-case.emulation.retroarchCores = [ pkgs.libretro.dolphin ];
    })
    (mkIf cfg.gba {
      samueldr.use-case.emulation.useRetroarch = mkDefault true;
      samueldr.use-case.emulation.retroarchCores = [ pkgs.libretro.mgba ];
    })
    (mkIf cfg.n64 {
      samueldr.use-case.emulation.useRetroarch = mkDefault true;
      samueldr.use-case.emulation.retroarchCores = [
        # Not reported yet AFAICT.
        # Regression from GCC14 update
        (pkgs.libretro.overrideScope (self: super: { stdenv = pkgs.gcc13Stdenv; })).mupen64plus
      ];
    })
    (mkIf cfg.ps1 {
      samueldr.use-case.emulation.useRetroarch = mkDefault true;
      samueldr.use-case.emulation.retroarchCores = [ pkgs.libretro.pcsx-rearmed ];
    })
    (mkIf cfg.ps2 {
      environment.systemPackages = [
        pkgs.pcsx2
      ];
    })
    (mkIf cfg.psp {
      environment.systemPackages = [
        # The SDL version doesn't work on the steam deck.
        # Vulkan issues...
        # Even then, forcing it to opengl doesn't work when launched under steam.
        pkgs.ppsspp-qt
      ];
    })
  ];
}
