{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.android;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.use-case.android = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable android tools.
      '';
    };
  };
  config = mkIf cfg.enable {
    programs.adb.enable = true;
    services.udev.packages = [ pkgs.android-udev-rules ];
    environment.systemPackages = with pkgs; [
      android-tools
    ];
    app-compartmentalization = {
      scoped-apps = {
        scrcpy.enable = true;
      };
    };
  };
}
