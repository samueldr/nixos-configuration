{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.common;
  inherit (lib)
    attrNames
    concatStringsSep
    concatMapStringsSep
    mkMerge
    mkIf
  ;
in
{
  config = mkIf cfg.enable ( mkMerge [
    {
      # https://userbase.kde.org/KDE_System_Administration/Configuration_Files
      # NOTE: See https://github.com/KDE/kconfig/blob/62cb8e29d7457bb5ac767d29205e547aec668c75/docs/options.md?plain=1#L11
      environment.etc = {
        "xdg/baloofilerc" = {
          text = ''
            [$i]
            [Basic Settings]
            Indexing-Enabled=false
          '';
        };
        # TODO: make dolphin more configurable for better touch experience.
        "xdg/dolphinrc" = {
          text =
            let
              previewPlugins = [
                "audiothumbnail"
                "blenderthumbnail"
                "comicbookthumbnail"
                "djvuthumbnail"
                "ebookthumbnail"
                "ffmpegthumbs"
                "fontthumbnail"
                "gsthumbnail"
                "imagethumbnail"
                "jpegthumbnail"
                "mobithumbnail"
                "opendocumentthumbnail"
                "svgthumbnail"
                #"directorythumbnail"
                #"exrthumbnail"
                #"kraorathumbnail"
                #"rawthumbnail"
                #"windowsexethumbnail"
                #"windowsimagethumbnail"
              ];
             in
            ''
              [DetailsMode]
              UseShortRelativeDates[$i]=false

              [General]
              AutoExpandFolders[$i]=true
              BrowseThroughArchives[$i]=true
              RememberOpenedTabs[$i]=false
              ShowZoomSlider[$i]=false

              [KFileDialog Settings]
              Places Icons Auto-resize[$i]=false
              Places Icons Static Size[$i]=32

              [MainWindow]
              MenuBar[$i]=Disabled
              ToolBarsMovable[$i]=Disabled

              [PlacesPanel]
              IconSize[$i]=32

              [PreviewSettings]
              Plugins[$i]=${concatStringsSep "," previewPlugins}

              [CompactMode]
              IconSize[$i]=32
              MaximumTextWidthIndex[$i]=1
              PreviewSize[$i]=32

              [DetailsMode]
              ExpandableFolders[$i]=false
              HighlightEntireRow[$i]=false
              IconSize[$i]=22
              PreviewSize[$i]=22
              SidePadding[$i]=0

              [IconsMode]
              IconSize[$i]=64
              MaximumTextLines[$i]=2
              TextWidthIndex[$i]=0
            ''
          ;
        };
        "xdg/kaccessrc" = {
          text = ''
            [$i]
            [Bell]
            SystemBell=false
            VisibleBellInvert=false

            [ScreenReader]
            Enabled=false
          '';
        };
        "xdg/kded5rc" = {
          text =
            let
              # We'll generate from this attrset, with *per-group* immutability.
              # The intent is that any non-imutable groups will be present in the user config in the future.
              modules = {
                appmenu = true;
                bluedevil = true;
                colorcorrectlocationupdater = true;
                freespacenotifier = true;
                gtkconfig = true;
                kded_touchpad = true;
                keyboard = true;
                khotkeys = true;
                kscreen = true;
                ksysguard = true;
                ktimezoned = true;
                networkmanagement = true;
                networkstatus = true;
                proxyscout = true;
                remotenotifier = true;
                statusnotifierwatcher = true;

                baloosearchmodule = false;
                browserintegrationreminder = false;
                device_automounter = false;
                plasma_accentcolor_service = false;
              };
            in
            concatMapStringsSep "\n" (name: ''
              [Module-${name}][$i]
              autoload=${builtins.toJSON modules."${name}"}
            '') (attrNames modules)
          ;
        };
        "xdg/kiorc" = {
          text = ''
            [$i]
            [Confirmations]
            ConfirmDelete=true
            ConfirmEmptyTrash=true
            ConfirmTrash=false

            [Executable scripts]
            behaviourOnLaunch=alwaysAsk
          '';
        };
        "xdg/krunnerrc" = {
          text = ''
            [$i]
            [Plugins]
            DictionaryEnabled=false
            appstreamEnabled=false
            baloosearchEnabled=false
            bookmarksEnabled=false
            browserhistoryEnabled=false
            browsertabsEnabled=false
            desktopsessionsEnabled=false
            helprunnerEnabled=false
            katesessionsEnabled=false
            konsoleprofilesEnabled=false
            org.kde.activities2Enabled=false
            webshortcutsEnabled=false
          '';
        };
        "xdg/kservicemenurc" = {
          text = ''
            [$i]
            [Show]
            forgetfileitemaction=true
            installFont=true
            kactivitymanagerd_fileitem_linking_plugin=false
            mountisoaction=true
            setAsWallpaper=true
            tagsfileitemaction=true
          '';
        };
        "xdg/ksmserverrc" = {
          text = ''
            [$i]
            [General]
            loginMode=emptySession
            shutdownType=2

            [LegacySession: saved at previous logout]
            count=0

            [Session: saved at previous logout]
            count=0
          '';
        };
        "xdg/kscreenlockerrc" = {
          text = ''
            [$i]
            [Daemon]
            Autolock=false

            [Greeter][Wallpaper][org.kde.image][General]
            Image=/etc/wallpaper
            PreviewImage=/etc/wallpaper
            SlidePaths=/var/empty
          '';
        };
        "xdg/kwalletrc" = {
          text = ''
            [$i]
            [Wallet]
            Enabled=false
            First Use=false
          '';
        };
        "xdg/plasma-localerc" = {
          text = ''
            [$i]
            [Formats]
            LANG=${config.i18n.defaultLocale}
          '';
        };
        "xdg/powerdevilrc" = {
          text = ''
            [$i]
            [BatteryManagement]
            BatteryCriticalAction=0
            BatteryCriticalLevel=2
            BatteryLowLevel=9
          '';
        };
        "xdg/powermanagementprofilesrc" = {
          # https://github.com/KDE/powerdevil/blob/e5295897636d76c7ed6cd1896e76df6f007ebeaf/daemon/powerdevilprofilegenerator.h#L27-L37
          text = ''
            [$i]
            [AC]
            icon=battery-charging

            [AC][HandleButtonEvents]
            powerButtonAction=1
            powerDownAction=16
            lidAction=1
            triggerLidActionWhenExternalMonitorPresent=false

            [Battery]
            icon=battery-060

            [Battery][DPMSControl]
            idleTime=180
            lockBeforeTurnOff=1

            [Battery][DimDisplay]
            idleTime=60000

            [Battery][HandleButtonEvents]
            powerButtonAction=1
            powerDownAction=16
            lidAction=1
            triggerLidActionWhenExternalMonitorPresent=true

            [Battery][SuspendSession]
            idleTime=300000
            suspendThenHibernate=false
            suspendType=1

            [LowBattery]
            icon=battery-low

            [LowBattery][BrightnessControl]
            value=20

            [LowBattery][DPMSControl]
            idleTime=120
            lockBeforeTurnOff=1

            [LowBattery][DimDisplay]
            idleTime=60000

            [LowBattery][HandleButtonEvents]
            powerButtonAction=1
            powerDownAction=16
            lidAction=1
            triggerLidActionWhenExternalMonitorPresent=true

            [LowBattery][SuspendSession]
            idleTime=180000
            suspendThenHibernate=false
            suspendType=1
          '';
        };
      };
    }
  ]);
}
