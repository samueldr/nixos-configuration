{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.common;
  inherit (lib)
    mkMerge
    mkIf
  ;
in
{
  config = mkIf cfg.enable ( mkMerge [
    {
      environment.etc = {
        # kwin will always end-up overriding XKB_DEFAULT_MODEL, even though it wants to
        # use it, since it uses a default value for `Model` in src/xkb.cpp, which means
        # when XKB_DEFAULT_MODEL is attempted to be used, it's skipped, since
        # `ruleNames.model` is not empty or null.
        "xdg/kxkbrc" = {
          text = ''
            [$i]
            [Layout]
            Model=${config.environment.variables.XKB_DEFAULT_MODEL}
          '';
        };
        "xdg/kcminputrc" = {
          text =
            let
              touchpadConfig = ''
                NaturalScroll=true
                TapToClick=true
              '';
              mouseConfig = ''
                MiddleButtonEmulation=true
                PointerAcceleration=0.400
                PointerAccelerationProfile=1
                NaturalScroll=false
              '';
            in
            ''
              [$i]
              [Libinput][Defaults][Touchpad]
              ${touchpadConfig}

              [Libinput][Defaults][Pointer]
              ${mouseConfig}

              [Mouse]
              cursorSize=48
              cursorTheme=Adwaita
            ''
          ;
        };
      };
    }
  ]);
}
