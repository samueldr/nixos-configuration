{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.common;
  inherit (lib)
    mkMerge
    mkIf
  ;
in
{
  config = mkIf cfg.enable ( mkMerge [
    {
      # Forcibly link, as apparently kglobalshortcutsrc won't use defaults properly.
      system.userActivationScripts.samueldr-plasma-keybinds = ''
        (
        cd ~/.config
        for f in kglobalshortcutsrc; do
          if ! test -L "$f"; then
            rm -f "$f"
          fi
          ln -sf /etc/xdg/"$f"
        done
        )
      '';
      environment.etc = {
        # NOTE: discrete items are marked immutable so new configs are saved to the user config.
        # Except for ActivityManager... I don't want any of it.
        "xdg/kglobalshortcutsrc" = {
          text = ''
            [ActivityManager][$i]
            _k_friendly_name=Activity Manager

            [KDE Keyboard Layout Switcher]
            Switch to Next Keyboard Layout=none,Meta+Alt+K,Switch to Next Keyboard Layout
            _k_friendly_name=Keyboard Layout Switcher

            [kaccess]
            Toggle Screen Reader On and Off=Meta+Alt+S,Meta+Alt+S,Toggle Screen Reader On and Off
            _k_friendly_name=Accessibility

            [kcm_touchpad]
            Disable Touchpad=Touchpad Off,Touchpad Off,Disable Touchpad
            Enable Touchpad=Touchpad On,Touchpad On,Enable Touchpad
            Toggle Touchpad=Touchpad Toggle\tMeta+Ctrl+Zenkaku Hankaku,Touchpad Toggle\tMeta+Ctrl+Zenkaku Hankaku,Toggle Touchpad
            _k_friendly_name=Touchpad

            [kded5]
            Show System Activity=Ctrl+Esc,Ctrl+Esc,Show System Activity
            _k_friendly_name=KDE Daemon
            display=Display\tMeta+P,Display\tMeta+P,Switch Display

            [khotkeys]
            _k_friendly_name=Custom Shortcuts Service
            {37e48c7f-8eda-4b32-8473-6534035e1169}=Alt+Return,none,terminal

            [kmix]
            _k_friendly_name=Audio Volume
            decrease_microphone_volume=Microphone Volume Down,Microphone Volume Down,Decrease Microphone Volume
            decrease_volume=Volume Down,Volume Down,Decrease Volume
            increase_microphone_volume=Microphone Volume Up,Microphone Volume Up,Increase Microphone Volume
            increase_volume=Volume Up,Volume Up,Increase Volume
            mic_mute=Microphone Mute\tMeta+Volume Mute,Microphone Mute\tMeta+Volume Mute,Mute Microphone
            mute=Volume Mute,Volume Mute,Mute

            [ksmserver]
            Halt Without Confirmation=none,,Halt Without Confirmation
            Lock Session=Ctrl+Alt+Backspace\tCtrl+Alt+Del,Meta+L\tScreensaver,Lock Session
            Log Out=none,Ctrl+Alt+Del,Log Out
            Log Out Without Confirmation=none,,Log Out Without Confirmation
            Reboot Without Confirmation=none,,Reboot Without Confirmation
            _k_friendly_name=KWin

            [kwin]
            Activate Window Demanding Attention=Meta+Ctrl+A,Meta+Ctrl+A,Activate Window Demanding Attention
            ClearLastMouseMark=Meta+Shift+F12,Meta+Shift+F12,Clear Last Mouse Mark
            ClearMouseMarks=Meta+Shift+F11,Meta+Shift+F11,Clear All Mouse Marks
            Decrease Opacity=none,,Decrease Opacity of Active Window by 5 %
            Edit Tiles=Shift+Meta+T,Meta+T,Toggle Tiles Editor
            Expose=Ctrl+F9,Ctrl+F9,Toggle Present Windows (Current desktop)
            ExposeAll=Ctrl+F10\tLaunch (C),Ctrl+F10\tLaunch (C),Toggle Present Windows (All desktops)
            ExposeClass=Ctrl+F7,Ctrl+F7,Toggle Present Windows (Window class)
            ExposeClassCurrentDesktop=none,none,Toggle Present Windows (Window class on current desktop)
            Increase Opacity=none,,Increase Opacity of Active Window by 5 %
            Invert=Meta+Ctrl+I,Meta+Ctrl+I,Toggle Invert Effect
            Invert Screen Colors=none,,Invert Screen Colors
            InvertWindow=Meta+Ctrl+U,Meta+Ctrl+U,Toggle Invert Effect on Window
            Kill Window=Meta+Ctrl+Esc,Meta+Ctrl+Esc,Kill Window
            Move Tablet to Next Output=none,none,Move the tablet to the next output
            MoveMouseToCenter=Meta+F6,Meta+F6,Move Mouse to Center
            MoveMouseToFocus=Meta+F5,Meta+F5,Move Mouse to Focus
            MoveZoomDown=none,none,Move Zoomed Area Downwards
            MoveZoomLeft=none,none,Move Zoomed Area to Left
            MoveZoomRight=none,none,Move Zoomed Area to Right
            MoveZoomUp=none,none,Move Zoomed Area Upwards
            Overview=Meta+Esc,Meta+W,Toggle Overview
            Setup Window Shortcut=none,,Setup Window Shortcut
            Show Desktop=none,Meta+D,Peek at Desktop
            ShowDesktopGrid=Meta+F8,Meta+F8,Show Desktop Grid
            Suspend Compositing=Alt+Shift+F12,Alt+Shift+F12,Suspend Compositing
            Switch One Desktop Down=none,Meta+Ctrl+Down,Switch One Desktop Down
            Switch One Desktop Up=none,Meta+Ctrl+Up,Switch One Desktop Up
            Switch One Desktop to the Left=none,Meta+Ctrl+Left,Switch One Desktop to the Left
            Switch One Desktop to the Right=none,Meta+Ctrl+Right,Switch One Desktop to the Right
            Switch Window Down=Meta+Alt+Down,Meta+Alt+Down,Switch to Window Below
            Switch Window Left=Meta+Alt+Left,Meta+Alt+Left,Switch to Window to the Left
            Switch Window Right=Meta+Alt+Right,Meta+Alt+Right,Switch to Window to the Right
            Switch Window Up=Meta+Alt+Up,Meta+Alt+Up,Switch to Window Above
            Switch to Desktop 1=Meta+1,Ctrl+F1,Switch to Desktop 1
            Switch to Desktop 10=none,,Switch to Desktop 10
            Switch to Desktop 11=none,,Switch to Desktop 11
            Switch to Desktop 12=none,,Switch to Desktop 12
            Switch to Desktop 13=none,,Switch to Desktop 13
            Switch to Desktop 14=none,,Switch to Desktop 14
            Switch to Desktop 15=none,,Switch to Desktop 15
            Switch to Desktop 16=none,,Switch to Desktop 16
            Switch to Desktop 17=none,,Switch to Desktop 17
            Switch to Desktop 18=none,,Switch to Desktop 18
            Switch to Desktop 19=none,,Switch to Desktop 19
            Switch to Desktop 2=Meta+2,Ctrl+F2,Switch to Desktop 2
            Switch to Desktop 20=none,,Switch to Desktop 20
            Switch to Desktop 3=Meta+3,Ctrl+F3,Switch to Desktop 3
            Switch to Desktop 4=Meta+4,Ctrl+F4,Switch to Desktop 4
            Switch to Desktop 5=Meta+5,,Switch to Desktop 5
            Switch to Desktop 6=Meta+6,,Switch to Desktop 6
            Switch to Desktop 7=Meta+7,,Switch to Desktop 7
            Switch to Desktop 8=Meta+8,,Switch to Desktop 8
            Switch to Desktop 9=Meta+9,,Switch to Desktop 9
            Switch to Next Desktop=Meta+Right,,Switch to Next Desktop
            Switch to Next Screen=Meta+Ctrl+H,,Switch to Next Screen
            Switch to Previous Desktop=Meta+Left,,Switch to Previous Desktop
            Switch to Previous Screen=Meta+Ctrl+L,,Switch to Previous Screen
            Switch to Screen 0=none,,Switch to Screen 0
            Switch to Screen 1=none,,Switch to Screen 1
            Switch to Screen 2=none,,Switch to Screen 2
            Switch to Screen 3=none,,Switch to Screen 3
            Switch to Screen 4=none,,Switch to Screen 4
            Switch to Screen 5=none,,Switch to Screen 5
            Switch to Screen 6=none,,Switch to Screen 6
            Switch to Screen 7=none,,Switch to Screen 7
            Switch to Screen Above=,,Switch to Screen Above
            Switch to Screen Below=,,Switch to Screen Below
            Switch to Screen to the Left=,,Switch to Screen to the Left
            Switch to Screen to the Right=,,Switch to Screen to the Right
            Toggle Night Color=none,none,Toggle Night Color
            Toggle Window Raise/Lower=none,,Toggle Window Raise/Lower
            ToggleCurrentThumbnail=Meta+Ctrl+T,Meta+Ctrl+T,Toggle Thumbnail for Current Window
            ToggleMouseClick=Meta+*,Meta+*,Toggle Mouse Click Effect
            TrackMouse=none,none,Track mouse
            Walk Through Desktop List=none,,Walk Through Desktop List
            Walk Through Desktop List (Reverse)=none,,Walk Through Desktop List (Reverse)
            Walk Through Desktops=none,,Walk Through Desktops
            Walk Through Desktops (Reverse)=none,,Walk Through Desktops (Reverse)
            Walk Through Windows=Meta+H,Alt+Tab,Walk Through Windows
            Walk Through Windows (Reverse)=Meta+L\tMeta+Shift+Tab,Alt+Shift+Backtab,Walk Through Windows (Reverse)
            Walk Through Windows Alternative=Meta+Tab,,Walk Through Windows Alternative
            Walk Through Windows Alternative (Reverse)=none,,Walk Through Windows Alternative (Reverse)
            Walk Through Windows of Current Application=none,Alt+`,Walk Through Windows of Current Application
            Walk Through Windows of Current Application (Reverse)=none,Alt+~,Walk Through Windows of Current Application (Reverse)
            Walk Through Windows of Current Application Alternative=none,,Walk Through Windows of Current Application Alternative
            Walk Through Windows of Current Application Alternative (Reverse)=none,,Walk Through Windows of Current Application Alternative (Reverse)
            Window Above Other Windows=Meta+T,,Keep Window Above Others
            Window Below Other Windows=none,,Keep Window Below Others
            Window Close=Meta+W,Alt+F4,Close Window
            Window Fullscreen=Meta+Shift+F,,Make Window Fullscreen
            Window Grow Horizontal=none,,Expand Window Horizontally
            Window Grow Vertical=none,,Expand Window Vertically
            Window Lower=none,,Lower Window
            Window Maximize=Meta+F,Meta+PgUp,Maximize Window
            Window Maximize Horizontal=none,,Maximize Window Horizontally
            Window Maximize Vertical=none,,Maximize Window Vertically
            Window Minimize=Meta+N\tMeta+Down,Meta+PgDown,Minimize Window
            Window Move=none,,Move Window
            Window Move Center=none,,Move Window to the Center
            Window No Border=Meta+D,,Hide Window Border
            Window On All Desktops=none,,Keep Window on All Desktops
            Window One Desktop Down=Meta+Ctrl+Shift+Down,Meta+Ctrl+Shift+Down,Window One Desktop Down
            Window One Desktop Up=Meta+Ctrl+Shift+Up,Meta+Ctrl+Shift+Up,Window One Desktop Up
            Window One Desktop to the Left=Meta+Ctrl+Shift+Left,Meta+Ctrl+Shift+Left,Window One Desktop to the Left
            Window One Desktop to the Right=Meta+Ctrl+Shift+Right,Meta+Ctrl+Shift+Right,Window One Desktop to the Right
            Window One Screen Down=,,Window One Screen Down
            Window One Screen Up=,,Window One Screen Up
            Window One Screen to the Left=,,Window One Screen to the Left
            Window One Screen to the Right=,,Window One Screen to the Right
            Window Operations Menu=none,Alt+F3,Window Operations Menu
            Window Pack Down=none,,Move Window Down
            Window Pack Left=none,,Move Window Left
            Window Pack Right=none,,Move Window Right
            Window Pack Up=none,,Move Window Up
            Window Quick Tile Bottom=none,Meta+Down,Quick Tile Window to the Bottom
            Window Quick Tile Bottom Left=none,,Quick Tile Window to the Bottom Left
            Window Quick Tile Bottom Right=none,,Quick Tile Window to the Bottom Right
            Window Quick Tile Left=none,Meta+Left,Quick Tile Window to the Left
            Window Quick Tile Right=none,Meta+Right,Quick Tile Window to the Right
            Window Quick Tile Top=none,Meta+Up,Quick Tile Window to the Top
            Window Quick Tile Top Left=none,,Quick Tile Window to the Top Left
            Window Quick Tile Top Right=none,,Quick Tile Window to the Top Right
            Window Raise=none,,Raise Window
            Window Resize=none,,Resize Window
            Window Shade=none,,Shade Window
            Window Shrink Horizontal=none,,Shrink Window Horizontally
            Window Shrink Vertical=none,,Shrink Window Vertically
            Window to Desktop 1=none,,Window to Desktop 1
            Window to Desktop 10=none,,Window to Desktop 10
            Window to Desktop 11=none,,Window to Desktop 11
            Window to Desktop 12=none,,Window to Desktop 12
            Window to Desktop 13=none,,Window to Desktop 13
            Window to Desktop 14=none,,Window to Desktop 14
            Window to Desktop 15=none,,Window to Desktop 15
            Window to Desktop 16=none,,Window to Desktop 16
            Window to Desktop 17=none,,Window to Desktop 17
            Window to Desktop 18=none,,Window to Desktop 18
            Window to Desktop 19=none,,Window to Desktop 19
            Window to Desktop 2=none,,Window to Desktop 2
            Window to Desktop 20=none,,Window to Desktop 20
            Window to Desktop 3=none,,Window to Desktop 3
            Window to Desktop 4=none,,Window to Desktop 4
            Window to Desktop 5=none,,Window to Desktop 5
            Window to Desktop 6=none,,Window to Desktop 6
            Window to Desktop 7=none,,Window to Desktop 7
            Window to Desktop 8=none,,Window to Desktop 8
            Window to Desktop 9=none,,Window to Desktop 9
            Window to Next Desktop=none,,Window to Next Desktop
            Window to Next Screen=Meta+Shift+Right,Meta+Shift+Right,Window to Next Screen
            Window to Previous Desktop=none,,Window to Previous Desktop
            Window to Previous Screen=Meta+Shift+Left,Meta+Shift+Left,Window to Previous Screen
            Window to Screen 0=none,,Window to Screen 0
            Window to Screen 1=none,,Window to Screen 1
            Window to Screen 2=none,,Window to Screen 2
            Window to Screen 3=none,,Window to Screen 3
            Window to Screen 4=none,,Window to Screen 4
            Window to Screen 5=none,,Window to Screen 5
            Window to Screen 6=none,,Window to Screen 6
            Window to Screen 7=none,,Window to Screen 7
            _k_friendly_name=KWin
            view_actual_size=Meta+0,Meta+0,Zoom to Actual Size
            view_zoom_in=Meta++\tMeta+=,Meta++,Zoom In
            view_zoom_out=Meta+-,Meta+-,Zoom Out

            [mediacontrol]
            _k_friendly_name=Media Controller
            mediavolumedown=none,,Media volume down
            mediavolumeup=none,,Media volume up
            nextmedia=Media Next,Media Next,Media playback next
            pausemedia=Media Pause,Media Pause,Pause media playback
            playmedia=none,,Play media playback
            playpausemedia=Media Play,Media Play,Play/Pause media playback
            previousmedia=Media Previous,Media Previous,Media playback previous
            stopmedia=Media Stop,Media Stop,Stop media playback

            [org.kde.dolphin.desktop]
            _k_friendly_name=Dolphin
            _launch=none,Meta+E,Dolphin

            [org.kde.krunner.desktop]
            RunClipboard=Alt+Shift+F2,Alt+Shift+F2,Run command on clipboard contents
            _k_friendly_name=KRunner
            _launch=Alt+Space\tAlt+F2\tSearch,Alt+Space\tAlt+F2\tSearch,KRunner

            [org.kde.plasma.emojier.desktop]
            _k_friendly_name=Emoji Selector
            _launch=Meta+.,Meta+.,Emoji Selector

            [org.kde.spectacle.desktop]
            ActiveWindowScreenShot=none,Meta+Print,Capture Active Window
            CurrentMonitorScreenShot=none,none,Capture Current Monitor
            FullScreenScreenShot=none,Shift+Print,Capture Entire Desktop
            OpenWithoutScreenshot=none,none,Launch without taking a screenshot
            RectangularRegionScreenShot=none,Meta+Shift+Print,Capture Rectangular Region
            WindowUnderCursorScreenShot=none,Meta+Ctrl+Print,Capture Window Under Cursor
            _k_friendly_name=Spectacle
            _launch=none,Print,Spectacle

            [org_kde_powerdevil]
            Decrease Keyboard Brightness=Keyboard Brightness Down,Keyboard Brightness Down,Decrease Keyboard Brightness
            Decrease Screen Brightness=Monitor Brightness Down,Monitor Brightness Down,Decrease Screen Brightness
            Hibernate=Hibernate,Hibernate,Hibernate
            Increase Keyboard Brightness=Keyboard Brightness Up,Keyboard Brightness Up,Increase Keyboard Brightness
            Increase Screen Brightness=Monitor Brightness Up,Monitor Brightness Up,Increase Screen Brightness
            PowerDown=Power Down,Power Down,Power Down
            PowerOff=none,none,Power Off
            Sleep=Sleep,Sleep,Suspend
            Toggle Keyboard Backlight=Keyboard Light On/Off,Keyboard Light On/Off,Toggle Keyboard Backlight
            Turn Off Screen=Power Off,Power Off,Turn Off Screen
            _k_friendly_name=Power Management

            [plasmashell]
            _k_friendly_name=Plasma
            activate task manager entry 1=none,Meta+1,Activate Task Manager Entry 1
            activate task manager entry 10=none,Meta+0,Activate Task Manager Entry 10
            activate task manager entry 2=none,Meta+2,Activate Task Manager Entry 2
            activate task manager entry 3=none,Meta+3,Activate Task Manager Entry 3
            activate task manager entry 4=none,Meta+4,Activate Task Manager Entry 4
            activate task manager entry 5=none,Meta+5,Activate Task Manager Entry 5
            activate task manager entry 6=none,Meta+6,Activate Task Manager Entry 6
            activate task manager entry 7=none,Meta+7,Activate Task Manager Entry 7
            activate task manager entry 8=none,Meta+8,Activate Task Manager Entry 8
            activate task manager entry 9=none,Meta+9,Activate Task Manager Entry 9
            clear-history=,,Clear Clipboard History
            clipboard_action=Meta+Ctrl+X,Meta+Ctrl+X,Automatic Action Popup Menu
            cycle-panels=none,Meta+Alt+P,Move keyboard focus between panels
            cycleNextAction=,,Next History Item
            cyclePrevAction=,,Previous History Item
            edit_clipboard=,,Edit Contents…
            manage activities=none,Meta+Q,Show Activity Switcher
            next activity=none,Meta+Tab,Walk through activities
            previous activity=none,Meta+Shift+Tab,Walk through activities (Reverse)
            repeat_action=Meta+Ctrl+R,Meta+Ctrl+R,Manually Invoke Action on Current Clipboard
            show dashboard=none,Ctrl+F12,Show Desktop
            show-barcode=,,Show Barcode…
            show-on-mouse-pos=Meta+V,Meta+V,Show Items at Mouse Position
            stop current activity=none,Meta+S,Stop Current Activity
            switch to next activity=none,,Switch to Next Activity
            switch to previous activity=none,,Switch to Previous Activity
            toggle do not disturb=none,none,Toggle do not disturb

            [systemsettings.desktop]
            _k_friendly_name=System Settings
            _launch=none,Tools,System Settings
            kcm-kscreen=none,none,Display Configuration
            kcm-lookandfeel=none,none,Global Theme
            kcm-users=none,none,Users
            powerdevilprofilesconfig=none,none,Energy Saving
            screenlocker=none,none,Screen Locking
          '';
        };
        "xdg/khotkeysrc" = {
          text = ''
            [$i]
            [Data]
            DataCount=2

            [Data_1]
            Comment=Comment
            Enabled=true
            Name=terminal
            Type=SIMPLE_ACTION_DATA

            [Data_1Actions]
            ActionsCount=1

            [Data_1Actions0]
            CommandURL=terminal
            Type=COMMAND_URL

            [Data_1Conditions]
            Comment=
            ConditionsCount=0

            [Data_1Triggers]
            Comment=Simple_action
            TriggersCount=1

            [Data_1Triggers0]
            Key=Alt+Return
            Type=SHORTCUT
            Uuid={37e48c7f-8eda-4b32-8473-6534035e1169}

            [Data_2]
            Comment=screenshot
            Enabled=true
            Name=screenshot
            Type=SIMPLE_ACTION_DATA

            [Data_2Actions]
            ActionsCount=1

            [Data_2Actions0]
            CommandURL=screenshot -s
            Type=COMMAND_URL

            [Data_2Conditions]
            Comment=
            ConditionsCount=0

            [Data_2Triggers]
            Comment=Simple_action
            TriggersCount=1

            [Data_2Triggers0]
            Key=Print
            Type=SHORTCUT
            Uuid={65400154-ae3f-4930-bca2-d7172da8a5ab}

            [Gestures]
            Disabled=true
            MouseButton=2
            Timeout=300

            [GesturesExclude]
            Comment=
            WindowsCount=0

            [Main]
            AlreadyImported=defaults,kde32b1,konqueror_gestures_kde321
            Disabled=false
            Version=2

            [Voice]
            Shortcut=
          '';
        };
      };
    }
  ]);
}
