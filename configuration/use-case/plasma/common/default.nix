{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.common;
  inherit (lib)
    mkMerge
    mkOption
    mkIf
    types
  ;
in
{
  imports = [
    ./input.nix
    ./kdeglobals.nix
    ./keybinds.nix
    ./kwin.nix
    ./misc-config.nix
  ];

  options.samueldr.use-case.plasma.common = {
    enable = mkOption {
      default = false;
      type = types.bool;
      internal = true;
      description = ''
        Enable use of any Plasma-based session.
      '';
    };
  };

  config = mkIf cfg.enable ( mkMerge [
    {
      services.xserver = {
        enable = true;
        displayManager.autoLogin = {
          enable = true;
          user = "samuel";
        };
        libinput.enable = true;
        displayManager.lightdm = {
          # Workaround for autologin only working at first launch.
          # A logout or session crashing will show the login screen otherwise.
          extraSeatDefaults = ''
            session-cleanup-script=${pkgs.procps}/bin/pkill -P1 -fx ${pkgs.lightdm}/sbin/lightdm
          '';
        };
      };
      hardware.sensor.iio.enable = true;
      powerManagement.enable = true;
      programs.light.enable = true;
      environment.systemPackages = with pkgs; [
        # The full plasma control panel.
        systemsettings

        # File manager
        dolphin
        libsForQt5.dolphin-plugins
        libsForQt5.kio-extras

        # Thumbnails
        ffmpegthumbs
        libsForQt5.kdegraphics-thumbnailers

        # Archives management
        ark
      ];
    }
    {
      programs.kdeconnect.enable = true;
    }
    {
      samueldr.hacks.remove-applications.patterns = [
        "*kwallet*desktop"
      ];
    }
    {
      # Remove what I don't use.
      services.xserver.desktopManager.plasma5.mobile.installRecommendedSoftware = false;
      environment.plasma5.excludePackages = with pkgs; [
        plasma-browser-integration
        konsole
      ];
      # Unneeded X stuff
      services.xserver.excludePackages = with pkgs; [
        xterm
        xorg.xinput
        xorg.xprop
        xorg.xset
        xorg.xsetroot
      ];
    }
    {
      samueldr.use-case.app.terminal.enable = true;
      samueldr.use-case.app.screenshot.enable = true;
      samueldr.programs.screenshot = {
        environment = "Plasma";
      };
    }
    # TODO: Generic wayland use-case?
    {
      environment.variables = {
        GDK_BACKEND = "wayland";
      };
      app-compartmentalization = {
        scoped-apps = {
          "firefox" = {
            commands = {
              "firefox" = {
                environment = {
                  GDK_BACKEND = "wayland";
                  MOZ_ENABLE_WAYLAND = "1";
                };
              };
            };
          };
        };
      };
    }
    (
      let
        wrapper = pkgs.callPackage (
          { runCommand
          , package
          , name
          , session-name
          , runtimeShell
          , systemd
          }:
          runCommand "${name}-wrapper" {
            meta.priority = 2;
            passthru.providedSessions = [ session-name ];
          } ''
            mkdir -p $out/share/wayland-sessions/
            sed \
              -e 's;Exec=.*;Exec='"$out"'/libexec/${name}-wrapper;' \
              -e 's;Exec=.*;Exec='"$out"'/libexec/${name}-wrapper;' \
              ${package}/share/wayland-sessions/${session-name}.desktop \
              > $out/share/wayland-sessions/${session-name}.desktop
            mkdir -p $out/libexec
            cat > $out/libexec/${name}-wrapper <<EOF
            #!${runtimeShell}
            echo ":: Workaround for plasma session being kinda broken..."
            ${systemd}/bin/systemctl --user stop graphical-session.target
            exec $(cat ${package}/share/wayland-sessions/${session-name}.desktop | grep '^Exec=' | cut -d= -f2-)
            EOF
            chmod +x $out/libexec/${name}-wrapper
          ''
        ) {
          package =
            if config.samueldr.use-case.plasma.mobile.enable
            then pkgs.libsForQt5.plasma5.plasma-mobile
            else pkgs.libsForQt5.plasma5.plasma-workspace
          ;
          name =
            if config.samueldr.use-case.plasma.mobile.enable
            then "plasma-mobile"
            else "plasma-desktop"
          ;
          session-name =
            if config.samueldr.use-case.plasma.mobile.enable
            then "plasma-mobile"
            else "plasmawayland"
          ;
        };
      in
      {
        # Workaround for the systemd services not exiting cleanly on sudden loss of session
        services.xserver.displayManager.sessionPackages = [ wrapper ];
        environment.systemPackages = [ wrapper ];
      }
    )
  ]);
}
