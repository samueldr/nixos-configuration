{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.common;
  inherit (lib)
    mkMerge
    mkIf
  ;
in
{
  config = mkIf cfg.enable ( mkMerge [
    {
      environment.etc = {
        # NOTE: discrete items are marked immutable so new configs are saved to the user config.
        "xdg/kdeglobals" = {
          text = ''
            [General]
            BrowserApplication[$i]=chromium-browser.desktop
            TerminalApplication[$i]=terminal
            TerminalService[$i]=terminal.desktop

            AccentColor[$i]=49,144,234
            ColorSchemeHash[$i]=c15f33214c9649239903147a46fd3fe8d9033037
            LastUsedCustomAccentColor[$i]=49,144,234

            XftAntialias[$i]=true
            XftHintStyle[$i]=hintslight
            XftSubPixel[$i]=rgb
            fixed[$i]=Go Mono,12,-1,5,50,0,0,0,0,0
            font[$i]=Roboto Condensed,12,-1,5,50,0,0,0,0,0
            menuFont[$i]=Roboto Condensed,12,-1,5,50,0,0,0,0,0
            smallestReadableFont[$i]=Roboto Condensed,8,-1,5,50,0,0,0,0,0
            toolBarFont[$i]=Roboto Condensed,12,-1,5,50,0,0,0,0,0

            [Icons]
            Theme[$i]=breeze

            [KDE]
            ShowDeleteCommand[$i]=false
            widgetStyle[$i]=Breeze

            [Locale]
            TimeFormat[$i]=HH:mm:ss

            [PreviewSettings]
            MaximumRemoteSize[$i]=0

            [Shortcuts]
            Close[$i]=Ctrl+W
            Help[$i]=
            WhatsThis[$i]=

            [WM]
            activeBackground[$i]=67,74,84
            activeBlend[$i]=112,118,127
            activeFont[$i]=Roboto Condensed,12,-1,5,57,0,0,0,0,0,Medium
            activeForeground[$i]=237,239,243
            inactiveBackground[$i]=237,239,243
            inactiveBlend[$i]=112,118,127
            inactiveForeground[$i]=172,177,186

            [ColorEffects:Disabled]
            ChangeSelectionColor[$i]=
            Color[$i]=56,56,56
            ColorAmount[$i]=0
            ColorEffect[$i]=0
            ContrastAmount[$i]=0.85
            ContrastEffect[$i]=1
            Enable[$i]=
            IntensityAmount[$i]=0
            IntensityEffect[$i]=0

            [ColorEffects:Inactive]
            ChangeSelectionColor[$i]=true
            Color[$i]=112,111,110
            ColorAmount[$i]=-0.8
            ColorEffect[$i]=0
            ContrastAmount[$i]=0.1
            ContrastEffect[$i]=0
            Enable[$i]=false
            IntensityAmount[$i]=0
            IntensityEffect[$i]=0

            [Colors:Button]
            BackgroundAlternate[$i]=99,167,232
            BackgroundNormal[$i]=217,221,229
            DecorationFocus[$i]=49,144,234
            DecorationHover[$i]=49,144,234
            ForegroundActive[$i]=49,144,234
            ForegroundInactive[$i]=188,192,202
            ForegroundLink[$i]=49,144,234
            ForegroundNegative[$i]=193,117,100
            ForegroundNeutral[$i]=176,128,0
            ForegroundNormal[$i]=67,74,84
            ForegroundPositive[$i]=129,148,107
            ForegroundVisited[$i]=69,40,134

            [Colors:Complementary]
            BackgroundAlternate[$i]=27,30,32
            BackgroundNormal[$i]=42,46,50
            DecorationFocus[$i]=49,144,234
            DecorationHover[$i]=49,144,234
            ForegroundActive[$i]=49,144,234
            ForegroundInactive[$i]=161,169,177
            ForegroundLink[$i]=49,144,234
            ForegroundNegative[$i]=218,68,83
            ForegroundNeutral[$i]=246,116,0
            ForegroundNormal[$i]=252,252,252
            ForegroundPositive[$i]=39,174,96
            ForegroundVisited[$i]=155,89,182

            [Colors:Header]
            BackgroundAlternate[$i]=239,240,241
            BackgroundNormal[$i]=222,224,226
            DecorationFocus[$i]=49,144,234
            DecorationHover[$i]=49,144,234
            ForegroundActive[$i]=49,144,234
            ForegroundInactive[$i]=112,125,138
            ForegroundLink[$i]=49,144,234
            ForegroundNegative[$i]=218,68,83
            ForegroundNeutral[$i]=246,116,0
            ForegroundNormal[$i]=35,38,41
            ForegroundPositive[$i]=39,174,96
            ForegroundVisited[$i]=155,89,182

            [Colors:Header][Inactive]
            BackgroundAlternate[$i]=227,229,231
            BackgroundNormal[$i]=239,240,241
            DecorationFocus[$i]=61,174,233
            DecorationHover[$i]=61,174,233
            ForegroundActive[$i]=61,174,233
            ForegroundInactive[$i]=112,125,138
            ForegroundLink[$i]=41,128,185
            ForegroundNegative[$i]=218,68,83
            ForegroundNeutral[$i]=246,116,0
            ForegroundNormal[$i]=35,38,41
            ForegroundPositive[$i]=39,174,96
            ForegroundVisited[$i]=155,89,182

            [Colors:Selection]
            BackgroundAlternate[$i]=108,175,238
            BackgroundNormal[$i]=108,175,238
            DecorationFocus[$i]=49,144,234
            DecorationHover[$i]=49,144,234
            ForegroundActive[$i]=49,144,234
            ForegroundInactive[$i]=255,255,255
            ForegroundLink[$i]=49,144,234
            ForegroundNegative[$i]=193,117,100
            ForegroundNeutral[$i]=255,221,0
            ForegroundNormal[$i]=255,255,255
            ForegroundPositive[$i]=129,148,107
            ForegroundVisited[$i]=69,40,134

            [Colors:Tooltip]
            BackgroundAlternate[$i]=234,236,241
            BackgroundNormal[$i]=234,236,241
            DecorationFocus[$i]=49,144,234
            DecorationHover[$i]=49,144,234
            ForegroundActive[$i]=49,144,234
            ForegroundInactive[$i]=155,166,178
            ForegroundLink[$i]=49,144,234
            ForegroundNegative[$i]=193,117,100
            ForegroundNeutral[$i]=176,128,0
            ForegroundNormal[$i]=67,74,84
            ForegroundPositive[$i]=129,148,107
            ForegroundVisited[$i]=69,40,134

            [Colors:View]
            BackgroundAlternate[$i]=248,249,250
            BackgroundNormal[$i]=248,249,250
            DecorationFocus[$i]=49,144,234
            DecorationHover[$i]=49,144,234
            ForegroundActive[$i]=49,144,234
            ForegroundInactive[$i]=136,135,134
            ForegroundLink[$i]=49,144,234
            ForegroundNegative[$i]=193,117,100
            ForegroundNeutral[$i]=176,128,0
            ForegroundNormal[$i]=67,74,84
            ForegroundPositive[$i]=129,148,107
            ForegroundVisited[$i]=69,40,134

            [Colors:Window]
            BackgroundAlternate[$i]=205,208,216
            BackgroundNormal[$i]=237,239,243
            DecorationFocus[$i]=49,144,234
            DecorationHover[$i]=49,144,234
            ForegroundActive[$i]=49,144,234
            ForegroundInactive[$i]=172,177,186
            ForegroundLink[$i]=49,144,234
            ForegroundNegative[$i]=193,117,100
            ForegroundNeutral[$i]=176,128,0
            ForegroundNormal[$i]=67,74,84
            ForegroundPositive[$i]=129,148,107
            ForegroundVisited[$i]=69,40,134
          '';
        };
      };
    }
  ]);
}
