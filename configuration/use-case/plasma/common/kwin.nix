{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.common;
  inherit (lib)
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.plasma.common = {
    kwin = {
      scale = mkOption {
        default = "1.0";
        # Number types don't necessarily format well for the config.
        type = types.str;
        description = ''
          KWin scale.
        '';
      };
    };
  };
  config = mkIf cfg.enable ( mkMerge [
    {
      environment.etc = {
        "xdg/kwinrc" = {
          # The whole file itself isn't immutable
          text = ''
            [Compositing][$i]
            GLPreferBufferSwap=e
            LatencyPolicy=Low

            [Desktops]
            Name_1[$i]=🍉
            Name_2[$i]=🍈
            Name_3[$i]=🍇
            Name_4[$i]=🍓
            Name_5[$i]=🍎
            Name_6[$i]=🍋
            Name_7[$i]=🍍
            Name_8[$i]=🍆
            Name_9[$i]=💩

            [Effect-slide][$i]
            HorizontalGap=0
            SlideDocks=true
            VerticalGap=0

            [Effect-overview][$i]
            TouchBorderActivate=4

            [Effect-desktopgrid][$i]
            BorderActivate=7

            [Effect-trackmouse][$i]
            Alt=true
            Meta=false

            [Effect-windowview][$i]
            BorderActivateAll=9

            [Plugins]
            kwin4_effect_fadeEnabled[$i]=true
            kwin4_effect_fullscreenEnabled[$i]=false
            kwin4_effect_scaleEnabled[$i]=false
            kwin4_effect_squashEnabled[$i]=false
            blurEnabled[$i]=false
            contrastEnabled[$i]=false
            invertEnabled[$i]=true
            sheetEnabled[$i]=true
            thumbnailasideEnabled[$i]=true
            touchpointsEnabled[$i]=true

            [TabBox][$i]
            ActivitiesMode=0
            OrderMinimizedMode=1
            TouchBorderActivate=6

            [TabBoxAlternative][$i]
            ActivitiesMode=0
            LayoutName=flipswitch

            [Tiling]
            padding=0

            [Xwayland][$i]
            Scale=${cfg.kwin.scale}

            # https://invent.kde.org/plasma/kwin/-/blob/9d353864a42c60cea90e931cf80766d1dd5e9a11/src/kcms/decoration/kwindecorationsettings.kcfg
            [org.kde.kdecoration2][$i]
            library=org.kde.breeze
            theme=Breeze
            CloseOnDoubleClickOnMenu=true
            # https://invent.kde.org/plasma/kwin/-/blob/9d353864a42c60cea90e931cf80766d1dd5e9a11/src/kcms/decoration/utils.cpp#L35-46
            ButtonsOnLeft=MSFH
            ButtonsOnRight=
            ShowToolTips=false
            # https://invent.kde.org/plasma/kwin/-/blob/9d353864a42c60cea90e931cf80766d1dd5e9a11/src/kcms/decoration/utils.cpp#L14-23
            BorderSize=None
            BorderSizeAuto=true

            [NightColor][$i]
            Active=true
            LatitudeAuto=46.74500
            LatitudeFixed=46.74500
            LongitudeAuto=-71.28700
            LongitudeFixed=-71.28700
            Mode=Location
            NightTemperature=5500
          '';
        };
        "xdg/breezerc" = {
          # https://invent.kde.org/plasma/breeze/-/blob/b733bd2b62f23ef741129caba0681e69c5bcca08/kdecoration/breezesettingsdata.kcfg
          text = ''
            [$i]
            [Common]
            ShadowSize=ShadowSmall
            ShadowStrength=84

            [Windeco]
            ButtonSize=ButtonSmall
          '';
        };
      };
    }
  ]);
}
