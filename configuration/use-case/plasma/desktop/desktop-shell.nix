{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.desktop;
  inherit (lib)
    concatMapStringsSep
    concatStringsSep
    genList
    mkDefault
    mkIf
    mkMerge
    mkOption
    optionalString
    optionals
    types
  ;
in
{
  options = {
    samueldr.use-case.plasma.desktop = {
      displayCount = mkOption {
        default = 1;
        type = types.int;
        description = ''
          Number of desktops this configuration supports.

          Considered a hardware config, set this to the maximum number of displays connected.
        '';
      };
    };
  };
  config = mkIf cfg.enable ( mkMerge [
    {
      samueldr.use-case.plasma.desktop.displayCount = mkDefault config.samueldr.hardware.maxDisplayCount;
    }
    {
      system.userActivationScripts.samueldr-plasma-desktop-shell = ''
        (
        cd ~/.config
        for f in plasma-org.kde.plasma.desktop-appletsrc plasmarc; do
          if ! test -L "$f"; then
            rm -f "$f"
          fi
          ln -sf /etc/xdg/"$f"
        done
        )
      '';

      environment.etc = {
        # This configures the desktop for the user.
        # Anyway wallpaper and pinned apps reset by immutability in a containment.
        # NOTE: These settings don't work like the other plasma settings.
        #       The shell will not merge configs from this file into the final file.
        #       This file needs to be linked in the user's `$XDG_CONFIG_HOME` folder.
        "xdg/plasma-org.kde.plasma.desktop-appletsrc" = {
          text =
            let
              # https://invent.kde.org/frameworks/plasma-framework/-/blob/f5e93034d30a2220268a6b991c6000e1507e13a3/src/plasma/plasma.h#L216-228
              # 4 doesn't seem to work?
              immutability = "2";
              # Break tray config out to improve readability
              trayConfig = ''
                ################################################################################
                ## System tray config
                ################################################################################

                [Containments][8]
                activityId=
                formfactor=2
                immutability=${immutability}
                lastScreen=0
                location=4
                plugin=org.kde.plasma.private.systemtray
                popupHeight=480
                popupWidth=480
                wallpaperplugin=org.kde.image

                [Containments][8][Applets][10]
                immutability=${immutability}
                plugin=org.kde.plasma.notifications

                [Containments][8][Applets][11]
                immutability=${immutability}
                plugin=org.kde.plasma.devicenotifier

                [Containments][8][Applets][11][Configuration][General]
                popupOnNewDevice=false

                [Containments][8][Applets][12]
                immutability=${immutability}
                plugin=org.kde.plasma.clipboard

                [Containments][8][Applets][13]
                immutability=${immutability}
                plugin=org.kde.plasma.printmanager

                [Containments][8][Applets][14]
                immutability=${immutability}
                plugin=org.kde.plasma.keyboardlayout

                [Containments][8][Applets][15]
                immutability=${immutability}
                plugin=org.kde.plasma.nightcolorcontrol

                [Containments][8][Applets][16]
                immutability=${immutability}
                plugin=org.kde.plasma.volume

                [Containments][8][Applets][16][Configuration]
                PreloadWeight=55

                [Containments][8][Applets][17]
                immutability=${immutability}
                plugin=org.kde.plasma.keyboardindicator

                [Containments][8][Applets][20]
                immutability=${immutability}
                plugin=org.kde.plasma.battery

                [Containments][8][Applets][21]
                immutability=${immutability}
                plugin=org.kde.plasma.networkmanagement

                [Containments][8][Applets][22]
                immutability=${immutability}
                plugin=org.kde.plasma.bluetooth

                [Containments][8][Applets][9]
                immutability=${immutability}
                plugin=org.kde.plasma.manage-inputmethod

                [Containments][8][General]
                extraItems=org.kde.plasma.battery,org.kde.plasma.mediacontroller,org.kde.plasma.manage-inputmethod,org.kde.plasma.notifications,org.kde.plasma.devicenotifier,org.kde.plasma.clipboard,org.kde.plasma.keyboardlayout,org.kde.plasma.nightcolorcontrol,org.kde.plasma.volume,org.kde.plasma.keyboardindicator,org.kde.plasma.networkmanagement,org.kde.plasma.bluetooth
                knownItems=org.kde.plasma.battery,org.kde.plasma.mediacontroller,org.kde.plasma.manage-inputmethod,org.kde.plasma.notifications,org.kde.plasma.devicenotifier,org.kde.plasma.clipboard,org.kde.plasma.keyboardlayout,org.kde.plasma.nightcolorcontrol,org.kde.plasma.volume,org.kde.plasma.keyboardindicator,org.kde.plasma.networkmanagement,org.kde.plasma.bluetooth

                ## End of system tray config
                ################################################################################
              '';
              panel = id:
                let
                  offset = num: toString (id * 100 + num);
                  isMain = id == 0;
                in
                ''
                  ################################################################################
                  ## Taskbar ${toString id}
                  ################################################################################

                  # Taskbar
                  [Containments][${offset 2}]
                  activityId=
                  formfactor=2
                  immutability=${immutability}
                  lastScreen=${toString id}
                  location=3
                  plugin=org.kde.panel
                  wallpaperplugin=org.kde.image

                  ${optionalString /*isMain*/ false ''
                    # kickoff
                    [Containments][${offset 2}][Applets][${offset 3}]
                    immutability=${immutability}
                    plugin=org.kde.plasma.kickoff
                    [Containments][${offset 2}][Applets][${offset 3}][Configuration]
                    PreloadWeight=100
                    popupHeight=544
                    popupWidth=685
                    # https://invent.kde.org/plasma/plasma-desktop/-/blob/master/applets/kickoff/package/contents/config/main.xml
                    [Containments][${offset 2}][Applets][${offset 3}][Configuration][General]
                    favoritesPortedToKAstats=true
                    icon=nix-snowflake
                    alphaSort=true
                    applicationsDisplay=1
                    favorites=preferred://browser,terminal.desktop
                    favoritesDisplay=1
                    showActionButtonCaptions=false
                    systemFavorites=suspend,reboot,shutdown
                    [Containments][${offset 2}][Applets][${offset 3}][Configuration][Shortcuts]
                    global=
                    [Containments][${offset 2}][Applets][${offset 3}][Shortcuts]
                    global=
                  ''}

                  # pager
                  [Containments][${offset 2}][Applets][${offset 4}]
                  immutability=${immutability}
                  plugin=org.kde.plasma.pager
                  [Containments][${offset 2}][Applets][${offset 4}][Configuration][General]
                  displayedText=1 # 0=Number 1=Name 2=None
                  showOnlyCurrentScreen=true

                  # taskmanager
                  [Containments][${offset 2}][Applets][${offset 5}]
                  immutability=${immutability}
                  plugin=org.kde.plasma.taskmanager
                  # https://invent.kde.org/plasma/plasma-desktop/-/blob/master/applets/taskmanager/package/contents/config/main.xml
                  [Containments][${offset 2}][Applets][${offset 5}][Configuration][General]
                  launchers=${/*preferred://browser*/""}
                  groupingStrategy=0
                  maxStripes=1
                  showOnlyCurrentScreen=true
                  showOnlyCurrentDesktop=true
                  showOnlyCurrentActivity=true
                  # tooltips routinely steal focus :<
                  showToolTips=false

                  # margin
                  [Containments][${offset 2}][Applets][${offset 6}]
                  immutability=${immutability}
                  plugin=org.kde.plasma.marginsseparator

                  ${optionalString isMain ''
                    # systemtray
                    [Containments][${offset 2}][Applets][${offset 7}]
                    immutability=${immutability}
                    plugin=org.kde.plasma.systemtray
                    [Containments][${offset 2}][Applets][${offset 7}][Configuration]
                    PreloadWeight=75
                    SystrayContainmentId=8
                  ''}

                  # digitalclock
                  [Containments][${offset 2}][Applets][${offset 18}]
                  immutability=${immutability}
                  plugin=org.kde.plasma.digitalclock
                  [Containments][${offset 2}][Applets][${offset 18}][Configuration][Appearance]
                  showSeconds=true
                  use24hFormat=2

                  # Taskbar configuration
                  [Containments][${offset 2}][General]
                  AppletOrder=${
                    concatMapStringsSep ";" (offset) ([]
                    ++ optionals isMain [
                      3   # kickoff
                    ] ++ [
                      4   # pager
                      5   # taskmanager
                      6   # margin
                    ] ++ optionals isMain [
                      7   # systemtray
                    ] ++ [
                      18  # digitalclock
                    ])
                  }

                  ## End of taskbar ${toString id}
                  ################################################################################
                ''
              ;

              desktop = id:
                let
                  offset = num: toString (id * 100 + num);
                  isMain = id == 0;
                in
                ''
                  ################################################################################
                  ## Desktop ${toString id}
                  ################################################################################

                  [Containments][${offset 1}]
                  #ItemGeometries-1920x1080=
                  #ItemGeometriesHorizontal=
                  lastScreen=${toString id}
                  # https://invent.kde.org/frameworks/plasma-framework/-/blob/f5e93034d30a2220268a6b991c6000e1507e13a3/src/plasma/plasma.h#L140-155
                  location=1
                  activityId=
                  # https://invent.kde.org/frameworks/plasma-framework/-/blob/f5e93034d30a2220268a6b991c6000e1507e13a3/src/plasma/plasma.h#L67-89
                  formfactor=0
                  immutability=${immutability}
                  plugin=org.kde.desktopcontainment
                  wallpaperplugin=org.kde.image

                  [Containments][${offset 1}][Wallpaper][org.kde.image][General]
                  Image=/etc/wallpaper

                  ## End of desktop ${toString id}
                  ################################################################################
                ''
              ;
            in
            # NOTE: Do not mark as immutable ([$i]), or else it will not launch properl
            ''
              [ActionPlugins][0]
              RightButton;NoModifier=org.kde.contextmenu

              [ActionPlugins][0][RightButton;NoModifier]
              _add panel=false
              _context=false
              _display_settings=true
              _lock_screen=true
              _logout=false
              _open_terminal=true
              _run_command=false
              _sep1=false
              _sep2=false
              _sep3=false
              _wallpaper=false
              add widgets=false
              configure=false
              configure shortcuts=false
              edit mode=false
              manage activities=false
              remove=false

              [ActionPlugins][1]
              RightButton;NoModifier=org.kde.contextmenu

              ${concatStringsSep "\n" (
                genList (num: ''
                  ${desktop num}
                  ${panel num}
                '') cfg.displayCount
              )}

              ${trayConfig}

              [ScreenMapping]
              itemsOnDisabledScreens=
              screenMapping=
            ''
          ;
        };
        "xdg/plasmarc" = {
          text = ''
            [$i]
            [Wallpapers]
            usersWallpapers=
          '';
        };
        "xdg/plasmashellrc" = {
          text = ''
            [KPropertiesDialog][$i]

            # Hopefully this prevents plasma from saving the thickness for other views
            [PlasmaViews][Panel 2][$i]
            [PlasmaViews][Panel 2][Defaults][$i]
            thickness=24
            [PlasmaViews][Panel 102][$i]
            [PlasmaViews][Panel 102][Defaults][$i]
            thickness=24
            [PlasmaViews][Panel 202][$i]
            [PlasmaViews][Panel 202][Defaults][$i]
            thickness=24
          '';
        };
      };
    }
  ]);
}
