{ config, lib, pkgs, ... }:

let
  enabled = config.samueldr.use-case.graphical-session.session == "plasma-desktop";
  cfg = config.samueldr.use-case.plasma.desktop;
  inherit (lib)
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  imports = [
    ./desktop-shell.nix
    ./kwin.nix
  ];

  options.samueldr.use-case.plasma.desktop = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable use of Plasma.
      '';
      internal = true;
    };
  };
  config = mkMerge [
    { samueldr.use-case.graphical-session.availableSessions = [ "plasma-desktop" ]; }
    { samueldr.use-case.plasma.desktop.enable = enabled; }
    (mkIf cfg.enable (mkMerge [
      {
        samueldr.use-case.plasma.common.enable = true;
        environment.plasma5.excludePackages = with pkgs; with libsForQt5; [
          elisa
          okular
          khelpcenter
          print-manager
        ];
        services.xserver = {
          desktopManager.plasma5 = {
            enable = true;
          };
          displayManager.defaultSession = "plasmawayland";
        };
        samueldr.hacks.remove-applications.patterns = [
          "*kinfocenter*desktop"
          "*kmenuedit*desktop"
        ];
      }
    ]))
  ];
}
