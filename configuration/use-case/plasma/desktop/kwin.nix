{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.desktop;
  inherit (lib)
    mkMerge
    mkIf
  ;
in
{
  config = mkIf cfg.enable ( mkMerge [
    {
      environment.etc = {
        "xdg/kwinrc" = {
          # The whole file itself isn't immutable
          text = ''
            [Input][$i]

            [Windows][$i]
            FocusPolicy=FocusFollowsMouse
            DelayFocusInterval=0
            RollOverDesktops=true
            SeparateScreenFocus=true
          '';
        };
      };
    }
  ]);
}
