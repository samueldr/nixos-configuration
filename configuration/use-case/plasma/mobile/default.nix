{ config, lib, pkgs, ... }:

let
  enabled = config.samueldr.use-case.graphical-session.session == "plasma-mobile";
  cfg = config.samueldr.use-case.plasma.mobile;
  inherit (lib)
    mkDefault
    mkMerge
    mkOption
    mkIf
    types
  ;
in
{
  imports = [
    ./kwin.nix
    ./mobile-shell.nix
  ];

  options.samueldr.use-case.plasma.mobile = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable use of Plasma Mobile.
      '';
      internal = true;
    };
  };
  config = mkMerge [
    { samueldr.use-case.graphical-session.availableSessions = [ "plasma-mobile" ]; }
    { samueldr.use-case.plasma.mobile.enable = enabled; }
    (mkIf cfg.enable ( mkMerge [
      {
        samueldr.use-case.plasma.common = {
          enable = true;
          kwin.scale = mkDefault "1.5";
        };
        services.xserver = {
          desktopManager.plasma5.mobile.enable = true;
          displayManager.defaultSession = "plasma-mobile";
        };
      }
    ]))
  ];
}
