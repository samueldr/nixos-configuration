{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.plasma.mobile;
  inherit (lib)
    concatStringsSep
    concatMapStringsSep
    mkMerge
    mkIf
  ;
in
{
  config = mkIf cfg.enable ( mkMerge [
    {
      system.userActivationScripts.samueldr-plasma-mobile-shell = ''
        (
        cd ~/.config
        for f in plasma-org.kde.plasma.phoneshell-appletsrc plasmamobilerc; do
          if ! test -L "$f"; then
            rm -f "$f"
          fi
          ln -sf /etc/xdg/"$f"
        done
        )
      '';

      environment.etc = {
        # This configures the home screen for the user.
        # Anyway wallpaper and pinned apps reset by immutability in a containment.
        # NOTE: These settings don't work like the other plasma settings.
        #       The shell will not merge configs from this file into the final file.
        #       This file needs to be linked in the user's `$XDG_CONFIG_HOME` folder.
        "xdg/plasma-org.kde.plasma.phoneshell-appletsrc" = {
          text =
            let
              # TODO: Submodule-based structured NixOS config for halcyon.
              # NOTE: Additional bogus apps not found in the system are skipped.
              pinned = [
                {"storageId" = "firefox.desktop"; "type" = "application"; }
                {"storageId" = "chromium-browser.desktop"; "type" = "application"; }
                {"storageId" = "quasselclient.desktop"; "type" = "application"; }
                {"storageId" = "terminal.desktop"; "type" = "application"; }

                # Example for a folder:
                #{
                #  type = "folder";
                #  name = "Some folder";
                #  "apps" = [
                #    "quasselclient.desktop"
                #    "org.kde.plasma.emojier.desktop"
                #  ];
                #}
              ];
            in
            ''
              [$i]
              [Containments][2]
              activityId=
              formfactor=2
              immutability=1
              lastScreen=0
              location=3
              plugin=org.kde.phone.panel
              wallpaperplugin=org.kde.image

              [Containments][2][Applets][3]
              immutability=1
              plugin=org.kde.plasma.notifications

              [Containments][3]
              Pinned=[${concatMapStringsSep "," (entry: builtins.toJSON entry) pinned}]
              activityId=
              formfactor=0
              immutability=0
              lastScreen=0
              location=0
              plugin=org.kde.phone.homescreen.halcyon
              wallpaperplugin=org.kde.image

              [Containments][3][Wallpaper][org.kde.image][General]
              Image=/etc/wallpaper

              [Containments][4]
              activityId=
              formfactor=3
              immutability=1
              lastScreen=0
              location=6
              plugin=org.kde.phone.taskpanel
              wallpaperplugin=org.kde.image
            ''
          ;
        };
        "xdg/plasmamobilerc" = {
          text =
            let
              disabled = [
                "org.kde.plasma.quicksetting.settingsapp"
                "org.kde.plasma.quicksetting.nightcolor"

                # TODO except if phone
                "org.kde.plasma.quicksetting.mobiledata"
                "org.kde.plasma.quicksetting.donotdisturb"
                "org.kde.plasma.quicksetting.flashlight"
              ];
              enabled = [
                "org.kde.plasma.quicksetting.wifi"
                "org.kde.plasma.quicksetting.bluetooth"
                "org.kde.plasma.quicksetting.airplanemode"

                "org.kde.plasma.quicksetting.caffeine"
                "org.kde.plasma.quicksetting.battery"
                "org.kde.plasma.quicksetting.location"

                "org.kde.plasma.quicksetting.screenrotation"
                "org.kde.plasma.quicksetting.keyboardtoggle"
                "org.kde.plasma.quicksetting.powermenu"

                "org.kde.plasma.quicksetting.audio"
                "org.kde.plasma.quicksetting.screenshot"
                "org.kde.plasma.quicksetting.record"
              ];
            in
            ''
              [$i]
              [QuickSettings]
              disabledQuickSettings=${concatStringsSep "," disabled}
              enabledQuickSettings=${concatStringsSep "," enabled}
            ''
          ;
        };
      };
    }
  ]);
}
