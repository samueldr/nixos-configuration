{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.tmux;
  inherit (lib)
    makeBinPath
    mkEnableOption
    mkForce
    mkIf
  ;
in
{
  options.samueldr.use-case.tmux = {
    enable = mkEnableOption "tmux user service";
  };
  config = mkIf cfg.enable {
    systemd.user.services = {
      tmux = {
        enable = true;

        environment.PATH = mkForce (makeBinPath (with pkgs;[
          # run-shell based scripts assume coreutils is present
          coreutils
          # If tmux is missing, run-shell based scripts will fail.
          tmux
        ]));

        wantedBy = [ "default.target" ];
        serviceConfig = {
          WorkingDirectory = "/Users/samuel/";
          Type = "forking";
          KillMode = "control-group";
          RestartSec = "2";
          ExecStart = pkgs.writeShellScript "tmux-start" ''
            # The tmux clients will misbehave if the environment is too thin here.
            # This will give us at the very least the expected PATH.
            eval $( ${pkgs.gnused}/bin/sed -e 's/\s\+DEFAULT=/=/' -e 's/PAM_USER/USER/' -e 's/@{/''${/g' < /etc/pam/environment )

            exec ${pkgs.tmux}/bin/tmux start-server
          '';
        };
        unitConfig = {
          ConditionPathExists = "/run/user/%U";
        };
        # Ensure we don't needlessly kill any sessions
        stopIfChanged = false;
        restartIfChanged = false;
        restartTriggers = [];
      };
    };
  };
}
