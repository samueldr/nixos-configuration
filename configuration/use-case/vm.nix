# Options for the VM build
{ config, lib, pkgs, options, ... }:

let
  inherit (lib)
    mkAfter
    mkBefore
    mkIf
  ;
in
{
  config =
    if (options.virtualisation ? qemu)
    then {
      # When building the VM, ensure early KMS happens.
      boot.initrd.kernelModules = [
        #"bochs"
        "virtio-gpu"
      ];

      # Serial output on the launching terminal.
      virtualisation.qemu.options = mkAfter [
        "-serial" "mon:stdio"
        "-vga" "virtio"
      ];

      # Enables multiplexing to ttyS0, but *don't make it the main console*.
      boot.kernelParams = mkBefore [
        "console=ttyS0"
      ];
    }
    else { }
  ;
}
