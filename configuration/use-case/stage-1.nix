{ config, lib, options, pkgs, ... }:

let
  cfg = config.samueldr.use-case.stage-1;
  inherit (lib)
    mkDefault
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.stage-1 = {
    availableStage-1 = mkOption {
      type = with types; listOf str;
      internal = true;
    };
    chosen = mkOption {
      type = types.enum cfg.availableStage-1;
      default = "systemd";
      description = ''
        Graphical session to use.
      '';
    };
  };
  config = mkMerge [
    {
      samueldr.use-case.stage-1.availableStage-1 = [
        "nixos"
        "systemd"
      ];
      boot.initrd.systemd.enable = cfg.chosen == "systemd";
      boot.initrd.enable = cfg.chosen == "nixos" || cfg.chosen == "systemd";
    }
  ];
}
