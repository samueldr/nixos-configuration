{ config, lib, options, pkgs, ... }:

let
  cfg = config.samueldr.use-case.mobile-nixos;
  channels = import ../../npins;
  inherit (lib)
    mkIf
    mkOption
    mkMerge
    types
  ;
in
{
  imports = [
    { imports = import (channels.mobile-nixos.outPath + "/modules/module-list.nix"); }
  ];
  options.samueldr.use-case.mobile-nixos = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable my usual Mobile NixOS usage. For Mobile NixOS devices.
      '';
    };
  };
  config = mkMerge [
    (mkIf (!cfg.enable) (
      # Ensure evaluation works even when Mobile NixOS is not imported.
      if options ? mobile
      then {
        mobile.enable = false;
      }
      else {}
    ))
    (mkIf cfg.enable (
      # Ensure evaluation works even when Mobile NixOS is not imported.
      if options ? mobile
      then {
        # Clearly state we use Mobile NixOS.
        mobile.enable = true;
        hardware.firmware = [ config.mobile.device.firmware ];
        mobile.beautification = {
          silentBoot = lib.mkDefault true;
          splash = lib.mkDefault true;
        };
        # This conflicts with Mobile NixOS's own beautification.
        # And anyway won't work as it requires the firmware to leave a logo...
        samueldr.use-case.graphical-boot.flickerFree.enable = false;
        # Ensure we're using the Mobile NixOS semantics for the kernel.
        samueldr.settings.kernel.enable = false;
        # For now the patches aren't composed into the target config
        gobohide.enable = lib.mkForce false;
        # Disable console color preset when using Mobile NixOS
        # This is to better compose with the logo semantics.
        console.colorPreset = null;
      }
      else {}
    ))
    (mkIf cfg.enable {
      # Ensure we fail the build if this is enabled and Mobile NixOS is not available.
      assertions = [
        {
          assertion = options ? mobile;
          message = "Mobile NixOS configuration is enabled, but Mobile NixOS is not imported.";
        }
      ];
    })
  ];
}
