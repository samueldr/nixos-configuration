{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.eink;
  inherit (lib)
    escapeShellArg
    mkForce
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.eink = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Defines a system using a eink display.
      '';
    };
  };
  config = mkMerge [
    (mkIf cfg.enable {
      # TODO: configure convergent session configs:
      #   - add refresh option in menu
      #   - change theme
      #   - change clock

      hacks.mergedIcons = {
        masqueradeAs = [
          "HighContrast"
        ];
        iconThemes =  mkForce [
          # Desired icon theme
          "${pkgs.gnome.gnome-themes-extra}/share/icons/HighContrast"
        ];
      };
      samueldr.convergent-session = {
        theme = {
          applications = {
            theme = lib.mkForce "HighContrast";
          };
          icons = {
            theme = lib.mkForce "HighContrast";
          };
        };
      };
      environment.systemPackages = [
        pkgs.eink-friendly-launcher
      ];
      nixpkgs.overlays = [(final: super: {
        eink-friendly-launcher = final.callPackage (
          let
            rev = "d07f9028a7afe049981b9a720a37df73a8e144df";
            archive = builtins.fetchTarball {
              url = "https://github.com/samueldr/eink-friendly-launcher/archive/${rev}.tar.gz";
              sha256 = "sha256:0vsfjjvhz7kfnzkaw85lna5pwljkna9l4y3s6n8ldpyrbc38qwhd";
            };
          in
          "${archive}/package.nix"
        ) { };
      })];
      # Ensure the xdg autostart file is ran.
      systemd.user.services = (
        config.lib.samueldr.convergent-session.userServiceFromAutostart
        { name = "com.samueldr.EinkFriendlyLauncher"; type = "oneshot"; }
      );
    })
  ];
}
