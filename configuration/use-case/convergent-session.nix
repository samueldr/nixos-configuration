{ config, lib, pkgs, ... }:

let
  enabled = config.samueldr.use-case.graphical-session.session == "convergent-session";
  inherit (lib)
    mkMerge
    mkIf
  ;
in
{
  config = mkMerge [
    { samueldr.use-case.graphical-session.availableSessions = [ "convergent-session" ]; }
    (mkIf enabled ( mkMerge [
      {
        # Use my custom terminal.
        samueldr.use-case.app.terminal.enable = true;
        # Use my screenshot app.
        samueldr.use-case.app.screenshot.enable = true;
        samueldr.programs.screenshot = {
          environment = "X11";
        };

        samueldr.convergent-session.enable = true;
        samueldr.convergent-session.autoLogin = {
          enable = true;
          user = "samuel";
        };
        samueldr.convergent-session.theme = {
          icons.theme = "merged-icons";
        };

        services.xbindkeys.enable = true;
        services.xbindkeys.bindings = {
          "samueldr-terminal" = {
            command = "${pkgs.samueldr-convergent-session.with-profile} terminal";
            keys = ["Mod1 + Return"];
          };

          "samueldr-screenshot" = {
            command = "${pkgs.samueldr-convergent-session.with-profile} screenshot -s";
            keys = [
              "Control + Mod2 + XF86Tools"
              "Control + XF86Tools"
              "Control + Print"
            ];
          };

          "media-prev" = {
            command = "${pkgs.playerctl}/bin/playerctl --player=mpv,audacious,cmus previous";
            keys = [
              "XF86AudioPrev"
            ];
          };
          "media-next" = {
            command = "${pkgs.playerctl}/bin/playerctl --player=mpv,audacious,cmus next";
            keys = [
              "XF86AudioNext"
            ];
          };
          "media-play-pause" = {
            command = "${pkgs.playerctl}/bin/playerctl --player=mpv,audacious,cmus play-pause";
            keys = [
              "XF86AudioPlay"
              "m:0x10 + c:208" # Headset
            ];
          };

          "screen-locker" = {
            command = "${pkgs.samueldr-convergent-session.with-profile} convergent.locker";
            keys = [
              "Control + Alt + Delete"
              "Control + Alt + BackSpace"
            ];
          };

          "screen-autoconfigure" = {
            command = "${pkgs.samueldr-convergent-session.with-profile} autorandr --change --default default --match-edid --force";
            keys = ["Control+Shift+Mod4 + p"];
          };
          "screen-common" = {
            command = "${pkgs.samueldr-convergent-session.with-profile} autorandr common";
            keys = ["Control+Shift+Mod4 + o"];
          };

          "brightness.increase" = {
            command = "${pkgs.samueldr-convergent-session.with-profile} light -A 1";
            keys = [
              "XF86MonBrightnessUp"
              "m:0x80 + c:233" # Chromebook
              "m:0x80 + c:73" # Chromebook (lazor)
            ];
          };
          "brightness.decrease" = {
            command = "${pkgs.samueldr-convergent-session.with-profile} light -U 1";
            keys = [
              "XF86MonBrightnessDown"
              "m:0x80 + c:232" # Chromebook
              "m:0x80 + c:72" # Chromebook (lazor)
            ];
          };

          # Forcibly hijacks some buttons so they do nothing bad.
          "keyboard.disable-home" = {
            command = "true";
            keys = [
              "XF86HomePage"
              "m:0x0 + b:13"
              "m:0x0 + b:10"
            ];
          };
        };

        services.unclutter-xfixes.enable = true;
        services.unclutter-xfixes.extraOptions = [
          "jitter 4"
          "ignore-scrolling"
        ];

        programs.light.enable = true;

        app-compartmentalization = {
          scoped-apps = {
            # Default software
            rox-filer.enable = true;
            eog.enable = true;
            file-roller.enable = true;

            gnome-disk-utility.enable = true;
            arandr.enable = true;
          };
        };

        #
        # Use autorandr to automatically configure displays
        #

        services.autorandr.enable = true;
        systemd.services.autorandr = {
          serviceConfig = {
            # Using `--match-edid` helps with an MST dock where the displays move about indexes.
            ExecStart = lib.mkForce "${pkgs.autorandr}/bin/autorandr --batch --change --default default --match-edid";
          };
        };

        services.udev.extraRules = ''
          ACTION=="change", SUBSYSTEM=="drm", ENV{SYSTEMD_WANTS}="autorandr.service"
        '';

        systemd.user.services = config.lib.samueldr.convergent-session.userServiceFromAutostart { name = "autorandr"; type = "oneshot"; };

        environment.etc."xdg/autorandr/postswitch" = {
          mode = "755";
          text = ''
            #!${pkgs.stdenv.shell}
            echo "Screen configuration (postswitch)"
            systemctl --user restart convergent-session.window-manager.service
          '';
        };
      }
      {
        services.redshift = {
          enable = true;
          executable = "/bin/redshift-gtk";
        };
      }
      (mkIf config.hardware.bluetooth.enable {
        # We don't want blueman installed in the global scope.
        # See <services/desktops/blueman.nix>
        # services.blueman.enable = true;
        services.dbus.packages = [ pkgs.blueman ];
        systemd.packages = [ pkgs.blueman ];

        # Add a user service for blueman
        systemd.user.services = lib.mkMerge [
          (config.lib.samueldr.convergent-session.userServiceFromAutostart { name = "blueman"; package = pkgs.blueman; })
        ];
      })
    ]))
  ];
}
