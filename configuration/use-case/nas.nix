{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.nas;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.use-case.nas = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable NAS config.
      '';
    };
  };
  config = mkIf cfg.enable {
    # Somehow `sshfs` breaks less than NFS, and has a better failure mode...
    # Go figure :/
    fileSystems."/net/NAS/data" = {
      device = "${pkgs.sshfs-fuse}/bin/sshfs#samuel@duffman.lan:/Volumes/data";
      fsType = "fuse";
      noCheck = true;
      options = [
        "_netdev"
        "x-systemd.idle-timeout=600"
        "x-systemd.automount"
        "noauto"
        "nofail"
        "noatime"
        "users"
        "IdentityFile=/Users/samuel/.ssh/keys/sshfs"
        "StrictHostKeyChecking=no"
        "UserKnownHostsFile=/dev/null"
        "allow_other"
        "reconnect"
      ];
    };
  };
}
