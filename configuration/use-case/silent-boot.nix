{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.silent-boot;
  inherit (lib)
    concatStrings
    genList
    mkAfter
    mkDefault
    mkForce
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.silent-boot = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable silent boot
      '';
    };
  };
  config = mkMerge [
    (mkIf cfg.enable {
      # Push login text to the bottom
      environment.etc.issue = lib.mkForce {
        text = concatStrings (genList (x: "\n") 100);
      };

      # Make things quieter
      boot.kernelParams = [
        "quiet"
        "rd.systemd.show_status=auto"
        "rd.udev.log-priority=3"
        "systemd.show_status=auto"
        "udev.log-priority=3"
      ];
      boot.consoleLogLevel = mkDefault 3;
    })
    (mkIf (cfg.enable && config.samueldr.use-case.stage-1.chosen == "nixos" ) {
      # Clear and push-down text opportunistically
      boot.initrd.preDeviceCommands = mkAfter ''
        clear
        (i=100; while [[ $((i--)) -gt 0 ]]; do echo ; done)
      '';

      boot.initrd.preLVMCommands = mkAfter ''
        clear
        (i=100; while [[ $((i--)) -gt 0 ]]; do echo ; done)
      '';

      boot.initrd.postDeviceCommands = mkAfter ''
        clear
        (i=100; while [[ $((i--)) -gt 0 ]]; do echo ; done)
      '';

      boot.initrd.postMountCommands = mkAfter ''
        clear
        (i=100; while [[ $((i--)) -gt 0 ]]; do echo ; done)
      '';

      boot.postBootCommands = mkAfter ''
        clear
        (i=100; while [[ $((i--)) -gt 0 ]]; do echo ; done)
      '';
    })
  ];
}
