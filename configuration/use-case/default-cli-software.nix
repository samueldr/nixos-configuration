{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.default-cli-software;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.use-case.default-cli-software = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable default CLI software.
      '';
    };
  };
  config = mkIf cfg.enable {
    samueldr.use-case.tmux.enable = true;
    samueldr.use-case.aliases.enable = true;

    environment.systemPackages = with pkgs; [
      # Utilities
      file
      moreutils
      ranger
      silver-searcher
      sshfs-fuse
      trash-cli

      # System
      htop
      ncdu

      # System facts
      input-utils
      pciutils
      usbutils

      # Basic development
      gitAndTools.gitFull
      tig
      man-pages

      # Networking
      whois

      # Internet
      aria2
      wget

      # Misc extraction tools
      p7zip
      unrar
      unzip

      # Some of my own tools
      burninate

      # Some useful Nix utilities
      (writeShellScriptBin "nix-du" ''nix-store --query --requisites "$@" | xargs du -csh | sort -h'')
    ];
    samueldr.hacks.remove-applications.patterns = [
      "htop.desktop"
      "ranger.desktop"
      "nvim.desktop"
    ];
    programs.command-not-found.enable = false;
    programs.nix-index.enable = true;
    programs.nix-index.enableBashIntegration = true;
  };
}
