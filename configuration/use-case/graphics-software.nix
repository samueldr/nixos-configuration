{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.graphics-software;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.use-case.graphics-software = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable Graphics software.
      '';
    };
  };
  config = mkIf cfg.enable {
    app-compartmentalization = {
      scoped-apps = {
        gcolor3.enable = true;
        gimp.enable = true;
        inkscape.enable = true;
        krita.enable = true;
      };
    };
  };
}
