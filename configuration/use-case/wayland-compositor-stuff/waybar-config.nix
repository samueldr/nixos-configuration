{ config, lib, ... }:
let
  enabled = config.samueldr.use-case.wayland-session.enable;
in
lib.mkIf (enabled) {
  environment.etc."xdg/waybar/config.jsonc" = {
    text = ''
      {
          "layer": "top",
          "height": 34,
          "spacing": 2,
          "reload_style_on_change": true,
          "modules-left": [
              "image#me",
              // "wlr/taskbar",
              "cpu",
              "memory",
              "temperature",
          ],
          "modules-center": [
          ],
          "modules-right": [
              //"idle_inhibitor",
              "backlight",
              "battery",
              "tray",
              "custom/notification",
              "clock",
          ],
          "image#me": {
            "path": "${../../../artwork/bar.svg}",
            "size": 57,
          },
          "battery": {
              "full-at": 97,
              "format": "{icon} {capacity:3}%",
              "format-icons": ["", "", "", "", ""],
              "tooltip-format": "{timeTo}",
          },
          "clock": {
              "interval": 1,
              "format": "{:%m/%d %H:%M:%S}"
              //"format-alt": "{:%a, %d. %b  %H:%M}"
          },

          "cpu": {
              "interval": 1,
              "format": "\uf2db{usage:3}%",// ({avg_frequency}/{min_frequency}/{max_frequency})",
              "tooltip": true,
          },

          "memory": {
              "format": "\uf538{percentage:3}%",
          },

          "temperature": {
              "format": "\uf2cb{temperatureC:3}°C",
          },

          "idle_inhibitor": {
              "format": "{icon}",
              "format-icons": {
                  "activated": "",
                  "deactivated": ""
              }
          },
          "wlr/taskbar": {
              "on-click": "activate",
          },

          "backlight": {
              "format": "\uf042{percent:3}%",
              "on-scroll-down": "light -T 0.9",
              "on-scroll-up":   "light -T 1.1",
              "reverse-scrolling": true,
              "smooth-scrolling-threshold": 2,
          },

          "tray": {
              "icon-size": 32,
              "show-passive-items": true,
              "spacing": 2,
          },

          "custom/notification": {
              "tooltip": false,
              "format": "{icon}",
              "format-icons": {
                   "notification":			   "\uF0F3<span foreground='red'><sup>\uf111</sup></span>",
                   "none":					   "\uF0F3",
                   "dnd-notification":		   "\uF1F6<span foreground='red'><sup>\uf111</sup></span>",
                   "dnd-none":				   "\uF1F6",
                   "inhibited-notification":	 "\uF1F6<span foreground='red'><sup>\uf111</sup></span>",
                   "inhibited-none":			 "\uF1F6",
                   "dnd-inhibited-notification": "\uF1F6<span foreground='red'><sup>\uf111</sup></span>",
                   "dnd-inhibited-none":		 "\uF1F6"
              },
              "return-type": "json",
              "exec-if": "swaync-client --version",
              "exec": "swaync-client -swb",
              "on-click": "swaync-client -t -sw",
              "on-click-right": "swaync-client -d -sw",
              "escape": true
          },
      }
    '';
  };
  environment.etc."xdg/waybar/style.css" = {
    text = ''
      * {
          /* `otf-font-awesome` is required to be installed for icons */
          font-family: FontAwesome, Go Mono;
          font-size: 19px;
      }

      window#waybar {
          background-color: #fff;
          border-bottom: 0;
          color: #000;
          transition-property: background-color;
          transition-duration: .5s;
      }

      window#waybar.hidden {
          opacity: 0.2;
      }

      button {
          /* Use box-shadow instead of border so the text isn't offset */
          box-shadow: inset 0 -3px transparent;
          /* Avoid rounded borders under each button name */
          border: none;
          border-radius: 0;
      }

      /* https://github.com/Alexays/Waybar/wiki/FAQ#the-workspace-buttons-have-a-strange-hover-effect */
      button:hover {
          background: inherit;
          box-shadow: inset 0 -3px #ffffff;
      }

      /* you can set a style on hover for any module like this */

      @keyframes blink {
          to {
              background-color: #ffffff;
              color: #000000;
          }
      }

      /* Using steps() instead of linear as a timing function to limit cpu usage */
      #battery.critical:not(.charging) {
          background-color: #f53c3c;
          color: #ffffff;
          animation-name: blink;
          animation-duration: 0.5s;
          animation-timing-function: steps(12);
          animation-iteration-count: infinite;
          animation-direction: alternate;
      }

      .module {
      }
      .modules-left > *:not(:first-child) > :first-child {
          padding-left: 1.6ex;
      }

      .modules-right > *:not(:last-child) > :last-child {
          padding-right: 1.6ex;
      }

      #clock {
          padding-right: 1.2ex;
      }

      #memory, #temperature {
          min-width: 7ex;
      }

      #cpu {
          min-width: 8ex;
          padding-left: 1.2ex;
      }
    '';
  };
}
