{ config, lib, ... }:
let
  enabled = config.samueldr.use-case.wayland-session.enable;
in
lib.mkIf (enabled) {
  environment.etc."xdg/gtk-3.0/settings.ini" = {
    # TODO: sync with compositor config and GTK4
    text = ''
      [Settings]
      gtk-cursor-theme-name=${config.environment.variables.XCURSOR_THEME}
      gtk-cursor-theme-size=${config.environment.variables.XCURSOR_SIZE}
    '';
  };
  programs.dconf = {
    enable = true;
  };
}
