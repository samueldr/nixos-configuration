{ config, lib, pkgs, ... }:

let
  enabled = config.samueldr.use-case.wayland-session.enable;
  inherit (lib)
    mkAfter
    mkForce
    mkIf
    mkMerge
    mkOption
    types
  ;

  inherit (config.lib.samueldr.convergent-session)
    userServiceFromAutostart
  ;

  baseService = {
    enable = true;
    serviceConfig = {
      Restart = "always";
      RestartSec= "5";
    };
    environment.PATH = mkForce null;
    wantedBy = [ "desktop-session.target" ];
    partOf = [ "desktop-session.target" ];
  };
in
{
  imports = [
    ./gtk-config.nix
    ./locker.nix
    ./swaync-config.nix
    ./waybar-config.nix
  ];
  options.samueldr.use-case.wayland-session = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Useful bits and bobs for wayland-based sessions.
      '';
    };
  };
  config = mkMerge [
    (mkIf enabled ( mkMerge [
      { # Autologin configuration
        services.displayManager = {
          enable = true;
          defaultSession = config.samueldr.use-case.graphical-session.session;
          autoLogin = {
            enable = true;
            user = "samuel";
          };
        };
        samueldr.autologin = {
          enable = true;
          session = lib.mkDefault config.samueldr.use-case.graphical-session.session;
        };
      }
      {
        services.displayManager.ly.enable = false;
        services.displayManager.sddm.enable = false;
        services.xserver.displayManager.gdm.enable = false;
        services.xserver.displayManager.lightdm.enable = false;
      }
      {
        environment.systemPackages = with pkgs; [
          # For the cursor
          adwaita-icon-theme
        ];

        systemd.user.targets."desktop-session" = {
          enable = true;
          bindsTo = [ "graphical-session.target" ];
          # We don't want to have it started by `graphical-session` or else
          # it will cause issues when Steam starts with Jovian NixOS.
          # The session script launcher restarts this target anyways.
          wantedBy = [ /*"graphical-session.target"*/ ];
        };

        environment.variables = {
          "XCURSOR_THEME"   = "Adwaita";
          "XCURSOR_SIZE"    = "48";    
          "QT_QPA_PLATFORM" = "wayland";
        };

        samueldr.convergent-session.theme = {
          icons.theme = "merged-icons";
        };

        # Use my screenshot app.
        samueldr.use-case.app.screenshot.enable = true;
        samueldr.programs.screenshot = {
          environment = "Wayland";
        };

        programs.light.enable = true;

        app-compartmentalization = {
          scoped-apps = {
            # Default software
            pcmanfm.enable = true;
            eog.enable = true;
            file-roller.enable = true;
            gnome-disk-utility.enable = true;
          };
        };

        # Tray type things
        systemd.user.services = lib.mkMerge [
          (userServiceFromAutostart { name = "nm-applet"; package = pkgs.networkmanagerapplet; })
        ];

        # For dbus service activation and such...
        services.blueman.enable = true;

        # Let the graphical environment handle all of this.
        services.logind = {
          powerKey = "lock";
          suspendKey = "lock";
          lidSwitch = "lock";
          # Works around the situation where the system is on power, lid closed, then disconnected...
          lidSwitchExternalPower = "lock";
          # ... except if e.g. external displays are connected.
          lidSwitchDocked = "ignore";

          rebootKey = "ignore";
          hibernateKey = "ignore";

          extraConfig = mkAfter ''
            # Wait only one second for peripherals before handling new events after suspend.
            HoldoffTimeoutSec=1
          '';
        };

        environment.variables = {
          # Those are lies, but it fixes some issues.
          XDG_CURRENT_DESKTOP= lib.mkDefault "xfce4";
          DESKTOP_SESSION = lib.mkDefault "gnome";
        };
      }
      {
        # Fails to run. I believe it's because I'm not enabling / using pipewire...
        # ... and in turn this prevents waybar from starting quickly since portals need to all be resolved.
        systemd.user.services."xdg-desktop-portal-wlr" = {
          enable = false;
          wantedBy = mkForce [ ];
        };
      }
      { # kanshi
        systemd.user.services."kanshi" = mkMerge [baseService {
          serviceConfig = {
            ExecStart = pkgs.writeShellScript "kanshi-but-with-less-fail" ''
              if test -e "$HOME/.config/kanshi/config"; then
                exec ${pkgs.with-profile} ${pkgs.kanshi}/bin/kanshi
              else
                exec -a '<ERROR>kanshi' ${pkgs.samueldr-hang-utility}/bin/hang '(missing config...)'
              fi
            '';
          };
        }];
      }
      { # waybar
        systemd.user.services."waybar" = mkMerge [baseService {
          serviceConfig = {
            ExecStart = ''
              ${pkgs.with-profile} ${pkgs.waybar}/bin/waybar
            '';
          };
        }];
      }
      { # swaybg
        systemd.user.services."swaybg" = mkMerge [baseService {
          serviceConfig = {
            ExecStart = ''
              ${pkgs.with-profile} ${pkgs.swaybg}/bin/swaybg --image "/etc/wallpaper" --mode fill
            '';
          };
        }];
      }
      { # swaync
        environment.systemPackages = with pkgs; [
          swaynotificationcenter
        ];
        systemd.user.services."swaync" = mkMerge [baseService {
          serviceConfig = {
            ExecStart = ''
              ${pkgs.with-profile} ${pkgs.swaynotificationcenter}/bin/swaync
            '';
          };
        }];
      }
      {
        systemd.user.services."sleep-and-lock-targets-listener" = mkMerge [baseService {
          serviceConfig = {
            ExecStart = ''
              ${pkgs.with-profile} ${pkgs.swayidle}/bin/swayidle \
              before-sleep '${pkgs.systemd}/bin/systemctl --user start before-sleep.target' \
              lock         '${pkgs.systemd}/bin/systemctl --user start session-locked.target' \
              unlock       '${pkgs.systemd}/bin/systemctl --user start session-unlocked.target'
            '';
          };
        }];
        systemd.user.targets."before-sleep" = {
          enable = true;
          requisite = [ "desktop-session.target" ];
        };
        systemd.user.targets."session-locked" = {
          enable = true;
          requisite = [ "desktop-session.target" ];
          unitConfig = {
            Conflicts = [
              "session-unlocked.target"
            ];
          };
        };
        systemd.user.targets."session-unlocked" = {
          enable = true;
          requisite = [ "desktop-session.target" ];
          unitConfig = {
            Conflicts = [
              "session-locked.target"
            ];
          };
        };
      }
      (
        let
          event-logger = 
          source:
          {
            systemd.user.services."on.${source}.event-logger" = {
              wantedBy = [ "${source}.target" ];
              serviceConfig = {
                Type = "oneshot";
                RemainAfterExit = false;
              };
              script = ''
                echo " :: Logging event from source '${source}'."
              '';
            };
          };
        in mkMerge [
          (event-logger "before-sleep")
          (event-logger "session-locked")
          (event-logger "session-unlocked")
        ]
      )
      {
        systemd.user.services."on.before-sleep.evict-ssh-agent" = {
          wantedBy = [ "before-sleep.target" ];
          serviceConfig = {
            Type = "oneshot";
            RemainAfterExit = false;
          };
          script = ''
            echo " :: Evicting keys from the SSH Agent "
            ${pkgs.with-profile} ${pkgs.openssh}/bin/ssh-add -D
          '';
        };
      }
      { # KeepassXC
        # Autostart
        systemd.user.services."keepassxc" = {
          after = [ "waybar.service" ];
          wantedBy = [ "desktop-session.target" ];
        };
      }
      { # Redshift
        # Configure the redshift service to use gammastep
        services.redshift = {
          enable = true;
          package = pkgs.gammastep;
          executable = "/bin/gammastep-indicator";
        };
        systemd.user.services.redshift = {
          partOf = mkForce baseService.partOf;
          wantedBy = mkForce baseService.wantedBy;
        };
        # But disable the actual gammastep user service.
        systemd.user.services.gammastep = {
          enable = false;
          overrideStrategy = "asDropin";
          partOf = [ /* "desktop-session.target" */ ];
          wantedBy = [ /* "desktop-session.target" */ ];
        };
      }
      ( # Launcher
        let
          # Wrapper around "normal" rofi
          rofi = pkgs.writeScriptBin "rofi" ''
            #!${pkgs.stdenv.shell}
            set -u

            if (( $# < 1 )); then
                set -- "-show" "run"
            fi

            ARGS=(
              -i # case-insensitive
              -combi-modes run,drun,window,ssh
              -config "${rofiConfig}"
            )

            serviceName="orphaned.keyboard-launcher.child.$(${pkgs.util-linux}/bin/uuidgen)"

            exec \
              systemd-run \
                --user \
                --collect \
                --quiet \
                --service-type=forking \
                --unit="$serviceName" \
              ${pkgs.with-profile} \
              ${pkgs.rofi-wayland}/bin/rofi "''${ARGS[@]}" "$@"
          '';

          # https://github.com/davatorium/rofi/blob/next/doc/rofi-theme.5.markdown#supported-properties
          # https://github.com/davatorium/rofi/blob/next/doc/rofi-theme.5.markdown#supported-element-path
          rofiConfig = pkgs.writeText "config.rasi" ''
            configuration {
              font: "Go Mono 16";
              show-icons: true;
              click-to-exit: ${if true then "true" else "false"};
              location: 2;
            }

            // Ensures no theme is loaded
            @theme "/dev/null"

            * {
              fg: #000000;
              bg: #ffffff;
              text-color: inherit;
              background-color: inherit;
              position: center;
            }

            entry, element-text {
            }
            element-text {
              vertical-align: 0.5;
            }
            element, prompt, entry {
              padding: 0.2em;
            }
            element normal {
              cursor: pointer;
              border: 0;
              text-color: @fg;
              background-color: @bg;
            }
            button selected,
            element.selected.normal,
            element.selected.urgent,
            element.selected.active {
              text-color: @bg;
              background-color: @fg;
            }
            list-view {
              scrollbar-width: 2em;
            }
            scrollbar {
            }
            window {
              border: 0.2em;
              background-color: @bg;
              border-color: @fg;
              padding: 0;
            }
            mainbox {
              padding: 0;
              border: 0;
            }
          '';
        in
        {
          environment.systemPackages = [
            rofi
          ];
          nixpkgs.overlays = [(final: super: {
            rofi-wayland-unwrapped = super.rofi-wayland-unwrapped.overrideAttrs({ patches ? [], ... }: {
              patches = []; /* Nixpkgs now has added my patch, conflicting with this pin */
              src = final.fetchFromGitHub {
                owner = "lbonn";
                repo = "rofi";
                rev = "b04bedca44b34e276a75785bec2c42a83d7a79b7";
                hash = "sha256-epxzpaULavF/fxQSMo7fhCL/y8sgLeQWtpEE3QHX+LQ=";
                fetchSubmodules = true;
              };
            });
          })];
        }
      )
    ]))
  ];
}
