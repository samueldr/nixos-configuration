{ config, lib, pkgs, ... }:
let
  cfg = config.samueldr.use-case.wayland-session.locker;
  inherit (lib)
    concatStringsSep
    mkForce
    mkOption
    types
  ;

  # Suspends the device if it should be...
  # In other words: off mains power, and after a timeout.
  suspend-if-needed = pkgs.writeScriptBin "suspend-if-needed" ''
    #!${pkgs.ruby}/bin/ruby

    INTERVAL = 60

    def log(s)
      puts ":: suspend-if-needed #{s}"
    end

    # Prevent "unexpectedly quick" suspends when removing power from a device...
    # ... this ensures that *at least* INTERVAL has passed.
    # It should suspend before 2 × INTERVAL.
    # (Seed with `no`, since either it'll be yes, and the conditional fail, or `no`, and we want to suspend.)
    last_is_on_power = "no"
    loop do
      sleep(INTERVAL)
      is_on_power = `${pkgs.systemd}/bin/systemd-ac-power -v`.strip()
      if is_on_power == "no" && last_is_on_power == "no" then
        log("suspending...")
        system("${pkgs.systemd}/bin/systemctl", "suspend")
      end
      last_is_on_power = is_on_power
    end
  '';

  enabled = config.samueldr.use-case.wayland-session.enable;

  samueldr-locker-internal = pkgs.writeShellScript "samueldr-locker" ''
    echo ":: samueldr-locker: locking!"
    # Suspend displays on lock
    ${cfg.monitorPoweroffCommand} &
    # Work around unsightly monitor resume with power key :/
    (sleep 0.5; ${cfg.monitorPoweroffCommand}) &

    # Lock
    CMD=(
      ${pkgs.swaylock}/bin/swaylock
        --color ${"b7caf2"}
        --image "${config.environment.etc."wallpaper".source}"
        --scaling fill
    )
    exec "''${CMD[@]}"
  '';

  samueldr-unlocker-internal = pkgs.writeShellScript "samueldr-unlocker" ''
    echo ":: samueldr-locker: unlocking!"
         ${pkgs.procps}/bin/pkill -SIGUSR1 ^swaylock$
    exec ${pkgs.procps}/bin/pwait          ^swaylock$
  '';

  samueldr-locker = pkgs.writeScriptBin "samueldr-locker" ''
    #!${pkgs.ruby}/bin/ruby
    
    require "json"
    require "shellwords"

    sessions = JSON.parse(`#{[
      "busctl",
      "--allow-interactive-authorization=false",
      "--json=pretty",
      "--system",
      "get-property",
      "org.freedesktop.login1",
      "/org/freedesktop/login1/user/self",
      "org.freedesktop.login1.User",
      "Sessions",
    ].shelljoin}`)["data"]
      .map(&:first)
    exec("loginctl", "lock-session", *sessions)
  '';
  samueldr-unlocker = pkgs.writeScriptBin "samueldr-unlocker" ''
    #!${pkgs.ruby}/bin/ruby
    
    require "json"
    require "shellwords"

    sessions = JSON.parse(`#{[
      "busctl",
      "--allow-interactive-authorization=false",
      "--json=pretty",
      "--system",
      "get-property",
      "org.freedesktop.login1",
      "/org/freedesktop/login1/user/self",
      "org.freedesktop.login1.User",
      "Sessions",
    ].shelljoin}`)["data"]
      .map(&:first)
    exec("loginctl", "unlock-session", *sessions)
  '';
in
{
  options.samueldr.use-case.wayland-session.locker = {
    monitorPoweronCommand = mkOption {
      default = "true";
      type = types.str;
      description = ''
        A command that turns the monitors on
      '';
    };
    monitorPoweroffCommand = mkOption {
      default = "true";
      type = types.str;
      description = ''
        A command that turns the monitors off
      '';
    };
  };
  config = lib.mkIf (enabled) {
    environment.systemPackages =
    [
      samueldr-locker
      samueldr-unlocker
      suspend-if-needed
    ];
    security.pam.services.swaylock = {}; # samueldr-locker wraps swaylock

    systemd.user.services."locker.suspend-if-needed" = {
      wantedBy = [ "session-locked.target" ];
      unitConfig = {
        Conflicts = [
          "session-unlocked.target"
        ];
      };
      serviceConfig = {
        Restart = "always";
        RestartSec= "1";
        ExecStart = "${suspend-if-needed}/bin/suspend-if-needed";
      };
    };
    systemd.user.services."locker.display-management" = {
      wantedBy = [ "session-locked.target" ];
      unitConfig = {
        Conflicts = [
          "session-unlocked.target"
        ];
      };
      serviceConfig = {
        Restart = "always";
        RestartSec= "1";
        ExecStart = concatStringsSep " " [
          "${pkgs.swayidle}/bin/swayidle"
          "timeout 1 true"
          "resume    '${cfg.monitorPoweronCommand}'"
          "timeout 5 '${cfg.monitorPoweroffCommand}'"
        ];
      };
    };
    systemd.user.services."locker.wake-display" = {
      wantedBy = [ "session-unlocked.target" ];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = false;
        ExecStart = cfg.monitorPoweronCommand;
      };
    };
    systemd.user.services."locker.locker" = {
      wantedBy = [ "session-locked.target" ];
      unitConfig = {
        Requires = [ "session-locked.target" ];
        Conflicts = [ "session-unlocked.target" ];
      };
      serviceConfig = {
        Restart = "always";
        RestartSec= "5";
        ExecStart = samueldr-locker-internal;
        ExecStop = samueldr-unlocker-internal;
        ExecStopPost = "${pkgs.systemd}/bin/systemctl --user start session-unlocked.target";
      };
    };
  };
}
