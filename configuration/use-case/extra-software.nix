{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.extra-gui-software;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.use-case.extra-gui-software = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable extra GUI software.
      '';
    };
  };
  config = mkIf cfg.enable {
    app-compartmentalization = {
      scoped-apps = {
        gnumeric.enable = true;
      };
    };
  };
}
