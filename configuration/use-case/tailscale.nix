{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.tailscale;
  inherit (lib)
    mkIf
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.tailscale = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable personal tailscale usage.
      '';
    };
  };
  config = mkIf cfg.enable {
    services.tailscale = {
      enable = true;
      useRoutingFeatures = lib.mkDefault "client";
    };
  };
}
