{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.modem;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.use-case.modem = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable modem config.
      '';
    };
  };
  config = mkIf cfg.enable {
    systemd.services = {
      ModemManager = {
        enable = true;
        wantedBy = [ "NetworkManager.service" ];
      };
    };
  };
}
