#
# Common graphical configuration.
#
# NOTE: This has to be environment-agnostic.
#       This should apply to e.g. custom session, plasma, gnome, phosh
#       and plamo equally.
#       (Except the default graphical-session to use...)
#       Enabling `common-graphical` will intrinsically use the default selected session.
#
{ config, lib, pkgs, options, ... }:

let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;

  yeah = lib.mkDefault true;

  isNixosStage1 = !(
    config.samueldr.use-case.mobile-nixos-stage-1.enable
    || config.boot.initrd.systemd.enable
  );
in
{
  options.samueldr.use-case.common-graphical = {
    enable = mkEnableOption "common graphical system config";
  };

  config = mkIf config.samueldr.use-case.common-graphical.enable {
    samueldr.use-case.graphical-session.enable = mkDefault true;
    samueldr.use-case.graphical-session.session = mkDefault "wayfire";

    #
    # Some necessary defaults and services
    #

    samueldr.use-case.audio.enable = true;
    services.udisks2.enable = true;

    environment.variables = {
      # Force non-overlay-scrolling.
      GTK_OVERLAY_SCROLLING = "0";

      # Ensure Qt apps scale the UI proportional to the DPI
      QT_AUTO_SCREEN_SCALE_FACTOR = "1";
    };

    samueldr.use-case.default-cli-software.enable = yeah;
    samueldr.use-case.default-gui-software.enable = yeah;

    # This adds an undesirable xterm system package, and xterm session.
    services.xserver.desktopManager.xterm.enable = false;

    #
    # Appearance things
    #

    samueldr.use-case.silent-boot.enable = yeah;
    samueldr.use-case.graphical-boot.enable = yeah;
    samueldr.use-case.graphical-boot.flickerFree.enable =
      # Our flicker-free config disables the first VT.
      # Anyway the NixOS stage-1 prints to the console, which breaks flicker-free.
      if !isNixosStage1 then yeah else false
    ;
    # XXX broken!!
    ##samueldr.use-case.graphical-boot.hackBGRT.enable =
    ##  # A shortcut to mean "supports ACPI, and thus the BGRT table"
    ##  if pkgs.stdenv.hostPlatform.isx86 then yeah else false
    ##;
    samueldr.use-case.pretty-console.enable = yeah;

    # Configure a wallpaper
    # Some environments may not use it by default, but it'll be there to pick.
    environment.etc."wallpaper".source = lib.mkDefault ../../artwork/samueldr.1440.png;

    #
    # Configure actual icon theme
    #

    # Force icon theme shenanigans
    hacks.mergedIcons = {
      enable = yeah;
      masqueradeAs = [
        "Adwaita"
      ];
      disabledThemes = [
        "Adwaita"
      ];
      iconThemes = [
        # Fallback
        "${pkgs.tango-icon-theme}/share/icons/Tango"
        "${pkgs.pantheon.elementary-icon-theme}/share/icons/elementary"

        # Desired icon theme
        "${pkgs.elementary-xfce-icon-theme}/share/icons/elementary-xfce"
      ];
      removeLegacyIcons = yeah;
    };

    #
    # Behaviour
    #

    # These are the base defaults.
    # It is expected that some power management daemon decides in the end.
    services.xserver.serverFlagsSection = ''
      Option "BlankTime" "0"
      Option "StandbyTime" "0"
      Option "SuspendTime" "0"
      Option "OffTime" "0"
    '';
    services.xserver.displayManager.setupCommands = ''
      ${pkgs.xorg.xset}/bin/xset dpms 0 0 0
      ${pkgs.xorg.xset}/bin/xset s off
      ${pkgs.xorg.xset}/bin/xset s off -dpms
      :
    '';
    services.libinput.enable = mkDefault true;

    # Enable pin support by default.
    samueldr.simple-numeric-pin = {
      enable = yeah;
      pamServices = [
        "i3lock" # Convergent session locker
        "kde"
        "phosh"
        "xfce4-screensaver"
      ];
    };

    # Don't use any user config
    fonts.fontconfig.includeUserConf = false;

    # Configure emoji font
    fonts.fontconfig.localConf = ''
      <?xml version="1.0"?>
      <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
      <fontconfig>
        <match target="scan">
          <test name="family">
            <string>Noto Color Emoji</string>
          </test>
          <edit name="scalable" mode="assign">
            <bool>true</bool>
          </edit>
        </match>

        <match target="pattern">
          <test name="prgname">
            <string>chrome</string>
          </test>
          <edit name="family" mode="prepend_first">
            <string>Noto Color Emoji</string>
          </edit>
        </match>
      </fontconfig>
    '';

    fonts.fontDir.enable = true;
    fonts.enableDefaultPackages = true;
    fonts.packages = with pkgs; [
      aileron             # Used for user interface
      go-font             # Main monospace font

      noto-fonts          # fallback font
      noto-fonts-cjk-sans # fallback font
      noto-fonts-emoji    # fallback font

      font-awesome        # Used for misc symbols

      # Misc additional fonts

      corefonts
      dejavu_fonts
      inconsolata
      proggyfonts
      roboto
      source-code-pro
      source-sans-pro
      source-serif-pro
      terminus_font
    ];

    boot.kernelParams = [
      "preempt=full"
    ];
  };
}
