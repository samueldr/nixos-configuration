{ config, lib, pkgs, options, ... }:

let
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.pretty-console = {
    enable = mkEnableOption "prettier console config";
  };

  config = mkIf config.samueldr.use-case.pretty-console.enable {
    console.colorPreset = lib.mkDefault "blackOnWhite";
    console.earlySetup = true;
    console.ttf-font = {
      fontfile = "${pkgs.go-font}/share/fonts/truetype/Go-Mono.ttf";

      #
      # Couldn't find a mathematical relationship with those that would work
      # through the aliasing.
      #
      # Maximum allowed is 32px tall.
      #
      # Using a common divisor for common display sizes is useful here.
      # E.g. by selecting 30px tall characters, they will fill the whole
      # display vertically for 720p, 1080p and 1440p.
      #
      # The size can be found out this way
      #  $ nix-build ... -A config.console.font && file -L ./result
      #  ./result: Linux/i386 PC Screen Font v2 data, 512 characters, Unicode directory, 30x16
      # Size is height first, width next.
      #

      # gcd(720, 1080, 1440)
      # ... 10 | 12 | 15 | 18 | 20 | 24 | 30 | 36 | 40 | 45 | 60 ...
      dpi = 64; ptSize = 30; # 30px

      # gcd(768, 1080, 1440)
      # 24
      #dpi = 65; ptSize = 24; # 24px
    };
  };
}
