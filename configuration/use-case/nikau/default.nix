{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.nikau;
  inherit (lib)
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.nikau = {
    server = {
      enable = mkOption {
        default = false;
        type = types.bool;
        description = ''
          Enable nikau network KVM server.
        '';
      };
      additionalargs = mkOption {
        default = [];
        type = with types; listof str;
        description = ''
          additional arguments to provide to the serverprocess.
        '';
      };
      shortcuts = {
        next = mkOption {
          default = "leftctrl,leftshift,esc";
          type = types.str;
          description = ''
            Shortcut for switching to the next client
          '';
        };
        previous = mkOption {
          default = "red,green,blue";
          type = types.str;
          description = ''
            Shortcut for switching to the previous client

            (The default effectively disables the previous client shortcut)
          '';
        };
        /*
          TODO: encode this, if desired...
          --shortcut-goto <key1,key2,key3=[fingerprint-prefix]>
              Keyboard shortcut for switching directly to a client by its fingerprint prefix, or to the server for an empty fingerprint
        */
      };
    };
    client = {
      enable = mkOption {
        default = false;
        type = types.bool;
        description = ''
          Enable nikau network KVM client.
        '';
      };
      host = mkOption {
        type = types.str;
        description = ''
          Hostname (or IP) for the server.
        '';
      };
      additionalargs = mkOption {
        default = [];
        type = with types; listof str;
        description = ''
          additional arguments to provide to the serverprocess.
        '';
      };
    };
  };
  config = mkMerge [
    (mkIf (cfg.server.enable || cfg.client.enable) {
      users.extraUsers.samuel = {
        extraGroups = [
          "input" # for running nikau as the user.
        ];
      };
      nixpkgs.overlays = [(final: super: {
        nikau = final.callPackage ./package.nix {};
      })];
    })

    (mkIf (cfg.server.enable) {
      networking.firewall.allowedUDPPorts = [
        1213
      ];
      systemd.user.services."nikau-server" = {
        enable = true;
        environment.PATH = lib.mkForce null;
        serviceConfig = {
          Restart = "always";
          RestartSec= "5";
          ExecStart = ''
            ${pkgs.nikau}/bin/nikau server --shortcut ${cfg.server.shortcuts.next} --shortcut-prev ${cfg.server.shortcuts.previous}
          '';
        };
        unitConfig = {
          ConditionPathExists = "/run/user/%U";
        };
        wantedBy = [ "graphical-session.target" ];
        bindsTo = [ "graphical-session.target" ];
        restartTriggers = [
          pkgs.nikau
        ];
      };
    })
    (mkIf (cfg.client.enable) {
      boot.kernelModules = [ "uinput" ];
      services.udev.extraRules = ''
        SUBSYSTEM=="misc", KERNEL=="uinput", MODE="0660", GROUP="input"
      '';
      systemd.user.services."nikau-client" = {
        enable = true;
        environment.PATH = lib.mkForce null;
        serviceConfig = {
          Restart = "always";
          RestartSec= "5";
          ExecStart = ''
            ${pkgs.nikau}/bin/nikau client "${cfg.client.host}"
          '';
        };
        unitConfig = {
          ConditionPathExists = "/run/user/%U";
        };
        wantedBy = [ "graphical-session.target" ];
        bindsTo = [ "graphical-session.target" ];
        restartTriggers = [
          pkgs.nikau
        ];
      };
    })
  ];
}
