{ lib
, fetchFromSourcehut
, rustPlatform
}:

rustPlatform.buildRustPackage rec {
  pname = "nikau";
  version = "0.2.1";

  src = fetchFromSourcehut {
    owner = "~nickbp";
    repo = pname;
    rev = "bac55c6a54bf1ca53347a98dbbb3f4b911c138bb";
    hash = "sha256-VKnHaedHN8Za3O6RPUsnzEf1Q/2wf7qrshevMOHrIjs=";
  };

  cargoHash = "sha256-DoqcdVcH9yfOKirZI4V580ZAC4OKIVegyurdzpN2NlU=";

  patches = [
    ./0001-log_device_info-Improve-touchpad-and-mouse-heuristic.patch
  ];

  postPatch = ''
    rm -r examples
  '';

  meta = with lib; {
    description = "Network KVM software for sharing input devices and clipboards across Linux machines";
    homepage = "https://nikau.nickbp.com/";
    license = licenses.gpl3;
    platforms = platforms.linux;
    maintainers = with maintainers; [ samueldr ];
  };
}
