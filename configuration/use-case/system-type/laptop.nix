{ config, lib, ... }:

let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.system-type.laptop = {
    enable = mkEnableOption "daily driver laptop style computer use case";
  };

  config = mkIf config.samueldr.use-case.system-type.laptop.enable {
    environment.etc."machine-info".text = lib.mkDefault (lib.mkAfter ''
      CHASSIS="laptop"
    '');
    samueldr.use-case.common-graphical.enable = true;
    samueldr.hardware.misc.logitech.enable = true;
  };
}
