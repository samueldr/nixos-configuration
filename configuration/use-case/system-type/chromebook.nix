{ config, lib, pkgs, ... }:

let
  inherit (lib)
    any
    concatStringsSep
    mapAttrsToList
    mkDefault
    mkEnableOption
    mkForce
    mkIf
    mkMerge
    mkOption
    types
  ;

  inherit (config.samueldr.hardware) cpu;
  cfg = config.samueldr.use-case.system-type.chromebook;

  checkGeneration = any (v: cpu.generation == v);

  withSOF = checkGeneration [
    "Gemini Lake"
  ];

  my_xkeyboardconfig = pkgs.xorg.xkeyboardconfig.overrideAttrs({ patches ? [], ... }: {
    patches = patches ++ [
      (builtins.toFile "chromebook-xkeyboardconfig.patch" ''
        From 4f789cf6405b5a28d6b3337d0b0050b7fefe154a Mon Sep 17 00:00:00 2001
        From: Samuel Dionne-Riel <samuel@dionne-riel.com>
        Date: Sun, 22 Jan 2023 20:01:12 -0500
        Subject: [PATCH] symbols/inet: Add power-user friendlier chromebook layout
        MIME-Version: 1.0
        Content-Type: text/plain; charset=UTF-8
        Content-Transfer-Encoding: 8bit

        That layout:

         - adds missing keys
         - makes F× keys available as expected

        The closest useful approximation of the F× key caps is mapped to 3rd
        level, which is accessed using right alt (alt gr).
        ---
         symbols/inet | 52 +++++++++++++++++++++++++++++++++++-----------------
         1 file changed, 35 insertions(+), 17 deletions(-)

        diff --git a/symbols/inet b/symbols/inet
        index b8015852..c6580b7d 100644
        --- a/symbols/inet
        +++ b/symbols/inet
        @@ -2022,23 +2022,41 @@ xkb_symbols "pc105" {
         
         partial alphanumeric_keys
         xkb_symbols "chromebook" {
        -	include "level3(ralt_switch)"
        -        key <FK01> {    [ XF86Back ] };
        -        key <FK02> {    [ XF86Forward ] };
        -        key <FK03> {    [ XF86Reload ] };
        -        key <FK04> {    [ F11 ] };
        -//        key <FK05> {    [ F5, F5, F5, F5 ] }; // Overview key
        -        key <FK06> {    [ XF86MonBrightnessDown ] };
        -        key <FK07> {    [ XF86MonBrightnessUp ] };
        -        key <FK08> {    [ XF86AudioMute ] };
        -        key <FK09> {    [ XF86AudioLowerVolume ] };
        -        key <FK10> {    [ XF86AudioRaiseVolume ] };
        -        key <BKSP> {    [ BackSpace, BackSpace, Delete ] };
        -        key <UP>   {    [ Up, Up, Prior, Up ] };
        -        key <DOWN> {    [ Down, Down, Next, Down ] };
        -        key <LEFT> {    [ Left, Left, Home, Left ] };
        -        key <RGHT> {    [ Right, Right, End, Right ] };
        -        key <LWIN> {    [ Super_L, Super_L, Caps_Lock, Super_L ] };
        +       include "level3(ralt_switch)"
        +        // Generic mapping for missing keys
        +        key <BKSP> { [ BackSpace, BackSpace, Delete             ] };
        +        key <UP>   { [ Up,        Up,        Prior,     Up      ] };
        +        key <DOWN> { [ Down,      Down,      Next,      Down    ] };
        +        key <LEFT> { [ Left,      Left,      Home,      Left    ] };
        +        key <RGHT> { [ Right,     Right,     End,       Right   ] };
        +        key <LWIN> { [ Super_L,   Super_L,   Caps_Lock, Super_L ] };
        +
        +        // Top row ("F" keys) on old layout
        +        // The old layout sends F× keys
        +        key <FK01> { [  F1,  F1, XF86Back              ] }; // ←
        +        key <FK02> { [  F2,  F2, XF86Forward           ] }; // →
        +        key <FK03> { [  F3,  F3, XF86Reload            ] }; // ⟳
        +        key <FK04> { [  F4,  F4, F11                   ] }; // (Fullscreen)
        +        key <FK05> { [  F5,  F5, F12                   ] }; // (Overview)
        +        key <FK06> { [  F6,  F6, XF86MonBrightnessDown ] }; // ☼
        +        key <FK07> { [  F7,  F7, XF86MonBrightnessUp   ] }; // ☀
        +        key <FK08> { [  F8,  F8, XF86AudioMute         ] }; // 🔇
        +        key <FK09> { [  F9,  F9, XF86AudioLowerVolume  ] }; // 🔉
        +        key <FK10> { [ F10, F10, XF86AudioRaiseVolume  ] }; // 🔊
        +
        +        // Top row ("F" keys) on new layout
        +        // The new layout does not send F× keys
        +        key <I166> { [  F1,  F1, XF86Back              ] }; // ←            | KEY BACK
        +        key <I181> { [  F2,  F2, XF86Reload            ] }; // ⟳            | KEY REFRESH
        +        key <I380> { [  F3,  F3, XF86FullScreen        ] }; // (Fullscreen) | KEY ZOOM -> FULL_SCREEN
        +        key <I128> { [  F4,  F4, XF86LaunchA           ] }; // (Overview)   | KEY SCALE
        +        key <I232> { [  F5,  F5, XF86MonBrightnessDown ] }; // ☼            | KEY BRIGHTNESSDOWN
        +        key <I233> { [  F6,  F6, XF86MonBrightnessUp   ] }; // ☀            | KEY BRIGHTNESSUP
        +        key <I256> { [  F7,  F7, XF86AudioMicMute      ] }; // (mic mute)   | KEY MICMUTE
        +        key <I121> { [  F8,  F8, XF86AudioMute         ] }; // 🔇           | KEY MUTE
        +        key <I122> { [  F9,  F9, XF86AudioLowerVolume  ] }; // 🔉           | KEY VOLUMEDOWN
        +        key <I123> { [ F10, F10, XF86AudioRaiseVolume  ] }; // 🔊           | KEY VOLUMEUP
        +        key <I150> { [ F11, F11, F12                   ] }; // 🔒           | KEY SLEEP
         };
         
         partial alphanumeric_keys
        -- 
        2.38.1
      '')
    ];
  });
in
{
  options.samueldr.use-case.system-type.chromebook = {
    enable = mkEnableOption "a device that was meant to be used with ChromeOS";
    ALSA_CONFIG_UCM2 = mkOption {
      type = with types; nullOr (oneOf [package path str]);
      default = null;
      internal = true;
    };
    ucmFilesToCopy = mkOption {
      type = with types; attrsOf str;
      default = {};
      internal = true;
    };
  };

  config = mkIf cfg.enable (mkMerge [
    {
      hardware.pulseaudio.enable = mkForce true;
      services.pipewire.enable = mkForce false;

      samueldr.use-case.common-graphical.enable = true;

      # With the following replacement, the "chromebook" xkeyboard config
      # will differ mainly in needing alt+gr to use the special function
      # of the marked keys.
      environment.variables = {
        XKB_DEFAULT_MODEL = "chromebook";
      };

      environment.sessionVariables = {
        # runtime override supported by multiple libraries e. g. libxkbcommon
        # https://xkbcommon.org/doc/current/group__include-path.html
        XKB_CONFIG_ROOT = "${my_xkeyboardconfig}/etc/X11/xkb";
      };

      services.xserver = {
        xkb.dir = "${my_xkeyboardconfig}/etc/X11/xkb";
        exportConfiguration = config.services.xserver.displayManager.startx.enable
          || config.services.xserver.displayManager.sx.enable;
      };
    }
    (mkIf (withSOF) {
      boot.extraModprobeConfig = ''
        options snd-intel-dspcfg dsp_driver=3
      '';
      hardware.firmware = with pkgs; [
        sof-firmware
      ];

      samueldr.use-case.system-type.chromebook.ALSA_CONFIG_UCM2 = "${(pkgs.callPackage (
        { alsa-ucm-conf
        , fetchFromGitHub
        , runCommand
        , ucmFilesToCopy
        }:
        runCommand "chromebook-ucm-conf" {
          src = fetchFromGitHub {
            owner = "WeirdTreeThing";
            repo = "chromebook-ucm-conf";
            rev = "a80f5ff1add9115672714516d124d707e94e689f";
            hash = "sha256-mrB5q9Fi8wKnX0auVgI31/inu3Vq/yGPtAdAGo7NTtw=";
          };
        } ''
          cp -r --no-preserve=all ${pkgs.alsa-ucm-conf} $out
          ${concatStringsSep "\n" (mapAttrsToList (
            srcPath: outPath:
            ''
              mkdir -vp "$out/share/alsa/ucm2/${dirOf outPath}"
              rm -rfv "$out/share/alsa/ucm2/${outPath}"
              cp -vr "$src/${srcPath}" "$out/share/alsa/ucm2/${outPath}"
            ''
          ) ucmFilesToCopy)}
        ''
      ) {
        inherit (cfg) ucmFilesToCopy;
      })}/share/alsa/ucm2";
    })
    (mkIf (cpu.generation == "Gemini Lake") {
      samueldr.use-case.system-type.chromebook.ucmFilesToCopy = {
        "glk/sof-glkda7219ma" =  "conf.d/sof-glkda7219ma";
        "glk/sof-cs42l42" =      "conf.d/sof-cs42l42";
        "glk/sof-glkrt5682ma" =  "conf.d/sof-glkrt5682ma";
        "dmic-common" =          "conf.d/dmic-common";
        "hdmi-common" =          "conf.d/hdmi-common";
      };
    })
    (mkIf (cfg.ALSA_CONFIG_UCM2 != null) (mkMerge [
      # TIP: use `aslaucm reload` to debug issues with UCM.
      {
        environment.variables.ALSA_CONFIG_UCM2 = cfg.ALSA_CONFIG_UCM2;
      }

      # Pulseaudio
      (mkIf (config.hardware.pulseaudio.enable) (let
        systemWide = config.hardware.pulseaudio.systemWide;
      in {
        systemd.services.pulseaudio = mkIf systemWide {
          environment.ALSA_CONFIG_UCM2 = config.environment.variables.ALSA_CONFIG_UCM2;
        };
        systemd.user.services.pulseaudio = mkIf (!systemWide) {
          environment.ALSA_CONFIG_UCM2 = config.environment.variables.ALSA_CONFIG_UCM2;
        };
      }))

      # Pipewire
      (mkIf (config.services.pipewire.enable) (let
        systemWide = config.services.pipewire.systemWide;
      in {
        systemd.services.pipewire = mkIf systemWide {
          environment.ALSA_CONFIG_UCM2 = config.environment.variables.ALSA_CONFIG_UCM2;
        };
        systemd.user.services.pipewire = mkIf (!systemWide) {
          environment.ALSA_CONFIG_UCM2 = config.environment.variables.ALSA_CONFIG_UCM2;
        };
      }))
    ]))
  ]);
}
