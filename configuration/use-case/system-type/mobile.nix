{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.system-type.mobile = {
    enable = mkEnableOption "anything tablet or phone like, will be enabled by either.";
  };

  config = mkIf config.samueldr.use-case.system-type.mobile.enable {
    samueldr.use-case.common-graphical.enable = true;
    samueldr.convergent-session.autorotate = mkDefault true;
    samueldr.convergent-session.on-screen-keyboard.enable = mkDefault true;
    samueldr.convergent-session.locker.usePinpad = true;
    environment.variables = {
      # https://invent.kde.org/plasma-mobile/angelfish/-/blob/049c78995fe6d7e0994f73f7b95fd33c65b41343/lib/settingshelper.cpp#L15
      QT_QUICK_CONTROLS_MOBILE = mkDefault "true";
    };

    services.xserver = {
      displayManager.lightdm = {
        greeters.gtk.extraConfig = lib.mkAfter ''
          keyboard=${pkgs.onboard}/bin/onboard
          a11y-states=+keyboard
        '';
      };
    };
    programs.firefox.preferences = {
      "browser.uidensity" = 2; # Touch
    };
  };
}
