{ config, lib, ... }:

let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.system-type.desktop = {
    enable = mkEnableOption "daily driver desktop style computer use case";
  };

  config = mkIf config.samueldr.use-case.system-type.desktop.enable {
    samueldr.use-case.common-graphical.enable = true;
    samueldr.hardware.misc.logitech.enable = true;
  };
}
