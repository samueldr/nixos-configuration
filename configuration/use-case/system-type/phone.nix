{ config, lib, ... }:

let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.system-type.phone = {
    enable = mkEnableOption "daily driver phone use case";
  };

  config = mkIf config.samueldr.use-case.system-type.phone.enable {
    environment.etc."machine-info".text = lib.mkDefault (lib.mkAfter ''
      CHASSIS="handset"
    '');
    samueldr.use-case.system-type.mobile.enable = true;
    samueldr.use-case.modem.enable = mkDefault true;
    # TODO: find an alternative phone-friendly session I like?
    samueldr.use-case.graphical-session.session = "convergent-session";
  };
}
