{ config, lib, ... }:

let
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.system-type.headless = {
    enable = mkEnableOption "headless use-case";
  };

  config = mkIf config.samueldr.use-case.system-type.headless.enable {
    samueldr.use-case.default-cli-software.enable = true;
    samueldr.use-case.common-graphical.enable = false;
    samueldr.use-case.graphical-session.enable = false;
    samueldr.use-case.graphical-session.session = "none";
    samueldr.use-case.graphics-software.enable = false;
    samueldr.settings.filesystem-shenanigans.enable = false;
    samueldr.use-case.stage-1.chosen = "systemd";
  };
}
