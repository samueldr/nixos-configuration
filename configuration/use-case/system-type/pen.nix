{ config, lib, ... }:

let
  inherit (lib)
  mkDefault
  mkEnableOption
  mkIf
  ;
in
{
  options.samueldr.use-case.system-type.pen = {
    enable = mkEnableOption "system with a stylus or pen";
  };

  config = mkIf config.samueldr.use-case.system-type.pen.enable {
    app-compartmentalization = {
      scoped-apps = {
        krita.enable = true;
        mypaint.enable = true;
        rnote.enable = true;
        xournal.enable = true;
      };
    };
  };
}
