{ config, lib, ... }:

let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.system-type.tablet = {
    enable = mkEnableOption "daily driver tablet use case";
  };

  config = mkIf config.samueldr.use-case.system-type.tablet.enable {
    environment.etc."machine-info".text = lib.mkDefault (lib.mkAfter ''
      CHASSIS="tablet"
    '');
    samueldr.use-case.system-type.mobile.enable = true;
    # TODO: find an alternative tablet-friendly session I like?
    samueldr.use-case.graphical-session.session = "convergent-session";
  };
}
