{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkAfter
    mkDefault
    mkEnableOption
    mkIf
    mkOption
    types
  ;
  cfg = config.samueldr.use-case.system-type.e-reader;
in
{
  options.samueldr.use-case.system-type.e-reader = {
    enable = mkEnableOption "daily driver e-Book reader use case";
    setupCommands = mkOption {
      default = "";
      type = types.lines;
      description = ''
        Additional commands to run before executing the e-Book reader software
        under a Wayland compositor.
      '';
    };
  };

  config = mkIf config.samueldr.use-case.system-type.e-reader.enable {
    # Nothing better than tablet...
    environment.etc."machine-info".text = lib.mkDefault (lib.mkAfter ''
      CHASSIS="tablet"
    '');
    # Assume an e-reader is e-ink
    samueldr.use-case.eink.enable = true;
    # We want to use our customized koreader
    samueldr.use-case.koreader.enable = true;

    # Ensures the system is configured for GUI use
    samueldr.use-case.common-graphical.enable = true;
    # Though don't enable most GUI software
    samueldr.use-case.default-gui-software.enable = false;
    # But yes enable the terminal, just in case.
    samueldr.use-case.app.terminal.enable = true;

    # This service is used to run the GUI software.
    services.cage = {
      enable = true;
      user = "samuel";
      program = pkgs.writeShellScript "koreader-cage-wrapper" ''
        ${cfg.setupCommands}

        exec ${pkgs.koreader}/bin/koreader
      '';
    };

    # This prevents nixos-rebuild from killing our app by activating getty again
    systemd.services."autovt@tty1".enable = false;
    systemd.services."cage-tty1" = {
      environment = {
        # Configure what is shown in the Exit menu
        KO_NO_EXIT = "1";
        # KO_NO_RESTART = "1";
        # Works around an issue where SDL inits with hardcoded 800x600
        SDL_FULLSCREEN = "1";
      };
      serviceConfig = {
        # The cage service doesn't autorestart by default...
        Restart = "always";
      };
    };

    services.logind.extraConfig = mkAfter ''
      HandlePowerKey=ignore
      HandlePowerKeyLongPress=ignore
      HandleRebootKey=reboot
      HandleRebootKeyLongPress=ignore
      HandleSuspendKey=suspend
      HandleSuspendKeyLongPress=ignore
      HandleHibernateKey=suspend
      HandleHibernateKeyLongPress=ignore
      HandleLidSwitch=suspend
      HandleLidSwitchExternalPower=suspend
      HandleLidSwitchDocked=suspend
      HoldoffTimeoutSec=1
    '';
  };
}
