{ config, lib, pkgs, ... }:

let
  enabled = config.samueldr.use-case.graphical-session.session == "wayfire";
  inherit (lib)
    mkForce
    mkMerge
    mkIf
  ;

  inherit (config.lib.samueldr.convergent-session)
    userServiceFromAutostart
  ;

  wlopm-all = pkgs.writeScript "wlopm-all" ''
    #!${pkgs.ruby}/bin/ruby
    require "json"
    require "shellwords"

    WLOPM = "${pkgs.wlopm}/bin/wlopm"

    command = ARGV[0]
    command ||= "--toggle"

    cmd = [WLOPM]

    JSON.parse(`#{[WLOPM, "--json"].shelljoin}`).map do |output|
      cmd << command
      cmd << output["output"]
    end

    puts " $ #{cmd.shelljoin()}"
    exec(*cmd)
  '';
in
{
  imports = [
  ];
  config = mkMerge [
    { samueldr.use-case.graphical-session.availableSessions = [ "wayfire" ]; }
    (mkIf enabled ( mkMerge [
      {
        samueldr.use-case.wayland-session.enable = true;
        samueldr.use-case.wayland-session.locker.monitorPoweroffCommand =
          "${wlopm-all} --off"
        ;
        samueldr.use-case.wayland-session.locker.monitorPoweronCommand =
          "${wlopm-all} --on"
        ;
        programs.wayfire = {
          plugins = with pkgs.wayfirePlugins; [
            #wcm
            #wf-shell
            #windecor
            #firedecor
            #wwp-switcher
            #focus-request
            #wayfire-shadows
            #wayfire-plugins-extra
          ];
          enable = true;
        };
        environment.systemPackages = with pkgs; [
          (pkgs.writeShellScriptBin "wayfire" ''
            exec systemd-run --user --pipe ${pkgs.wayfire}/bin/wayfire --config "/etc/xdg/wayfire.ini" "$@"
          '')
        ];
        nixpkgs.overlays = [(final: super: {
          wayfire =
            let
              wayfire = super.wayfire.overrideAttrs ({ patches ? [], ... }: {
                patches = patches ++ [
                  (final.fetchpatch {
                    # workaround: remove exit shortcut
                    url = "https://github.com/samueldr/wayfire/commit/0631af741b266f92ddc7b9b2c71c760a702a03bc.patch";
                    hash = "sha256-YwbMLA5Uy1Ffjx/s4YTw2y6t/Agfnm/4rFn3iKApuhU=";
                  })
                  (final.fetchpatch {
                    # output/render-manager: Don't spam journal when not active
                    url = "https://github.com/samueldr/wayfire/commit/b255a2d66854abdf7fab6f47b9b7288d45307c52.patch";
                    hash = "sha256-98PBZvI9n8ap/K6bR3xlhij/Y4fF5Ww5vMBdubdZrb8=";
                  })
                ];
              });
            in
            final.symlinkJoin {
              name = wayfire.name;
              meta = wayfire.meta // {
                priority = 10;
              };
              passthru = wayfire.passthru // {
              };
              paths =
                [
                  (pkgs.writeTextDir "share/wayland-sessions/wayfire.desktop" ''
                    [Desktop Entry]
                    Name=Wayfire
                    Exec=wayfire-session
                    TryExec=wayfire-session
                    Icon=
                    Type=Application
                    DesktopNames=Wayfire
                  '')
                  (final.writeShellScriptBin "wayfire-session" ''
                    set -e
                    set -u
                    PS4=" $ "
                    set -x

                    at_exit() {
                      # Cleanup...
                      systemctl --user stop --force desktop-session.target || :

                      # Unset environment that we've set.
                      systemctl --user unset-environment "''${VARS[@]}" "''${CLEANUP[@]}"
                    }
                    trap at_exit EXIT SIGINT SIGTERM SIGKILL

                    CLEANUP=(
                      # Some other variables some desktop environments set
                      # Some won't actually be unset, but reset to their default.
                      "XDG_CACHE_HOME"
                      "XDG_CONFIG_DIRS"
                      "XDG_CONFIG_HOME"
                      "XDG_DATA_DIRS"
                      "XDG_DATA_HOME"
                      "XDG_DESKTOP_PORTAL_DIR"
                      "XDG_RUNTIME_DIR"
                      "XDG_SEAT"
                      "XDG_SESSION_DESKTOP"
                      "XDG_SESSION_ID"
                      "XDG_VTNR"
                    )

                    # TODO: sync user environment variables.
                    VARS=(
                      # Coming from system config
                      "XDG_CURRENT_DESKTOP"
                      "XDG_SESSION_TYPE"
                      "QT_QPA_PLATFORM"
                      "XCURSOR_THEME"
                      "XCURSOR_SIZE"
                      # Actually set within wayfire
                      "DISPLAY"
                      "WAYLAND_DISPLAY"
                    )

                    # Clear some environment...
                    systemctl --user unset-environment "''${CLEANUP[@]}"

                    # Sync required environment...
                    systemctl --user import-environment "''${VARS[@]}"

                    # But ensure wlroots doesn't try to use these bogus values...
                    systemctl --user unset-environment WAYLAND_DISPLAY DISPLAY

                    # Reset failed state of all user units.
                    systemctl --user reset-failed

                    RUNNER=(
                      systemd-run
                        --user
                        --scope
                        --unit="wayfire-session"
                        --collect
                        --quiet

                        # Ensures anything that needed to run beforehand has ran.
                        --property=After=graphical-session-pre.target
                        --property=Wants=graphical-session-pre.target

                        # Ensures both that `graphical-session.target` is ready, and stops if it is stopped.
                        --property=BindsTo="graphical-session.target"
                        # Sychronize a stop from this service meaning a stop to the desktop-session.target.
                        # NOTE: desktop-session.target is started *within* the compositor.
                        --property=PropagatesStopTo="desktop-session.target"
                        # And ensure the other way too...
                        --property=StopPropagatedFrom="desktop-session.target"

                      ${final.with-profile} ${wayfire}/bin/wayfire --config "/etc/xdg/wayfire.ini"
                    )
                    "''${RUNNER[@]}"
                  '')
                  wayfire
                ]
              ;
            }
          ;
        })];

        # TODO: more discretelier options?
        environment.etc = {
          "xdg/wayfire.ini" = {
            # Force a file to be written
            mode = "0444";
            source = (pkgs.formats.ini {}).generate "wayfire.ini" {
              core = {
                background_color = "\\#666666ff";
                close_top_view = "<super> KEY_W";
                focus_button_with_modifiers = "false";
                focus_buttons = "BTN_LEFT | BTN_MIDDLE | BTN_RIGHT";
                focus_buttons_passthrough = "true";
                max_render_time = "-1";
                plugins = lib.concatStringsSep " " [
                  "foreign-toplevel"
                  "gtk-shell"
                  "wayfire-shell"

                  "autostart"
                  "command"
                  "move"
                  "resize"
                  "window-rules"
                  "wm-actions"
                  "session-lock"
                  "xdg-activation"
                  "shortcuts-inhibit"
                  "preserve-output"
                  "place"
                  "decoration"

                  "fast-switcher"
                  "switcher"
                  "scale"

                  "expo"
                  "oswitch"
                  "vswitch"
                  "vswipe"

                  "alpha"
                  "animate"
                  "invert"
                  "zoom"
                  "wrot"
                ];
                preferred_decoration_mode = "server";
                transaction_timeout = "100";
                vheight = "3";
                vwidth = "3";
                xwayland = "true";
              };

              workarounds = {
                all_dialogs_modal = "true";
                app_id_mode = "stock";
                discard_command_output = "true";
                dynamic_repaint_delay = "false";
                enable_input_method_v2 = "false";
                enable_so_unloading = "false";
                force_preferred_decoration_mode = "true";
                remove_output_limits = "false";
                use_external_output_configuration = "true";
              };

              autostart = {
                autostart_wf_shell = "true";
                # This needs to happen within the process to import properly.
                _01_sync_env = "systemctl --user import-environment WAYLAND_DISPLAY DISPLAY";
                # Signal the session is ready
                _02_desktop_session_ready = "systemctl --user start desktop-session.target";
              };

              input = {
                click_method = "default";
                cursor_size = config.environment.variables.XCURSOR_SIZE;
                cursor_theme = config.environment.variables.XCURSOR_THEME;
                disable_touchpad_while_mouse = "false";
                disable_touchpad_while_typing = "true";
                drag_lock = "false";
                gesture_sensitivity = "1.000000";
                kb_capslock_default_state = "false";
                kb_numlock_default_state = "true";
                kb_repeat_delay = "400";
                kb_repeat_rate = "40";
                left_handed_mode = "false";
                middle_emulation = "true";
                modifier_binding_timeout = "400";
                mouse_accel_profile = "none";
                mouse_cursor_speed = "0.000000";
                mouse_scroll_speed = "1.000000";
                mouse_natural_scroll = false;
                natural_scroll = "true";
                scroll_method = "default";
                tablet_motion_mode = "default";
                tap_to_click = "true";
                touchpad_accel_profile = "default";
                touchpad_cursor_speed = "0.000000";
                touchpad_scroll_speed = "1.000000";
                xkb_layout = config.environment.variables.XKB_DEFAULT_LAYOUT;
                xkb_model = config.environment.variables.XKB_DEFAULT_MODEL;
                xkb_options = config.environment.variables.XKB_DEFAULT_OPTIONS;
                #xkb_rules = "evdev";
                #xkb_variant = "";
              };

              command = {
                # Launcher
                binding_launcher = "<alt> KEY_SPACE";
                command_launcher = "rofi";

                # Terminal
                binding_terminal = "<alt> KEY_ENTER";
                command_terminal = "terminal";

                # Lock key bindings
                binding_lock1 = "<super><alt> KEY_L";
                command_lock1 = "samueldr-locker";
                binding_lock2 = "<ctrl><alt>  KEY_L";
                command_lock2 = "samueldr-locker";
                binding_lock3 = "<ctrl><alt>  KEY_BACKSPACE";
                command_lock3 = "samueldr-locker";
                binding_lock4 = "<ctrl><alt>  KEY_DELETE";
                command_lock4 = "samueldr-locker";

                repeating_binding_brigtnessdn = "KEY_BRIGHTNESSDOWN";
                command_brigtnessdn = "${pkgs.light}/bin/light -T 0.9";
                repeating_binding_brigtnessup = "KEY_BRIGHTNESSUP";
                command_brigtnessup = "${pkgs.light}/bin/light -T 1.1";

                binding_prevsong  = "KEY_PREVIOUSSONG";
                command_prevsong  = "${pkgs.playerctl}/bin/playerctl --player=mpv,audacious,cmus previous";
                binding_nextsong  = "KEY_NEXTSONG";
                command_nextsong  = "${pkgs.playerctl}/bin/playerctl --player=mpv,audacious,cmus next";
                binding_play      = "KEY_PLAY";
                command_play      = "${pkgs.playerctl}/bin/playerctl --player=mpv,audacious,cmus play-pause";
                binding_playpause = "KEY_PLAYPAUSE";
                command_playpause = "${pkgs.playerctl}/bin/playerctl --player=mpv,audacious,cmus play-pause";

                # Additional bindings on my K360 keyboard with `Fn` that could be used.
                # KEY_HOMEPAGE    -> F1 No label
                # KEY_MAIL        -> F2 No label
                # KEY_SEARCH      -> F3 No label
                # KEY_CALC        -> F4 No label
                # KEY_CONFIG      -> F5 Music note
              };

              wm-actions = {
                minimize = "<super> KEY_M";
                send_to_back = "none";
                toggle_always_on_top = "<super> KEY_T";
                toggle_fullscreen = "<shift> <super> KEY_F";
                toggle_maximize = "<super> KEY_F";
                toggle_showdesktop = "none";
                toggle_sticky = "<shift> <super> KEY_T";
              };

              window-rules = {
              };

              move = {
                activate = "<super> BTN_LEFT";
                enable_snap = "true";
                enable_snap_off = "true";
                join_views = "false";
                preview_base_border = "\\#404080CC";
                preview_base_color = "\\#8080FF80";
                preview_border_width = "3";
                quarter_snap_threshold = "50";
                snap_off_threshold = "10";
                snap_threshold = "10";
                workspace_switch_after = "-1";
              };

              resize = {
                activate = "<super> BTN_RIGHT";
                activate_preserve_aspect = "<alt> <super> BTN_RIGHT";
              };

              place = {
                mode = "center";
              };

              decoration = {
                # Bogus font
                font = "Sans 0";
                # We only make it a bordered thing
                title_height = "0";
                border_size = "2";
                # With these good and usual colours
                active_color = "\\#FF0000FF";
                inactive_color = "\\#000000FF";
                # Without buttons
                button_order = "";
                # ???
                ignore_views = "none";
              };

              preserve-output = {
                last_output_focus_timeout = "10000";
              };

              shortcuts-inhibit = {
                break_grab = "none";
                ignore_views = "none";
                inhibit_by_default = "none";
              };

              expo = {
                background = "\\#1A1A1AFF";
                duration = "300";
                inactive_brightness = "0.700000";
                keyboard_interaction = "true";
                offset = "10";
                select_workspace_1 = "KEY_1";
                select_workspace_2 = "KEY_2";
                select_workspace_3 = "KEY_3";
                select_workspace_4 = "KEY_4";
                select_workspace_5 = "KEY_5";
                select_workspace_6 = "KEY_6";
                select_workspace_7 = "KEY_7";
                select_workspace_8 = "KEY_8";
                select_workspace_9 = "KEY_9";
                toggle = "<super> KEY_E";
                transition_length = "200";
              };

              scale = {
                toggle = "<super> KEY_TAB";
                duration = "250";
                include_minimized = true;
                title_font_size = "0";
              };

              fast-switcher = {
                activate          = "<super> KEY_L";
                activate_backward = "<super> KEY_H";
                inactive_alpha = "0.5";
              };

              switcher = {
                next_view = "<shift> <super> KEY_L";
                prev_view = "<shift> <super> KEY_H";
                speed = "500";
                view_thumbnail_rotation = "30";
                view_thumbnail_scale = "1.000000";
              };

              oswitch = {
                next_output = "<alt> <super> KEY_RIGHT";
                next_output_with_win = "<ctrl> <alt> <super> KEY_RIGHT";
                prev_output = "<alt> <super> KEY_LEFT";
                prev_output_with_win = "<ctrl> <alt> <super> KEY_LEFT";
              };

              vswitch = {
                background = "\\#1A1A1AFF";
                binding_down = "<super> KEY_DOWN";
                binding_last = "";
                binding_left = "<super> KEY_LEFT";
                binding_right = "<super> KEY_RIGHT";
                binding_up = "<super> KEY_UP";
                duration = "300";
                gap = "20";
                send_win_down = "";
                send_win_last = "";
                send_win_left = "";
                send_win_right = "";
                send_win_up = "";
                with_win_down = "<shift> <super> KEY_DOWN";
                with_win_last = "";
                with_win_left = "<shift> <super> KEY_LEFT";
                with_win_right = "<shift> <super> KEY_RIGHT";
                with_win_up = "<shift> <super> KEY_UP";
                wraparound = "false";
              };

              vswipe = {
                background = "\\#1A1A1AFF";
                delta_threshold = "24.000000";
                duration = "180";
                enable_free_movement = "true";
                enable_horizontal = "true";
                enable_smooth_transition = "true";
                enable_vertical = "true";
                fingers = "4";
                gap = "16.000000";
                speed_cap = "0.050000";
                speed_factor = "256.000000";
                threshold = "0.350000";
              };

              # Eye candy

              animate = {
                close_animation = "zoom";
                duration = "250";
                enabled_for = ''(type equals "toplevel" | (type equals "x-or" & focusable equals true))'';
                fade_duration = "400";
                fade_enabled_for = ''type equals "overlay"'';
                open_animation = "zoom";
                startup_duration = "600";
                zoom_duration = "500";
                zoom_enabled_for = "none";
              };

              invert = {
                preserve_hue = "true";
                toggle = "<super> KEY_I";
              };

              wrot = {
                activate = "<ctrl> <super> BTN_RIGHT";
                activate-3d = "<shift> <super> BTN_RIGHT";
                invert = "false";
                reset = "<ctrl> <super> KEY_R";
                reset-one = "<super> KEY_R";
                reset_radius = "25.000000";
                sensitivity = "24";
              };

              zoom = {
                interpolation_method = "0";
                modifier = "<super> ";
                smoothing_duration = "300";
                speed = "0.010000";
              };

              # Steam Deck
              "input-device:FTS3528:00 2808:1015" = { output = "eDP-1"; };
              "input-device:FTS3528:00 2808:1015 UNKNOWN" = { output = "eDP-1"; };
            };
          };
        };
      }
    ]))
  ];
}
