{ config, lib, pkgs, ... }:

let
  enabled = config.samueldr.use-case.graphical-session.session == "niri";
  inherit (lib)
    mkIf
    mkMerge
  ;
in
{
  imports = [
    ./niri-config.nix
  ];
  config = mkMerge [
    { samueldr.use-case.graphical-session.availableSessions = [ "niri" ]; }
    (mkIf enabled ( mkMerge [
      {
        samueldr.use-case.wayland-session.enable = true;
        samueldr.use-case.wayland-session.locker.monitorPoweroffCommand =
          "niri msg action power-off-monitors"
        ;
      }
      { # niri itself
        services.displayManager = {
          sessionData.sessionNames = [ "niri" ];
        };
        environment.systemPackages = with pkgs; [
          # Replace niri with one forcing /etc/xdg/niri/config.kdl
          (pkgs.writeShellScriptBin "niri" ''
            exec systemd-run --user --pipe ${pkgs.niri}/bin/niri "$@"
          '')
          niri
        ];
        services.displayManager.sessionPackages = with pkgs; [
          niri
        ];
        nixpkgs.overlays = [(final: super: {
          niri =
          let
            # TODO: rebase my patches
            niri = (final.callPackage ./niri/package.nix {}).overrideAttrs({ patches ? [], ...}: {
              patches = patches ++ [
                # Hacky monitor model/manufacturer matching
                (final.fetchpatch {
                  url = "https://github.com/YaLTeR/niri/compare/v0.1.5...samueldr:niri:eadc73797963bfe76b270fa1e432e7ab80bde430.patch";
                  hash = "sha256-WiZMqm+pXsKMflSVMLRoL7jrGloAB7NQIPoYcshM/bc=";
                })
                # https://github.com/samueldr/niri/commits/wip/unclutter/
                (final.fetchpatch {
                  url = "https://github.com/YaLTeR/niri/compare/v0.1.5...samueldr:niri:952c5389229c5b46adef5191fed5b50db0bb541c.patch";
                  hash = "sha256-K+4ZruhxO0i4bv3T67oPHhIkN9o2EXtFxEL3a8bBdmE=";
                })
              ];
            });
          in
          final.symlinkJoin {
            name = niri.name;
            passthru = niri.passthru // {
              providedSessions = [ "niri" ];
            };
            meta = niri.meta // {
              priority = 10;
            };
            paths =
              [
                # Replace niri-session with one that is less... terrible...
                (final.writeShellScriptBin "niri-session" ''
                  set -e
                  set -u
                  PS4=" $ "
                  set -x

                  # TODO: sync with niri config
                  VARS=(
                    # Coming from system config
                    "XDG_CURRENT_DESKTOP"
                    "XDG_SESSION_TYPE"
                    "QT_QPA_PLATFORM"
                    "XCURSOR_THEME"
                    "XCURSOR_SIZE"
                    # There's no Xwayland
                    "DISPLAY"
                    # Actually set within Niri
                    "WAYLAND_DISPLAY"
                    "NIRI_SOCKET"
                  )

                  CLEANUP=(
                    # Some other variables some desktop environments set
                    # Some won't actually be unset, but reset to their default.
                    "XDG_CACHE_HOME"
                    "XDG_CONFIG_DIRS"
                    "XDG_CONFIG_HOME"
                    "XDG_DATA_DIRS"
                    "XDG_DATA_HOME"
                    "XDG_DESKTOP_PORTAL_DIR"
                    "XDG_RUNTIME_DIR"
                    "XDG_SEAT"
                    "XDG_SESSION_DESKTOP"
                    "XDG_SESSION_ID"
                    "XDG_VTNR"
                  )

                  at_exit() {
                    # Cleanup...
                    systemctl --user stop --force desktop-session.target || :

                    # Unset environment that we've set.
                    systemctl --user unset-environment "''${VARS[@]}" "''${CLEANUP[@]}"
                  }
                  trap at_exit EXIT SIGINT SIGTERM SIGKILL

                  # Make sure there's no already running session.
                  if systemctl --user -q is-active niri.service; then
                    echo 'A niri session is already running.'
                    exit 1
                  fi

                  # Reset failed state of all user units.
                  systemctl --user reset-failed

                  # Clear some environment...
                  systemctl --user unset-environment "''${CLEANUP[@]}"

                  # Sync required environment...
                  systemctl --user import-environment "''${VARS[@]}"

                  # But ensure wlroots doesn't try to use these bogus values...
                  systemctl --user unset-environment WAYLAND_DISPLAY DISPLAY

                  # Start niri and wait for it to terminate.
                  systemctl --user --wait start niri.service

                  # Force stop of graphical-session.target.
                  systemctl --user start --job-mode=replace-irreversibly niri-shutdown.target
                '')
                niri
              ]
            ;
          };
        })];
        systemd.user.services."niri" = {
          overrideStrategy = "asDropin";
          upholds = [ "desktop-session.target" ];
          bindsTo = [ "graphical-session.target" "desktop-session.target" ];
          requisite = [ "desktop-session.target" ];
          after = [ "graphical-session-pre.target" ];
          wants = [ "graphical-session-pre.target" ];
          serviceConfig = {
            # Update ExecStart for the user service with our edited one.
            # Hack for drop-in, adds `ExecStart=` as an empty value first, resetting the value.
            ExecStart = ''

              ExecStart=${pkgs.with-profile} ${pkgs.niri}/bin/niri --config "/etc/xdg/niri/config.kdl" --session
            '';
          };
        };
      }
    ]))
  ];
}
