{ config, lib, options, pkgs, ... }:

let
  cfg = config.samueldr.use-case.jovian-nixos;
  channels = import ../../npins;
  inherit (lib)
    mkDefault
    mkForce
    mkIf
    mkOption
    mkMerge
    types
  ;
in
{
  imports = [
    (channels.Jovian-NixOS.outPath + "/modules")
  ];
  options.samueldr.use-case.jovian-nixos = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable Jovian NixOS usage.
      '';
    };
    useSteamSession = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Start steam by default.
      '';
    };
    hardware = mkOption {
      type = types.enum [ "other" "steamdeck" "gpd-win-mini" ];
      default = "other";
      description = ''
        The hardware using this configuration.
      '';
    };
  };
  config = mkIf cfg.enable (mkMerge [
    (
      # Ensure evaluation works even when Jovian NixOS is not enabled, if this module is not enabled.
      if options ? jovian
      then {
        programs.steam.enable = true;
        programs.steam.remotePlay.openFirewall = true;
        networking.firewall.allowedTCPPorts = [
          27040
        ];

        jovian = {
          devices =
            if (cfg.hardware != "other")
            then { "${cfg.hardware}".enable = true; }
            else {}
          ;
          steam = {
            enable = true;
            autoStart = cfg.useSteamSession;
            user = "samuel";
            desktopSession = config.samueldr.use-case.graphical-session.session;
          };
        };

        services.displayManager.autoLogin.enable = mkIf cfg.useSteamSession (mkForce false);
        services.xserver.displayManager = mkIf cfg.useSteamSession {
          lightdm.enable = mkForce false;
        };
        services.displayManager.sddm.enable = mkForce false;

        boot.kernelPackages = mkIf (cfg.hardware == "steamdeck") pkgs.linuxPackages_jovian;
        hardware.pulseaudio.enable = mkIf (cfg.hardware == "steamdeck") (mkForce false);

        samueldr.use-case = {
          # Ensure at least those are enabled...
          app.terminal.enable = true;
          common-graphical.enable = true;
        };

        # We're using the Jovian NixOs autoStart instead.
        samueldr.convergent-session.autoLogin.enable = mkForce false;

        nixpkgs.overlays = [(final: prev: {
          steam = prev.steam.override {
            extraPreBwrapCmds = ''
              # Working around gobohide idiosyncrasies
              explicit+=( /sys /run /var )
              ignored+=( ''${explicit[@]} )
              declare -a auto_mounts
              for dir in ''${explicit[@]}; do
                auto_mounts+=(--bind "$dir" "$dir")
              done
            '';
          };
        })];
      }
      else {}
    )
    # Ensure we fail the build if this is enabled and Jovian NixOS is not available.
    {
      assertions = [
        {
          assertion = options ? jovian;
          message = "Jovian NixOS configuration is enabled, but Jovian NixOS is not imported.";
        }
      ];
    }
  ]);
}
