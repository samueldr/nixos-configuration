{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.default-gui-software;
  inherit (lib)
    mkDefault
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.use-case.default-gui-software = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable default GUI software.
      '';
    };
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      # Multimedia
      cmus
      yt-dlp

      # Not strictly GUI, but makes no sense without GUI
      ddcutil
      read-edid
    ];
    app-compartmentalization = {
      scoped-apps = {
        mpv = {
          package = pkgs.mpv-unwrapped.wrapper {
            mpv = pkgs.mpv-unwrapped;
            scripts = [ pkgs.mpvScripts.mpris ];
          };
          enable = true;
        };
        evince.enable = true;
      };
      services = {
        keepassxc.enable = true;
        nheko.enable = true;
        quasselclient.enable = true;
        audacious.enable = true;
        mumble.enable = true;
      };
    };
    samueldr.hacks.remove-applications.patterns = [
      "umpv.desktop"
    ];
    samueldr.use-case.app.terminal.enable = mkDefault true;
    # setuid for ddcutil
    security.wrappers = {
      ddcutil = {     
        source = "${pkgs.ddcutil}/bin/ddcutil";
        owner = "root"; 
        group = "root";  
      };                 
    };                                         
    # for ddcutil
    boot.kernelModules = [ "i2c-dev" ];        
  };
}
