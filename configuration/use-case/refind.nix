{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.refind;
  inherit (lib)
    mkIf
    mkMerge
    mkOption
    types
  ;
  inherit (pkgs.stdenv.hostPlatform)
    efiArch
  ;
in
{
  options.samueldr.use-case.refind = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable minimalistic use of rEFInd.
      '';
    };
  };
  config = mkMerge [
    (mkIf cfg.enable {
      boot.loader.grub.extraFiles = {
        "EFI/BOOT/boot${efiArch}.efi" = "${pkgs.refind}/share/refind/refind_${efiArch}.efi";
        "EFI/BOOT/refind.conf" = pkgs.writeText "refind.conf" ''
          timeout 1
          use_nvram false
          hideui all
        '';
      };
    })
  ];
}
