{ config, lib, options, pkgs, ... }:

let
  cfg = config.samueldr.use-case.graphical-boot;
  inherit (lib)
    mkBefore
    mkDefault
    mkIf
    mkMerge
    mkOption
    optional
    optionalString
    types
  ;
  mkUserDefault = lib.mkOverride ((pkgs.lib.mkDefault null).priority - 10);
  inherit (pkgs.stdenv.hostPlatform)
    efiArch
  ;
in
{
  options.samueldr.use-case.graphical-boot = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable graphical boot
      '';
    };
    resolution = {
      width = mkOption {
        type = types.int;
        description = ''
          Width of the generated image for the main panel.

          Used as a workaround for GRUB idiosyncracies
        '';
      };
      height = mkOption {
        type = types.int;
        description = ''
          Height of the generated image for the main panel.

          Used as a workaround for GRUB idiosyncracies
        '';
      };
    };
    flickerFree = {
      enable = mkOption {
        default = false;
        type = types.bool;
        description = ''
          Prepare the theme for "flicker-free" boot.

          The current implementation uses the BGRT.

          Use the HackBGRT options to configure the logo shown during boot.
        '';
      };
    };
    hackBGRT = {
      enable = mkOption {
        default = false;
        type = types.bool;
        description = ''
          Use HackBGRT to configure the logo shown during boot.
        '';
      };
      bootPath = mkOption {
        type = types.str;
        internal = true;
        description = ''
          Path of the next stage during boot.

          Generally, this will be the configured bootloader (GRUB, systemd-boot).
        '';
      };
      bitmap = mkOption {
        type = with types; oneOf [ path package ];
        internal = true;
        description = ''
          Bitmap for the replacement BGRT.

          This is the internal final representation. It should be set by other options.
        '';
      };
      rotate = mkOption {
        type = types.enum [ "normal" "clockwise" "counter-clockwise" "upside-down" ];
        default = "normal";
        description = ''
          Whether to rotate the image or not.

          This can be used on systems where the panel orientation is not its "native" orientation.

          > **TIP**: For counter-clockwise or clockwise rotation, you may want need to rotate
          > the image against the panel's rotation to make it correctly orientated.
        '';
      };
      png = {
        file = mkOption {
          type = with types; nullOr (oneOf [ path package ]);
          default = null;
          description = ''
            PNG file to convert automatically to the proper format for HackBGRT.
          '';
        };
        autocrop = mkOption {
          type = types.bool;
          default = true;
          description = ''
            Autocrop the input file.
          '';
        };
      };
      svg = {
        file = mkOption {
          type = with types; nullOr (oneOf [ path package ]);
          default = null;
          description = ''
            When set, the bitmap used for HackBGRT will be built from that SVG file.

            It will do so by setting the `png.file` option accordingly.

            For convenience, you can design at e.g. 1920×1080, and `png.autocrop` will
            crop the image accordingly.

            Using a full screen resolution is useful when combined with `exportHeight`.
            For example, when setting `exportHeight` to `720` for a 720p display, the
            design of the source file (e.g. 1080p) will scale as expected.

            This is useful when deploying the same configuration on a varied set of
            hardware, to keep the logos scaled to their displays.
          '';
        };
        exportHeight = mkOption {
          type = with types; nullOr int;
          default = null;
          description = ''
            Height to use as render height for the input file.

            When null, the file intrinsics will be implicitly used.
          '';
        };
      };
    };
  };
  config = mkMerge [
    ({
      nixpkgs.overlays = [(final: super: {
        hackBGRT = final.callPackage (
          { stdenv
          , fetchFromGitHub
          , binutils-unwrapped
          , gnu-efi
          }:

          let
            archids = {
              x86_64-linux = { hostarch = "x86_64"; efiPlatform = "x64"; };
              i686-linux = rec { hostarch = "ia32"; efiPlatform = hostarch; };
              aarch64-linux = { hostarch = "aarch64"; efiPlatform = "aa64"; };
            };

            inherit
              (archids.${stdenv.hostPlatform.system} or (throw "unsupported system: ${stdenv.hostPlatform.system}"))
              hostarch efiPlatform;
          in

          stdenv.mkDerivation rec {
            pname = "HackBGRT";
            version = "unstable-2023-03-09";

            src = fetchFromGitHub {
              owner = "samueldr";
              repo = pname;
              rev = "8765491a9f051aebfa8546b84c438e91ab5264d5";
              sha256 = "sha256-zNaRNUUsfg/CrBMEpxNDzC1A00AJdEzQFT2R6N8v2WU=";
            };

            buildInputs = [
              gnu-efi
            ];

            makeFlags = [
              "GIT_DESCRIBE=NixOS-${version}"
              "EFIINC=${gnu-efi}/include/efi"
              "EFILIB=${gnu-efi}/lib"
              "GNUEFILIB=${gnu-efi}/lib"
              "EFICRT0=${gnu-efi}/lib"
              "HOSTARCH=${hostarch}"
              "ARCH=${hostarch}"
            ];

            installPhase = ''
              runHook preInstall

              mkdir -p $out/
              cp HackBGRT.efi $out/HackBGRT${efiPlatform}.efi

              mkdir -p $out/share/doc/HackBGRT
              cp -t $out/share/doc/HackBGRT README.efilib LICENSE

              runHook postInstall
            '';

            hardeningDisable = [ "all" ];
          }
        ) { };
      })];
      samueldr.use-case.graphical-boot.hackBGRT = {
        svg.file = ../../artwork/bgrt.svg;
      };
    })
    (mkIf (cfg.enable && !(options ? mobile)) {
      # Mobile NixOS has better semantics around setting a console.
      boot.kernelParams = mkBefore [
        "console=tty0"
      ];
    })
    (mkIf cfg.enable {
      samueldr.use-case.silent-boot.enable = true;
      boot.plymouth.enable = true;
      boot.plymouth.theme = mkDefault "spinner";
      boot.loader.grub.theme = pkgs.callPackage ../../artwork/grub-theme {
        logo = config.samueldr.use-case.graphical-boot.hackBGRT.png.file;
      };
      boot.kernelParams = [
        "splash"
      ];
    })
    (mkIf (cfg.enable && !cfg.hackBGRT.enable && !cfg.flickerFree.enable) {
      # GRUB puts that picture in the top-left.
      boot.loader.grub.splashImage = pkgs.runCommandNoCC "grub-splash-workaround.png" { nativeBuildInputs = [ pkgs.imagemagick ]; } ''
        convert ${config.samueldr.use-case.graphical-boot.hackBGRT.png.file} \
          -gravity center \
          -background black \
          -extent ${toString cfg.resolution.width}x${toString cfg.resolution.height} PNG32:$out
      '';
      boot.loader.grub.splashMode = "normal";
      boot.loader.grub.backgroundColor = "#000000";
    })
    (mkIf (cfg.enable && cfg.flickerFree.enable) {
      warnings = []
        ++ optional (!(options.boot.loader.grub ? timeoutStyle))
          "Option `boot.loader.grub.timeoutStyle` is missing, GRUB will be shown even though flicker-free boot is requested."
      ;
      boot.kernelParams = [
        # Dropping fbcon from VT1 means the BGRT (or the GRUB splashImage) is shown.
        # NOTE: this requires using a stage-1 that can cope without an fbcon.
        "fbcon=vc:3-6"
        # fbcon on 3 through 6 so I can have two graphical sessions without fbcon being seen.
      ];
      systemd.services."autovt@tty1".enable = false;
      systemd.services."autovt@tty2".enable = false;
      # Don't show grub needlessly for long, but ensure it can get a keypress.
      boot.loader.timeout = 1;
      boot.loader.grub = mkMerge [
        # `timeoutStyle`is a new option, let's keep my config backward compatible.
        (if !(options.boot.loader.grub ? timeoutStyle) then {} else {
          # `hidden` is the only value allowing flicker-free boot.
          # `countdown` shows a text-mode countdown.
          # `menu` obviously shows the menu.
          timeoutStyle = "hidden";
        })
        {
          # For proper flicker-free, no background should be used.
          splashImage = null;
        }
      ];
      boot.plymouth.theme = mkUserDefault "bgrt";
    })
    (mkIf (cfg.hackBGRT.enable) {
      assertions = [
        {
          assertion = cfg.enable;
          message = "hackBGRT is enabled without enabling samueldr.use-case.graphical-boot; this will not work as expected.";
        }
      ];
    })
    (mkIf (cfg.enable && cfg.hackBGRT.enable) {
      # We're assuming we'll have the fallback bootloader location be the entrypoint.
      boot.loader.efi.canTouchEfiVariables = lib.mkForce false;
      boot.loader.grub.splashImage = null;
      samueldr.use-case.graphical-boot = {
        hackBGRT = mkMerge [
          (mkIf (config.boot.loader.grub.enable) {
            bootPath = ''\EFI\NixOS-boot\grub${efiArch}.efi'';
          })
        ];
      };
      boot.loader.grub.extraFiles = {
        "EFI/BOOT/boot${efiArch}.efi" = "${pkgs.hackBGRT}/HackBGRT${efiArch}.efi";
        # Sample config: https://github.com/samueldr/HackBGRT/blob/wip/config.txt
        "EFI/HackBGRT/config.txt" = pkgs.writeText "HackBGRT.conf" ''
          boot=${cfg.hackBGRT.bootPath}
          image=path=\EFI\bgrt.bmp
        '';
        "EFI/bgrt.bmp" =
          if cfg.hackBGRT.rotate == "normal"
          then cfg.hackBGRT.bitmap
          else (
            pkgs.runCommand "bgrt-rotated.bmp" { nativeBuildInputs = [ pkgs.imagemagick ]; } ''
              convert ${cfg.hackBGRT.bitmap} -rotate ${{
                "clockwise" = "90";
                "counter-clockwise" = "270";
                "upside-down" = "180";
              }."${cfg.hackBGRT.rotate}"} -type truecolor BMP3:$out
            ''
          )
        ;
      };
    })
    (mkIf (cfg.hackBGRT.png.file != null) {
      samueldr.use-case.graphical-boot = {
        hackBGRT.bitmap = pkgs.runCommand "bgrt.bmp" { nativeBuildInputs = [ pkgs.imagemagick ]; } ''
          convert ${cfg.hackBGRT.png.file} ${optionalString cfg.hackBGRT.png.autocrop "-trim"} -type truecolor BMP3:$out
        '';
      };
    })
    (mkIf (cfg.hackBGRT.svg.file != null) {
      samueldr.use-case.graphical-boot = {
        hackBGRT.png.file = pkgs.runCommand "bgrt.png" { nativeBuildInputs = [ pkgs.inkscape pkgs.imagemagick ]; } ''
          inkscape ${cfg.hackBGRT.svg.file} \
            ${optionalString (cfg.hackBGRT.svg.exportHeight != null) "--export-height=${toString cfg.hackBGRT.svg.exportHeight}"} \
            --export-filename $out

          ${optionalString cfg.hackBGRT.png.autocrop ''
            mogrify -trim PNG32:$out
          ''}
        '';
      };
    })
  ];
}
