{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.aliases;
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
    mkOption
    types
  ;

  mkSerialHelper = { name, baud }: pkgs.writeShellScriptBin "serial-${name}" ''
    exec ${pkgs.picocom}/bin/picocom -b ${toString baud} "$@"
  '';
in
{
  options.samueldr.use-case.aliases = {
    enable = mkEnableOption "personalized aliases commands";
    terminal-emulator = mkOption {
      type = with types; listOf str;
      default = [ "false" ];
      description = ''
        Command to use to run an arbitrary command in a terminal emulator.
      '';
    };
    video-height = mkOption {
      type = types.int;
      default = 1080;
      description = ''
        Max height of a video downloaded using `yt`.
      '';
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      (
        pkgs.writeScriptBin "irb" ''
          #!${pkgs.ruby}/bin/ruby
          exec("${pkgs.ruby}/bin/irb", *ARGV)
        ''
      )
      (
        pkgs.writeScriptBin "git-emptify" ''
          #!${pkgs.ruby}/bin/ruby
          BRANCH_NAME="000/readme"

          DIR = `git rev-parse --show-toplevel`.strip

          Dir.chdir(DIR) do
            # Do a weird dance since you can't -B and --orphan.
            system "git checkout -B #{BRANCH_NAME}-tmp"
            system "git branch -D #{BRANCH_NAME}"
            system "git checkout --orphan #{BRANCH_NAME}"
            system "git branch -D #{BRANCH_NAME}-tmp"
            # Ensure nothing is staged
            system "git reset"
            # Make an empty commit
            system "git commit --allow-empty -m 'Empty root commit.'"
          end
        ''
      )
      (
        pkgs.writeShellScriptBin "git-necromancy" ''
          PS4=" $ ";
          set -x;
          for rev in $(git rev-list --reverse --no-merges --author-date-order --all); do
            git checkout $rev -- .
          done
          git checkout HEAD -- .
        ''
      )
      (
        pkgs.writeScriptBin "git-prune-branches" ''
          #!${pkgs.ruby}/bin/ruby
          ALLOW_LIST = [
            "develop",
            "development",
            "main",
            "master",
            "released",
          ]
          branches = `git branch --merged`
            .strip.split("\n")
            .select { |branch| !branch.match(/^[*+]/) }
            .map(&:strip)
            .select { |branch| !ALLOW_LIST.include?(branch) }

          if ARGV.first == "--yes" then
            exec("git", "branch", "-d", *branches)
          else
            puts branches.map{|branch| " - #{branch}"}.join("\n")
          end
        ''
      )
      (
        pkgs.writeScriptBin "logs" ''
          #!${pkgs.ruby}/bin/ruby
          cmd = %w(${pkgs.systemd}/bin/journalctl -b --lines=200 --all --exclude-identifier syncthing)
          %w(
            syncthing
          ).each do |identifier|
            cmd << "--exclude-identifier" << identifier
          end
          exec(*cmd, *ARGV)
        ''
      )
      (
        pkgs.writeScriptBin "nix-print-roots" ''
          #!${pkgs.ruby}/bin/ruby
          puts(
            `nix-store --gc --print-roots`
              .strip.split(/\n+/)
              .filter do |line|
                case ARGV.join(" ")
                when /-vv\b/
                  true
                when /-v\b/
                  !line.match(%r[^{|^/proc])
                else
                  !line.match(%r[^{|^/nix/var|^/proc])
                end
              end
              .join("\n")
          )
        ''
      )
      (
        pkgs.writeScriptBin "ssh.term" ''
          #!${pkgs.ruby}/bin/ruby
          TERM = ${builtins.toJSON cfg.terminal-emulator}
          CMD = ["ssh", *ARGV]
          exec(*TERM, *CMD);
        ''
      )
      (
        pkgs.writeScriptBin "vvim" ''
          #!${pkgs.ruby}/bin/ruby
          TERM = ${builtins.toJSON cfg.terminal-emulator}
          CMD = ["nvim", *ARGV]
          exec(*TERM, *CMD);
        ''
      )
      (
        pkgs.writeScriptBin "with_x11" ''
          #!${pkgs.ruby}/bin/ruby
          # This script borrows the DISPLAY and XAUTHORITY from Xwayland args.
          # Useful when running with a reduced environment.

          if ARGV.length < 1 then
            $stderr.puts <<~EOF
            Usage: with_x11 <cmdline...>

            Launches the given cmdline with an environment suitable for launching under Xwayland.
            EOF
            exit 1
          end

          # TODO: detect other X servers
          data = `${pkgs.procps}/bin/pgrep -fla Xwayland`.strip
          # 1688 /nix/store/eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee-xwayland-22.1.8/bin/Xwayland :0 -auth /.../..../..../xauth_HAUOEc -listenfd 54 -listenfd 55 -displayfd 46 -rootless -wm 49
          # ¯¯¯¯ ------------------------------------------------------------------------ ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
          pid, argv0, argv = data.split(/\s+/, 3)
          # Group in sub-arrays for every parameter
          args = argv.split(/\s+/).slice_before {|el| el.match(/^-/)}
          params, positional = args.partition { |arg| arg.first.match(/^-/) }
          # Extricate positional args from their arrays
          positional.map! do |arr|
            arr.first
          end

          # Add to the current environment
          ENV["DISPLAY"] = positional.shift
          ENV["XAUTHORITY"] = params.find { |args| args.first == "-auth" }.last

          # `exec` into the desired cmdline
          exec(*ARGV)
        ''
      )
      (
        pkgs.writeScriptBin "yt" ''
          #!${pkgs.ruby}/bin/ruby

          OPTS = [
            "-o", "%(upload_date)s - %(uploader)s - %(title)s.%(id)s.%(ext)s",
            "-f", "bestvideo[height<=${toString cfg.video-height}][vcodec!^=vp9]+bestaudio/best[height<=${toString cfg.video-height}]",
            "--embed-subs",
            "--embed-metadata",
          ]

          # Nice the running process since it *may* end up merging file formats, and
          # when it does, it can cause a running process (let's say video player) to
          # stutter on some lower perfs machines.
          exec("nice", "-n20", "${pkgs.yt-dlp}/bin/yt-dlp", *OPTS, *ARGV)
        ''
      )
      (mkSerialHelper { name = "allwinner"; baud = "115200"; })
      (mkSerialHelper { name = "rockchip";  baud = "1500000"; })
      (mkSerialHelper { name = "esp32";     baud = "460800"; })
    ];
  };
}
