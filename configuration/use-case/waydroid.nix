{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkOption
    mkIf
    types
  ;

  cfg = config.samueldr.use-case.waydroid;

  waydroid-wrapper = pkgs.callPackage (
    { writeShellScript
    , waydroid
    }:
    writeShellScript "waydroid-wrapper" ''
      ARGS=(
          ${waydroid}/bin/waydroid
              --details-to-stdout
              show-full-ui
      )

      PS4=" $ "
      set -x
      exec "''${ARGS[@]}"
    ''
  ) {};
  weston-waydroid-ini = pkgs.callPackage (
    { writeText
    }:
    writeText "weston-waydroid.ini" ''
      [core]
      idle-time=0
      backend=x11
      xwayland=false
      icon_filename=/run/current-system/sw/share/icons/hicolor/512x512/apps/waydroid.png
      title=Waydroid
      wm_class_name=weston-waydroid
      wm_class_class=weston-waydroid

      [shell]
      client=${waydroid-wrapper}
      locking=false
      animation=false
      startup-animation=false
      background-image=
      background-color=0x00000000
      panel-position=none

      [output]
      name=X1
      transform=normal
      scale=1

      [libinput]
      enable-tap=true
    ''
  ) {};

  weston-waydroid-wrapper = pkgs.callPackage (
    { writeShellScriptBin
    , systemd
    , weston
    , weston-waydroid-ini
    , with-profile
    }:
    writeShellScriptBin "weston-waydroid" ''
      ARGS=(
        ${systemd}/bin/systemd-run
            --user
            --collect
            --quiet
            --unit=weston-waydroid

        ${with-profile}

        ${weston}/bin/weston
          --socket wayland-waydroid
          --config ${weston-waydroid-ini}
      )

      PS4=" $ "
      set -x
      exec "''${ARGS[@]}"
    ''
  ) {
    inherit weston-waydroid-ini;
    weston = pkgs.weston.overrideAttrs({ patches ? [], ... }: {
      patches = patches ++ [
        (pkgs.fetchpatch {
          url = "https://github.com/samueldr-wip/freedesktop-weston/commit/cc20abbab531370d3925b32a66e4eb4bdad31a5e.patch";
          hash = "sha256-efty5b4+BvlOU5RhMt2u7A/OeUeqEUCjaaUiUq9trBY=";
        })
      ];
    });
  };
  weston-waydroid-desktop-file = pkgs.callPackage (
    { runCommand, waydroid }:
    runCommand "weston-waydroid-desktop-file" {
      meta.priority = 2;
      inherit waydroid;
    } ''
      (
      mkdir -p $out/share/applications/
      cd $out/share/applications/
      cat "$waydroid/share/applications/Waydroid.desktop" > Waydroid.desktop
      substituteInPlace Waydroid.desktop \
        --replace 'Exec=waydroid first-launch' 'Exec=weston-waydroid'
      )
    ''
  ) {};
in
{
  options.samueldr.use-case.waydroid = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable use of waydroid on this system.
      '';
    };
  };

  config = mkIf (cfg.enable) {
    virtualisation.waydroid.enable = true;
    environment.systemPackages = [
      weston-waydroid-wrapper
      weston-waydroid-desktop-file
    ];

    fileSystems."/Users/samuel/Android" = {
      device = "${pkgs.bindfs}/bin/bindfs#/Users/samuel/.local/share/waydroid/data/media/0/";
      fsType = "fuse";
      noCheck = true;
      options = [
        "_netdev"
        "x-systemd.idle-timeout=600"
        "x-systemd.automount"
        "noauto"
        "nofail"
        "noatime"
        "users"
        "allow_other"
        "map=10138/samuel:@10138/@users"
        "perms=do+rwx:o+rw"
      ];
    };
  };
}
