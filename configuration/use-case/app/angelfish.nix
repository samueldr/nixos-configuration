{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.app.angelfish;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
  inherit (config.samueldr.hardware) memory;
in
{
  options.samueldr.use-case.app.angelfish = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable configured use of angelfish.
      '';
    };
  };
  config = mkIf cfg.enable {

    environment.variables = {
      BROWSER = [
        "angelfish"
      ];
    };

    app-compartmentalization = {
      scoped-apps = {
        "angelfish" = {
          enable = true;
          commands =
            let
              cfg = {
                systemdRunArguments = [
                  "-p" "MemoryHigh=${toString (memory / 8 * 5)}M"
                  "-p" "MemoryMax=${ toString (memory / 8 * 6)}M"
                ];
                environment = {
                };
              };
            in
            {
              "angelfish" = cfg;
              "angelfish-webapp" = cfg;
            }
          ;
        };
      };
    };
  };
}
