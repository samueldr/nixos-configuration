{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.app.screenshot;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
  inherit (config.samueldr.hardware) memory;
in
{
  options.samueldr.use-case.app.screenshot = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable configured use of screenshot tool.
      '';
    };
  };
  config = mkIf cfg.enable {
    samueldr.programs.screenshot = {
      enable = true;
      rsync_url_host = "https://stuff.samueldr.com/screenshots";
      rsync_destination = "stuff.samueldr.com:www/com.samueldr.stuff/www/screenshots";
    };
  };
}
