{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.app.firefox;
  inherit (lib)
    mkDefault
    mkOption
    mkIf
    types
  ;
  inherit (config.samueldr.hardware) memory;
in
{
  options.samueldr.use-case.app.firefox = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable configured use of firefox.
      '';
    };
  };
  config = mkIf cfg.enable {

    environment.variables = {
      BROWSER = [
        "firefox"
      ];
    };

    app-compartmentalization = {
      scoped-apps = {
        "firefox" = {
          enable = true;
          commands = {
            "firefox" = {
              systemdRunArguments = [
                "-p" "MemoryHigh=${toString (memory / 8 * 5)}M"
                "-p" "MemoryMax=${ toString (memory / 8 * 6)}M"
              ];
              environment = {
                MOZ_USE_XINPUT2 = "1";
              };
            };
          };
        };
      };
    };
    # This won't actually work until firefox has been started once...
    # ... but it's good enough, as it points to an evergreen location.
    system.userActivationScripts = {
      "firefox-userchrome" = {
        text = ''
          for p in $(grep ^Path= ~/.mozilla/firefox/profiles.ini | ${pkgs.gnused}/bin/sed -e 's/Path=//'); do
            (
            cd ~/.mozilla/firefox/$p
            mkdir -p chrome
            ln -fs /etc/firefox/userchrome.css chrome/userChrome.css
            )
          done
        '';
      };
    };
    environment.etc."firefox/userchrome.css".text = ''
      /* https://www.reddit.com/r/firefox/comments/b3slhs/why_is_there_absolutely_no_possible_way_to/ej1z94w/ */
      .tabbrowser-tab {
        min-width: initial !important;
      }
      .tab-content {
        overflow: hidden !important;
      }

      /* Custom slimmer tabs */
      .tab-background {
        margin-top: 2px !important;
        margin-bottom: 2px !important;
      }
      .tab-label-container {
        height: var(--tab-min-height) !important;
      }
      :root {
        --tab-min-height: 26px !important;
      }

      /* better tab bar appearance */
      :root {
        --toolbar-bgcolor: #eee !important;
        --toolbar-color: #000 !important;
        --my-tabs-bg: #000 !important;
        --my-tabs-fg: #989 !important;
        --my-tabs-border-inactive: 1px solid #444 !important;
        --my-tabs-border-active:   1px solid #aaa !important;
      }

      *[privatebrowsingmode] {
        --toolbar-bgcolor: #111 !important;
        --toolbar-color: #ddd !important;
        --my-tabs-bg: #000 !important;
        --my-tabs-fg: #444 !important;
        --my-tabs-border-inactive: 1px solid #333 !important;
        --my-tabs-border-active:   1px solid #666 !important;
      }

      #TabsToolbar {
        appearance: unset !important;
        background: var(--my-tabs-bg) !important;
      }
      .tabbrowser-tab {
        border: var(--my-tabs-border-inactive) !important;
        border-bottom: 0 !important;
        background: var(--my-tabs-bg) !important;
        color: var(--my-tabs-fg) !important;
        border-radius: 4px 4px 0 0 !important;
      }
      .tabbrowser-tab .tab-background {
        background: transparent !important;
        border: 0 !important;
        box-shadow: none !important;
      }
      .tabbrowser-tab[selected],
      .tabbrowser-tab[visuallyselected="true"] {
        /*
        border: 1px solid var(--toolbar-color) !important;
        */
        border: var(--my-tabs-border-active) !important;
        border-bottom: 0 !important;
        background: var(--toolbar-bgcolor) !important;
        color: var(--toolbar-color) !important;
      }

      /* Tighter tab bar */
      .tabs-alltabs-button {
        display: none !important;
      }
      .titlebar-spacer {
        width: 1ex !important;
      }
      .titlebar-buttonbox-container {
        display: none !important;
      }

      /* Window focus appearance */
      #navigator-toolbox-background {
        appearance: none !important;
        background-color: unset !important;
        background-image: linear-gradient(180deg, rgba(80,80,80,1) 0%, rgba(200,200,200,1) 55%, rgba(255,255,255,1) 100%) !important;
      }
      #navigator-toolbox:-moz-window-inactive {
      }
    '';
    programs.firefox.enable = true;
    # https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
    programs.firefox.autoConfig = ''
    '';
    # https://github.com/mozilla/policy-templates/blob/master/README.md
    programs.firefox.policies = {
      DisableFeedbackCommands = true;
      DisableFirefoxAccounts = true;
      DisableFirefoxScreenshots = true;
      DisableFirefoxStudies = true;
      DisablePocket = true;
      DisableProfileImport = true;
      DisableProfileRefresh = true;
      DisableSetDesktopBackground = true;
      DisableTelemetry = true;
      DisableThirdPartyModuleBlocking = true;
      HardwareAcceleration = true;
      NetworkPrediction = false;
      NewTabPage = true;
      NoDefaultBookmarks = true;
      OfferToSaveLogins = false;
      OverrideFirstRunPage = "about:blank";
      OverridePostUpdatePage = "about:blank";
      PasswordManagerEnabled = false;
      PictureInPicture.Enabled = false;
      PictureInPicture.Locked = false;
      RequestedLocales = "en-CA,en-GB,en-US,en,fr-CA,fr";
      SearchSuggestEnabled = false;

      FirefoxHome = {
        Search = false;
        TopSites = false;
        SponsoredTopSites = false;
        Highlights = false;
        Pocket = false;
        SponsoredPocket = false;
        Snippets = false;
        Locked = true;
      };

      Homepage = {
        URL = "about:blank";
        Locked = true;
        StartPage = "none";
      };

      UserMessaging = {
        WhatsNew = false;
        ExtensionRecommendations = false;
        FeatureRecommendations = false;
        UrlbarInterventions = false;
        SkipOnboarding = true;
        MoreFromMozilla = false;
      };

      ExtensionSettings = {
        # uBlock
        "uBlock0@raymondhill.net" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
        };
        # Stylus
        "{7a7a4a92-a2a0-41d1-9fd7-1e92480d612d}" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/styl-us/latest.xpi";
        };
        # synccit
        "{ae1eba58-4e65-48ae-8436-d93346bd059d}" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/synccit/latest.xpi";
        };
        # Onetab
        "extension@one-tab.com" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/onetab/latest.xpi";
        };
        # Tampermonkey
        "firefox@tampermonkey.net" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/tampermonkey/latest.xpi";
        };
        # uBlacklist
        "@ublacklist" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/ublacklist/latest.xpi";
        };
        # User-Agent Switcher and Manager
        "{a6c4a591-f1b2-4f03-b3ff-767e5bedf4e7}" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/user-agent-string-switcher/latest.xpi";
        };
        # Firefox Multi-Account Containers
        "@testpilot-containers" = {
          "installation_mode" = "force_installed";
          "install_url" = "https://addons.mozilla.org/firefox/downloads/latest/multi-account-containers/latest.xpi";
        };
      };
    };
    programs.firefox.preferences = {
      "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

      "browser.newtabpage.activity-stream.discoverystream.enabled" = false;
      "browser.newtabpage.activity-stream.showWeather" = false;
      "browser.newtabpage.activity-stream.system.showWeather" = false;
      "browser.startup.page" = 3; # Blank
      "browser.tabs.warnOnClose" = true;
      "extensions.formautofill.creditCards.enabled" = false;
      "extensions.pocket.enabled" = false;
      "general.autoScroll" = true;
      "general.smoothScroll" = false;
      "intl.locale.requested" = "en-CA,en-GB,en-US,en,fr-CA,fr";
      "browser.uitour.enabled" = false;
      "browser.translations.neverTranslateLanguages" = "fr";

      # Spywarefox
      "browser.shopping.experience2023.active" = false;
      "browser.shopping.experience2023.survey.enabled" = false;
      "dom.private-attribution.submission.enabled" = false;

      # UI
      "browser.uidensity" = mkDefault 1; # 0: default, 1: compact, 2: touch

      # Tab bar
      "browser.tabs.inTitlebar" = mkDefault 2;
      "toolkit.tabbox.switchByScrolling" = true;

      # Toolbar
      "browser.proton.toolbar.version"= 3;
      "browser.toolbars.bookmarks.showOtherBookmarks" = false;
      "browser.download.autohideButton" = false;

      # Actual toolbar placement
      "browser.uiCustomization.state" = mkDefault (builtins.toJSON {
        currentVersion = 19;
        placements = {
          toolbar-menubar = [ "menubar-items" ];
          TabsToolbar =     [ "tabbrowser-tabs" "alltabs-button" ];
          nav-bar = [
            "back-button"
            "forward-button"
            "stop-reload-button"
            "urlbar-container"
            # "save-to-pocket-button" # Pocket integration, yuck
            # "fxa-toolbar-menu-button" # Firefox account button
            "ublock0_raymondhill_net-browser-action"
            "_testpilot-containers-browser-action"
            "unified-extensions-button"
          ];
          PersonalToolbar = [ "personal-bookmarks" ];

          unified-extensions-area = [
            "extension_one-tab_com-browser-action"
            "firefox_tampermonkey_net-browser-action"
            "_ublacklist-browser-action"
            "_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action" # Stylus
            "_a6c4a591-f1b2-4f03-b3ff-767e5bedf4e7_-browser-action" # User-Agent Switcher and Manager
          ];
          widget-overflow-fixed-list = [
            "downloads-button"
            "new-tab-button"
          ];
        };
      });

      # URL bar
      "browser.urlbar.autoFill" = false;
      "browser.urlbar.bestMatch.enabled" = false;
      "browser.urlbar.maxHistoricalSearchSuggestions" = 5;
      "browser.urlbar.maxRichResults" = 20;
      "browser.urlbar.merino.enabled" = false;
      "browser.urlbar.quicksuggest.enabled" = false;
      "browser.urlbar.quicksuggest.migrationVersion" = 2;
      "browser.urlbar.quicksuggest.scenario" = "history";
      "browser.urlbar.richSuggestions.tail" = false;
      "browser.urlbar.suggest.addons" = false;
      "browser.urlbar.suggest.bestmatch" = false;
      "browser.urlbar.suggest.bookmark" = false;
      "browser.urlbar.suggest.calculator" = false;
      "browser.urlbar.suggest.engines" = false;
      "browser.urlbar.suggest.history" = false;
      "browser.urlbar.suggest.mdn" = false;
      "browser.urlbar.suggest.openpage" = false;
      "browser.urlbar.suggest.pocket" = false;
      "browser.urlbar.suggest.quicksuggest.nonsponsored" = false;
      "browser.urlbar.suggest.quicksuggest.sponsored" = false;
      "browser.urlbar.suggest.remotetab" = false;
      "browser.urlbar.suggest.searches" = false;
      "browser.urlbar.suggest.topsites" = false;
      "browser.urlbar.suggest.weather" = false;
      "browser.urlbar.trimURLs" = false;
      "browser.urlbar.shortcuts.bookmarks" = true;
      "browser.urlbar.shortcuts.history" = true;
      "browser.urlbar.shortcuts.tabs" = false;

      "browser.urlbar.resultGroups" = builtins.toJSON {
        children = [
          {
            children = [
              { group = "heuristicTest"; }
              { group = "heuristicExtension"; }
              { group = "heuristicSearchTip"; }
              { group = "heuristicOmnibox"; }
              { group = "heuristicEngineAlias"; }
              { group = "heuristicBookmarkKeyword"; }
              { group = "heuristicAutofill"; }
              { group = "heuristicPreloaded"; }
              { group = "heuristicTokenAliasEngine"; }
              { group = "heuristicFallback"; }
            ];
            maxResultCount = 1;
          }
          { availableSpan = 5; group = "extension"; }
          {
            children = [
              {
                children = [
                  { children = [ { flex = 2; group = "formHistory"; } { flex = 4; group = "remoteSuggestion"; } ]; flexChildren = true; } { group = "tailSuggestion"; }
                ];
                flex = 2;
              }
              {
                children = [
                  { availableSpan = 3; group = "inputHistory"; }
                  {
                    children = [
                      { flex = 1; group = "remoteTab"; }
                      { flex = 2; group = "general"; }
                      { flex = 2; group = "aboutPages"; }
                      { flex = 1; group = "preloaded"; }
                    ];
                    flexChildren = true;
                  }
                  { group = "inputHistory"; }
                ];
                flex = 1;
                group = "generalParent";
              }
            ];
            flexChildren = true;
          }
        ];
      };
    };
  };
}
