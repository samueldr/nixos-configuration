{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.app.terminal;
  inherit (lib)
    mkDefault
    mkOption
    mkIf
    types
  ;
  inherit (config.samueldr.hardware) memory;
in
{
  options.samueldr.use-case.app.terminal = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable configured use of terminal.
      '';
    };
  };
  config = mkIf cfg.enable {
    samueldr.use-case.aliases.terminal-emulator = [ "terminal" "-e" ];
    samueldr.terminal.enable = true;
    app-compartmentalization = {
      scoped-apps = {
        "terminal" = {
          package = pkgs.samueldr-terminal;
          enable = true;
          commands = {
            "terminal" = {};
          };
          useGlobalEnvironment = true;
        };
      };
    };
  };
}

