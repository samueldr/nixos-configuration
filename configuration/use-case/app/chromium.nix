{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.app.chromium;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
  inherit (config.samueldr.hardware) memory;
in
{
  options.samueldr.use-case.app.chromium = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable configured use of firefox.
      '';
    };
  };
  config = mkIf cfg.enable {

    environment.variables = {
      BROWSER = [
        "chromium"
      ];
    };

    app-compartmentalization = {
      scoped-apps = {
        "chromium" = {
          enable = true;
          commands =
            let
              cfg = {
                systemdRunArguments = [
                  "-p" "MemoryHigh=${toString (memory / 8 * 5)}M"
                  "-p" "MemoryMax=${ toString (memory / 8 * 6)}M"
                ];
                environment = {
                };
              };
            in
            {
              "chromium" = cfg;
              "chromium-browser" = cfg;
            }
          ;
        };
      };
    };
    programs.chromium.enable = true;
    programs.chromium.extensions = [
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
      "occjjkgifpmdgodlplnacmkejpdionan" # autoscroll
      "clngdbkpkpeebahjckkjfobafhncgmne" # stylus
      "hdgloanjhdcenjgiafkpbehddcnonlic" # protect my choices
      "djgggkkgpoeknlpdllmhdagbfnhaigmd" # synccit
      "chphlpgkkbolifaimnlloiipkdnihall" # onetab
      "mohaicophfnifehkkkdbcejkflmgfkof" # nitter redirect
      "dhdgffkkebhmkfjojejmpbldmpobfkfo" # tampermonkey
      "pncfbmialoiaghdehhbnbhkkgmjanfhe" # uBlacklist
      ];
      programs.chromium.extraOpts = {
      "BrowserSignin" = 0;
      "SyncDisabled" = true;
      "PasswordManagerEnabled" = false;
      "SpellcheckEnabled" = true;
      "SpellcheckLanguage" = [
        "en-CA"
        "fr-CA"
        "en-GB"
        "en"
        "fr"
      ];
      "ScrollToTextFragmentEnabled" = false;
      "NTPContentSuggestionsEnabled" = false;
      "SearchSuggestEnabled" = false;
      "AutofillCreditCardEnabled" = false;
    };
  };
}
