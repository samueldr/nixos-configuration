{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.oled;
  inherit (lib)
    mkForce
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.oled = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Defines a system using an OLED display.
      '';
    };
  };
  config = mkMerge [
    (mkIf cfg.enable {
      console.colorPreset = lib.mkForce "whiteOnBlack";
    })
  ];
}
