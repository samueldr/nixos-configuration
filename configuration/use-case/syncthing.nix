{ config, lib, ... }:

let
  cfg = config.samueldr.use-case.syncthing;
  inherit (lib)
    mkIf
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.syncthing = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Enable personal syncthing usage.
      '';
    };
  };
  config = mkIf cfg.enable {
    services.syncthing = {
      enable = true;
      openDefaultPorts = true;
      group = "users";
      user = "samuel";
      configDir = "/Users/samuel/.local/share/syncthing";
    };
  };
}
