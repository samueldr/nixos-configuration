{ config, lib, options, pkgs, ... }:

let
  cfg = config.samueldr.use-case.graphical-session;
  inherit (lib)
    mkDefault
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options.samueldr.use-case.graphical-session = {
    enable = mkOption {
      default = false;
      type = types.bool;
      description = ''
        Use a graphical session
      '';
    };
    availableSessions = mkOption {
      type = with types; listOf str;
      internal = true;
    };
    session = mkOption {
      type = types.enum cfg.availableSessions;
      default = "none";
      description = ''
        Graphical session to use.
      '';
    };
  };
  config = mkMerge [
    { samueldr.use-case.graphical-session.availableSessions = [ "none" ]; }
    (mkIf cfg.enable {
      services.xserver = {
        enable = mkDefault true;
        displayManager.lightdm = {
          enable = mkDefault true;
        };
      };
    })
  ];
}

