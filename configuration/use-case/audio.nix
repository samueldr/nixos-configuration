{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.use-case.audio;
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.use-case.audio = {
    enable = mkEnableOption "configs for audio";
  };

  config = mkIf cfg.enable {
    # Audio configuration
    hardware.pulseaudio.enable = true;
    # FIXME: migrate to pipewire...
    services.pipewire.enable = false;

    # Full for bluetooth audio support.
    hardware.pulseaudio.package = pkgs.pulseaudioFull;

    environment.variables = {
      SDL_AUDIODRIVER = "pulse";
    };

    environment.systemPackages = with pkgs; [
      # TUI for Pulse Audio
      ncpamixer
    ];

    # Ensure the xdg autostart file is ran.                                                    
    systemd.user.services =                                                                    
      (config.lib.samueldr.convergent-session.userServiceFromAutostart { name = "pulseaudio"; type = "oneshot"; })
    ;                                                                                          

    # Assumes audio == GUI... fair enough for now.
    app-compartmentalization = {
      scoped-apps = {
        # Sound
        paprefs.enable = mkDefault true;
        pavucontrol.enable = mkDefault true;
      };
    };
  };
}
