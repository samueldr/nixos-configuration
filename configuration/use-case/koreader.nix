{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
    mkMerge
  ;
  koreader-wip = 
    let
      rev = "22fed6861ae3d0941e8854f56a0f69844654d1fc";
      archive = (builtins.fetchTarball {
        url = "https://github.com/samueldr-wip/koreader-wip/archive/${rev}.tar.gz";
        sha256 = "sha256:1gif4xd1zx3qshy24a8wr468y3nlnzq4lidm85855x15ammg58k5";
      });
    in
    import archive {}
  ;
  koreader = koreader-wip.configured-koreader;
in
{
  options.samueldr.use-case.koreader = {
    enable = mkEnableOption "use of heavily customized koreader";
    autostart = mkEnableOption "launching koreader automatically";
  };

  config = mkIf config.samueldr.use-case.koreader.enable (mkMerge [
    {
      environment.systemPackages = [
        koreader
      ];
      nixpkgs.overlays = [(final: super: {
        inherit koreader;
      })];

      # System-wide optional dependencies for koreader
      programs.light.enable = true;
      networking.networkmanager.enable = true;
    }
    (mkIf config.samueldr.use-case.koreader.autostart {
      environment.systemPackages = [
          (pkgs.writeTextDir "etc/xdg/autostart/koreader.desktop" ''
            [Desktop Entry]
            Type=Application
            Name=Auto-launch koreader
            Exec=${koreader}/bin/koreader
            X-XFCE-Autostart-Override=true
          '')
      ];
      systemd.user.services =
        config.lib.samueldr.convergent-session.userServiceFromAutostart
        { name = "koreader"; type = "oneshot"; }
      ;
    })
  ]);
}
