{ lib, options, ... }:

let
  inherit (lib)
    mkDefault
    mkOption
    types
  ;
in
{
  options.samueldr.hardware.storage = {
    size = mkOption {
      default = 256 * 1024;
      type = types.int;
      description = ''
        Size in MiB for the "main" storage area, e.g. the disk with the rootfs.

        This is informal, the default should be fine in most use cases.

        Some tweaks will apply depending on the storage size reported.

        This is recommended to be configured on hardware with constrained storage!
      '';
    };
  };

  config = {
    # Assumes trimable storage by default.
    # Should be safe in these modern times.
    services.fstrim.enable = mkDefault true;
    fileSystems."/" = {
      options = [ "noatime" "nodiratime" "discard" ];
    };
  };
}
