{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.hardware.misc.rtl8821;
  inherit (lib)
    mkEnableOption
    mkIf
    mkMerge
  ;
in
{
  options.samueldr.hardware.misc.rtl8821 = {
    au.enable = mkEnableOption "usage of RTL8821AU hardware";
    cu.enable = mkEnableOption "usage of RTL8821CU hardware";
  };
  config = mkMerge [
    (mkIf cfg.au.enable {
      boot.extraModulePackages = [
        # TP-Link AC600 Archer T2U Nano
        config.boot.kernelPackages.rtl8821au
      ];              
    })
    (mkIf cfg.cu.enable {
      boot.extraModulePackages = [
        # Cheapo Wi-Fi adapter         
        config.boot.kernelPackages.rtl8821cu
      ];              
      # for usb_modeswitch
      hardware.usbWwan.enable = true;
    })
  ];
}
