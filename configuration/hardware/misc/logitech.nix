{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.hardware.misc.logitech;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.misc.logitech = {
    enable = mkEnableOption "configured usage of Logitech hardware";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = [
      pkgs.solaar
    ];
    services.udev.packages = [
      pkgs.logitech-udev-rules
      pkgs.solaar
    ];

    # Pretty much means that there's logitech hardware.
    # This ensures they always can be used during initrd.
    boot.initrd.kernelModules = [
      "hid_logitech_dj"
      "hid_logitech_hidpp"
    ];
  };
}
