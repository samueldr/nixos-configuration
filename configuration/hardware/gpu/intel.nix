{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.hardware.definitions.gpu.Intel;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.gpu.Intel = {
    enable = mkEnableOption "Intel gpu";
  };
  config = mkIf cfg.enable {
    boot.initrd.availableKernelModules = [ "i915" ];

    boot.kernelParams = mkIf (config.samueldr.use-case.graphical-boot.flickerFree.enable) [
      "i915.fastboot=1"
    ];

    boot.kernel.sysctl = {
      # Exposes performance counters
      "dev.i915.perf_stream_paranoid" = "0";
    };
  };
}
