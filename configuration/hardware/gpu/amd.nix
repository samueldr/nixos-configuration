{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.hardware.definitions.gpu.AMD;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.gpu.AMD = {
    enable = mkEnableOption "AMD gpu";
  };
  config = mkIf cfg.enable {
    boot.initrd.kernelModules = [ "amdgpu" ];
  };
}
