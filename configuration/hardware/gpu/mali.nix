{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.hardware.definitions.gpu.Mali;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.gpu.Mali = {
    enable = mkEnableOption "Mali gpu";
  };
  config = mkIf cfg.enable {
    boot.initrd.kernelModules = [ "panfrost" ];
  };
}
