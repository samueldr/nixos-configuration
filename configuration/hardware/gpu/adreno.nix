{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.hardware.definitions.gpu.Adreno;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.gpu.Adreno = {
    enable = mkEnableOption "Adreno gpu";
  };
  config = mkIf cfg.enable {
  };
}
