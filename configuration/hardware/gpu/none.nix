{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.hardware.definitions.gpu.None;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.gpu.None = {
    enable = mkEnableOption "No GPU";
  };
  config = mkIf cfg.enable {
  };
}
