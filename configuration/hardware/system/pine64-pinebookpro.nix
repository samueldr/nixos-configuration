{ config, lib, ... }:

let
  name = "Pine64 Pinebook Pro";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkBefore
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Rockchip";
          generation = "RK3399";
          # This is a sufficient approximation for our usage of cores vs. threads even if it is a lie.
          cores = 2; # two big cores
          threads = 4; # four LITTLE cores
        };
        gpu = {
          type = "Mali";
          # ff9a0000.gpu: mali-t860
          generation = "T860";
        };
        memory = 4 * 1024;
      };
      use-case.system-type.laptop.enable = mkDefault true;
    };

    hardware.bluetooth.enable = true;

    boot.initrd.availableKernelModules = [
      "usb_storage"
      "usbhid"
    ];

    boot.initrd.kernelModules = [
      # Rockchip modules
      "rockchip_rga"
      "rockchip_saradc"
      "rockchip_thermal"
      "rockchipdrm"

      # GPU/Display modules
      "analogix_dp"
      "cec"
      "drm"
      "drm_kms_helper"
      "dw_hdmi"
      "dw_mipi_dsi"
      "gpu_sched"
      "panel_edp"
      "panel_simple"
      "panfrost"
      "pwm_bl"

      # USB / Type-C related modules
      "fusb302"
      "tcpm"
      "typec"

      # Misc. modules
      "cw2015_battery"
      "gpio_charger"
      "rtc_rk808"
    ];

    services.udev.extraHwdb = lib.mkMerge [
      ''
        evdev:input:b0003v258Ap001E*
          KEYBOARD_KEY_700a5=brightnessdown
          KEYBOARD_KEY_700a6=brightnessup
          KEYBOARD_KEY_70066=sleep
      ''
      # https://github.com/elementary/os/blob/05a5a931806d4ed8bc90396e9e91b5ac6155d4d4/build-pinebookpro.sh#L253-L257
      # Disable the "keyboard mouse" in libinput. This is reported by the keyboard firmware
      # and is probably a placeholder for a TrackPoint style mouse that doesn't exist
      ''
        evdev:input:b0003v258Ap001Ee0110-e0,1,2,4,k110,111,112,r0,1,am4,lsfw
          ID_INPUT=0
          ID_INPUT_MOUSE=0
      ''
    ];

    # https://github.com/elementary/os/blob/05a5a931806d4ed8bc90396e9e91b5ac6155d4d4/build-pinebookpro.sh#L253-L257
    # Mark the keyboard as internal, so that "disable when typing" works for the touchpad
    environment.etc."libinput/local-overrides.quirks".text = ''
      [Pinebook Pro Keyboard]
      MatchUdevType=keyboard
      MatchBus=usb
      MatchVendor=0x258A
      MatchProduct=0x001E
      AttrKeyboardIntegration=internal
    '';

    # The default powersave makes the wireless connection unusable.
    networking.networkmanager.wifi.powersave = lib.mkDefault false;

    boot.kernelParams = mkBefore [
      # [/proc/device-tree]/chosen/stdout-path ends up being preferred.
      "console=tty0"
    ];
  };
}
