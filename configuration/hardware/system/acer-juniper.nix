{ config, lib, ... }:

let
  name = "Acer Chromebook 311";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "MediaTek";
          generation = "Kompanio 500";
          cores = 8;
          maxParallelTasks = 4;
        };
        gpu = {
          type = "Mali";
          generation = "G72";
        };
        memory = 4 * 1024;
      };
      use-case.system-type.tablet.enable = mkDefault false;
      use-case.system-type.laptop.enable = mkDefault true;
      use-case.system-type.chromebook.enable = mkDefault true;
    };
    hardware.bluetooth.enable = true;
  };
}
