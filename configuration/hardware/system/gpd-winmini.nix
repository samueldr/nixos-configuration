{ config, lib, ... }:

let
  name = "GPD Win Mini";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "AMD";
          generation = "Zen 4";
          cores = 8;
          threads = 16;
        };
        gpu = {
          type = "AMD";
          generation = "RDNA 3";
        };
        memory = 32 * 1024;
      };
      use-case.system-type.mobile.enable = mkDefault true;
      use-case.graphical-boot.hackBGRT.rotate = "clockwise";
      use-case.graphical-boot.hackBGRT.svg.exportHeight = 1080;
    };
    hardware.bluetooth.enable = true;
    boot.initrd.availableKernelModules = [
      "nvme"
      "xhci_pci"
      "usbhid"
      "sdhci_pci"
    ];

    # Not a tablet, exactly, but this is the closest chassis for it.
    environment.etc."machine-info".text = lib.mkDefault (lib.mkAfter ''
      CHASSIS="tablet"
    '');
  };
}
