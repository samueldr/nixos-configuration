{ config, lib, ... }:

let
  name = "HP z420";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Intel";
          generation = "Haswell";
          cores = 6;
          threads = 12;
        };
        gpu = {
          # Actually a lie, but headlessly used
          type = "None";
        };
        memory = 64 * 1024; # As configured
        maxDisplayCount = 1;
      };
      use-case.system-type.headless.enable = lib.mkDefault true;
    };
    boot.initrd.availableKernelModules = [ "ata_generic" "ehci_pci" "ahci" "isci" "xhci_pci" "firewire_ohci" "usbhid" "sd_mod" "sr_mod" ];
    boot.kernelModules = [ "kvm-intel" ];
  };
}
