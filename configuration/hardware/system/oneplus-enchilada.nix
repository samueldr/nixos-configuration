{ config, lib, ... }:

let
  name = "OnePlus OnePlus 6";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Qualcomm";
          generation = "SDM845";
          cores = 8;
          maxParallelTasks = 4;
        };
        gpu = {
          type = "Adreno";
          generation = "630";
        };
        memory = 8 * 1024;
      };
      use-case.system-type.phone.enable = mkDefault true;
    };
    hardware.bluetooth.enable = true;
  };
}
