{ config, lib, ... }:

let
  name = "Valve Steam Deck";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "AMD";
          generation = "Zen 2";
          cores = 4;
          threads = 8;
        };
        gpu = {
          type = "AMD";
          generation = "RDNA 2";
        };
        memory = 16 * 1024;
      };
      use-case.system-type.mobile.enable = mkDefault true;
      use-case.graphical-boot.hackBGRT.rotate = "clockwise";
      use-case.graphical-boot.hackBGRT.svg.exportHeight = 800;
    };
    hardware.bluetooth.enable = true;
    boot.initrd.availableKernelModules = [
      "nvme"
      "xhci_pci"
      "usbhid"
      "sdhci_pci"
    ];

    # Not a tablet, exactly, but this is the closest chassis for it.
    environment.etc."machine-info".text = lib.mkDefault (lib.mkAfter ''
      CHASSIS="tablet"
    '');

    # Firmware updates are configured for fwupd in Jovian NixOS.
    services.fwupd.enable = true;
  };
}
