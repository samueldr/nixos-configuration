{ config, lib, ... }:

let
  name = "StarBook (5800U)";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "AMD";
          # 113-CEZANNE-020
          generation = "Zen 3";
          cores = 8;
          threads = 16;
        };
        gpu = {
          type = "AMD";
          generation = "Cezanne"; # ?
        };
        memory = 32 * 1024; # As configured
        maxDisplayCount = 3;
      };
      use-case.system-type.laptop.enable = lib.mkDefault true;
    };
    hardware.bluetooth.enable = true;
    boot.initrd.availableKernelModules = [
      "nvme"
      "xhci_pci"
      "usb_storage"
      "sd_mod"
    ];

    # Firmware updates are provided on LVFS.
    services.fwupd.enable = true;
    services.fwupd.extraRemotes = [
      "lvfs-testing"
    ];
  };
}
