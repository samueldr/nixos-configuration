{ config, lib, ... }:

let
  name = "ASUS TP300LA";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Intel";
          generation = "Haswell";
          cores = 4;
        };
        gpu = {
          type = "Intel";
          # Intel® HD Graphics 4400
          generation = "Haswell";
        };
        memory = lib.mkDefault (8 * 1024);
        maxDisplayCount = 2;
      };
      use-case.system-type.laptop.enable = lib.mkDefault true;
    };
    hardware.bluetooth.enable = true;
    boot.initrd.availableKernelModules = [
      "xhci_pci"
      "ehci_pci"
      "ahci"
      "sd_mod"
    ];
  };
}
