{ config, lib, options, pkgs, ... }:

let
  name = "Acer Chromebook Spin 513";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
    mkMerge
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable (
    mkMerge [
      {
        assertions = [
          {
            assertion = options ? mobile;
            message = "Mobile NixOS is required for evaluating a ${name} configuration.";
          }
        ];
      }
      (if !(options ? mobile) then {} else {
        samueldr = {
          hardware = {
            cpu = {
              type = "Qualcomm";
              generation = "Snapdragon 7c";
              cores = 8;
              maxParallelTasks = 4;
            };
            gpu = {
              type = "Adreno";
              generation = "630";
            };
            memory = mkDefault (4 * 1024); # up to 8 GiB
          };
          use-case.system-type.chromebook.enable = mkDefault true;
          use-case.system-type.laptop.enable = mkDefault true;
          use-case.system-type.pen.enable = mkDefault true;
        };
        hardware.bluetooth.enable = true;

        # Use the unredistributable modem firmware for Wi-Fi.
        hardware.firmware = [
          pkgs.chromeos-sc7180-unredistributable-firmware
        ];
      })
    ]
  );
}

