{ config, lib, options, pkgs, ... }:

let
  name = "Lenovo Chromebook Duet 3";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkForce
    mkIf
    mkMerge
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable (
    mkMerge [
      {
        assertions = [
          {
            assertion = options ? mobile;
            message = "Mobile NixOS is required for evaluating a ${name} configuration.";
          }
        ];
      }
      (if !(options ? mobile) then {} else {
        samueldr = {
          hardware = {
            cpu = {
              type = "Qualcomm";
              generation = "Snapdragon 7c";
              cores = 8;
              maxParallelTasks = 4;
            };
            gpu = {
              type = "Adreno";
              generation = "630";
            };
            memory = mkDefault (4 * 1024); # up to 8 GiB
          };
          use-case.system-type.tablet.enable = mkDefault true;
          use-case.system-type.chromebook.enable = mkDefault true;
          use-case.system-type.pen.enable = mkDefault true;
        };
        hardware.bluetooth.enable = true;

        # Use the unredistributable modem firmware for Wi-Fi.
        hardware.firmware = [
          pkgs.chromeos-sc7180-unredistributable-firmware
        ];

        # Customize the kernel a bit
        mobile.boot.stage-1 = {
          kernel.package = mkForce (
            (pkgs.callPackage <mobile-nixos/devices/families/mainline-chromeos-sc7180/kernel> {})
            .overrideAttrs({ patches ? [], ... }: {
              patches = patches ++ [
                (builtins.toFile "0001-sc7180-trogdor-Change-backlight-curve.patch" ''
From 0d95ffdbc1f9dc3243adde275a7975d8d9a8a102 Mon Sep 17 00:00:00 2001
From: Samuel Dionne-Riel <samuel@dionne-riel.com>
Date: Mon, 23 Jan 2023 12:04:03 -0500
Subject: [PATCH] sc7180-trogdor: Change backlight curve

This should probably be done only for wormdingler with BOE display
(1024), but eh, this is good enough for personal use.
---
 arch/arm64/boot/dts/qcom/sc7180-trogdor.dtsi | 11 ++++++-----
 1 file changed, 6 insertions(+), 5 deletions(-)

diff --git a/arch/arm64/boot/dts/qcom/sc7180-trogdor.dtsi b/arch/arm64/boot/dts/qcom/sc7180-trogdor.dtsi
index eae22e6e97c1..953da4378c7b 100644
--- a/arch/arm64/boot/dts/qcom/sc7180-trogdor.dtsi
+++ b/arch/arm64/boot/dts/qcom/sc7180-trogdor.dtsi
@@ -310,14 +310,15 @@ pp3300_hub: pp3300-hub-regulator {
 	backlight: backlight {
 		compatible = "pwm-backlight";
 
-		/* The panels don't seem to like anything below ~ 5% */
+		/* Tweaked values preferring subtler adjustments at lower values. */
+		/* Tested on google,wormdingler-sku1024 */
 		brightness-levels = <
-			196 256 324 400 484 576 676 784 900 1024 1156 1296
-			1444 1600 1764 1936 2116 2304 2500 2704 2916 3136
-			3364 3600 3844 4096
+			1 64 128 192
+			256 512 1024
+			2048 4096
 		>;
 		num-interpolated-steps = <64>;
-		default-brightness-level = <951>;
+		default-brightness-level = <512>;
 
 		pwms = <&cros_ec_pwm 1>;
 		enable-gpios = <&tlmm 12 GPIO_ACTIVE_HIGH>;
-- 
2.39.0
                '')
              ];
            })
          );
        };
      })
    ]
  );
}
