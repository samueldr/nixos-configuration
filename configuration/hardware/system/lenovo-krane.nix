{ config, lib, ... }:

let
  name = "Lenovo Chromebook Duet";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "MediaTek";
          generation = "Kompanio 500";
          cores = 8;
          maxParallelTasks = 4;
        };
        gpu = {
          type = "Mali";
          generation = "G72";
        };
        memory = 4 * 1024;
      };
      use-case.system-type.tablet.enable = mkDefault true;
      use-case.system-type.chromebook.enable = mkDefault true;
      use-case.system-type.pen.enable = mkDefault true;
    };
    hardware.bluetooth.enable = true;
  };
}
