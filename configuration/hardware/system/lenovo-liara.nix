{ config, lib, ... }:

let
  name = "Lenovo 14e Chromebook";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "AMD";
          generation = "Stoney Ridge";
          cores = 2;
        };
        gpu = {
          type = "AMD";
          generation = "R5";
        };
        memory = lib.mkDefault (4 * 1024);
      };
      use-case.system-type.chromebook.enable = mkDefault true;
      use-case.system-type.laptop.enable = mkDefault true;
    };
    hardware.bluetooth.enable = true;
    boot.initrd.availableKernelModules = [
      "xhci_pci"
      "ehci_pci"
      "sdhci_pci"
    ];
  };
}
