{ config, lib, ... }:

let
  name = "Asus Chromebook Tablet CT100PA";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Rockchip";
          # OP1 is RK3399
          generation = "RK3399";
        };
        gpu = {
          type = "Mali";
          # ff9a0000.gpu: mali-t860
          generation = "T860";
        };
        memory = 4 * 1024;
      };
      use-case.system-type.tablet.enable = mkDefault true;
    };
    hardware.bluetooth.enable = true;
  };
}
