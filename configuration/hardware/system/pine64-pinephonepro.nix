{ config, lib, ... }:

let
  name = "Pine64 Pinephone Pro";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkBefore
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Rockchip";
          generation = "RK3399";
          # This is a sufficient approximation for our usage of cores vs. threads even if it is a lie.
          cores = 2; # two big cores
          threads = 4; # four LITTLE cores
        };
        gpu = {
          type = "Mali";
          # ff9a0000.gpu: mali-t860
          generation = "T860";
        };
        memory = 4 * 1024;
      };
      use-case.system-type.phone.enable = mkDefault true;
    };

    hardware.bluetooth.enable = true;

    # The default powersave makes the wireless connection unusable.
    networking.networkmanager.wifi.powersave = lib.mkDefault false;
  };
}
