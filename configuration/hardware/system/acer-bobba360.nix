{ config, lib, ... }:

let
  name = "Acer Chromebook Spin 511";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Intel";
          # Intel(R) Celeron(R) N4020 CPU @ 1.10GHz
          generation = "Gemini Lake";
          cores = 2;
        };
        gpu = {
          type = "Intel";
          generation = "Gemini Lake";
        };
        memory = lib.mkDefault (4 * 1024);
      };
      use-case.system-type.chromebook.enable = mkDefault true;
      use-case.system-type.laptop.enable = mkDefault true;
    };
    hardware.bluetooth.enable = true;
    boot.initrd.availableKernelModules = [
      "xhci_pci"
      "ehci_pci"
      "sdhci_pci"
    ];
  };
}
