{ config, lib, ... }:

let
  name = "Acer C720P";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkDefault
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Intel";
          generation = "Haswell";
          cores = 2;
        };
        gpu = {
          type = "Intel";
          # Intel® HD Graphics (Haswell)
          generation = "Haswell";
        };
        memory = lib.mkDefault (2 * 1024);
      };
      use-case.system-type.chromebook.enable = mkDefault true;
      use-case.system-type.laptop.enable = mkDefault true;
    };
    hardware.bluetooth.enable = true;
    boot.initrd.availableKernelModules = [
      "xhci_pci"
      "ehci_pci"
      "ahci"
      "sd_mod"
    ];
  };
}
