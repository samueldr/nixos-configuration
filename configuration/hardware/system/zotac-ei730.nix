{ config, lib, ... }:

let
  name = "ZOTAC ZBOX EI730";
  cfg = config.samueldr.hardware.definitions.system.${name};
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.system.${name} = {
    enable = mkEnableOption "definition for '${name}'";
  };
  config = mkIf cfg.enable {
    samueldr = {
      hardware = {
        cpu = {
          type = "Intel";
          generation = "Haswell";
          cores = 4;
        };
        gpu = {
          type = "Intel";
          # Intel® HD Graphics 4400
          generation = "Haswell";
        };
        memory = lib.mkDefault (16 * 1024);
      };
      use-case.system-type.desktop.enable = lib.mkDefault true;
    };
    hardware.bluetooth.enable = true;
    boot.kernelModules = [ "kvm-intel" ];
    boot.initrd.availableKernelModules = [
      "xhci_pci"
      "ehci_pci"
      "ahci"
      "sd_mod"
      "rtsx_usb_sdmmc"
    ];
    powerManagement.cpuFreqGovernor = "performance";
  };
}
