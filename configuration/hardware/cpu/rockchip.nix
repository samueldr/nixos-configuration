{ config, lib, ... }:

let
  cfg = config.samueldr.hardware.definitions.cpu.Rockchip;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.cpu.Rockchip = {
    enable = mkEnableOption "Rockchip cpu";
  };
  config = mkIf cfg.enable {
    nixpkgs.hostPlatform = "aarch64-linux";
  };
}
