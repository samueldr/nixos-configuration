{ config, lib, ... }:

let
  cfg = config.samueldr.hardware.definitions.cpu.Qualcomm;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.cpu.Qualcomm = {
    enable = mkEnableOption "Qualcomm cpu";
  };
  config = mkIf cfg.enable {
    nixpkgs.hostPlatform = "aarch64-linux";
  };
}
