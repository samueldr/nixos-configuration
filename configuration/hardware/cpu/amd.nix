{ config, lib, ... }:

let
  cfg = config.samueldr.hardware.definitions.cpu.AMD;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.cpu.AMD = {
    enable = mkEnableOption "AMD cpu";
  };
  config = mkIf cfg.enable {
    boot.kernelModules = [ "kvm-amd" ];
    hardware.cpu.amd.updateMicrocode = true;
    nixpkgs.hostPlatform = "x86_64-linux";
    powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  };
}
