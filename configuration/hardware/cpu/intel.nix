{ config, lib, ... }:

let
  cfg = config.samueldr.hardware.definitions.cpu.Intel;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.cpu.Intel = {
    enable = mkEnableOption "Intel cpu";
  };
  config = mkIf cfg.enable {
    boot.kernelModules = [ "kvm-intel" ];
    hardware.cpu.intel.updateMicrocode = true;
    nixpkgs.hostPlatform = "x86_64-linux";
    powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  };
}
