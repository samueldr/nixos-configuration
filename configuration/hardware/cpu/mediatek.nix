{ config, lib, ... }:

let
  cfg = config.samueldr.hardware.definitions.cpu.MediaTek;
  inherit (lib)
    mkEnableOption
    mkIf
  ;
in
{
  options.samueldr.hardware.definitions.cpu.MediaTek = {
    enable = mkEnableOption "MediaTek cpu";
  };
  config = mkIf cfg.enable {
    nixpkgs.hostPlatform = "aarch64-linux";

    services.xserver.videoDrivers = [
      "modesetting"
    ];

    # Ensures X11 works on MT8183
    services.xserver.extraConfig = ''
      Section "ServerFlags"
        Option          "AutoAddGPU" "off"
        Option          "Debug" "dmabuf_capable"
      EndSection

      Section "OutputClass"
        Identifier      "panfrost"
        MatchDriver     "mediatek"
        Driver          "modesetting"
        Option          "PrimaryGPU" "true"
      EndSection

      Section "OutputClass"
        Identifier      "kmsdev"
        MatchDriver     "modesetting"
        Option          "kmsdev" "/dev/dri/card1"
      EndSection

      Section "OutputClass"
        Identifier      "accel"
        MatchDriver     "modesetting"
        Option          "AccelMethod" "glamor"
      EndSection
    '';
  };
}
