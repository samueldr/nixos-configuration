{ config, lib, modulesPath, ... }:

let
  cfg = config.samueldr.hardware;
  inherit (lib)
    mkOption
    types
  ;
in
{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ]
    ++
    builtins.map
    (name: ./. + "/${name}")
    (builtins.filter (name: name != "default.nix") (builtins.attrNames (builtins.readDir ./.)))
  ;

  options.samueldr = {
    hardware = {
      cpu = {
        generation = mkOption {
          type = types.str;
        };
        type = mkOption {
          type = types.str;
        };
        cores = mkOption {
          type = types.int;
          description = ''
            Number of cores for the CPU.

            For BIG.little, sum them up.

            Should be equal to:

            ```
             $ lscpu | grep 'Core.s. per socket' | sed -e 's/^.*:\s*//'
            ```
          '';
        };
        threads = mkOption {
          type = types.int;
          default = config.samueldr.hardware.cpu.cores;
          description = ''
            Number of "threads" for the CPU.

            Should be equal to:

            ```
             $ lscpu | grep '^CPU(s):' | sed -e 's/^.*:\s*//'
            ```
          '';
        };
        maxParallelTasks = mkOption {
          type = types.int;
          default = config.samueldr.hardware.cpu.threads;
          description = ''
            Maximum amount of tasks to run.

            This is useful to change the default for heterogeneous systems like
            ARM systems with big.LITTLE clusters.
          '';
        };
      };
      gpu = {
        generation = mkOption {
          type = types.str;
        };
        type = mkOption {
          type = types.str;
        };
      };
      system = mkOption {
        type = types.str;
        description = ''
          The integrated "system" name.

          E.g. the model number.
        '';
      };
      memory = mkOption {
        type = types.int;
        description = ''
          Memory size, in MiB.
        '';
      };
      maxDisplayCount = mkOption {
        type = types.int;
        default = 1;
        description = ''
          Maximum amount of displays that can be used.

          Count the internal display as one.

          If the system has e.g. 4 outputs, but can only drive 3, set to 3.
        '';
      };
    };
  };

  config = {
    samueldr.hardware.definitions.cpu.${cfg.cpu.type}.enable = true;
    samueldr.hardware.definitions.gpu.${cfg.gpu.type}.enable = true;
    samueldr.hardware.definitions.system.${cfg.system}.enable = true;
  };
}
