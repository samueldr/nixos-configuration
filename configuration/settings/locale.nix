{ config, lib, ... }:

let
  cfg = config.samueldr.settings.locale;
  inherit (lib)
    mkDefault
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.locale = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable defaults for my locale preferences.
      '';
    };
  };
  config = mkIf cfg.enable {
    services.xserver.xkb.options = config.environment.variables.XKB_DEFAULT_OPTIONS;
    services.xserver.xkb.layout = config.environment.variables.XKB_DEFAULT_LAYOUT;
    services.xserver.xkb.model = config.environment.variables.XKB_DEFAULT_MODEL;
    environment.variables = {
      XKB_DEFAULT_LAYOUT = "ca";
      XKB_DEFAULT_OPTIONS = "compose:menu, compose:rctrl-altgr, compose:rwin";
      XKB_DEFAULT_MODEL = mkDefault "pc104";
    };
    i18n.defaultLocale = "en_CA.UTF-8";
    i18n.extraLocaleSettings = {
      LC_TIME = "en_GB.UTF-8";
    };
    console.keyMap = "cf";
    time.timeZone = "America/Montreal";
  };
}
