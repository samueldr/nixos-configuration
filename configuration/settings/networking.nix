{ config, lib, pkgs, ... }:

let
  inherit (lib)
    concatStringsSep
    fixedWidthNumber
    mod
    toInt
  ;
  # TODO: make the network map a module so I can refer to it in other use-cases.
  networkMap = {
    # Servers / Static systems (30~49)
    DUFFMAN      = "30";
    clancywiggum = "32";
    krusty       = "34";

    # Game devices (60~69)
    dashdingo    = "60";
    warriormarge = "61";
    mrdirt       = "62";

    # Laptops (70~89)
    santaslittlehelper = "70";
    ralphwiggum        = "71";
    luann              = "72";
    frankgrimes        = "73";
    lumpy              = "74";
    pinchy             = "75";

    # Tablets / Phones (90~99)
    snowball-two  = "90";
    snowball-four = "91";
    snowball-five = "92";
    scratchy      = "95";
  };
  digits = networkMap."${config.networking.hostName}";
  digits' = toInt digits;
  series = 1;
  macaddr = concatStringsSep ":" [
    "02"      # Private allocation
    "02" "02" # Free real estate
    "${fixedWidthNumber 2 series}" # Series
    "0${toString (digits' / 100)}" # BCD hundreds digit
    "${fixedWidthNumber 2 (mod digits' 100)}" # BCD digits
  ];
in
let
  cfg = config.samueldr.settings.networking;
  inherit (lib)
    makeBinPath
    mkDefault
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.networking = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Using my no-nonsense networking defaults.
      '';
    };
  };
  config = mkIf cfg.enable {
    # Networking is hard.
    networking.networkmanager.enable = true;

    # Some network interfaces shouldn't be managed by NetworkManager.
    networking.networkmanager.unmanaged = [
      "interface-name:ve-*"
      "interface-name:vb-*"
      "interface-name:vbox*"
      "br0"
      "lo"
    ];

    networking.networkmanager.dispatcherScripts = [
      # Sometimes avahi gets screwey with resolving names...
      # ... reset its state, and nscd's, such that resolutions are fresh.
      {
        type = "basic";
        source = pkgs.writeShellScript "retrigger-nscd-avahi" ''
          PATH="${makeBinPath (with pkgs; [
            sudo
          ])}:$PATH"
          retrigger_avahi() {
            sudo /run/current-system/sw/bin/systemctl restart nscd avahi-daemon
          }

          case $2 in
            up)
              retrigger_avahi
            ;;
          esac
        '';
      }
    ];
    security.sudo.extraRules = [
      {
        users = [ "ALL" ];
        commands = [
          { command = "/run/current-system/sw/bin/systemctl restart nscd avahi-daemon"; options = [ "NOPASSWD" ]; }
        ];
      }
    ];

    # Make stage-2 startup go brrrrr.
    systemd.targets.network-online.wantedBy = lib.mkForce [];
    systemd.services.NetworkManager-wait-online.wantedBy = lib.mkForce [];

    # DNS is hard.
    services.avahi.enable = true;
    services.avahi.nssmdns4 = true;
    services.avahi.publish.enable = true;
    services.avahi.publish.addresses = true;
    services.avahi.publish.workstation = mkDefault true;

    # Defining default bonds for roaming devices.
    samueldr.nm-localbonding.bonds = {
      "homebond" = {
        series = 1;
        # NOTE: needs to be enabled per-device!
        enable = mkDefault false;
        macAddress = macaddr;
        wired = [
          { name = "dock-big-workdesk"; address = "00:E0:4C:68:0C:A8"; }
          { name = "dock-pine64";       address = "00:E0:4C:36:00:02"; }
          { name = "dock-cheapo-gray";  address = "00:E0:4C:68:01:D0"; }
          { name = "eth-usb-hub-1";     address = "00:E0:4C:68:03:4A"; }
          { name = "eth-hub-ky-888-1";  address = "00:E0:4C:68:04:23"; }
          { name = "eth-hub-ky-888-2";  address = "00:E0:4C:70:0C:53"; }
          { name = "eth-white-cheap";   address = "00:E0:4C:53:44:58"; }

          # AX88179 adapters.
          { name = "dock-stardock";     address = "F8:E4:3B:37:7D:F3"; } # with cdc_ncm
          { name = "dock-stardock-ax";  address = "E2:27:D1:35:A7:F5"; } # with ax driver
          { name = "dock-steamdeck";    address = "10:82:86:1C:D9:CC"; } # with cdc_ncm
          { name = "dock-steamdeck-ax"; address = "86:50:C0:41:13:42"; } # with ax driver
        ];
        networkConfig = {
          ipv4 = {
            address1 = "192.168.1.${digits}/24,192.168.1.1";
            dns = "192.168.1.1;";
            method = "manual";
          };
          ipv6 = {
            addr-gen-mode = "stable-privacy";
            method = "auto";
          };
        };
      };
    };
  };
}
