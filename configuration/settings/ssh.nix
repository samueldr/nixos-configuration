{ config, lib, ... }:

let
  cfg = config.samueldr.settings.ssh;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.ssh = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable usage of ssh server things.
      '';
    };
  };
  config = mkIf cfg.enable {
    # Enable the OpenSSH daemon.
    services.openssh.enable = true;

    # Allow X11 forwarding
    services.openssh.settings.X11Forwarding = true;
  };
}
