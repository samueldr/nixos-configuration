{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.settings.channels;
  channels = import ../../npins;
  inherit (lib)
    mkBefore
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.channels = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable my defaults for "channels" configuration...

        This ensures `<nixpkgs>` refs (e.g. nix-shell`) work.
      '';
    };
  };
  config = mkIf cfg.enable {
    environment.etc = {
      "system-channels" = {
        source = pkgs.callPackage (
          { runCommand }:
          runCommand "system-channels" {} ''
            (
              PS4=" $ "
              set -x
              mkdir -vp $out
              ln -v -s ${channels.nixpkgs.outPath} $out/nixpkgs
            )
          ''
        ) { };
      };
    };
    nix = {
      nixPath = mkBefore [
        "/etc/system-channels"
      ];
    };
  };
}
