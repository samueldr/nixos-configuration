{ config, lib, ... }:

let
  cfg = config.samueldr.settings.users;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.users = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable defaults for users.
      '';
    };
  };
  config = mkIf cfg.enable {
    system.activationScripts.users-folder = {
      supportsDryActivation = true;
      text = ''
        install -m 0755 -d /Users
      '';
    };

    users.mutableUsers = false;
    users.extraUsers.samuel = {
      hashedPassword = lib.mkDefault (
        builtins.trace
        "Did you forget to include your secrets?"
        # 1234
        "$6$EfV4PHNJaftl1w19$WQI076X8LEKxzL4e4G4/FLlmZ0MpdMI8QiOefLcug66KEfDUkAuU6RdXipktkcOiLdX..IsgB/.ldKh1sECHI1"
      );
      home = "/Users/samuel";
      linger = true;
      description = "Samuel Dionne-Riel";
      isNormalUser = true;
      uid = 1000;
      extraGroups = [
        "adbusers"
        "adm"
        "audio"
        "docker"
        "dialout" # serial
        "wheel"
        "networkmanager"
        "systemd-journal"
        "kvm"
        "vboxusers"
        "libvirtd"
        "video"
        "feedbackd"
        "wireshark"
      ];
    };
  };
}
