{ config, lib, ... }:

let
  cfg = config.samueldr.settings.location;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.location = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable defaults for location.
      '';
    };
  };
  config = mkIf cfg.enable {
    location = {
      provider = "manual";
      latitude  = 46.8114288;
      longitude = -71.2537676;
    };
  };
}
