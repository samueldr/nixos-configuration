{ config, lib, ... }:

let
  cfg = config.samueldr.settings.redshift;
  inherit (lib)
    mkDefault
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.redshift = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable defaults for redshift.
      '';
    };
  };
  config = mkIf cfg.enable {
    services.redshift = {
      temperature = {
        day = mkDefault 6500;
        night = mkDefault 5500;
      };
      brightness = {
        day = mkDefault "1.0";
        night = mkDefault "0.9";
      };
    };
  };
}
