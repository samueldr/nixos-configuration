#
# Common configuration for any system.
#
# NOTE: This has to reasonably apply to *any* system.
#       Any SBC, laptop, desktop, phone, tablet.
#
{ config, lib, ... }:

let
  cfg = config.samueldr.settings.common-settings;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.common-settings = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable default common settings for my config.
      '';
    };
  };

  config = mkIf cfg.enable {
    samueldr.profile.enable = true;

    programs.bash.completion.enable = true;
    programs.ssh.startAgent = true;
    security.sudo.enable = true;
    # meh...
    # TODO: make a wrapper tool that calls `sudo dmesg` and use password-less sudo for dmesg for wheel users?
    boot.kernel.sysctl."kernel.dmesg_restrict" = false;

    # Attempts to keep the journal size under control
    # This will limit its size to the smallest between 4GiB or 1/64th of the total storage
    # This e.g. limits the journal size to 256MiB on a 16GiB rootfs
    # NOTE: 4GiB is the upstream default for SystemMaxUse.
    services.journald.extraConfig = ''
      SystemMaxUse=${toString (lib.min (config.samueldr.hardware.storage.size / 64) (4 * 1024))}M
    '';
  };
}
