{ config, lib, ... }:

let
  cfg = config.samueldr.settings.zram;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.zram = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable usage of zram.
      '';
    };
  };
  config = mkIf cfg.enable {
    zramSwap.enable = true;
  };
}
