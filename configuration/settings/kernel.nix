{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.settings.kernel;
  inherit (lib)
    mkOption
    mkOverride
    mkIf
    types
  ;
in
{
  options.samueldr.settings.kernel = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable default kernel settings.
      '';
    };
  };
  config = mkIf cfg.enable {
    boot.kernelPackages = mkOverride 1001 pkgs.linuxPackages_latest;
  };
}
