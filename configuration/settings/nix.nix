{ config, lib, pkgs, ... }:

let
  cfg = config.samueldr.settings.nix;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.nix = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable my defaults for Nix.
      '';
    };
  };
  config = mkIf cfg.enable {
    environment.systemPackages = [
      pkgs.nix-output-monitor
    ];
    nix = {
      package = pkgs.lix;
      daemonIOSchedPriority = 4;
      daemonCPUSchedPolicy = "idle";
      daemonIOSchedClass = "idle";

      settings = {
        auto-optimise-store = true;
        cores = 0;
        sandbox = true;
        substituters = [
          "https://cache.nixos.org"
        ];
        trusted-users = [ "root" "@wheel" ];
        max-jobs = config.samueldr.hardware.cpu.maxParallelTasks;
      };

      nixPath = [
        "nixos-config=/etc/nixos/configuration.nix"
      ];
    };
  };
}
