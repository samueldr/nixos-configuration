{ config, lib, ... }:

let
  cfg = config.samueldr.settings.nixos;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.nixos = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Enable defaults for my usage of NixOS.
      '';
    };
  };
  config = mkIf cfg.enable {
    nixpkgs.config.allowUnfree = true;

    environment.pathsToLink = [
      "/share"
    ];

    # Write the kernel version in the generation name.
    # I do enough fiddling around that this is useful.
    system.nixos.tags = [
      "kernel_${config.boot.kernelPackages.kernel.version}"
    ];
  };
}
