{ config, lib, ... }:

let
  cfg = config.samueldr.settings.filesystem-shenanigans;
  inherit (lib)
    mkOption
    mkIf
    types
  ;
in
{
  options.samueldr.settings.filesystem-shenanigans = {
    enable = mkOption {
      default = true;
      type = types.bool;
      description = ''
        Do shenanigans with the filesystem layout.
      '';
    };
  };

  config = mkIf cfg.enable {
    # Hide things.
    gobohide.enable = true;
    # In addition to the defaut FHS paths being hidden, let's hide
    # the default home and media paths.
    gobohide.hiddenPaths = {
      "/home" = true;
      "/media"= true;
    };

    #
    # Mounts in /Volumes/
    #
    # The next two rules, in conjunction with gobohide, will make mounts appear
    # to mount under /Volumes/.
    #
    # The actual mounts will be in /media/, which is hidden and bind mounted.
    #
    # The `mount` command will, though, list two mounts per mount.
    #
    services.udev.extraRules = ''
      # UDISKS_FILESYSTEM_SHARED
      # ==1: mount filesystem to a shared directory (/media/VolumeName)
      # ==0: mount filesystem to a private directory (/run/media/$USER/VolumeName)
      # See udisks(8)
      ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
    '';
    # NOTE: this will not work in the VM, as the vmVariant fileSystems is
    #       overriden too harshly for this to work.
    fileSystems."/Volumes" = {
      neededForBoot = false;
      device = "/media";
      options = [ "bind" ];
    };

    system.activationScripts.media-folder = {
      supportsDryActivation = true;
      text = ''
        install -m 0755 -d /media
      '';
    };
  };
}
