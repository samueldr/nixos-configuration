{ config, lib, ... }:

let
  cfg = config.samueldr.boot;
  inherit (lib)
    mkBefore
    mkDefault
    mkOption
    mkIf
    mkMerge
    types
  ;
in
{
  options.samueldr = {
    boot = {
      type = mkOption {
        type = types.enum [
          "efi"
          "legacy"
          "mobile-nixos"
        ];
        description = ''
          Boot loading style for this platform.

          NOTE: `mobile-nixos` is reserved for platforms where there is no OS-managed bootloader.
          Do not use `mobile-nixos` where only its stage-1 is used.
        '';
      };
      device = mkOption {
        description = "The device on which the legacy boot bootloader will be installed.";
        type = types.str;
        example = "/dev/disk/by-id/ata-WDC_WD5001AALS-00L3B2_WD-WCASY8359264";
      };
    };
  };

  config = mkMerge [
    (mkIf (cfg.type == "efi") {
      boot.loader.grub.device = "nodev";
      boot.loader.grub.efiSupport = true;
      boot.loader.efi.canTouchEfiVariables = true;
    })
    (mkIf (cfg.type == "legacy") {
      boot.loader.grub.efiSupport = false;
      boot.loader.efi.canTouchEfiVariables = false;
      boot.loader.grub.device = cfg.device;
    })
    (mkIf (cfg.type == "efi" || cfg.type == "legacy") {
      boot.loader.grub.enable = mkDefault true;
      boot.loader.timeout = mkDefault 2;
    })
    (mkIf (cfg.type == "mobile-nixos") {
      boot.loader.grub.enable = mkDefault false;
    })
  ];
}
