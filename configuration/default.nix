let
  pathExists = dir: name:
    builtins.filter (f: f == name) (builtins.attrNames (builtins.readDir dir)) == [ name ]
  ;
  withSecrets = pathExists ../. "secrets";
in
{ lib, options, ... }:

{
  imports = [
    (if withSecrets then ../secrets else {})
    ../modules
    ./boot
    ./hacks
    ./hardware
    ./settings
    ./use-case
  ];
  config =
    if (options.virtualisation ? qemu)
    then { }
    else {
      assertions = [
        {
          assertion = withSecrets;
          message = "Secrets need to be added for a system build.";
        }
      ];
    }
  ;
}
