{ pkgs, ... }:

{
  # Blacklist cdc_ncm (and modules requiring it) so the ax88179_178a module gets used
  boot.blacklistedKernelModules = [
    "cdc_ncm"
    "cdc_mbim"
    "cdc_wdm"
  ];

  # Ensure the ax88179_178a module is loader... it'll be needed to force using it.
  boot.kernelModules = [ "ax88179_178a" ];

  # Whenever a module matching the USB ID is added, ensure it's known by the module.
  services.udev.extraRules = ''
    ACTION=="add", \
      ATTRS{idVendor}=="0b95", \
      ATTRS{idProduct}=="1790", \
      RUN+="${pkgs.kmod}/bin/modprobe xpad", \
      RUN+="${pkgs.bash}/bin/sh -c 'echo 0x0b95 0x1790 0 0x0b95 0x1790 > /sys/bus/usb/drivers/ax88179_178a/new_id'"
  '';

  #
  # NOTES:
  #
  # The driver knows about this hardware already:
  #  - https://github.com/torvalds/linux/blob/bfa8f18691ed2e978e4dd51190569c434f93e268/drivers/net/usb/ax88179_178a.c#L1860-L1864
  #
  # Though since c67cc4315a8e605ec875bd3a1210a549e3562ddc the driver is bound specifically to the vendor interface:
  #  - https://github.com/torvalds/linux/commit/c67cc4315a8e605ec875bd3a1210a549e3562ddc
  #
  # Using cdc_ncm would be fine... if changing the mac address worked.
  #
}
