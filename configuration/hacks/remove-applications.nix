{ config, lib, ... }:

let
  cfg = config.samueldr.hacks.remove-applications;
  inherit (lib)
    concatMapStringsSep
    mkForce
    mkIf
    mkOption
    optional
    types
  ;
in
{
  options = {
    samueldr.hacks = {
      remove-applications = {
        enable = mkOption {
          default = true;
          internal = true;
          type = types.bool;
        };
        patterns = mkOption {
          default = [];
          description = ''
            List of filenames or patterns to remove from `$out/share/applications` from the system path.

            The patterns are not escaped, allowing use of shell globs.
          '';
          type = with types; listOf str;
        };
      };
    };
  };

  config = mkIf cfg.enable {
    samueldr.hacks.remove-applications.patterns = []
      ++ optional config.documentation.nixos.enable "nixos-manual.desktop"
    ;
    environment.extraSetup = ''
      echo " :: Removing unwanted desktop files"
      (
      cd $out/share/applications
      ${concatMapStringsSep "\n" (pattern: ''
        rm -v ${pattern}
      '') cfg.patterns}
      )
    '';
  };
}
