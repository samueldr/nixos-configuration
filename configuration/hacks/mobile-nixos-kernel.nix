{ config, lib, options, ... }:

let
  inherit (lib)
    mkForce
    mkIf
    mkOverride
    mkOption
    types
  ;
in
{
  options = {
    samueldr.hacks = {
      mobile = {
        buildWithKernel = mkOption {
          default = true;
          description = ''
            Disable to skip kernel build in the NixOS system.

            This is used to speed-up development cycle of non-boot parts on-device.
          '';
        };
      };
    };
  };

  config =
    if options ? mobile
    then mkIf (!config.samueldr.hacks.mobile.buildWithKernel) {
      mobile.quirks.supportsStage-0 = mkForce false;
      mobile.boot.stage-1.kernel.package = mkOverride 20 null;
      mobile.boot.stage-1.kernel.useNixOSKernel = mkOverride 20 false;
      mobile.rootfs.shared.enabled = mkForce true;
      samueldr.settings.kernel.enable = false;
    }
    else { }
  ;
}
