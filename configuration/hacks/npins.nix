{ config, lib, pkgs, ... }:

let
  inherit (lib)
    mkForce
    mkIf
    mkOverride
    mkOption
    types
    mkMerge
  ;
in
{
  options = {
    samueldr.hacks = {
      npins = {
        nixos-rebuild = mkOption {
          default = true;
          description = ''
            Use a wrapped `nixos-rebuild` that points to the system configuration's `npins` wrapper.
          '';
        };
      };
    };
  };

  config = mkMerge [
    (mkIf (config.samueldr.hacks.npins.nixos-rebuild) {
      environment.systemPackages = [
        (pkgs.callPackage (
          { runCommand
          , nixos-rebuild
          }:
          let
            # Would we want to make this configurable?
            #cfg_path = toString ../..;
            cfg_path = "/etc/nixos";
            nix_path ="nixpkgs=${cfg_path}/support/nixos-rebuild/nixpkgs:nixos-config=${cfg_path}/configuration.nix";
          in
          runCommand "npins-nixos-rebuild-workaround" { meta = { priority = 1; }; }
          ''
            mkdir -p $out/bin
            sed \
              -e 's;^export NIX_PATH=.*$;export NIX_PATH=${nix_path};' \
              -e 's;exec nixos-rebuild;exec ${nixos-rebuild}/bin/nixos-rebuild;' \
              < ${../../support/nixos-rebuild/nixos-rebuild} \
              > $out/bin/nixos-rebuild
            chmod a+x $out/bin/*
          ''
        ) {})
      ];
    })
  ];
}
