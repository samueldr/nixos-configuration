{ stdenv
, logo
, file
}:

stdenv.mkDerivation {
  name = "custom-logo-grub-theme";

  src = ./.;

  nativeBuildInputs = [
    file
  ];

  postFetch = ''
    rm default.nix
  '';

  buildPhase = ''
    runHook preBuild

    cp -v "${logo}" logo.png

    eval $(file -b logo.png  | cut -d, -f2 | sed -e 's/ //g' | sed -e 's/^/logo_width=/' -e 's/x/;logo_height=/')
    export logo_width
    export logo_height
    export logo_half_width=$(( logo_width / 2 ))
    export logo_half_height=$(( logo_height / 2 ))

    substituteAllInPlace theme.txt

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p $out
    cp -vt $out/ *

    runHook postInstall
  '';
}
