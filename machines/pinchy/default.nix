{
  networking.hostName = "pinchy";

  samueldr = {
    boot.type = "efi";
    hardware.system = "Acer Chromebook Spin 511";
    hardware.storage.size = 32 * 1024;
    use-case = {
      app.firefox.enable = true;
      syncthing.enable = true;
      android.enable = true;
      tailscale.enable = true;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/7168a4d5-b9f7-4a87-b69a-afe1ab00c3f7";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/049C-4970";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-PINCHY-ROOTFS" = {
      device = "/dev/disk/by-uuid/b0ce3d37-47fa-4bbf-b419-8b4c62c66a76";
      allowDiscards = true;
    };
  };

  swapDevices = [
    { device = "/var/swapfile"; size = 2 * 1024; }
  ];

  samueldr.nm-localbonding.bonds = {
    "homebond" = {
      enable = true;
      wifiConfig.connection.interface-name = "wlp0s12f0";
    };
  };

  system.stateVersion = "23.11";
}
