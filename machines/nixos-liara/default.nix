{
  networking.hostName = "nixos-liara";

  samueldr = {
    boot.type = "efi";
    hardware.system = "Lenovo 14e Chromebook";
    hardware.storage.size = 32 * 1024;
    use-case = {
      app.chromium.enable = true;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/df1ee13e-312f-4a35-b7ef-4960621516ff";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/AAAC-C2F1";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-NIXOS-LIARA-ROOTFS" = {
      device = "/dev/disk/by-uuid/f6d334c8-3d13-4e6b-9b64-17f684d17836";
      allowDiscards = true;
    };
  };

  system.stateVersion = "22.11";
}
