{
  #
  # Machine-specific facts
  #

  networking.hostName = "santaslittlehelper";

  samueldr = {
    boot.type = "efi";
    hardware.system = "Acer C720P";
    hardware.storage.size = 32 * 1024;
    use-case = {
      app.chromium.enable = true;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/dde7a3c5-f9c9-4e89-9b42-665a996db587";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/D3D7-B8E9";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "santaslittlehelper-system" = {
      device = "/dev/disk/by-uuid/a200d61d-a4aa-4c04-8565-1a904e9e3855";
      allowDiscards = true;
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/9d25a087-36bb-4111-a8e6-40053c3c1751"; }
  ];

  system.stateVersion = "17.03";
}
