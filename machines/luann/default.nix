{
  networking.hostName = "luann";

  samueldr = {
    boot.type = "efi";
    hardware.system = "Pine64 Pinebook Pro";
    hardware.storage.size = 128 * 1024;
    use-case = {
      app.chromium.enable = true;
      syncthing.enable = true;
      refind.enable = true;
    };
    hardware.misc.rtl8821.au.enable = true;
  };

  specialisation = {
    "With systemd Stage-1" = {
      configuration = {
        samueldr.use-case.stage-1.chosen = "systemd";
      };
    };
    "With NixOS Stage-1" = {
      configuration = {
        samueldr.use-case.stage-1.chosen = "nixos";
      };
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/32d82037-24f0-4308-8833-32af4346efc2";
      fsType = "ext4";
    };
    "/boot" = {
      device = "/dev/disk/by-uuid/B15A-DC88";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-LUANN-ROOTFS" = {
      device = "/dev/disk/by-uuid/afdd40db-1450-4771-b401-17ba4a45a4df";
    };
  };

  swapDevices = [
    { device = "/var/swap"; size = 8 * 1024; }
  ];

  system.stateVersion = "22.11";
}
