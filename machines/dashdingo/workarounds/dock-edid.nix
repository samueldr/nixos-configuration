{ config, lib, ... }:

let
  cfg = config.samueldr.hacks.dock-edid;
  inherit (lib)
    mkIf
    mkMerge
    mkOption
    types
  ;
in
{
  options = {
    samueldr.hacks.dock-edid = {
      enable = mkOption {
        default = false;
        type = types.bool;
      };
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      # Generic built-in EDID data sets are used, if one of
      # edid/1024x768.bin, edid/1280x1024.bin,
      # edid/1680x1050.bin, or edid/1920x1080.bin is given
      # and no file with the same name exists. Details and
      #
      # See also: drivers/gpu/drm/drm_edid_load.c
      boot.kernelParams = [
        (
          "drm.edid_firmware=" +
          (lib.concatStringsSep "," [
            # NOTE:
            # On the steam deck dock, using MST:
            #    - `DP-2` seems to the DP port
            #    - `DP-3` seems to be the HDMI port
            "DP-2:edid/1920x1080.bin" # Forcing DP port to 1080p, assuming HDMI->DP adapter
            # Sometimes the MST stuff glitches out...
            # ... the DP interfaces previously added are still present, and new
            # interfaces show up. To this day, the order is stable within this
            # failure mode.
            "DP-4:edid/1920x1080.bin" # Forcing DP port to 1080p, assuming HDMI->DP adapter
          ])
        )
      ];
    })
  ];
}
