{ lib, pkgs, ... }:

{
  imports = [
    # Import the Steam Deck enhancements project
    <jovian-nixos/modules>

    ./workarounds/dock-edid.nix
  ];

  networking.hostName = "dashdingo";

  samueldr = {
    boot.type = "efi";
    hardware.system = "Valve Steam Deck";
    hardware.storage.size = 256 * 1024;

    use-case = {
      # Enable the Steam Deck enhancements use case
      jovian-nixos.enable = true;
      jovian-nixos.useSteamSession = true;
      jovian-nixos.hardware = "steamdeck";

      # All emulators
      emulation.enable = true;

      # That's mostly how I'm using it right now.
      system-type.laptop.enable = true;

      # Other things this is used for...
      app.chromium.enable = true;
      syncthing.enable = true;
      tailscale.enable = true;
    };
  };

  specialisation = {
    "With systemd Stage-1" = {
      configuration = {
        samueldr.use-case.stage-1.chosen = "systemd";
      };
    };
    "With NixOS Stage-1" = {
      configuration = {
        samueldr.use-case.stage-1.chosen = "nixos";
      };
    };
    "With mainline Linux" = {
      configuration = {
        boot.kernelPackages = lib.mkForce pkgs.linuxPackages_latest;
      };
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/2f0737ae-73cd-4a26-8078-3bc6284b16ff";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/E576-42F9";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "dashdingo-luks" = {
      device = "/dev/disk/by-partuuid/41ea02a1-77fc-e142-871c-c79203596aae";
      allowDiscards = true;
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/a9eb22ad-3740-430c-b273-e47bbc5d0e82"; }
  ];

  system.stateVersion = "21.05";
}
