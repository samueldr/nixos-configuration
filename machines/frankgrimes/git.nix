{
  samueldr.git-syncupate = {
    enable = true;
    enableSchedule = true;
    location = "/Users/samuel/.local/var/git";
    repos = {
      "linux" = {
        remotes = [
          "torvalds"
          "stable"
          "samueldr"
          "jovian"
        ];
      };

      "nixpkgs" = {
        remotes = [
          "NixOS"
        ];
      };
      "nixpkgs (wip repos)" = {
        repo = "nixpkgs";
        prune = true;
        remotes = [
          "samueldr"
        ];
      };

      "mobile-nixos" = {
        remotes = [
          "mobile-nixos"
        ];
      };
      "mobile-nixos (wip repos)" = {
        repo = "mobile-nixos";
        prune = true;
        remotes = [
          "samueldr"
        ];
      };

      "u-boot" = {
        remotes = [
          "denx"
          "samueldr"
          "tow-boot"
        ];
      };
    };
  };
}
