{ lib, ... }:
{
  imports = [
    ./git.nix
  ];

  networking.hostName = "frankgrimes";

  samueldr = {
    boot.type = "efi";
    hardware.system = "StarBook (5800U)";
    hardware.storage.size = 894 * 1024;
    use-case = {
      android.enable = true;
      app.firefox.enable = true;
      extra-gui-software.enable = true;
      graphics-software.enable = true;
      nas.enable = true;
      syncthing.enable = true;
      tailscale.enable = true;
    };
  };
  app-compartmentalization = {
    scoped-apps = {
      darktable.enable = true;
    };
    services = {
      thunderbird.enable = true;
    };
  };

  specialisation = {
    "With systemd Stage-1" = {
      configuration = {
        samueldr.use-case.stage-1.chosen = "systemd";
      };
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/8cf45825-19f1-4e5e-a4fa-c84adfdb84b8";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/313F-F8F0";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-FRANKGRIMES-ROOTFS" = {
      device = "/dev/disk/by-uuid/e36d53bb-60a7-45b5-a827-3da28361f47d";
      allowDiscards = true;
      bypassWorkqueues = true;
    };
  };

  swapDevices = [
    { device = "/var/swapfile"; size = 16 * 1024; }
  ];

  system.stateVersion = "23.05";
}
