{ lib, ... }:

{
  systemd.services."serial-getty@ttySOL" = {
    enable = true;
    wantedBy = [ "multi-user.target" ];
  };

  # The serial port is made available by this module
  # This allows stage-1 to work
  boot.initrd.availableKernelModules = [
    "8250_pci"
  ];

  boot.kernelParams = lib.mkAfter [
    "console=tty0"
    "console=ttyS0,115200n8"
  ];

  # Configure for GRUB.
  # The `--port` parameter can be found via `dmesg | grep tty`
  #   [    1.248075] 0000:00:16.3: ttyS0 at I/O 0xe060 (irq = 17, base_baud = 115200) is a 16550A
  #                                             ^^^^^^
  boot.loader.grub.extraConfig = ''
    serial --speed=115200 --port=0xe060 --word=8 --parity=no --stop=1
    terminal_input console serial
    terminal_output gfxterm serial
  '';

  # https://wiki.debian.org/AMT/SerialOverLan
  services.udev.extraRules = ''
    KERNEL!="ttyS[0-9]", GOTO="no_serial"

    ATTRS{vendor}!="0x8086", GOTO="vendor_not_intel"

    #2977  82946GZ/GL KT Controller
    ATTRS{device}=="0x2977", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #2997  82Q963/Q965 KT Controller
    ATTRS{device}=="0x2997", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #29a7  82P965/G965 KT Controller
    ATTRS{device}=="0x29a7", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #29b7  82Q35 Express Serial KT Controller
    ATTRS{device}=="0x29b7", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #29c7  82G33/G31/P35/P31 Express Serial KT Controller
    ATTRS{device}=="0x29c7", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #29d7  82Q33 Express Serial KT Controller
    ATTRS{device}=="0x29d7", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #29e7  82X38 Express Serial KT Controller
    ATTRS{device}=="0x29e7", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #29f7  Server Serial KT Controller
    ATTRS{device}=="0x29f7", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #2a07  Mobile PM965/GM965 KT Controller
    ATTRS{device}=="0x2a07", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    #2a17  Mobile GME965/GLE960 KT Controller
    ATTRS{device}=="0x2a17", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"

    GOTO="vendor_not_intel"

    LABEL="no_serial"

    # 00:16.3 Serial controller [0700]: Intel Corporation C600/X79 series chipset KT Controller [8086:1d3d] (rev 05)
    KERNEL!="ttyS[0-9]", GOTO="no_serial"
    ATTRS{vendor}!="0x8086", GOTO="vendor_not_intel"
    ATTRS{device}=="0x1d3d", SYMLINK+="ttySOL", ENV{COMMENT}="Serial-Over-Lan $attr{vendor}:$attr{device}"
    GOTO="vendor_not_intel"
    LABEL="no_serial"
  '';
}
