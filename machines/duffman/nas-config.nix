{ lib, pkgs, ... }:

{
  # Disks for the NAS
  fileSystems = {
    "/run/zfs/pools/new-big-storage" = {
      device = "new-big-storage";
      fsType = "zfs";
      options = [
        "zfsutil"
        "nofail"
      ];
    };
    "/run/zfs/pools/other-big-storage" = {
      device = "other-big-storage";
      fsType = "zfs";
      options = [
        "zfsutil"
        "nofail"
      ];
    };
    "/run/zfs/pools/redundant-storage" = {
      device = "redundant-storage";
      fsType = "zfs";
      options = [
       "zfsutil"
       "nofail"
      ];
    };
  };

  networking.hostId = "0611c67b";
  boot.supportedFilesystems = [ "zfs" ];

  systemd.tmpfiles.rules = [
    "w- /sys/module/zfs/parameters/zfs_arc_min - - - - ${toString (1 * 1024 * 1024)}"
    "w- /sys/module/zfs/parameters/zfs_arc_max - - - - ${toString (8 * 1024 * 1024)}"
  ];

  powerManagement.powerUpCommands =
    let
      # Put only spinning rust from the NAS here.
      drives = [
        # new-big-storage
        "/dev/disk/by-id/ata-ST4000NE001-2MA101_WS23K892"

        # other-big-storage
        "/dev/disk/by-id/ata-HGST_HDN726040ALE614_K7HKVMEL"

        # redundant-storage
        "/dev/disk/by-id/ata-ST3500418AS_5VMJ3Y8X"
        "/dev/disk/by-id/ata-ST500DM002-1BD142_S2AMP3HF"
        "/dev/disk/by-id/ata-WDC_WD5001AALS-00L3B2_WD-WCASY6986843"
        "/dev/disk/by-id/ata-WDC_WD5001AALS-00L3B2_WD-WCASY8359264"

        # SSDs (don't spindown)
        #"/dev/disk/by-id/ata-INTEL_SSDSC2KW256G8_BTLA75220359256CGN"
        #"/dev/disk/by-id/ata-Samsung_SSD_860_EVO_1TB_S59VNS0N603560M"
      ];
    in
    lib.concatMapStringsSep "\n" (drive:
      # 10 minutes spindown ( 120 * 5 second increments, see manpage)
      # 30 minutes spindown ((241 - 240)*30 second increments, see manpage)
      # 60 minutes spindown ((242 - 240)*30 second increments, see manpage)
      #                       ^^^
      ''
        ${pkgs.hdparm}/sbin/hdparm -S 241 ${lib.escapeShellArg drive}
      ''
    )
    drives
  ;
}
