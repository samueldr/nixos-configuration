{ lib, pkgs, ... }:

{
  systemd.user = {
    services = {
      "mirror.steamdeck" = {
        wantedBy = [
          "default.target"
        ];
        wants = [
          "mirror.steamdeck.timer"
        ];
        description = ''
          Regularly fetch Steam Deck packages source
        '';
        script = ''
          PATH="${lib.makeBinPath [ pkgs.bash pkgs.nix ]}:$PATH"
          PS4=" $ "
          set -x
          cd /misc/steam/PKGBUILD-repo-mirror
          env -i ./doit.sh
        '';
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = false;
        };
      };
    };
    timers = {
      "mirror.steamdeck" = {
        wantedBy = [
          "timers.target"
          "default.target"
        ];
        timerConfig = {
          OnUnitActiveSec = "2h";
          RandomizedDelaySec = "30m";
        };
      };
    };
  };
}
