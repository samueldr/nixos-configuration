{ lib, pkgs, ... }:
{
  imports = [
    ./amt.nix
    ./nas-config.nix
    ./steamdeck.nix
  ];

  networking.hostName = "DUFFMAN";
  networking.interfaces.eno1.wakeOnLan.enable = true;
  networking.nat = {
    externalInterface = "eno1";
  };

  samueldr = {
    boot.type = "efi";
    hardware.system = "HP z420";
    hardware.storage.size = 915 * 1024;
    use-case = {
      syncthing.enable = true;
      tailscale.enable = true;
      stage-1.chosen = "systemd"; # Headless...
    };
  };

  boot.kernelPackages = pkgs.linuxPackages_5_15; # zfs

  services.tailscale = {
    useRoutingFeatures = "both";
  };

  specialisation = {
    "With NixOS Stage-1" = {
      configuration = {
        samueldr.use-case.stage-1.chosen = lib.mkForce "nixos";
      };
    };
  };

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-uuid/20FB-FC0A";
      fsType = "vfat";
    };

    "/" = {
      device = "/dev/mapper/LUKS-DUFFMAN-ROOT";
      fsType = "ext4";
    };

    "/Users" = {
      device = "/dev/mapper/LUKS-DUFFMAN-HOME";
      fsType = "ext4";
    };
  };

  boot.initrd.luks.devices = {
    LUKS-DUFFMAN-ROOT = {
      device = "/dev/disk/by-uuid/1b7eca49-bce7-45e4-9660-697603c75e7a";
      allowDiscards = true;
    };
    LUKS-DUFFMAN-HOME = {
      device = "/dev/disk/by-uuid/0a37c657-8466-45d6-b69b-a1d0638ca28f";
      allowDiscards = true;
    };
  };

  system.stateVersion = "17.09";
}
