{ config, lib, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "pine64-pinephonepro"; })
  ];

  networking.hostName = "scratchy";

  samueldr = {
    boot.type = "mobile-nixos";
    hardware.system = "Pine64 Pinephone Pro";
    hardware.storage.size = 128 * 1024;
    use-case = {
      mobile-nixos.enable = true;
      app.firefox.enable = true;
      syncthing.enable = true;
      waydroid.enable = true;
    };
    # Don't build the kernel with toplevel
    hacks.mobile.buildWithKernel = false;

    convergent-session.theme = {
      font = {
        DPI = 144;
      };
    };
  };
  services.redshift = {
    temperature = {
      night = 4400;
    };
  };

  # Use Fabric Desktop stuff
  fabric = {
    overlay.enable = true;
    desktop = {
      launcher.enable = true;
    };
  };

  environment.variables = {
    FABRIC_DPI = toString config.samueldr.convergent-session.theme.font.DPI;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/da231030-9d44-4013-a027-656900d099aa";
      fsType = "ext4";
      autoResize = lib.mkForce false;
    };
  };
  
  boot.initrd.luks.devices = {
    "LUKS-SCRATCHY-ROOTFS" = {
      device = "/dev/disk/by-uuid/4fa1ccfc-8b76-4909-9c9d-555c597a63e2";
    };
  };

  system.stateVersion = "22.11";
}
