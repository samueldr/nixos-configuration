{ lib, pkgs, ... }:

{
  networking.hostName = "warriormarge";

  samueldr = {
    boot.type = "efi";
    hardware.system = "Valve Steam Deck";
    hardware.storage.size = 1024 * 1024;

    use-case = {
      # Enable the Steam Deck enhancements use case
      jovian-nixos.enable = true;
      jovian-nixos.useSteamSession = true;
      jovian-nixos.hardware = "steamdeck";

      # All emulators
      emulation.enable = true;

      # Other things this is used for...
      app.firefox.enable = true;
      syncthing.enable = true;
      tailscale.enable = true;

      oled.enable = true;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/a0d570d5-2d75-49c9-ac4c-7b134e3eb6ee";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/F1D3-3D75";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-WARRIORMARGE-ROOTFS" = {
      device = "/dev/disk/by-uuid/0ff18222-3b39-4f41-971c-d97325de2c0e";
      allowDiscards = true;
    };
  };

  swapDevices = [
    { device = "/var/swapfile"; size = 8 * 1024; }
  ];

  samueldr.nm-localbonding.bonds = {
    "homebond" = {
      enable = true;
      wifiConfig.connection.interface-name = "wlo1";
    };
  };

  system.stateVersion = "23.11";
}
