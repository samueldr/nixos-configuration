{ config, lib, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "oneplus-enchilada"; })
  ];

  networking.hostName = "mn-enchilada";

  samueldr = {
    boot.type = "mobile-nixos";
    hardware.system = "OnePlus OnePlus 6";
    hardware.storage.size = 64 * 1024;
    use-case = {
      mobile-nixos.enable = true;
      app.angelfish.enable = true;
      syncthing.enable = true;
    };
    # Don't build the kernel with toplevel
    hacks.mobile.buildWithKernel = false;

    convergent-session.theme = {
      font = {
        DPI = 260;
      };
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/9e3ae193-c592-4357-9915-b2a4f431eea9";
      fsType = "ext4";
      autoResize = lib.mkForce false;
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-ENCHILADA-ROOTFS" = {
      device = "/dev/disk/by-uuid/ad87ea80-e4e4-4abe-8e36-7caea14431c2";
    };
  };

  system.stateVersion = "21.05";

  environment.etc."xdg/convergent-session/window-manager/config.lua" = {
    text = ''
      return {
        quirk_op6_notch = true,
      }
    '';
  };
}
