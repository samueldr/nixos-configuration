{ lib, pkgs, ... }:

{
  networking.hostName = "ralphwiggum";

  samueldr = {
    boot.type = "efi";
    hardware.system = "ASUS TP300LA";
    hardware.storage.size = 120 * 1024;
    use-case = {
      app.firefox.enable = true;
      syncthing.enable = true;
      android.enable = true;
      tailscale.enable = true;
    };
    convergent-session.on-screen-keyboard.enable = true;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/144aa333-b42d-4d27-9133-29864ae21e81";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/0F6A-FA1E";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "ralphwiggum-system" = {
      device = "/dev/disk/by-uuid/e248aeab-a766-42d0-9a14-bf392ad4429a";
      allowDiscards = true;
    };
  };

  swapDevices = [
    { device = "/var/swapfile"; size = 8192; }
  ];

  system.stateVersion = "17.03";
}
