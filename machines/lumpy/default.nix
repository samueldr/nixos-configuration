{ config, lib, pkgs, ... }:

let
  channels = import ../../npins;
in
{
  imports = [
    (import (channels.mobile-nixos.outPath + "/lib/configuration.nix") {
      device = "acer-lazor";
    })
  ];

  networking.hostName = "lumpy";

  samueldr = {
    boot.type = "mobile-nixos";
    hardware.system = "Acer Chromebook Spin 513";
    hardware.storage.size = 64 * 1024;
    hardware.memory = 4 * 1024;
    use-case = {
      mobile-nixos.enable = true;
      app.firefox.enable = true;
      syncthing.enable = true;
      nas.enable = true;
    };
    # Don't build the kernel with toplevel
    hacks.mobile.buildWithKernel = false;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/8074df2f-e481-4bad-ad3f-3fa446dc0733";
      fsType = "ext4";
    };
  };

  swapDevices = [
    { device = "/var/swap"; size = 4 * 1024; }
  ];
  
  boot.initrd.luks.devices = {
    "LUKS-LUMPY-ROOTFS" = {
      device = "/dev/disk/by-uuid/468e18ef-ba01-48a7-83f2-30322426bca3";
    };
  };

  system.stateVersion = "23.11";
}
