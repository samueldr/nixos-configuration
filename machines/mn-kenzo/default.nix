{ config, lib, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "acer-juniper"; })
  ];

  networking.hostName = "mn-kenzo";

  samueldr = {
    boot.type = "mobile-nixos";
    hardware.system = "Acer Chromebook 311";
    hardware.storage.size = 32 * 1024;
    use-case = {
      mobile-nixos.enable = true;
      app.firefox.enable = true;
      syncthing.enable = true;
    };
    # Don't build the kernel with toplevel
    hacks.mobile.buildWithKernel = false;
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/1b8262f3-3978-4212-ade8-2da1a287adde";
      fsType = "ext4";
    };
  };
  
  boot.initrd.luks.devices = {
    "LUKS-MOBILE-NIXOS-ROOTFS" = { # TODO: change this label
      device = "/dev/disk/by-uuid/754d4457-bab9-4bfb-907f-9a1ef4a8fc9e";
    };
  };

  system.stateVersion = "23.11";
}
