{ config, lib, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "lenovo-krane"; })
  ];

  networking.hostName = "snowball-four";

  samueldr = {
    boot.type = "mobile-nixos";
    hardware.system = "Lenovo Chromebook Duet";
    hardware.storage.size = 64 * 1024;
    use-case = {
      mobile-nixos.enable = true;
      app.angelfish.enable = true;
      app.firefox.enable = true;
      syncthing.enable = true;
    };
    # Don't build the kernel with toplevel
    hacks.mobile.buildWithKernel = false;

    convergent-session.theme = {
      font = {
        DPI = 200;
      };
    };
  };

  # NOTE: Breaks X11 when automatically rotated ?? :/
  samueldr.convergent-session.autorotate = false;

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/a1f63e27-3b96-4144-898f-c0faebb9dc7e";
      fsType = "ext4";
      autoResize = lib.mkForce false;
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-SNOWBALL-FOUR-ROOTFS" = {
      device = "/dev/disk/by-uuid/04c84e9e-037f-484f-9460-a3a23bccd37e";
    };
  };

  system.stateVersion = "22.11";
}
