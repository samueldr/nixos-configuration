#
# Misc WIP things to make adjusting the TDP work.
#

{ config, pkgs, ... }:

let
  performance = toString (30 * 1000);
  power-save  = toString ( 8 * 1000);
in
{
  environment.systemPackages = with pkgs; [
    ryzenadj
  ];

  boot.kernelParams = [
    # For ryzenadj
    "iomem=relaxed"
  ];

  services.udev.extraRules = ''
    SUBSYSTEM=="power_supply", KERNEL=="ACAD", ATTR{online}=="1", RUN+="${pkgs.ryzenadj}/bin/ryzenadj --stapm-limit ${performance} --fast-limit ${performance} --slow-limit ${performance}"
    SUBSYSTEM=="power_supply", KERNEL=="ACAD", ATTR{online}=="0", RUN+="${pkgs.ryzenadj}/bin/ryzenadj --stapm-limit ${power-save}  --fast-limit ${power-save}  --slow-limit ${power-save}"
  '';
}
