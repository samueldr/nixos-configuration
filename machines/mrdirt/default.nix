{ lib, ... }:

{
  imports = [
    ./tdp.nix
  ];

  networking.hostName = "mrdirt";

  samueldr = {
    boot.type = "efi";
    hardware.system = "GPD Win Mini";
    hardware.storage.size = 480 * 1024;

    use-case = {
      jovian-nixos.enable = true;
      jovian-nixos.useSteamSession = true;
      jovian-nixos.hardware = "gpd-win-mini";

      # All emulators
      emulation.enable = true;

      app.firefox.enable = true;
      syncthing.enable = true;
      nas.enable = true;
      tailscale.enable = true;

      # For full end-to-end testing of Jovian NixOS assumptions...
      stage-1.chosen = "systemd";
    };

    convergent-session.on-screen-keyboard = {
      enable = false;
    };

    convergent-session.theme = {
      font = {
        DPI = 96*2;
      };
    };
  };

  specialisation = {
    "With Mobile NixOS Stage-1" = {
      configuration = {
        samueldr.use-case.stage-1.chosen = lib.mkForce "mobile-nixos";
      };
    };
  };

  app-compartmentalization = {
    services = {
      mumble.enable = true;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/980e4555-4249-4c01-8fd7-9c933febcd93";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/BAA2-907F";
      fsType = "vfat";
    };
  };

  boot.initrd.luks.devices = {
    "LUKS-MRDIRT-ROOTFS" = {
      device = "/dev/disk/by-uuid/d6268795-e39f-45ac-892f-eff863ac09df";
      allowDiscards = true;
    };
  };

  swapDevices = [
    { device = "/var/swapfile"; size = 8 * 1024; }
  ];

  system.stateVersion = "24.05";
}
