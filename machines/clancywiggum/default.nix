{
  networking.hostName = "clancywiggum";

  samueldr = {
    boot.type = "efi";
    hardware.system = "ZOTAC ZBOX EI730";
    hardware.storage.size = 256 * 1024;
    use-case = {
      app.firefox.enable = true;
      syncthing.enable = true;
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/6321b1b6-bd4d-4328-9afe-9deebf3856be";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/32AB-B53B";
      fsType = "vfat";
    };

    "/Users" = {
      device = "/dev/disk/by-uuid/34b9c6c8-4920-4165-88eb-3ab769a54407";
      fsType = "ext4";
    };
  };

  boot.initrd.luks.devices = {
    luks-clancywiggum-home = {
      device = "/dev/disk/by-uuid/b93aff71-8b53-4981-9f32-1269d7093c7d";
      allowDiscards = true;
    };

    luks-clancywiggum-root = {
      device = "/dev/disk/by-uuid/5ba47c0f-f9d8-4b4f-af7a-afbcee8b2279";
      allowDiscards = true;
    };
  };

  swapDevices = [
    { device = "/var/swapfile"; size = 8192; }
  ];

  system.stateVersion = "18.09";
}
