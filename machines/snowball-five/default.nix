{ config, lib, pkgs, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "lenovo-wormdingler"; })
  ];

  networking.hostName = "snowball-five";

  samueldr = {
    boot.type = "mobile-nixos";
    hardware.system = "Lenovo Chromebook Duet 3";
    hardware.storage.size = 128 * 1024;
    hardware.memory = 8 * 1024;
    use-case = {
      mobile-nixos.enable = true;
      app.angelfish.enable = true;
      app.firefox.enable = true;
      app.chromium.enable = true;
      syncthing.enable = true;
    };
    # Don't build the kernel with toplevel
    hacks.mobile.buildWithKernel = false;

    convergent-session.theme = {
      font = {
        DPI = 144;
      };
    };
  };
  environment.variables = {
    #"KWIN_DRM_NO_AMS" = "1";
  };
  app-compartmentalization = {
    scoped-apps = {
      "chromium" = {
        commands =
          let
            cfg = {
              systemdRunArguments = [
                # Run on the "primary", or "big" cores
                # XXX option to control that
                # XXX may not help as much as hoped
                "-p" "CPUAffinity=6,7"
              ];
            };
          in
          {
            "chromium" = cfg;
            "chromium-browser" = cfg;
          }
        ;
      };
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/c5a26318-f663-464e-97e7-0dab565acfb0";
      fsType = "ext4";
      autoResize = lib.mkForce false;
    };
  };
  
  boot.initrd.luks.devices = {
    "LUKS-SNOWBALL-FIVE-ROOTFS" = {
      device = "/dev/disk/by-uuid/bb2699e4-a95d-4c7a-8ff2-e80a08bbe36a";
      allowDiscards = true;
    };
  };

  system.stateVersion = "22.11";
}
