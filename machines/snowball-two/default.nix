{ config, lib, ... }:

{
  imports = [
    (import <mobile-nixos/lib/configuration.nix> { device = "asus-dumo"; })
  ];

  networking.hostName = "snowball-two";

  samueldr = {
    boot.type = "mobile-nixos";
    hardware.system = "Asus Chromebook Tablet CT100PA";
    hardware.storage.size = 32 * 1024;
    use-case = {
      mobile-nixos.enable = true;
      app.angelfish.enable = true;
      app.firefox.enable = true;
      syncthing.enable = true;
    };
    # Don't build the kernel with toplevel
    hacks.mobile.buildWithKernel = false;

    convergent-session.theme = {
      font = {
        DPI = 200;
      };
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/8cbe2a32-400a-4e0d-b01f-07b93565b07e";
      fsType = "ext4";
      autoResize = lib.mkForce false;
    };
  };
  
  boot.initrd.luks.devices = {
    "LUKS-SNOWBALL-TWO-ROOTFS" = {
      device = "/dev/disk/by-uuid/09e53d6b-8829-446e-a0aa-226d21d7df37";
    };
  };

  system.stateVersion = "22.11";
}
