# samueldr's configuration

* * *

## Structure

### `/machines`

Configuration for every individual machines (pets).

Not all machines are consistently under use, and some may be broken in subtle
ways at runtime.

There is no SLA.


### `/modules`

These are self-contained and independent *modules sytem* modules implementing
different set of features in the configuration.

They are not all self-written. When they're not, proper attribution is added.


### `/configuration`

These are the implementation of my actual configuration. Their organization
is still being defined.

Mostly, `use-case` describes "tasks" or "reasons" a computer is used for.
The `system-type` configurations are coarse description of the usage of the
system. The `hardware` descriptions are used to configure basic facts about
the hardware, and enable a default `system-type`.

Configuration under `settings` are acceptable defaults that are generally
acceptable.


* * *

## Usage

Don't.

Also, I'm still thinking about the tooling to use the configuration. For
usage with `nixos-rebuild`, place a `configuration.nix` file with the
proper imports.

```nix
{
  imports = [
    ./machines/machine-hostname
    ./configuration
  ];
}
```

Though I need to consider options for making it easier to batch build the
different machine configurations without evaluating locally.
