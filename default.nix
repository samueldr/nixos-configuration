let
  nixpkgs = (import ./npins).nixpkgs;
in
import (nixpkgs + "/nixos")
